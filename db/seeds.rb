# encoding: utf-8

#Badges
badges = [
  {:title => "1st_level", :rule => 0, :image => 1},
  {:title => "2nd_level", :rule => 25, :image => 1},
  {:title => "3rd_level", :rule => 75, :image => 2},
  {:title => "4rd_level", :rule => 200, :image => 3},
  {:title => "5th_level", :rule => 400, :image => 4},
  {:title => "6th_level", :rule => 700, :image => 5},
  {:title => "7th_level", :rule => 1000, :image => 6},
  {:title => "8th_level", :rule => 1500, :image => 7},
  {:title => "9th_level", :rule => 2000, :image => 8},
  {:title => "10th_level", :rule => 2500, :image => 9},
  {:title => "11th_level", :rule => 3000, :image => 10},
  {:title => "12th_level", :rule => 3500, :image => 11},
  {:title => "13th_level", :rule => 5000, :image => 12},
  {:title => "14th_level", :rule => 6500, :image => 13},
  {:title => "15th_level", :rule => 9000, :image => 14},
  {:title => "16th_level", :rule => 12000, :image => 15},
  {:title => "17th_level", :rule => 15000, :image => 16},
  {:title => "18th_level", :rule => 20000, :image => 17}
]

badges.each do |attributes|
  Badge.find_or_initialize_by_title(attributes[:title], attributes).tap do |t|
    t.save!
  end
end

#SettingTypes
types = [
  { :name => "integer" },
  { :name => "string"  },
  { :name => "decimal" }
]

types.each do |attributes|
  SettingType.find_or_initialize_by_name(attributes[:name]).tap do |t|
    t.save!
  end
end

# #Basic ApplicationSetting

settings = [
  #Number of ideas user can enter per day without paying
  { :name => "Ideju limits dienā", :code => "IDEA_LIMIT_PER_DAY", :value => "5", :setting_type => SettingType.find_by_name("integer") },
  # Starting with this level user is allowed to participate in money contests
  { :name => "Ierobežojums uz lietotāja līmeni", :code => "SUFFICIENT_USER_LEVEL", :value => "8", :setting_type => SettingType.find_by_name("integer") },
  # Spam filter
  { :name => "Trashhold sūdzībām par spamu", :code => "SPAM_FILTER_TRASHOLD", :value => "1", :setting_type => SettingType.find_by_name("integer") },
  # Showed count to check for lame idea
  { :name => "Idejas pārbaudes atrāžu skaits", :code => "ELIMINATE_CHECK", :value => "6", :setting_type => SettingType.find_by_name("integer") },
  { :name => "Idejas pārbaudes atrāžu skaits (2.pakāpe)", :code => "ELIMINATE_CHECK_LEVEL_2", :value => "20", :setting_type => SettingType.find_by_name("integer") },
  { :name => "Idejas pārbaudes atrāžu skaits (3.pakāpe)", :code => "ELIMINATE_CHECK_LEVEL_3", :value => "30", :setting_type => SettingType.find_by_name("integer") },
  # Vote count to check for lame idea
  { :name => "Idejas pārbaudes minimālais balsu skaits", :code => "ELIMINATE_MIN", :value => "2", :setting_type => SettingType.find_by_name("integer") },
  { :name => "Idejas pārbaudes minimālais balsu skaits (2.pakāpe)", :code => "ELIMINATE_MIN_LEVEL_2", :value => "8", :setting_type => SettingType.find_by_name("integer") },
  { :name => "Idejas pārbaudes minimālais balsu skaits (3.pakāpe)", :code => "ELIMINATE_MIN_LEVEL_3", :value => "12", :setting_type => SettingType.find_by_name("integer") },
  # How many times an idea can be shown to the user
  { :name => "Maksimālais idejas atrāžu skaits vienam lietotājam", :code => "IDEA_SHOW_COUNT", :value => "3", :setting_type => SettingType.find_by_name("integer") },
  { :name => "Minimālais naudas apjoms santīmos(vai FB centos), lai izņemtu nopelnīto", :code => "MINIMUM_TO_CHECK_OUT", :value => "10000", :setting_type => SettingType.find_by_name("integer") },
  { :name => "FB noklusētās profila bildes URL (male)", :code => "DEFAULT_FB_MALE_PICTURE", :value => "http://b.static.ak.fbcdn.net/rsrc.php/v1/yo/r/UlIqmHJn-SK.gif", :setting_type => SettingType.find_by_name("string") },
  { :name => "FB noklusētās profila bildes URL (female)", :code => "DEFAULT_FB_FEMALE_PICTURE", :value => "https://fbcdn-profile-a.akamaihd.net/static-ak/rsrc.php/v1/y9/r/IB7NOFmPw2a.gif", :setting_type => SettingType.find_by_name("string") },
  { :name => "Treshhold uz crowdsourcinga rezultātiem pēc kopējā dueļu skaita", :code => "CROWDSOURCING_RESULTS_TRESHHOLD", :value => 10, :setting_type => SettingType.find_by_name("integer") }
]

settings.each do |attributes|
  ApplicationSetting.find_or_create_by_code(attributes[:code], attributes)
end

# # t.references :service
# # t.string :service_provider_name
# # t.string :environment
# # t.string :service_social_network_id

service_providers_ids = [
  #diennakts abonements
  [
    { :service_provider_name => "draugiem", :environment => "draugiem_test",  :service_social_network_id => 1224 },
    { :service_provider_name => "draugiem", :environment => "draugiem",       :service_social_network_id => 1213 },
    { :service_provider_name => "draugiem", :environment => "development",    :service_social_network_id => 1245 }
  ],
  
  #nedēļas abonements
  [
    { :service_provider_name => "draugiem", :environment => "draugiem_test",  :service_social_network_id => 1225 },
    { :service_provider_name => "draugiem", :environment => "draugiem",       :service_social_network_id => 1214 },
    { :service_provider_name => "draugiem", :environment => "development",    :service_social_network_id => 1246 }
  ],
  
  #mēneša abonements
  [
    { :service_provider_name => "draugiem", :environment => "draugiem_test",  :service_social_network_id => 1226 },
    { :service_provider_name => "draugiem", :environment => "draugiem",       :service_social_network_id => 1215 },
    { :service_provider_name => "draugiem", :environment => "development",    :service_social_network_id => 1247 }
  ],
  
  #VIP konts mēneša
  [
    { :service_provider_name => "draugiem", :environment => "draugiem_test",  :service_social_network_id => 1275 },
    { :service_provider_name => "draugiem", :environment => "draugiem",       :service_social_network_id => 1274 },
    { :service_provider_name => "draugiem", :environment => "development",    :service_social_network_id => 1278 }
  ],
  
  #VIP konts gada
  [
    { :service_provider_name => "draugiem", :environment => "draugiem_test",  :service_social_network_id => 1277 },
    { :service_provider_name => "draugiem", :environment => "draugiem",       :service_social_network_id => 1276 },
    { :service_provider_name => "draugiem", :environment => "development",    :service_social_network_id => 1279 }
  ]
]

# # t.string :name
# # t.integer :price
# # t.string :currency
# # t.integer :duration
# # t.string :duration_units

services = [
  { :name => "Diennakts abonements", :code => "DAYNIGHT_SUBSCRIPTION",
    :price_in_cents => 100, :currency => "USD", 
    :duration => 24, :duration_units => "hours",   
    :local_service_flag => false },
     
  { :name => "Nedēļas abonements", :code => "WEEK_SUBSCRIPTION", 
    :price_in_cents => 300, :currency => "USD", 
    :duration => 7, :duration_units => "days",  
    :local_service_flag => false },
     
  { :name => "Mēneša abonements", :code => "MONTH_SUBSCRIPTION",
    :price_in_cents => 900, :currency => "USD", 
    :duration => 1, :duration_units => "months", 
    :local_service_flag => false },
  
  { :name => "VIP konta mēneša abonements", :code => "VIP_ACCOUNT_MONTHLY",
    :price_in_cents => 990, :currency => "USD", 
    :duration => 1, :duration_units => "months", 
    :local_service_flag => false },
  
  { :name => "VIP konta gada abonements", :code => "VIP_ACCOUNT_ANNUAL",
    :price_in_cents => 7890, :currency => "USD",
    :duration => 12, :duration_units => "months",
    :local_service_flag => false },
  
  { :name => "VIP konta aboenments iekšējiem lietotajiem", :code => "VIP_ACCOUNT_LIFELONG",
    :price_in_cents => 0, :currency => "USD",
    :duration => 600, :duration_units => "months",
    :local_service_flag => true },

  { :name => "internal.pricing.services.mini.title", :code => "ORGANIZATION_SUBSCRIPTION_MINI",
    :price_in_cents => 4900, :currency => "USD",
    :duration => 1, :duration_units => "months",
    :user_limit => 20, :crowdsourcing_flag => false, 
    :unique_design_flag => false,
    :intranet_flag => false,
    :local_service_flag => true },

  { :name => "internal.pricing.services.small.title", :code => "ORGANIZATION_SUBSCRIPTION_SMALL",
    :price_in_cents => 14900, :currency => "USD",
    :duration => 1, :duration_units => "months",
    :user_limit => 50, :crowdsourcing_flag => true, 
    :unique_design_flag => false,
    :intranet_flag => false,
    :local_service_flag => true },

  { :name => "internal.pricing.services.medium.title", :code => "ORGANIZATION_SUBSCRIPTION_MEDIUM",
    :price_in_cents => 24900, :currency => "USD",
    :duration => 1, :duration_units => "months",
    :user_limit => 100, :crowdsourcing_flag => true, 
    :unique_design_flag => false,
    :intranet_flag => false,
    :local_service_flag => true },

  { :name => "internal.pricing.services.standard.title", :code => "ORGANIZATION_SUBSCRIPTION_STANDARD",
    :price_in_cents => 39900, :currency => "USD",
    :duration => 1, :duration_units => "months",
    :user_limit => 250, :crowdsourcing_flag => true, 
    :unique_design_flag => false,
    :intranet_flag => false,
    :local_service_flag => true },

  { :name => "internal.pricing.services.large.title", :code => "ORGANIZATION_SUBSCRIPTION_LARGE",
    :price_in_cents => 49900, :currency => "USD",
    :duration => 1, :duration_units => "months",
    :user_limit => 500, :crowdsourcing_flag => true, 
    :unique_design_flag => false,
    :intranet_flag => false,
    :local_service_flag => true },

  { :name => "internal.pricing.services.extra_large.title", :code => "ORGANIZATION_SUBSCRIPTION_EXTRA_LARGE",
    :price_in_cents => 99900, :currency => "USD",
    :duration => 1, :duration_units => "months",
    :user_limit => 0, :crowdsourcing_flag => true, 
    :unique_design_flag => false,
    :intranet_flag => false,
    :local_service_flag => true },

  { :name => "internal.pricing.services.vip.title", :code => "ORGANIZATION_SUBSCRIPTION_VIP",
    :price_in_cents => 600000, :currency => "USD",
    :duration => 0, :duration_units => "months",
    :user_limit => 0, :crowdsourcing_flag => true, 
    :unique_design_flag => false,
    :intranet_flag => true,
    :local_service_flag => true },

  { :name => "internal.pricing.services.vip_extra.title", :code => "ORGANIZATION_SUBSCRIPTION_VIP_EXTRA",
    :price_in_cents => 900000, :currency => "USD",
    :duration => 0, :duration_units => "months",
    :user_limit => 0, :crowdsourcing_flag => true, 
    :unique_design_flag => true,
    :intranet_flag => true,
    :local_service_flag => true },
]

services.each_with_index do |attrib, index|
  s = Service.find_or_initialize_by_code(attrib[:code], attrib)
  p s.id.nil? ? "Veidoju jaunu pakalpojumu ar kodu: #{s.code}" : "Pakalpojums ar kodu #{s.code} jau eksistē"
  
  s.service_provider_ids = service_providers_ids[index].map do |sid|
    ServiceProviderId.find_or_initialize_by_service_social_network_id( sid[:service_social_network_id].to_s, sid )
  end unless s.local?
  
  s.save!
end

if AppUtils.facebook_env?
  Organization.find_or_create_by_name "IdeaAcademy", { :heard_from => :other, :phone_number => "22222222" }
  user = User.find_by_user_type('guest')
  unless user
    user = User.new(password: 'guestuser', password_confirmation: 'guestuser', email: 'guest@guest.com', first_name: 'Guest', last_name: 'User', user_type: 'guest')
  end
  user.set_default_organization Organization.find_by_name("IdeaAcademy")
  user.save!
  UserPopup.create!({ :user_id => user.id, :typekey => "welcome_user", :seen => false })
end


#Contests
contests = [
  {:category_id => 1, :title => "Sauklis produktam, kas veicina gremošanas sistēmas veselību", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "text", :active_flag => true, :logo_file_name => "Missing"},
  {:category_id => 2, :title => "Ideju Akadēmijas jaunais logo", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "text", :status => "live", :logo_file_name => "Missing"},
  {:category_id => 2, :title => "Ideju istaba jaunajai spēles versijai", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "text", :status => "draft", :logo_file_name => "Missing"},
  {:category_id => 2, :title => "Uz kāda veida jautājumiem idejas ,izmantojot IA, varētu iegūt Latvijas pašvaldības", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "text", :status => "live", :logo_file_name => "Missing"},
  {:category_id => 3, :title => "Idejas filozofijas dienas popularizēšanai skolēniem", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "text", :status => "live", :logo_file_name => "Missing"},
  {:category_id => 4, :title => "Loreum ipsum dul sit amet1?", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "video", :status => "live", :logo_file_name => "Missing"},
  {:category_id => 2, :title => "Loreum ipsum dul sit amet2?", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "text", :status => "done", :logo_file_name => "Missing"},
  {:category_id => 3, :title => "Loreum ipsum dul sit amet3?", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "text", :status => "live", :logo_file_name => "Missing"},
  {:category_id => 4, :title => "Loreum ipsum dul sit amet4?", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "image", :status => "live", :logo_file_name => "Missing"},
  {:category_id => 1, :title => "Loreum ipsum dul sit amet5?", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "text", :status => "live", :logo_file_name => "Missing"},
  {:category_id => 6, :title => "Loreum ipsum dul sit amet6?", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "text", :status => "done", :logo_file_name => "Missing"},
  {:category_id => 6, :title => "Loreum ipsum dul sit amet7?", :description => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", :award => "Lorem ipsums", :typekey => "text", :status => "done", :logo_file_name => "Missing"}
]

contests.each do |c|
  Contest.find_or_initialize_by_title(c[:title], c).tap do |t|
    t.save!
  end
end
 
# user_providers
# No need for this, authorization is ready
# user_providers = [
#   {:id => 1, :user_id => 1, :origin_user_id => 1, :social_id => "191878", :name => "draugiem", :token => "213", :secret => "123" },
#   {:id => 2, :user_id => 2, :origin_user_id => 2, :social_id => "293878", :name => "draugiem", :token => "4324", :secret => "213"  },
#   {:id => 3, :user_id => 3, :origin_user_id => 3, :social_id => "391878", :name => "draugiem", :token => "5345", :secret => "423"  }
# ]

# user_providers.each do |attributes|
#   UserProvider.find_or_initialize_by_id(attributes[:id], attributes).tap do |t|
#     t.save!
#   end
# end
 
#Users
# No need for this, authorization is ready
# users = [
#   {:id => 1, :origin_provider_id => 1, :first_name => "Fricis", :last_name => "Bārda", :email => "fricis@barda.lv", :website => "http://fricis.barda.lv", :phone => "6876786768" },
#   {:id => 2, :origin_provider_id => 2, :first_name => "Bricis", :last_name => "Fārda", :email => "bricis@barda.lv", :website => "http://bricis.barda.lv", :phone => "7876786768" },
#   {:id => 3, :origin_provider_id => 3, :first_name => "Ricis", :last_name => "Ārda", :email => "ricis@barda.lv", :website => "http://ricis.barda.lv", :phone => "8876786768" }
# ]

# users.each do |attributes|
#   User.find_or_initialize_by_id(attributes[:id], attributes).tap do |t|
#     t.save!
#   end
# end

#User Friends
# user_friends = [
#   {:user_id => 1, :friend_id => 2},
#   {:user_id => 2, :friend_id => 1},
#   {:user_id => 2, :friend_id => 3}
# ]

# user_friends.each do |attributes|
#   UserFriend.find_or_initialize_by_user_id_and_friend_id(attributes[:user_id], attributes[:friend_id]).tap do |t|
#     t.save!
#   end
# end

#Rooms
# rooms = [
#   {:id => 1, :contest_id => 1, :participants => 2}
# ]

# rooms.each do |attributes|
#   Room.find_or_initialize_by_id(attributes[:id], attributes).tap do |t|
#     t.save!
#   end
# end

# #Room Users
# room_users = [
#   {:id => 1, :contest_id => 1, :room_id => 1, :user_id => 1},
#   {:id => 2, :contest_id => 1, :room_id => 1, :user_id => 2}
# ]

# room_users.each do |attributes|
#   RoomUser.find_or_initialize_by_id(attributes[:id], attributes).tap do |t|
#     t.save!
#   end
# end

# #Ideas
# ideas = [
#   {:id => 1, :contest_id => 1, :typekey => "text", :text => "Vien velīgi laba ideja. Noteikti balsojat", :user_id => 1, :duels_won => 2, :duels_in => 3, :room_id => 1, :category_id => 1},
#   {:id => 2, :contest_id => 1, :typekey => "text", :text => "Kaut kāda cita ideja", :user_id => 1, :duels_won => 2, :duels_in => 5,:room_id => 1, :category_id => 1},
#   {:id => 3, :contest_id => 1, :typekey => "text", :text => "Lorem ipsum ideja", :user_id => 2, :duels_won => 2, :duels_in => 3, :room_id => 1, :category_id => 1},
#   {:id => 4, :contest_id => 1, :typekey => "text", :text => "Pipsum dipsum ideja", :user_id => 2, :duels_won => 2, :duels_in => 2, :room_id => 1, :category_id => 1},
#   {:id => 5, :contest_id => 1, :typekey => "text", :text => "Otra dipsum pipsum ideja", :user_id => 2, :duels_won => 2, :duels_in => 6,:room_id => 1, :category_id => 1},
#   {:id => 6, :contest_id => 1, :typekey => "text", :text => "Atkal kāda cita ideja", :user_id => 1, :duels_won => 2, :duels_in => 8, :is_deleted => false, :room_id => 1, :category_id => 1},
#   {:id => 7, :contest_id => 1, :typekey => "text", :text => "Idejas teksts vienkāršs, lai pamana, ka cita ideja", :user_id => 2, :duels_won => 2, :room_id => 1, :duels_in => 9, :category_id => 1}
# ]


# ideas.each do |attributes|
#   Idea.find_or_initialize_by_id(attributes[:id], attributes).tap do |t|
#     t.save!
#   end
# end

# #SatatisticCategoryUser
# statistics = [
#   {:id => 1, :category_id => 1, :user_id => 1, :total_points => 100, :level =>  2, :idea_points => 50, :vote_points => 50 },
#   {:id => 2, :category_id => 2, :user_id => 1, :total_points => 500, :level =>  6, :idea_points => 250, :vote_points => 250 },
#   {:id => 3, :category_id => 3, :user_id => 1, :total_points => 200, :level =>  3, :idea_points => 50, :vote_points => 150 },
#   {:id => 4, :category_id => 4, :user_id => 1, :total_points => 600, :level =>  7, :idea_points => 50, :vote_points => 550 },
#   {:id => 5, :category_id => 5, :user_id => 1, :total_points => 210, :level =>  3, :idea_points => 150, :vote_points => 60 },
#   {:id => 6, :category_id => 1, :user_id => 2, :total_points => 2880, :level =>  14, :idea_points =>880, :vote_points => 2000 },
#   {:id => 7, :category_id => 2, :user_id => 2, :total_points => 260, :level =>  3, :idea_points => 50, :vote_points => 210 },
#   {:id => 8, :category_id => 3, :user_id => 2, :total_points => 250, :level =>  3, :idea_points => 50, :vote_points => 250 },
#   {:id => 9, :category_id => 1, :user_id => 3, :total_points => 700, :level =>  8, :idea_points => 350, :vote_points => 350 },
#   {:id => 10, :category_id => 1, :user_id => 4, :total_points => 500, :level =>  6, :idea_points => 500, :vote_points => 0 }
# ]

# statistics.each do |attributes|
#   StatisticCategoryUser.find_or_initialize_by_id(attributes[:id], attributes).tap do |t|
#     t.save!
#   end
# end

#Log Activity


#Log Cash


#Log Votes

