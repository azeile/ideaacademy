class CreateVisitorAttributes < ActiveRecord::Migration
  def change
    create_table :visitor_attributes do |t|
      t.string :ip_address

      t.timestamps
    end
  end
end
