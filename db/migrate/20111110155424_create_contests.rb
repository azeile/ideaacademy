class CreateContests < ActiveRecord::Migration
  def change
    create_table :contests do |t|
      t.integer :category_id

      t.string :title
      t.text :description
      t.string :award
      t.string :typekey # Instead of "type"?

      t.timestamps
    end
  end
end
