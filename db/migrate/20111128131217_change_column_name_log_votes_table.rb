class ChangeColumnNameLogVotesTable < ActiveRecord::Migration
  def change
    rename_column :log_votes, :ingored_idea_id, :ignored_idea_id
  end
end
