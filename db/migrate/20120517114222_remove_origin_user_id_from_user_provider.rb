class RemoveOriginUserIdFromUserProvider < ActiveRecord::Migration
  def up
    remove_column :user_providers, :origin_user_id
  end

  def down
    add_column :user_providers, :origin_user_id, :integer
  end
end
