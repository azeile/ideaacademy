class AddSentFlagToCrowdsourcedContest < ActiveRecord::Migration
  def change
    add_column :crowdsourced_contests, :sent_flag, :boolean
  end
end
