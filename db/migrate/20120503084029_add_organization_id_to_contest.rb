class AddOrganizationIdToContest < ActiveRecord::Migration
  def change
    add_column :contests, :organization_id, :integer
  end
end
