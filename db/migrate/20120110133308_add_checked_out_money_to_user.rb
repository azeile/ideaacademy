class AddCheckedOutMoneyToUser < ActiveRecord::Migration
  def change
    add_column :users, :checked_out_amount, :integer, :default => 0
  end
end
