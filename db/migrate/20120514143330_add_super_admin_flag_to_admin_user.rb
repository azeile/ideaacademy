class AddSuperAdminFlagToAdminUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :superadmin_flag, :boolean
  end
end
