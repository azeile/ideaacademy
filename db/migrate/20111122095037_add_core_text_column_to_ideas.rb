class AddCoreTextColumnToIdeas < ActiveRecord::Migration
  def change
    add_column :ideas, :core_text, :string
  end
end
