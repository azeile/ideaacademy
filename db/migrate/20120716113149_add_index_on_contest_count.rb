class AddIndexOnContestCount < ActiveRecord::Migration
  def up
    add_index :contests, [ :category_id, :ends_at, :active_flag ], :name => "idx_category_contest_count"
  end

  def down
    remove_index :name => "idx_category_contest_count"
  end
end
