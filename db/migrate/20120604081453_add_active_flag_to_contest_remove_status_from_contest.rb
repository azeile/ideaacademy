class AddActiveFlagToContestRemoveStatusFromContest < ActiveRecord::Migration
  def up
    add_column :contests, :active_flag, :boolean
    remove_column :contests, :status
  end

  def down
    remove_column :contests, :active_flag
    add_column :contests, :status, :string
  end
end
