class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.integer :contest_id

      t.integer :participants

      t.timestamps
    end
  end
end
