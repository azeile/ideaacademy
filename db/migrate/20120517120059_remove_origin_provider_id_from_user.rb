class RemoveOriginProviderIdFromUser < ActiveRecord::Migration
  def up
    remove_column :users, :origin_provider_id
  end

  def down
    add_column :users, :origin_provider_id, :integer
  end
end
