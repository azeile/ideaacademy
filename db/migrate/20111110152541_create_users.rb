class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :origin_provider_id

      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :website
      t.string :phone
      t.string :image_url

      t.timestamps
    end
  end
end
