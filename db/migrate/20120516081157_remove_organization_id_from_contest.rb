class RemoveOrganizationIdFromContest < ActiveRecord::Migration
  def up
    remove_column :contests, :organization_id
  end

  def down
    add_column :contests, :organization_id, :integer
  end
end
