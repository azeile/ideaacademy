class AddLivePointsToStatisticCategoryUser < ActiveRecord::Migration
  def change
    add_column :statistic_category_users, :live_points, :integer
  end
end
