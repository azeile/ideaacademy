class CreateTmpLogVotes < ActiveRecord::Migration
  def change
    create_table :tmp_log_votes do |t|
      t.integer :contest_id
      t.integer :room_id
      t.integer :selected_idea_id
      t.integer :ignored_idea_id
      t.integer :tmp_user_id
      t.string :status
      
      t.timestamps
    end
    
    add_index :tmp_log_votes, [:contest_id, :room_id]
  end
end