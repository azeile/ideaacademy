class AddIndicesToServiceSubscriptions < ActiveRecord::Migration
  def up
    add_index :subscriptions, [ :subscriber_id, :subscriber_type, :date_from, :date_to ], :name => :active_subscription_index
    add_index :subscriptions, :service_id, :name                                                => :service_id_fk
    add_index :services, :code, :name                                                           => :service_code_index
  end
  
  def down
    remove_index :subscriptions, :name =>  :active_subscription_index
    remove_index :subscriptions, :name =>  :service_id_fk
    remove_index :services, :name =>       :service_code_index
  end
end
