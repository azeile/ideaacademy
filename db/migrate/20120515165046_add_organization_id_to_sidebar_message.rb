class AddOrganizationIdToSidebarMessage < ActiveRecord::Migration
  def change
    add_column :sidebar_messages, :organization_id, :integer
  end
end
