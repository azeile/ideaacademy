class CreateBadges < ActiveRecord::Migration
  def change
    create_table :badges do |t|
      t.string :title
      t.integer :rule
      t.string :image

      t.timestamps
    end
  end
end
