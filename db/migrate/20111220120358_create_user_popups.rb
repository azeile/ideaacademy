class CreateUserPopups < ActiveRecord::Migration
  def change
    create_table :user_popups do |t|
      t.integer :user_id
      t.boolean :seen, :default => false
      t.string :typekey

      t.timestamps
    end
  end
end
