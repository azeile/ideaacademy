class ChangeIdeasTable < ActiveRecord::Migration
  def change
    add_column :ideas, :room_id, :integer
    add_column :ideas, :category_id, :integer
    add_column :ideas, :in_topthree, :boolean
    add_index :ideas, [:contest_id, :user_id, :category_id, :room_id], :unique => false, :name => "ideas_indices"
  end
end
