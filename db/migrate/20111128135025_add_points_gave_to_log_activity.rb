class AddPointsGaveToLogActivity < ActiveRecord::Migration
  def change
    remove_column :statistic_contest_room_users, :points_gave
    add_column :log_activities, :points_gave, :integer, :default => 0
  end
end
