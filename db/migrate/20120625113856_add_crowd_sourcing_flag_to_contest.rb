class AddCrowdSourcingFlagToContest < ActiveRecord::Migration
  def change
    add_column :contests, :crowdsourcing_flag, :boolean
  end
end
