class AddRatioToCrowdsourcingResult < ActiveRecord::Migration
  def up
    add_column :crowdsourcing_results, :ratio, :decimal, :precision => 6, :scale => 2
  end
  
  def down
    remove_column :crowdsourcing_results, :ratio
  end
  
end
