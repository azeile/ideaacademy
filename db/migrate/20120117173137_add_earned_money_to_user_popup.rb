class AddEarnedMoneyToUserPopup < ActiveRecord::Migration
  def change
    add_column :user_popups, :earned_money, :integer
  end
end
