class AddPointColumnsToUserCategoryStatistic < ActiveRecord::Migration
  def change
    rename_column :statistic_category_ordered_users, :points, :total_points
    add_column :statistic_category_ordered_users, :vote_points, :integer
    add_column :statistic_category_ordered_users, :rate_points, :integer
    
    rename_column :statistic_category_users, :points, :total_points
    add_column :statistic_category_users, :vote_points, :integer
    add_column :statistic_category_users, :rate_points, :integer
  end
end
