class CreateTmpUsers < ActiveRecord::Migration
  def change
    create_table :tmp_users do |t|
      t.string :session_id

      t.timestamps
    end
    
    add_index :tmp_users, :session_id, :unique => true
  end
end