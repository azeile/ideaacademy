class CreateCrowdsourcingResults < ActiveRecord::Migration
  def change
    create_table :crowdsourcing_results do |t|
      t.string :text
      t.string :author
      t.integer :wins
      t.integer :total_duels
      t.integer :points
      t.string :author_image_url
      t.integer :crowdsourcing_contest_id

      t.timestamps
    end
  end
end
