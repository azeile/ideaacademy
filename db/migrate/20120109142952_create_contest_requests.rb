class CreateContestRequests < ActiveRecord::Migration
  def change
    create_table :contest_requests do |t|
      t.references :contest
      t.string :person_type
      t.string :email
      t.string :phone
      t.string :name
      t.string :company_name
      t.text :address

      t.timestamps
    end
    add_index :contest_requests, :contest_id
  end
end
