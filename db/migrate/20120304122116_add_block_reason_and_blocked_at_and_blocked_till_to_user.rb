class AddBlockReasonAndBlockedAtAndBlockedTillToUser < ActiveRecord::Migration
  def change
    add_column :users, :block_reason, :string
    add_column :users, :blocked_at, :datetime
    add_column :users, :blocked_till, :datetime
  end
end
