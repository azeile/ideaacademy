class AddIndexOnOriginId < ActiveRecord::Migration
  def up
    execute "CREATE UNIQUE INDEX origin_id_idx
          ON contests(origin_id)
          WHERE origin_id IS NOT NULL;"
  end

  def down
    drop_index :name => "origin_id_idx"
  end
end
