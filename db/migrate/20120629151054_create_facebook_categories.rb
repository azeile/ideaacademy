class CreateFacebookCategories < ActiveRecord::Migration
  def change
    create_table :facebook_categories do |t|
      t.string :name
      t.integer :fb_id

      t.timestamps
    end
  end
end
