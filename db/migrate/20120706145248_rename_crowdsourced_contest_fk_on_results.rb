class RenameCrowdsourcedContestFkOnResults < ActiveRecord::Migration
  def up
    rename_column :crowdsourcing_results, :crowdsourcing_contest_id, :crowdsourced_contest_id
  end

  def down
    rename_column :crowdsourcing_results, :crowdsourced_contest_id, :crowdsourcing_contest_id
  end
end
