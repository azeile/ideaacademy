class RemoveCurrencyFromLogCash < ActiveRecord::Migration
  def up
    remove_column :log_cashes, :currency
    remove_column :log_cashes, :changed
    remove_column :log_cashes, :status
    remove_column :log_cashes, :idea_id
    add_column :log_cashes, :earned_money, :integer
  end

  def down
    add_column :log_cashes, :currency, :integer
    add_column :log_cashes, :changed, :string
    add_column :log_cashes, :status, :integer
    add_column :log_cashes, :idea_id, :integer
    remove_column :log_cashes, :earned_money
  end
end
