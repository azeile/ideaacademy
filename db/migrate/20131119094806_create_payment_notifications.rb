class CreatePaymentNotifications < ActiveRecord::Migration
  def up
    create_table :payment_notifications do |t|
      t.text :params
      t.references :user
      t.references :service
      t.string :status
      t.string :subscription_timestamp
      t.string :transaction_id

      t.timestamps
    end
    
    add_index :payment_notifications, :subscription_timestamp
  end

  def down
    drop_table :payment_notifications
  end
end
