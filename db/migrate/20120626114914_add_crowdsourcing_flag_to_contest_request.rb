class AddCrowdsourcingFlagToContestRequest < ActiveRecord::Migration
  def change
    add_column :contest_requests, :crowdsourcing_flag, :boolean
  end
end
