class AddInternalPricingServicesFieldsToServices < ActiveRecord::Migration
  def change
    add_column :services, :user_limit, :integer, :default => 0
    add_column :services, :crowdsourcing_flag, :boolean, :default => false
    add_column :services, :unique_design_flag, :boolean, :default => false
    add_column :services, :intranet_flag, :boolean, :default => false
  end
end
