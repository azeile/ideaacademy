class CreateSpamReports < ActiveRecord::Migration
  def change
    create_table :spam_reports do |t|
      t.integer :user_id
      t.integer :idea_id

      t.timestamps
    end
  end
end
