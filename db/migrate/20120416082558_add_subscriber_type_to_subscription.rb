class AddSubscriberTypeToSubscription < ActiveRecord::Migration
  def change
    add_column :subscriptions, :subscriber_type, :string
  end
end
