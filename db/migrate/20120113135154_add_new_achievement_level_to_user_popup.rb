class AddNewAchievementLevelToUserPopup < ActiveRecord::Migration
  def change
    add_column :user_popups, :new_achievement_level, :integer
  end
end
