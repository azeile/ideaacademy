class AddColumnStatusToLogVotes < ActiveRecord::Migration
  def change
    add_column :log_votes, :status, :string
  end
end
