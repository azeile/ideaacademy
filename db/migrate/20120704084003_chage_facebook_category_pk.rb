class ChageFacebookCategoryPk < ActiveRecord::Migration
  def change
    execute <<-EOS
      CREATE UNIQUE INDEX fb_id_temp_idx ON facebook_categories(fb_id);
    EOS

    execute <<-EOS
      ALTER TABLE facebook_categories DROP CONSTRAINT facebook_categories_pkey,
      ADD CONSTRAINT facebook_categories_pkey PRIMARY KEY USING INDEX fb_id_temp_idx;
    EOS
  end
end
