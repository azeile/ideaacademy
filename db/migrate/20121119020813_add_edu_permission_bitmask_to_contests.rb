class AddEduPermissionBitmaskToContests < ActiveRecord::Migration
  def change
    add_column :contests, :edu_access_mask, :integer
  end
end
