class AddUniqueScoreIndexToStatistics < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("CREATE UNIQUE INDEX user_ranks_idx ON statistic_category_users (total_points DESC NULLS LAST, category_id DESC, user_id DESC);")
  end

  def down
    remove_index :statistic_category_users, :name => "user_ranks_idx"
  end
end
