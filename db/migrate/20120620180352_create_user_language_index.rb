class CreateUserLanguageIndex < ActiveRecord::Migration
  def up
    add_index :user_languages, [:user_id, :language], :unique => true, :name => "user_id_language_idx"
  end

  def down
    remove_index :user_languages, :name => :user_id_language_idx
  end
end
