class AddCounterCacheToRooms < ActiveRecord::Migration
  def change
    add_column :contests, :rooms_count, :integer, :default => 0
    
    Contest.reset_column_information
    Contest.find(:all).each do |c|
      Contest.update_counters c.id, :rooms_count => c.rooms.length
    end
  end
end
