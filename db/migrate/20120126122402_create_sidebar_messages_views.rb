class CreateSidebarMessagesViews < ActiveRecord::Migration
  def change
    create_table :sidebar_messages_views do |t|
      t.integer :sidebar_message_id
      t.integer :viewer_id

      t.timestamps
    end
  end
end
