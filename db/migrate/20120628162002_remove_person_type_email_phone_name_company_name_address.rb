class RemovePersonTypeEmailPhoneNameCompanyNameAddress < ActiveRecord::Migration
  def up
    remove_column :contest_requests, :person_type
    remove_column :contest_requests, :company_name
    remove_column :contest_requests, :email
    remove_column :contest_requests, :name
    remove_column :contest_requests, :address
  end

  def down
    add_column :contest_requests, :person_type, :string
    add_column :contest_requests, :company_name, :string
    add_column :contest_requests, :email, :string
    add_column :contest_requests, :name, :string
    add_column :contest_requests, :address, :string
  end
end
