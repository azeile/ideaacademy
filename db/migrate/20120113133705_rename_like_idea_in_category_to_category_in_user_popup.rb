class RenameLikeIdeaInCategoryToCategoryInUserPopup < ActiveRecord::Migration
  def up
    rename_column :user_popups, :like_idea_in_category, :category
  end

  def down
    rename_column :user_popups, :category, :like_idea_in_category
  end
end
