class AddUnlimitedIdeasExpiresAtToUser < ActiveRecord::Migration
  def change
    add_column :users, :unlimited_ideas_expires_at, :datetime
  end
end
