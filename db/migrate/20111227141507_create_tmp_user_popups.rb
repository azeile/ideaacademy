class CreateTmpUserPopups < ActiveRecord::Migration
  def change
    create_table :tmp_user_popups do |t|
      t.integer :tmp_user_id
      t.boolean :seen, :default => false
      t.string :typekey

      t.timestamps
    end
  end
end
