class AddNewAchievementMessageToUserPopup < ActiveRecord::Migration
  def change
    add_column :user_popups, :new_achievement_message, :text
  end
end
