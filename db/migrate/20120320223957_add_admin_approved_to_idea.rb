class AddAdminApprovedToIdea < ActiveRecord::Migration
  def change
    add_column :ideas, :admin_approved, :boolean
  end
end
