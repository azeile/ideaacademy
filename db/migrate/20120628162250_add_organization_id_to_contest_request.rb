class AddOrganizationIdToContestRequest < ActiveRecord::Migration
  def change
    add_column :contest_requests, :organization_id, :integer
  end
end
