class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.string  :email, null: false
      t.integer :organization_id, null: false
      t.integer :inviter_id, null: false

      t.timestamps
    end

    add_index :invitations, :organization_id
    add_index :invitations, :inviter_id
  end
end
