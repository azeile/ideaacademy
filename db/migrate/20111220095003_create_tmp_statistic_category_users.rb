class CreateTmpStatisticCategoryUsers < ActiveRecord::Migration
  def change
    create_table :tmp_statistic_category_users do |t|
      t.integer :category_id
      t.integer :tmp_user_id

      t.integer :total_points
      t.integer :live_points
      t.integer :level

      t.timestamps
    end
    
    add_index :tmp_statistic_category_users, [:category_id, :tmp_user_id], :unique => true, :name => "category_user_category_user_index"
  end
end
