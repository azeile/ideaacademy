class CreatePaymentTransactions < ActiveRecord::Migration
  def change
    create_table :payment_transactions do |t|
      t.integer :provider_transaction_id
      t.integer :service_id
      t.integer :user_provider_id
      t.string :status
      t.decimal :payment_sum

      t.timestamps
    end
  end
end
