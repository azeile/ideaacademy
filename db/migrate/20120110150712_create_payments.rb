class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.boolean :status
      t.float :amount
      t.string :currency
      t.references :user
      t.string :account
      t.string :name

      t.timestamps
    end
    add_index :payments, :user_id
  end
end
