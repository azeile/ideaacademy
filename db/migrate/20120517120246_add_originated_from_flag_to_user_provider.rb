class AddOriginatedFromFlagToUserProvider < ActiveRecord::Migration
  def change
    add_column :user_providers, :originated_from_flag, :boolean
  end
end
