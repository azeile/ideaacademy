class AddLikeIdeaInCategoryToUserPopup < ActiveRecord::Migration
  def change
    add_column :user_popups, :like_idea_in_category, :string
  end
end
