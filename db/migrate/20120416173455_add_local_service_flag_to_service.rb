class AddLocalServiceFlagToService < ActiveRecord::Migration
  def change
    add_column :services, :local_service_flag, :boolean
  end
end
