class AddIdeaIdToLogCashLog < ActiveRecord::Migration
  def change
    add_column :log_cashes, :idea_id, :integer
  end
end
