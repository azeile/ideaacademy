class AddIndicesToStatisticTables < ActiveRecord::Migration
  def change
    add_index :statistic_category_ordered_users, [:category_id, :user_id], :unique => true, :name => "statistic_category_ordered_users_unique_indices"
    add_index :statistic_category_users, [:category_id, :user_id], :unique => true, :name => "statistic_category_users_unique_indices"
    add_index :statistic_contest_room_users, [:contest_id, :room_id, :user_id], :unique => true, :name => "statistic_contest_room_users_unique_indices"
    add_index :statistic_contest_users, [:contest_id, :user_id], :unique => true, :name => "statistic_contest_users_unique_indices"
  end
end
