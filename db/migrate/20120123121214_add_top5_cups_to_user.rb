class AddTop5CupsToUser < ActiveRecord::Migration
  def change
    add_column :users, :top5_cups, :integer, :default => 0
  end
end
