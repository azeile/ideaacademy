class RepairUserImageUrls < ActiveRecord::Migration
  def up
    if User && User.attribute_names.include?("image_url")
      User.where(:image_url => "f").update_all(:image_url => nil)
    end
  end

  def down
  end
end
