class AddOrganizationIdToCrowdsourcedContest < ActiveRecord::Migration
  def change
    add_column :crowdsourced_contests, :organization_id, :integer
  end
end
