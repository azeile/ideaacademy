class CreateUserMessages < ActiveRecord::Migration
  def change
    create_table :user_messages do |t|
      t.text :body, null: false
      t.integer :sender_id, null: false
      t.integer :receiver_id, null: false

      t.timestamps
    end

    add_index :user_messages, :sender_id
    add_index :user_messages, :receiver_id
  end
end
