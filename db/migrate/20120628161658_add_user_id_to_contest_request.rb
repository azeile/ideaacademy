class AddUserIdToContestRequest < ActiveRecord::Migration
  def change
    add_column :contest_requests, :user_id, :integer
  end
end
