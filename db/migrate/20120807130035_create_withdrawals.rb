class CreateWithdrawals < ActiveRecord::Migration
  def change
    create_table :withdrawals do |t|
      t.boolean :status
      t.float :amount
      t.string :currency
      t.references :user
      t.string :account
      t.string :name

      t.timestamps
    end
    add_index :withdrawals, :user_id
  end
end
