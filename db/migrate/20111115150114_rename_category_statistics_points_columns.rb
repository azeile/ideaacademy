class RenameCategoryStatisticsPointsColumns < ActiveRecord::Migration
  def change
    rename_column :statistic_category_users, :rate_points, :idea_points
    rename_column :statistic_category_ordered_users, :rate_points, :idea_points
  end
end
