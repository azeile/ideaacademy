class AddCodeAndDescriptionToService < ActiveRecord::Migration
  def change
    add_column :services, :code, :string
    add_column :services, :description, :text
  end
end
