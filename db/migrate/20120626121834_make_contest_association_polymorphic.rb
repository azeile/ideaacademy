class MakeContestAssociationPolymorphic < ActiveRecord::Migration
  def up
    rename_column :contest_requests, :contest_id, :parent_id
    add_column    :contest_requests, :type,       :string
  end

  def down
    rename_column :contest_requests, :parent_id, :contest_id
    remove_column :contest_requests, :type
  end
end
