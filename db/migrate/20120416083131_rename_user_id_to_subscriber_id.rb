class RenameUserIdToSubscriberId < ActiveRecord::Migration
  def up
    rename_column :subscriptions, :user_id, :subscriber_id
  end

  def down
    rename_column :subscriptions, :subscriber_id, :user_id
  end
end
