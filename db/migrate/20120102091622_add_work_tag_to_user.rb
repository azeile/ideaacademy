class AddWorkTagToUser < ActiveRecord::Migration
  def change
    add_column :users, :work_tag, :boolean, :default => false
  end
end
