class AddIsMoneyToContest < ActiveRecord::Migration
  def change
    add_column :contests, :is_money, :boolean, :default => false
  end
end
