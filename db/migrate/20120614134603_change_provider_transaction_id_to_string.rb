class ChangeProviderTransactionIdToString < ActiveRecord::Migration
  def up
    change_column :payment_transactions, :provider_transaction_id, :string
  end

  def down
    change_column :payment_transactions, :provider_transaction_id, :integer
  end
end
