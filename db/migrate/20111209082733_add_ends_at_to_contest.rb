class AddEndsAtToContest < ActiveRecord::Migration
  def change
    add_column :contests, :ends_at, :datetime
  end
end
