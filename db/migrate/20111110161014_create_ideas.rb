class CreateIdeas < ActiveRecord::Migration
  def change
    create_table :ideas do |t|
      t.integer :contest_id
      t.integer :user_id

      t.string :typekey # Instead of "type"?
      t.text :text
      t.string :video_info
      t.string :video_image
      t.integer :duels_won, :default => 0
      t.integer :duels_in, :default => 0

      t.boolean :is_deleted, :default => false

      t.timestamps
    end
  end
end
