class AddLanguageToContest < ActiveRecord::Migration
  def change
    add_column :contests, :language, :string
  end
end
