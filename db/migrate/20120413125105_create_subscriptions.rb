class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :service
      t.references :user
      t.datetime :date_from
      t.datetime :date_to

      t.timestamps
    end
    add_index :subscriptions, :user_id
  end
end
