class CreateLogVotes < ActiveRecord::Migration
  def change
    create_table :log_votes do |t|
      t.integer :contest_id
      t.integer :room_id
      t.integer :selected_idea_id
      t.integer :ingored_idea_id
      t.integer :user_id

      t.timestamps
    end
  end
end
