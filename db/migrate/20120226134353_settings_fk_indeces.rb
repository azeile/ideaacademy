class SettingsFkIndeces < ActiveRecord::Migration
  def up
    add_index :application_settings, :code, :unique => true
  end

  def down
    remove_index :application_settings, :column_name =>:code
  end
end
