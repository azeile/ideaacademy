class CreateLogActivities < ActiveRecord::Migration
  def change
    create_table :log_activities do |t|
      t.integer :contest_id
      t.integer :category_id
      t.integer :room_id
      t.integer :user_id
      t.integer :author_id

      t.string :typekey
      t.integer :points
      t.integer :status

      t.timestamps
    end
  end
end
