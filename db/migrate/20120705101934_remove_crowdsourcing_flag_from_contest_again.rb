class RemoveCrowdsourcingFlagFromContestAgain < ActiveRecord::Migration
  def up
    remove_column :contests, :crowdsourcing_flag
  end

  def down
    add_column :contests, :crowdsourcing_flag, :boolean
  end
end
