class CreateServiceProviderIds < ActiveRecord::Migration
  def change
    create_table :service_provider_ids do |t|
      t.references :service
      t.string :service_provider_name
      t.string :environment
      t.string :service_social_network_id

      t.timestamps
    end
  end
end
