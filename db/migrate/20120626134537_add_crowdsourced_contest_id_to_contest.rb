class AddCrowdsourcedContestIdToContest < ActiveRecord::Migration
  def change
    add_column :contests, :crowdsourced_contest_id, :integer
  end
end
