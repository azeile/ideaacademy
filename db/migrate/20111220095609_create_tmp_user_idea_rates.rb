class CreateTmpUserIdeaRates < ActiveRecord::Migration
  def change
    create_table :tmp_user_idea_rates do |t|
      t.integer :tmp_user_id
      t.integer :idea_id
      t.integer :rate_count, :default => 0
      
      t.timestamps
    end
    
    add_index :tmp_user_idea_rates, [:tmp_user_id, :idea_id], :unique => true
  end
end