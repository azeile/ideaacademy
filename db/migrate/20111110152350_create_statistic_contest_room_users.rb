class CreateStatisticContestRoomUsers < ActiveRecord::Migration
  def change
    create_table :statistic_contest_room_users do |t|
      t.integer :contest_id
      t.integer :room_id
      t.integer :user_id

      t.integer :vote_points
      t.integer :idea_points
      t.integer :total_points

      t.timestamps
    end
  end
end
