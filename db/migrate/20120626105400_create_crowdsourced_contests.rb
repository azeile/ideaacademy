class CreateCrowdsourcedContests < ActiveRecord::Migration
  def change
    create_table :crowdsourced_contests do |t|
      t.string    :title
      t.text      :description
      t.integer   :category_id
      t.boolean   :money_flag
      t.string    :award
      t.string    :typekey
      t.integer   :user_per_room, :default => 200
      t.integer   :rooms_count, :defafult => 100
      t.boolean   :active_flag
      t.datetime  :ends_at
      t.string    :language

      t.string    "smaller_logo_file_name"
      t.string    "smaller_logo_content_type"
      t.integer   "smaller_logo_file_size"
      t.datetime  "smaller_logo_updated_at"

      t.string   "logo_file_name"
      t.string   "logo_content_type"
      t.integer  "logo_file_size"
      t.datetime "logo_updated_at"

      t.timestamps
    end
  end
end
