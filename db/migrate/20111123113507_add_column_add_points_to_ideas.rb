class AddColumnAddPointsToIdeas < ActiveRecord::Migration
  def change
    add_column :ideas, :add_points, :boolean, :default => false
  end
end
