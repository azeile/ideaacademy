class CreateSidebarMessages < ActiveRecord::Migration
  def change
    create_table :sidebar_messages do |t|
      t.string :title
      t.text :message
      t.boolean :active, :default => true

      t.timestamps
    end
  end
end
