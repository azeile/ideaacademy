class CreateTmpRoomUsers < ActiveRecord::Migration
  def change
    create_table :tmp_room_users do |t|
      t.integer :contest_id
      t.integer :room_id
      t.integer :tmp_user_id

      t.timestamps
    end
    
    add_index :tmp_room_users, [:contest_id, :room_id, :tmp_user_id]
  end
end
