class AddIdeaIdToActivityLog < ActiveRecord::Migration
  def change
    add_column :log_activities, :idea_id, :integer
  end
end
