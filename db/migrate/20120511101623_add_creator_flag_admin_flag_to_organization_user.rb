class AddCreatorFlagAdminFlagToOrganizationUser < ActiveRecord::Migration
  def change
    add_column :organization_users, :creator_flag, :boolean
    add_column :organization_users, :admin_flag, :boolean
  end
end
