class AddColumnPointsGaveToStatisticContestRoomUsers < ActiveRecord::Migration
  def change
    add_column :statistic_contest_room_users, :points_gave, :integer, :default => 0
  end
end
