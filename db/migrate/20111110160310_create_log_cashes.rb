class CreateLogCashes < ActiveRecord::Migration
  def change
    create_table :log_cashes do |t|
      t.integer :contest_id
      t.integer :idea_id
      t.integer :user_id

      t.integer :currency
      t.string :changed
      t.integer :status

      t.timestamps
    end
  end
end
