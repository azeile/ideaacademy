class MakeContestImagePolymorphic < ActiveRecord::Migration
  def up
    add_column    :contest_images, :imageable_type, :string
    rename_column :contest_images, :contest_id,     :imageable_id
  end

  def down
    add_column    :contest_images, :imageable_type, :string
    rename_column :contest_images, :imageable_id,   :contest_id
  end
end
