class ChangeImageColumns < ActiveRecord::Migration
  def change
    remove_column :contests, :contest_image_id
    add_column :contest_images, :contest_id, :integer
  end
end
