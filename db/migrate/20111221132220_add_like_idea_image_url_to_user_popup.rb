class AddLikeIdeaImageUrlToUserPopup < ActiveRecord::Migration
  def change
    add_column :user_popups, :like_idea_image_url, :string
  end
end
