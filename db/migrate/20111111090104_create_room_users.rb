class CreateRoomUsers < ActiveRecord::Migration
  def change
    create_table :room_users do |t|
      t.integer :contest_id
      t.integer :room_id
      t.integer :user_id

      t.timestamps
    end
  end
end
