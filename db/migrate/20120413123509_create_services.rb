class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.integer :price
      t.string :currency
      t.integer :duration
      t.string :duration_units

      t.timestamps
    end
  end
end
