class AddCounterCacheToContests < ActiveRecord::Migration
  def change
    add_column :categories, :contests_count, :integer, :default => 0
    
    Category.reset_column_information
    Category.find(:all).each do |c|
      Category.update_counters c.id, :contests_count => c.contests.length
    end
  end
end
