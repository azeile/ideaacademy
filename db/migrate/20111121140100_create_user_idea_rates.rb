class CreateUserIdeaRates < ActiveRecord::Migration
  def change
    create_table :user_idea_rates do |t|
      t.integer :user_id
      t.integer :idea_id
      t.integer :rate_count, :default => 0

      t.timestamps
    end

    add_index :user_idea_rates, [:user_id, :idea_id], :unique => true
  end
end
