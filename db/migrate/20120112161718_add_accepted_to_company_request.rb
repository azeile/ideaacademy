class AddAcceptedToCompanyRequest < ActiveRecord::Migration
  def change
    add_column :contest_requests, :accepted, :boolean, :default => false
  end
end
