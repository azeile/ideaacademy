class AddIndexToContestImagePolymorphicFk < ActiveRecord::Migration
  def up
    add_index :contest_images, [ :imageable_id, :imageable_type], :unique => true, :name => :contest_img_fk_idx
  end

  def down
    drop_index :contest_images, :name => :contest_img_fk_idx
  end
end
