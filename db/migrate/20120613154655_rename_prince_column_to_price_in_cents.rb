class RenamePrinceColumnToPriceInCents < ActiveRecord::Migration
  def up
    rename_column :services , :price, :price_in_cents
  end

  def down
    rename_column :services, :price_in_cents, :price
  end
end
