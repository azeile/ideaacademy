class AddTriggerToStatisticCategoryOrderUsers < ActiveRecord::Migration
  def change
    
    #execute <<-SQL      
    #ALTER TABLE "statistic_category_ordered_users" DROP COLUMN id RESTRICT;
    #ALTER TABLE "statistic_category_ordered_users" ADD PRIMARY KEY ("category_id","order_id");
    #CREATE LANGUAGE plpgsql;
    #CREATE OR REPLACE FUNCTION category_ordered_users_order_id_auto()
    #  RETURNS trigger AS $$
    #DECLARE
    #  _rel_id constant int := 'statistic_category_ordered_users'::regclass::int;
    #  _category_id int;
    #BEGIN
    #  IF NEW.order_id = 0 THEN
    #    _category_id = NEW.category_id;
    #    PERFORM pg_advisory_lock(_rel_id, _category_id);
    #    SELECT COALESCE(MAX(order_id) + 1, 1)
    #    INTO NEW.order_id
    #    FROM statistic_category_ordered_users
    #    WHERE category_id = NEW.category_id;
    #  END IF;
    #  RETURN NEW;
    #END;
    #$$ LANGUAGE plpgsql STRICT;

    #CREATE TRIGGER category_ordered_users_order_id_auto
    #  BEFORE INSERT ON statistic_category_ordered_users
    #  FOR EACH ROW
    #  EXECUTE PROCEDURE category_ordered_users_order_id_auto();
     
    #CREATE OR REPLACE FUNCTION category_ordered_users_order_auto_unlock()
    #  RETURNS trigger AS $$
    #DECLARE
    #  _rel_id constant int := 'statistic_category_ordered_users'::regclass::int;
    #  _category_id int;
    #BEGIN
    #  _category_id = NEW.category_id;
    #  PERFORM pg_advisory_unlock(_rel_id, _category_id);
    #  RETURN NEW;
    #END;
    #$$ LANGUAGE plpgsql STRICT;

    #CREATE TRIGGER category_ordered_users_order_auto_unlock
    #  AFTER INSERT ON statistic_category_ordered_users
    #  FOR EACH ROW
    #  EXECUTE PROCEDURE category_ordered_users_order_auto_unlock();
    #SQL
    
  end
end
