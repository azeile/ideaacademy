class AddUsersPerRoomToContest < ActiveRecord::Migration
  def change
    add_column :contests, :users_per_room, :integer, :default => 100
  end
end
