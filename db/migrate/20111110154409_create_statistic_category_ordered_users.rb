class CreateStatisticCategoryOrderedUsers < ActiveRecord::Migration
  def change
    create_table :statistic_category_ordered_users do |t|
      t.integer :category_id
      t.integer :order_id, :default => 0
      t.integer :user_id

      t.integer :points
      t.integer :level

      t.timestamps
    end
  end
end
