class AddEarnedMoneyToUser < ActiveRecord::Migration
  def change
    add_column :users, :earned_money, :integer, :default => 0
  end
end
