class AddOrganizationNameAndOriginIdToContest < ActiveRecord::Migration
  def change
    add_column :contests, :origin_id, :integer
    add_column :contests, :organization_name, :string
  end
end
