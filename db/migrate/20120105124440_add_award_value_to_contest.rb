class AddAwardValueToContest < ActiveRecord::Migration
  def change
    add_column :contests, :award_value, :integer, :default => nil
  end
end
