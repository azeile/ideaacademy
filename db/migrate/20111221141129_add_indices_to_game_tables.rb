class AddIndicesToGameTables < ActiveRecord::Migration
  def change
    add_index :log_votes, :room_id
    add_index :log_votes, :contest_id
    add_index :log_votes, [:selected_idea_id, :ignored_idea_id]
    add_index :room_users, :contest_id
    add_index :room_users, :room_id
    add_index :room_users, :user_id
  end
end
