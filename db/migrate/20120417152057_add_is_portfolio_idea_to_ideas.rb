class AddIsPortfolioIdeaToIdeas < ActiveRecord::Migration
  def change
    add_column :ideas, :is_portfolio_idea, :boolean
  end
end
