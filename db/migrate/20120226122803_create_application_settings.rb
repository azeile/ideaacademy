class CreateApplicationSettings < ActiveRecord::Migration
  def change
    create_table :application_settings do |t|
      t.integer :setting_type_id
      t.string :code
      t.string :value
      t.string :name

      t.timestamps
    end
  end
end
