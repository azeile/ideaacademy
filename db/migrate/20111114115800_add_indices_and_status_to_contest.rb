class AddIndicesAndStatusToContest < ActiveRecord::Migration
  def change
    add_column :contests, :status, :string
    add_index :contests, [:category_id]
  end
end
