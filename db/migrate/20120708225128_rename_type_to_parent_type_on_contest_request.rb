class RenameTypeToParentTypeOnContestRequest < ActiveRecord::Migration
  def up
    rename_column :contest_requests, :type, :parent_type
  end

  def down
    rename_column :contest_requests, :parent_type, :type
  end
end
