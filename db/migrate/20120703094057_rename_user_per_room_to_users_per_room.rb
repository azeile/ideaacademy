class RenameUserPerRoomToUsersPerRoom < ActiveRecord::Migration
  def up
    rename_column :crowdsourced_contests, :user_per_room, :users_per_room
  end

  def down
    rename_column :crowdsourced_contests, :users_per_room, :user_per_room
  end
end
