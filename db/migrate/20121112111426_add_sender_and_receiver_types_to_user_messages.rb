class AddSenderAndReceiverTypesToUserMessages < ActiveRecord::Migration
  def up
    column_exists?(:user_messages, :sender_type) do
      add_column :user_messages, :sender_type, :string
    end
    column_exists?(:user_messages, :receiver_type) do
      add_column :user_messages, :receiver_type, :string
    end
  end

  def down
    column_exists?(:user_messages, :sender_type) do
      remove_column :user_messages, :sender_type
    end
    column_exists?(:user_messages, :receiver_type) do
      remove_column :user_messages, :sender_type
    end
  end
end
