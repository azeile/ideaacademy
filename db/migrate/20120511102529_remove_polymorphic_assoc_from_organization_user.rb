class RemovePolymorphicAssocFromOrganizationUser < ActiveRecord::Migration
  def up
    remove_index :organization_users, :name => :employee_fk
    
    remove_column :organization_users, :employee_type
    rename_column :organization_users, :employee_id, :user_id
  end

  def down
    add_column :organization_users, :employee_type, :string
    rename_column :organization_users, :user_id, :employee_id
    
    add_index :organization_users, [:employee_id, :employee_type], :name => :employee_fk
  end
end
