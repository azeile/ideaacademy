class AddRetryTimesToPaymentTransaction < ActiveRecord::Migration
  def change
    add_column :payment_transactions, :retry_times, :integer
  end
end
