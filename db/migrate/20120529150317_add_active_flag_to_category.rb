class AddActiveFlagToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :active_flag, :boolean
  end
end
