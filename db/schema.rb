# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131119094806) do

  create_table "active_admin_comments", :force => true do |t|
    t.integer  "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.boolean  "superadmin_flag"
    t.datetime "locked_at"
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "application_settings", :force => true do |t|
    t.integer  "setting_type_id"
    t.string   "code"
    t.string   "value"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "application_settings", ["code"], :name => "index_application_settings_on_code", :unique => true

  create_table "badges", :force => true do |t|
    t.string   "title"
    t.integer  "rule"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "contests_count",  :default => 0
    t.integer  "organization_id"
    t.boolean  "active_flag"
  end

  create_table "contest_images", :force => true do |t|
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "imageable_id"
    t.string   "imageable_type"
  end

  add_index "contest_images", ["imageable_id", "imageable_type"], :name => "contest_img_fk_idx", :unique => true

  create_table "contest_requests", :force => true do |t|
    t.integer  "parent_id"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "accepted",           :default => false
    t.boolean  "crowdsourcing_flag"
    t.string   "parent_type"
    t.integer  "user_id"
    t.integer  "organization_id"
  end

  add_index "contest_requests", ["parent_id"], :name => "index_contest_requests_on_contest_id"

  create_table "contests", :force => true do |t|
    t.integer  "category_id"
    t.string   "title"
    t.text     "description"
    t.string   "award"
    t.string   "typekey"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "users_per_room",            :default => 100
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.datetime "ends_at"
    t.boolean  "is_money",                  :default => false
    t.integer  "award_value"
    t.integer  "rooms_count",               :default => 0
    t.string   "smaller_logo_file_name"
    t.string   "smaller_logo_content_type"
    t.integer  "smaller_logo_file_size"
    t.datetime "smaller_logo_updated_at"
    t.boolean  "active_flag"
    t.string   "language"
    t.integer  "crowdsourced_contest_id"
    t.integer  "origin_id"
    t.string   "organization_name"
    t.integer  "edu_access_mask"
  end

  add_index "contests", ["category_id", "ends_at", "active_flag"], :name => "idx_category_contest_count"
  add_index "contests", ["category_id"], :name => "index_contests_on_category_id"
  add_index "contests", ["origin_id"], :name => "origin_id_idx", :unique => true

  create_table "crowdsourced_contests", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "category_id"
    t.boolean  "money_flag"
    t.string   "award"
    t.string   "typekey"
    t.integer  "users_per_room",            :default => 200
    t.integer  "rooms_count"
    t.boolean  "active_flag"
    t.datetime "ends_at"
    t.string   "language"
    t.string   "smaller_logo_file_name"
    t.string   "smaller_logo_content_type"
    t.integer  "smaller_logo_file_size"
    t.datetime "smaller_logo_updated_at"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "organization_id"
    t.boolean  "sent_flag"
  end

  create_table "crowdsourcing_results", :force => true do |t|
    t.string   "text"
    t.string   "author"
    t.integer  "wins"
    t.integer  "total_duels"
    t.integer  "points"
    t.string   "author_image_url"
    t.integer  "crowdsourced_contest_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "ratio",                   :precision => 6, :scale => 2
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "facebook_categories", :id => false, :force => true do |t|
    t.integer  "id",         :null => false
    t.string   "name"
    t.integer  "fb_id",      :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ideas", :force => true do |t|
    t.integer  "contest_id"
    t.integer  "user_id"
    t.string   "typekey"
    t.text     "text"
    t.string   "video_info"
    t.string   "video_image"
    t.integer  "duels_won",          :default => 0
    t.integer  "duels_in",           :default => 0
    t.boolean  "is_deleted",         :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "room_id"
    t.integer  "category_id"
    t.boolean  "in_topthree"
    t.string   "core_text"
    t.boolean  "add_points",         :default => false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "admin_approved"
    t.boolean  "is_portfolio_idea"
  end

  add_index "ideas", ["contest_id", "user_id", "category_id", "room_id"], :name => "ideas_indices"

  create_table "invitations", :force => true do |t|
    t.string   "email",           :null => false
    t.integer  "organization_id", :null => false
    t.integer  "inviter_id",      :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invitations", ["inviter_id"], :name => "index_invitations_on_inviter_id"
  add_index "invitations", ["organization_id"], :name => "index_invitations_on_organization_id"

  create_table "log_activities", :force => true do |t|
    t.integer  "contest_id"
    t.integer  "category_id"
    t.integer  "room_id"
    t.integer  "user_id"
    t.integer  "author_id"
    t.string   "typekey"
    t.integer  "points"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "idea_id"
    t.integer  "points_gave", :default => 0
  end

  create_table "log_cashes", :force => true do |t|
    t.integer  "contest_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "earned_money"
    t.integer  "idea_id"
  end

  create_table "log_votes", :force => true do |t|
    t.integer  "contest_id"
    t.integer  "room_id"
    t.integer  "selected_idea_id"
    t.integer  "ignored_idea_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
  end

  add_index "log_votes", ["contest_id"], :name => "index_log_votes_on_contest_id"
  add_index "log_votes", ["room_id"], :name => "index_log_votes_on_room_id"
  add_index "log_votes", ["selected_idea_id", "ignored_idea_id"], :name => "index_log_votes_on_selected_idea_id_and_ignored_idea_id"

  create_table "organization_users", :force => true do |t|
    t.integer  "user_id"
    t.integer  "organization_id"
    t.boolean  "default_flag"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "creator_flag"
    t.boolean  "admin_flag"
  end

  create_table "organizations", :force => true do |t|
    t.string   "name"
    t.string   "heard_from"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone_number"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "org_type"
  end

  create_table "payment_notifications", :force => true do |t|
    t.text     "params"
    t.integer  "user_id"
    t.integer  "service_id"
    t.string   "status"
    t.string   "subscription_timestamp"
    t.string   "transaction_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "payment_notifications", ["subscription_timestamp"], :name => "index_payment_notifications_on_subscription_timestamp"

  create_table "payment_transactions", :force => true do |t|
    t.string   "provider_transaction_id"
    t.integer  "service_id"
    t.integer  "user_provider_id"
    t.string   "status"
    t.decimal  "payment_sum"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "retry_times"
  end

  create_table "room_users", :force => true do |t|
    t.integer  "contest_id"
    t.integer  "room_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "room_users", ["contest_id"], :name => "index_room_users_on_contest_id"
  add_index "room_users", ["room_id"], :name => "index_room_users_on_room_id"
  add_index "room_users", ["user_id"], :name => "index_room_users_on_user_id"

  create_table "rooms", :force => true do |t|
    t.integer  "contest_id"
    t.integer  "participants"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "locked",       :default => false
  end

  create_table "service_provider_ids", :force => true do |t|
    t.integer  "service_id"
    t.string   "service_provider_name"
    t.string   "environment"
    t.string   "service_social_network_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "services", :force => true do |t|
    t.string   "name"
    t.integer  "price_in_cents"
    t.string   "currency"
    t.integer  "duration"
    t.string   "duration_units"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code"
    t.text     "description"
    t.boolean  "local_service_flag"
    t.integer  "user_limit",         :default => 0
    t.boolean  "crowdsourcing_flag", :default => false
    t.boolean  "unique_design_flag", :default => false
    t.boolean  "intranet_flag",      :default => false
  end

  add_index "services", ["code"], :name => "service_code_index"

  create_table "setting_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sidebar_messages", :force => true do |t|
    t.string   "title"
    t.text     "message"
    t.boolean  "active",          :default => true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "organization_id"
  end

  create_table "sidebar_messages_views", :force => true do |t|
    t.integer  "sidebar_message_id"
    t.integer  "viewer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "spam_reports", :force => true do |t|
    t.integer  "user_id"
    t.integer  "idea_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "statistic_category_ordered_users", :force => true do |t|
    t.integer  "category_id"
    t.integer  "order_id",     :default => 0
    t.integer  "user_id"
    t.integer  "total_points"
    t.integer  "level"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "vote_points"
    t.integer  "idea_points"
  end

  add_index "statistic_category_ordered_users", ["category_id", "user_id"], :name => "statistic_category_ordered_users_unique_indices", :unique => true

  create_table "statistic_category_users", :force => true do |t|
    t.integer  "category_id"
    t.integer  "user_id"
    t.integer  "total_points"
    t.integer  "level"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "vote_points"
    t.integer  "idea_points"
    t.integer  "live_points"
  end

  add_index "statistic_category_users", ["category_id", "user_id"], :name => "statistic_category_users_unique_indices", :unique => true
  add_index "statistic_category_users", ["total_points", "category_id", "user_id"], :name => "user_ranks_idx", :unique => true

  create_table "statistic_contest_room_users", :force => true do |t|
    t.integer  "contest_id"
    t.integer  "room_id"
    t.integer  "user_id"
    t.integer  "vote_points"
    t.integer  "idea_points"
    t.integer  "total_points"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "statistic_contest_room_users", ["contest_id", "room_id", "user_id"], :name => "statistic_contest_room_users_unique_indices", :unique => true

  create_table "statistic_contest_users", :force => true do |t|
    t.integer  "contest_id"
    t.integer  "user_id"
    t.integer  "vote_points"
    t.integer  "idea_points"
    t.integer  "total_points"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "statistic_contest_users", ["contest_id", "user_id"], :name => "statistic_contest_users_unique_indices", :unique => true

  create_table "subscriptions", :force => true do |t|
    t.integer  "service_id"
    t.integer  "subscriber_id"
    t.datetime "date_from"
    t.datetime "date_to"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "subscriber_type"
  end

  add_index "subscriptions", ["service_id"], :name => "service_id_fk"
  add_index "subscriptions", ["subscriber_id", "subscriber_type", "date_from", "date_to"], :name => "active_subscription_index"
  add_index "subscriptions", ["subscriber_id"], :name => "index_subscriptions_on_user_id"

  create_table "tmp_log_votes", :force => true do |t|
    t.integer  "contest_id"
    t.integer  "room_id"
    t.integer  "selected_idea_id"
    t.integer  "ignored_idea_id"
    t.integer  "tmp_user_id"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tmp_log_votes", ["contest_id", "room_id"], :name => "index_tmp_log_votes_on_contest_id_and_room_id"

  create_table "tmp_room_users", :force => true do |t|
    t.integer  "contest_id"
    t.integer  "room_id"
    t.integer  "tmp_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tmp_room_users", ["contest_id", "room_id", "tmp_user_id"], :name => "index_tmp_room_users_on_contest_id_and_room_id_and_tmp_user_id"

  create_table "tmp_statistic_category_users", :force => true do |t|
    t.integer  "category_id"
    t.integer  "tmp_user_id"
    t.integer  "total_points"
    t.integer  "live_points"
    t.integer  "level"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tmp_statistic_category_users", ["category_id", "tmp_user_id"], :name => "category_user_category_user_index", :unique => true

  create_table "tmp_user_idea_rates", :force => true do |t|
    t.integer  "tmp_user_id"
    t.integer  "idea_id"
    t.integer  "rate_count",  :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tmp_user_idea_rates", ["tmp_user_id", "idea_id"], :name => "index_tmp_user_idea_rates_on_tmp_user_id_and_idea_id", :unique => true

  create_table "tmp_user_popups", :force => true do |t|
    t.integer  "tmp_user_id"
    t.boolean  "seen",        :default => false
    t.string   "typekey"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tmp_users", :force => true do |t|
    t.string   "session_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tmp_users", ["session_id"], :name => "index_tmp_users_on_session_id", :unique => true

  create_table "user_badges", :force => true do |t|
    t.integer  "user_id"
    t.integer  "badge_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_category_notifications", :force => true do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_friends", :force => true do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_idea_rates", :force => true do |t|
    t.integer  "user_id"
    t.integer  "idea_id"
    t.integer  "rate_count", :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_idea_rates", ["user_id", "idea_id"], :name => "index_user_idea_rates_on_user_id_and_idea_id", :unique => true

  create_table "user_languages", :force => true do |t|
    t.integer  "user_id"
    t.string   "language"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_languages", ["user_id", "language"], :name => "user_id_language_idx", :unique => true

  create_table "user_messages", :force => true do |t|
    t.text     "body",        :null => false
    t.integer  "sender_id",   :null => false
    t.integer  "receiver_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_messages", ["receiver_id"], :name => "index_user_messages_on_receiver_id"
  add_index "user_messages", ["sender_id"], :name => "index_user_messages_on_sender_id"

  create_table "user_popups", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "seen",                    :default => false
    t.string   "typekey"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "like_idea_first_name"
    t.string   "like_idea_last_name"
    t.string   "like_idea_image_url"
    t.string   "like_idea_my_image_url"
    t.string   "category"
    t.integer  "new_achievement_level"
    t.integer  "earned_money"
    t.text     "new_achievement_message"
  end

  create_table "user_providers", :force => true do |t|
    t.integer  "user_id"
    t.integer  "social_id",            :limit => 8
    t.string   "name"
    t.string   "token"
    t.string   "secret"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "originated_from_flag"
  end

  create_table "users", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "website"
    t.string   "phone"
    t.string   "image_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "about"
    t.boolean  "work_tag",                                 :default => false
    t.boolean  "receive_notifications",                    :default => false
    t.integer  "earned_money",                             :default => 0
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "checked_out_amount",                       :default => 0
    t.integer  "top5_cups",                                :default => 0
    t.datetime "unlimited_ideas_expires_at"
    t.string   "block_reason"
    t.datetime "blocked_at"
    t.datetime "blocked_till"
    t.string   "encrypted_password",                       :default => ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                            :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "authentication_token"
    t.string   "title"
    t.string   "gender"
    t.string   "invitation_token",           :limit => 60
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.string   "user_type"
  end

  add_index "users", ["email"], :name => "index_users_on_email"
  add_index "users", ["invitation_token"], :name => "index_users_on_invitation_token"
  add_index "users", ["invited_by_id"], :name => "index_users_on_invited_by_id"

  create_table "visitor_attributes", :force => true do |t|
    t.string   "ip_address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "withdrawals", :force => true do |t|
    t.boolean  "status"
    t.float    "amount"
    t.string   "currency"
    t.integer  "user_id"
    t.string   "account"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "withdrawals", ["user_id"], :name => "index_withdrawals_on_user_id"

end
