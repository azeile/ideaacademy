module ActiveAdmin::UsersHelper
  def user_type_translations type
    translations = {
      teacher: t( "internal.user.types.teacher" ,  default: "Teacher"),
      parent:  t( "internal.user.types.parent"  ,  default: "Parent"),
      student: t( "internal.user.types.student" ,  default: "Student")
    }
    translations[type]
  end

  def user_types
    EduUser::TYPES.map { |type| user_type_translations type.to_sym }
  end
end
