module RegistrationsHelper
  def heard_from_options
    Organization::HEARD_FROM_OPTIONS.map {|hf| [ I18n.t(hf[0], default: hf[0].split(".").last.humanize), hf[1] ]  }
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def is_active?(action)
    "active" if send(:"#{action}_internal_url") == request.url
  end

  def organization_types
    translations = {organization: t("internal.organization.types.organization", default: "Organization"),
                    school:       t("internal.organization.types.school", default: "School")
    }
    Organization::TYPES.map { |type| [translations[type], type] }
  end
end
