module ApplicationHelper
  include ActiveAdmin::UsersHelper

  def service_duration service
    if service.duration > 0
      if I18n.locale == :en
        pluralize(service.duration, service.duration_units.singularize).capitalize
      else
        I18n.t "service.duration.#{ service.duration == 1 ? service.duration_units.singularize : service.duration_units }"
      end
    else
      I18n.t "internal.pricing.table.duration.unlimited", default: "Unlimited"
    end
  end

  def paypal_url(service)
    values = {
      :business => "arvis.zeile-facilitator@gmail.com",
      :cmd => "_xclick",
      :upload => 1,
      :return => thanks_for_payment_url,
      :invoice => Time.now.to_i
    }
    
    values.merge!({
      "amount" => service.price,
      "item_name" => I18n.t(service.name),
      "item_number" => service.id,
      "quantity" => 1
    })
    
    "https://www.sandbox.paypal.com/cgi-bin/webscr?" + values.to_query
  end

  def t key, options={}
    (I18n.t key, options).html_safe
  end
end
