module MainHelper
  def body_id
    if AppUtils.facebook_env?
      "facebook-body"
    end
  end
end
