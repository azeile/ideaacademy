module InternalHelper
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def is_active?(action)
    "active" if send(:"#{action}_internal_url") == request.url
  end

  def facebook_app_link
    FACEBOOK_CONF[:omni_auth_full_host_path]
  end
end
