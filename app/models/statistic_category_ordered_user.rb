# == Schema Information
#
# Table name: statistic_category_ordered_users
#
#  id           :integer          not null, primary key
#  category_id  :integer
#  order_id     :integer          default(0)
#  user_id      :integer
#  total_points :integer
#  level        :integer
#  created_at   :datetime
#  updated_at   :datetime
#  vote_points  :integer
#  idea_points  :integer
#

class StatisticCategoryOrderedUser < ActiveRecord::Base

  #Relations
  belongs_to :category
  belongs_to :user
  belongs_to :badge, :foreign_key => "level"
  
  #Validations
  # => Relations
  validates :category_id, :presence => true
  validates :user_id, :presence => true
  
  CLOSEST_ARRAY = [-3,-2,-1,1,2,3]
    
  class << self
    def update_statistic
      @categories = Category.find(:all)
      @categories.each do |category|
        new_order = 0
        clear_statistic_by_category_id category.id
        @statistics = StatisticCategoryUser.list_by_category_id( category.id ).order( 'total_points DESC' )
        self.transaction do
          @statistics.each do |record|
            new_order = new_order + 1
            ordered_record = self.new
            ordered_record.order_id = new_order
            ordered_record.user_id = record.user_id
            ordered_record.category_id = record.category_id
            ordered_record.level = record.level
            ordered_record.total_points = record.total_points
            ordered_record.vote_points = record.vote_points
            ordered_record.idea_points = record.idea_points
            ordered_record.save
          end
        end
      end

      def update_statistics category, user

      end     
    end
    
    def clear_statistic_by_category_id category_id
      self.delete_all(:category_id => category_id)
    end
    
    def get_statistics category, user, page
      page = 1 unless page

      limit = 20

      pre_offset = limit.to_i * page.to_i

      unless page == 0
        offset = pre_offset.to_i - limit.to_i
      else
        offset = 0
      end

      @current_user = user
      statistics = self.find(:all, :conditions => "category_id = #{category.id} and total_points > 0 ", :order => "total_points DESC", :limit => limit, :offset => offset )

      total_players = self.find(:all, :conditions => "category_id = #{category.id} and total_points > 0 ", :order => "total_points DESC").count

      prepare_json statistics, total_players, offset
    end
    
    protected
      def prepare_json statistics, total_players, offset
        @players = []
        
        offset = 0 unless offset

        show_friends = false
        show_more = false
        me = false

        statistics.each_with_index do |statistic, index|
          unless statistic.user.nil?
            friend = @current_user ? ( @current_user.user_friends.where(:friend_id => statistic.user_id).empty? ? false : true) : false
            me = index if me?(statistic.user)
            show_friends = true if show_friends == false && friend

            order = offset + index + 1

            @players << {
              :order => order,
              :first_name => statistic.user.first_name,
              :last_name => statistic.user.last_name,
              :points => statistic.total_points,
              :vote_points => statistic.vote_points.nil? ? 0 : statistic.vote_points,
              :idea_points => statistic.idea_points.nil? ? 0 : statistic.idea_points,
              :image => statistic.user.profile_image,
              :badge_image => statistic.badge.image, 
              :friend => friend,
              :me => me?( statistic.user ),
              :close_friend => false, 
              :close_user => false,
              :show => me?(statistic.user) || [1,2,3].include?( statistic.order_id ) ? true : false,
              :user_id => statistic.user.id
            }
          end
        end
        set_closest_friends(me) if show_friends
        set_closest_users(me)

        if @players.any?
          status = true
          message = 'statistics_loaded'
        else
          status = false
          message = I18n.t("statuses.statistics_not_found")
        end

        { :status => status, :message => message, :show_friends => show_friends, :show_more => @players.size > 3, :total_players => total_players, :players => @players }
      end
      
    private
      def me? user
        return false unless @current_user
        @current_user == user
      end

      def set_closest_friends me
        return unless me
        friend_statistics = []      
        orders = []
        me_in_friends = false

        @players.each do |statistic|
          friend_statistics << statistic if statistic[:friend] || statistic[:me]
        end

        friend_statistics.each_with_index do |statistic, index|
          me_in_friends = index if statistic[:me]
        end

        places = closest me_in_friends
        friend_statistics.each_with_index do |statistic, index|
          orders << statistic[:order] if places.include?( index )
        end

        @players.each do |statistic|
          statistic[:close_friend] = true if orders.include?( statistic[:order] )
        end

      end

      def set_closest_users me
        return unless me
        places = closest me      
        @players.each_with_index do |statistic, index|
          statistic[:close_user] = true if places.include?( index )
        end
      end

      def closest me
        places = []
        CLOSEST_ARRAY.each do |closest|
          place = me + closest
          places << place if place >= 0 
        end
        places
      end
  end
  
  
  
end
