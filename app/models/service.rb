# == Schema Information
#
# Table name: services
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  price_in_cents     :integer
#  currency           :string(255)
#  duration           :integer
#  duration_units     :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  code               :string(255)
#  description        :text
#  local_service_flag :boolean
#  user_limit         :integer          default(0)
#  crowdsourcing_flag :boolean          default(FALSE)
#  unique_design_flag :boolean          default(FALSE)
#  intranet_flag      :boolean          default(FALSE)
#

class Service < ActiveRecord::Base
  DURATIONS_UNITS           = ["hours", "days", "months"]
  UNLIMITED_IDEAS_SERVICES  = [ 
                                "VIP_ACCOUNT_ANNUAL", 
                                "VIP_ACCOUNT_MONTHLY", 
                                "MONTH_SUBSCRIPTION", 
                                "WEEK_SUBSCRIPTION", 
                                "DAYNIGHT_SUBSCRIPTION", 
                                "VIP_ACCOUNT_LIFELONG" 
                              ]
  
  has_many :subscriptions
  has_many :service_provider_ids
  
  validates_presence_of :name, :code, :duration, :duration_units
  validates_inclusion_of :duration_units, :in => DURATIONS_UNITS
  validates_numericality_of :duration
  validates_uniqueness_of :code
  
  accepts_nested_attributes_for :service_provider_ids
  
  after_initialize :set_defaults
  
  scope :by_provider, lambda {|provider| joins(:service_provider_ids).where("service_provider_ids.service_provider_name = ? AND environment = ?", provider, Rails.env)}
  scope :by_provider_service_id, lambda {|provider, service_id| by_provider(provider).where("service_provider_ids.service_social_network_id = ?", service_id.to_s)}

  scope :internal,      where("local_service_flag = ?", true)
  scope :organization,  lambda { internal.where("code like 'ORG%'") }
  scope :not_internal,  where("local_service_flag = ?", false)
  scope :vips, where("code LIKE 'VIP%'")

  scope :not_internal_vips, lambda { not_internal.vips }
  scope :not_internal_none_vips, lambda { not_internal.vips }

  monetize :price_in_cents, :as => :price
  
  class << self
    def subscribe service_code, subscriber
      Service.find_by_code(service_code).subscribe subscriber
    end
  end
  
  def period
    duration.send(duration_units)
  end

  def subscribe subscriber
    Subscription.transaction do
      begin
        subscr    = subscriber.subscriptions.active.by_service_code(code).active.first
         
        subscribe_from = if subscr.nil?
          ( duration_units == 'months' ) || ( duration_units == 'days' ) ? Date.today.to_datetime : DateTime.now
        else
          subscr.date_to
        end

        subscr = Subscription.create!({:service     => self, 
                                       :subscriber  => subscriber, 
                                       :date_from   => subscribe_from, 
                                       :date_to     => subscribe_from + period
                                    })
      rescue ActiveRecord::Rollback
        errors.add_to_base I18n.t("activerecord.errors.models.service.general")
        return false
      end
    end    
  end

  def price_in currency
    currency = "USD" unless currency
       
    if currency.to_s.downcase.to_sym == :fb_credits
      price.cents / 10
    else
      price.exchange_to(currency).dollars
    end
  end
  
  def default_price
    price_in :usd
  end
  
  def default_currency
    case
      when AppUtils.facebook_env? then "USD"
      else currency
    end
  end

  def default_name
    I18n.t "services.names.#{code.downcase}"
  end

  def paypal_url invoice, return_url
    query_values = {
      :business => "arvis.zeile-facilitator@gmail.com",
      :cmd => "_xclick",
      :upload => 1,
      :return => return_url,
      :invoice => invoice
    }
    
    query_values.merge!({
      "amount" => price,
      "item_name" => name,
      "item_number" => id,
      "quantity" => 1
    })
    
    "https://www.sandbox.paypal.com/cgi-bin/webscr?" + query_values.to_query
  end
  
  def as_json options={}
    {
      :price      => default_price,
      :code       => code,
      :name       => default_name,
      :currency   => default_currency,
      :service_id => id,
      :payment_gateway_url => "/pay-with-paypal/#{id}"
    }
  end

  def as_json_for_fb
    {
      :price => price_in(:fb_credits),
      :title => default_name,
      :description => default_name
    }
  end
  
  def draugiem_id
    service_provider_ids.find_by_service_provider_name("draugiem").service_social_network_id unless local?
  end
  
  def local?
    local_service_flag
  end

  def crowdsourcing?; crowdsourcing_flag; end
  def unique_design?; unique_design_flag; end
  def intranet?; intranet_flag; end
    
  private
  
    def set_defaults
      self.local_service_flag = true if id.nil? && local_service_flag.nil?
    end
end
