# == Schema Information
#
# Table name: ideas
#
#  id                 :integer          not null, primary key
#  contest_id         :integer
#  user_id            :integer
#  typekey            :string(255)
#  text               :text
#  video_info         :string(255)
#  video_image        :string(255)
#  duels_won          :integer          default(0)
#  duels_in           :integer          default(0)
#  is_deleted         :boolean          default(FALSE)
#  created_at         :datetime
#  updated_at         :datetime
#  room_id            :integer
#  category_id        :integer
#  in_topthree        :boolean
#  core_text          :string(255)
#  add_points         :boolean          default(FALSE)
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  admin_approved     :boolean
#  is_portfolio_idea  :boolean
#

class Idea < ActiveRecord::Base
  TYPES = %w(text image video)
   
  #Relations
  belongs_to :user
  belongs_to :contest
  belongs_to :room
  belongs_to :category
  has_many :user_idea_rates
  has_many :spam_reports
  has_one :organization, :through => :category

  has_attached_file :image,
    :styles => { :medium => "270x200#", :normal => "560x340^" }
    
  #Validations
  validates :user_id, :presence => true
  validates :contest_id, :presence => true
  validates :category_id, :presence => true
  validates :text, :presence => true, :length => { :minimum => 1, :maximum => 140 }, :unless => :image?
  validates :typekey, :presence => true, :inclusion => {:in => TYPES, :message => "is not valid type"}
  validates :core_text, :presence => true, :unless => :image?
  #validates_attachment_size :image, :less_than => 3.megabytes
  #validates_attachment_content_type :image, :content_type => ["image/jpeg", "image/jpg", "image/png"]
  validate :number_of_portfolio_ideas, :on => :update
    
  #Scopes
  scope :recent, lambda { |limit = 10| order("ideas.created_at DESC").limit(limit) }
  scope :top, lambda { order("duels_won DESC, duels_in") }
  scope :with_user, { :joins => :user }
  scope :users_best_ideas, lambda { |user_id| where(:user_id => user_id).order("duels_won DESC").limit(10) }
  scope :best_top_ideas, lambda { |contest_id, top = 3| where(:contest_id => contest_id).order("duels_won DESC, duels_in").limit(top) }
  scope :contest_user_ideas, lambda { |user_id, contest_id, room_id| where({ :user_id => user_id, :contest_id => contest_id, :room_id => room_id }).order("duels_won DESC") }
  scope :not_deleted, where(:is_deleted => false)
  scope :points_giving_ideas_for_user, lambda { |user_id, contest_id| where(:contest_id => contest_id, :user_id => user_id, :add_points => true).order("duels_won") }

  scope :list_my_by_contest_id, lambda {|user_id, contest_id| where( :contest_id => contest_id, :user_id => user_id )}
  scope :list_by_contest_id, lambda {|contest_id| where( :contest_id => contest_id )}
  scope :list_by_contest_id_and_room_id, lambda {|contest_id, room_id| where( :contest_id => contest_id, :room_id => room_id )}
  scope :list_by_contest_id_and_room_id_with_offset, lambda {|contest_id, room_id, offset| 
    where( :contest_id => contest_id, :room_id => room_id ).order('duels_won desc, id asc').limit(10).offset(offset || 0)
  }
  scope :list_by_contest_id_and_room_id_with_users, lambda { |contest_id, room_id, offset| 
    list_by_contest_id_and_room_id_with_offset(contest_id, room_id, offset).includes(:user) 
  }
  scope :order_by_duels_won, order("duels_won DESC, duels_in, add_points DESC")
  scope :today_added_by_user, lambda { |user_id| where("user_id = ? AND created_at > ? ", user_id, Date.today.to_datetime) }
  scope :rarest, order("duels_in ASC")
  scope :portfolio, where(:is_portfolio_idea => true)

  #Callbacks
  before_validation :set_core_text
  validate :similar_idea_text, :valid_youtube_link
  before_create :set_defaults, :set_add_points, :set_video_info
  after_create :add_user_to_room, :add_to_log
  
  def exceeded_spam_limit?
    spam_reports_count > ApplicationSettings["SPAM_FILTER_TRASHOLD"]
  end
  
  def spam_reports_count
    spam_reports.count
  end

  def logo_image
    image.url(:medium)
  end

  def similar?
    exept = self.new_record? ? "" : " AND id <> #{id}" 
    ideas = Idea.where("core_text = '#{core_text}' AND contest_id = #{contest_id}#{exept}")
    !ideas.empty?
  end
  
  def update_duels_in_plus_one
    self.duels_in = duels_in + 1
    self.save
  end

  def update_duels_in_and_duels_won_plus_one
    self.duels_in = duels_in + 1
    self.duels_won = duels_won + 1
    self.save
    
    update_add_points
  end 
  
  def current_users current_user
    user == current_user
  end 
  
  def update_add_points
    return true if add_points
    points_giving_ideas = Idea.points_giving_ideas_for_user( user_id, contest_id )
    if points_giving_ideas.size >= IDEA_LIMIT_FOR_POINTS
      if self.duels_won > points_giving_ideas.first.duels_won
        set_and_save_add_points true
        points_giving_ideas.first.set_and_save_add_points false
        return true
      end
    else
      set_and_save_add_points true
      return true
    end
    false
  end

  def for_contest_results
    {
      :crowdsourced_contest_id  => contest.origin_id,
      :text                     => image? ? image.url : text,
      :wins                     => duels_won,
      :total_duels              => duels_in,
      :points                   => add_points == false ? 0 : duels_won * POINTS_IDEA,
      :author                   => user.to_s,
      :author_image_url         => user.profile_image,
      :ratio                    => ratio.to_f.round(2)
    }
  end

  def as_json options={}
    if !options.nil? && options[:all_attributes] == true
      {
        :id => id,
        :text => image? ? image.url(:medium) : text,
        :duels_won => duels_won,
        :duels_in => duels_in,
        :points => add_points == false ? 0 : duels_won * POINTS_IDEA,
        :prefix => user.title_prefix(user.level_for_category self.category),
        :first_name => user.first_name,
        :last_name => user.last_name,
        :show_idea => true,
        :image_url => user.profile_image,
        :typekey => typekey,
        :video_info => video_info,
        :video_image => video_image,
        :image => image? ? image.url(:normal) : ""
      }
    elsif options[:profile_ideas] == true
      {
        :id => id,
        :text => image? ? image.url(:medium) : text,
        :contest_id => self.contest.id,
        :contest_title => self.contest.title,
        :is_portfolio_idea => self.is_portfolio_idea?,
        :title => self.contest.title,
        :typekey => typekey,
        :video_info => video_info,
        :video_image => video_image
      }
    else
      {
        :id => id,
        :text => image? ? image.url(:medium) : text,
        :image => image? ? image.url(:normal) : "",
        :video_info => video_info,
        :video_image => video_image, 
        :typekey => typekey 
      }
    end
  end

  def text?
    typekey == "text"
  end

  def image?
    typekey == "image"
  end

  def video?
    typekey == "video"
  end
   
  class << self
    def ideas_for_vote user, room
      user = User.default_guest_user unless user
      joins("LEFT JOIN user_idea_rates ON user_idea_rates.idea_id = ideas.id AND user_idea_rates.user_id = #{user.id}")
      .where("((ideas.duels_in < #{ApplicationSetting["ELIMINATE_CHECK"]}) OR (ideas.duels_in >= #{ApplicationSetting["ELIMINATE_CHECK"]} AND ideas.duels_in < #{ApplicationSetting["ELIMINATE_CHECK_LEVEL_2"]} AND ideas.duels_won > #{ApplicationSetting["ELIMINATE_MIN"]})" +
                        "OR (ideas.duels_in >= #{ApplicationSetting["ELIMINATE_CHECK_LEVEL_2"]} AND ideas.duels_in < #{ApplicationSetting["ELIMINATE_CHECK_LEVEL_3"]} AND ideas.duels_won > #{ApplicationSetting["ELIMINATE_MIN_LEVEL_2"]})" +
                        "OR (ideas.duels_in >= #{ApplicationSetting["ELIMINATE_CHECK_LEVEL_3"]} AND ideas.duels_won > #{ApplicationSetting["ELIMINATE_MIN_LEVEL_3"]})" +
                        ")" +
                        "AND (user_idea_rates.rate_count < #{ApplicationSetting["IDEA_SHOW_COUNT"]} OR user_idea_rates.rate_count IS NULL) " + 
                        "AND (ideas.is_deleted = false) AND (ideas.user_id <> #{user.id}) AND (ideas.room_id = #{room.id}) " +
                        "AND ( coalesce(ideas.admin_approved, false) OR (SELECT COUNT(1) FROM spam_reports sr WHERE sr.idea_id = ideas.id ) < #{ApplicationSetting['SPAM_FILTER_TRASHOLD']})"
      )
    end
    
    def as_crowdsourcing_results
      select("ideas.*, ( CAST(ideas.duels_won as FLOAT ) / CAST(ideas.duels_in as FLOAT) ) as ratio")
      .where("ideas.duels_in > ?", ApplicationSetting["CROWDSOURCING_RESULTS_TRESHHOLD"])
      .order("ratio DESC")
    end 
    
    def my_best_ideas user, contest, room
      ideas = []

      my_ideas = Idea.contest_user_ideas user.id, contest.id, room.id
      my_ideas.each do |idea|
        idea = {
          :id => idea.id,
          :text => idea.text,
          :duels_won => idea.duels_won,
          :duels_in => idea.duels_in,
          :points => idea.add_points == false ? 0 : idea.duels_won * POINTS_IDEA,
          :first_name => user.first_name,
          :last_name => user.last_name,
          :image_url => user.profile_image
        }
        ideas << idea
      end
      return ideas
    end

    def ideas_without_authors contest_id, room_id, offset
      list_by_contest_id_and_room_id_with_offset(contest_id, room_id, offset).reorder("id DESC").to_a.map do |idea|
        {
          :id => idea.id,
          :text => idea.text,
          :video_info => idea.video_info,
          :video_image => idea.video_image,
          :image => idea.image? ? idea.image.url(:normal) : "",
          :typekey => idea.typekey
        }
      end
    end
    
    def authors_without_ideas contest_id, room_id, offset
      list_by_contest_id_and_room_id_with_users(contest_id, room_id, offset).to_a.map do |idea|
        defaults = if idea.contest.done?
          {
            :text => idea.text,
            :video_info => idea.video_info,
            :video_image => idea.video_image,
            :image => idea.image? ? idea.image.url(:normal) : ""
          }
        else
          {}
        end
        
        defaults.merge({
          :id => idea.id,
          :duels_won => idea.duels_won,
          :duels_in => idea.duels_in,
          :points => idea.add_points == false ? 0 : idea.duels_won * POINTS_IDEA,
          :prefix => idea.user && idea.user.title_prefix(idea.user.level_for_category idea.category),
          :first_name => idea.user && idea.user.first_name,
          :last_name => idea.user && idea.user.last_name,
          :image_url => idea.user && idea.user.profile_image,
          :typekey => idea.typekey,
          :show_idea => idea.contest.done?
        })
      end
    end
  end

  def to_s
    self.text
  end
  
  protected
    def id_from_youtube_link link
      link[/v=([^&]+)/, 1]
    end
    
    def set_defaults
      self.in_topthree = false
      return true
    end
    
    def set_video_info
      return unless video? 
      id = id_from_youtube_link self.text
      self.video_info = id
      self.video_image = "http://img.youtube.com/vi/#{id}/0.jpg"
    end
  
    def add_to_log
      LogActivity.new_idea self
    end    
  
    def set_core_text
      return if self.text == nil || self.text.empty?
      if video?
        self.core_text = id_from_youtube_link self.text
      else
        self.core_text = self.text.parameterize
      end
    end
    
    def add_user_to_room 
      contest.add_user_to_room( user, room_id )
    end
    
    def set_add_points
      points_giving_ideas_count = Idea.points_giving_ideas_for_user( user_id, contest_id ).size
      self.add_points = points_giving_ideas_count < IDEA_LIMIT_FOR_POINTS ? true : false
      true
    end
    
    def set_and_save_add_points val
      self.add_points = val
      self.save
    end
  
    def similar_idea_text
      return if image?
      return if self.typekey == nil || !self.typekey == "text"
      errors.add(:text, I18n.t("statuses.similar_text_error")) if similar?
    end
    
    def valid_youtube_link
      return unless video?
      begin
        uri = URI.parse(self.text)
        if uri.host == "www.youtube.com" && uri.path == "/watch" && self.core_text.size > 0
          uri = URI.parse("http://gdata.youtube.com/feeds/api/videos/#{core_text}")
          http = Net::HTTP.new(uri.host, uri.port)
          res = http.get(uri.request_uri)
          return if res.body != "Invalid id" && res.body != "Video not found"
          errors.add(:text, I18n.t("statuses.wrong_youtube_video"))          
        else
          errors.add(:text, I18n.t("statuses.wrong_youtube_address"))
        end
      rescue
        errors.add(:text, I18n.t("statuses.wrong_youtube_address"))
      end
    end

    def number_of_portfolio_ideas
      errors.add(:base, "Can have at most 10 portfolio ideas") if self.is_portfolio_idea && self.user.ideas.portfolio.count >= 10
    end
end
