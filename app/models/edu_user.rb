module EduUser

  TYPES = %w[teacher parent student]

  def edu_user?
    organizations.first.school?
  end

end
