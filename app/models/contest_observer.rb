class ContestObserver < ActiveRecord::Observer
  
  def after_save(model)
    if model.active_flag_changed? && model.status == "live"
      NotificationMailer.delay.contest_new_notification(model)
    end
  end
end
