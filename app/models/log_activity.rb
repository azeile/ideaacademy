# == Schema Information
#
# Table name: log_activities
#
#  id          :integer          not null, primary key
#  contest_id  :integer
#  category_id :integer
#  room_id     :integer
#  user_id     :integer
#  author_id   :integer
#  typekey     :string(255)
#  points      :integer
#  status      :integer
#  created_at  :datetime
#  updated_at  :datetime
#  idea_id     :integer
#  points_gave :integer          default(0)
#

class LogActivity < ActiveRecord::Base
  
  #Relations
  belongs_to :contest
  belongs_to :category
  belongs_to :room
  belongs_to :user
  belongs_to :author, :class_name => "User"

  #Validations
  # => Relations
  validates :typekey, :presence => true
  validates :status, :presence => true

  #Scopes
  scope :list_by_user_id, lambda { |user_id| where(:user_id => user_id).order('created_at DESC').limit(3) }
  scope :list_by_user_id_for_update, lambda { |user_id, last_update| where("user_id = ? AND created_at > ?", user_id, last_update) }
  scope :list_older_by_date, lambda { |user_id, last_update| where("user_id = ? AND created_at < ?", user_id, last_update) }
  #scope :list_by_user_id_for_update, lambda { |user_id, last_update, now| where("user_id = ? AND created_at > ? AND created_at < ?", user_id, last_update, now).order('created_at DESC') }

  ADD_POINTS_VOTE = 'points_vote'
  ADD_POINTS_IDEA = 'points_idea'
  ADD_NEW_IDEA = 'new_idea'
  
  ADD_POINTS_VOTE_TEXT = 'points_vote'
  ADD_POINTS_IDEA_TEXT = 'points_idea'
  ADD_NEW_IDEA_TEXT = 'new_idea'

  class << self
    def friends_in_category user, category_id
      friends = user.user_friends
      friend_ids = friends.collect {|friend| friend.friend_id }
      return 0 if friend_ids.empty?
      return self.find(:all, :select => 'DISTINCT user_id', :conditions => {:user_id => friend_ids, :category_id => category_id }).size
    end
    
    def user_voted user, idea
      record = self.new
      record.category_id = idea.contest.category_id
      record.contest_id = idea.contest.id
      record.room_id = idea.room_id
      record.idea_id = idea.id
      record.user_id = user.id 
      record.typekey = LogActivity::ADD_POINTS_VOTE
      record.status = LogActivity::ADD_POINTS_VOTE_TEXT
      record.author_id = idea.user_id
      record.points = POINTS_VOTE
      record.points_gave = idea.add_points ? POINTS_IDEA : 0
      record.save
      
      record
    end

    def user_idea_voted user, idea
      record = self.new
      record.category_id = idea.contest.category_id
      record.contest_id = idea.contest.id
      record.room_id = idea.room_id
      record.idea_id = idea.id
      record.user_id = idea.user_id
      record.typekey = LogActivity::ADD_POINTS_IDEA
      record.status = LogActivity::ADD_POINTS_IDEA_TEXT
      record.author_id = user.id
      record.points = idea.add_points ? POINTS_IDEA : 0
      record.points_gave = POINTS_VOTE
      record.save  
      
      record    
    end 
    
    def new_idea idea
      record = self.new
      record.category_id = idea.contest.category_id
      record.contest_id = idea.contest.id
      record.room_id = idea.room_id
      record.idea_id = idea.id
      record.user_id = idea.user_id
      record.typekey = LogActivity::ADD_NEW_IDEA
      record.status = LogActivity::ADD_NEW_IDEA_TEXT
      record.save
      
      record
    end
    
  end

  def image_url
    case typekey
    when LogActivity::ADD_POINTS_VOTE
      author.profile_image
    when LogActivity::ADD_POINTS_IDEA
      author.profile_image
    when LogActivity::ADD_NEW_IDEA
      contest.smaller_logo.url(:thumbnail) if contest.smaller_logo?
    end
  end
  
  def as_json(options={})
    {
      :typekey => self.typekey,
      :timestamp => self.created_at.to_i,
      :message => I18n.t("facebook.log_typekeys.#{self.typekey}", :author => self.author.nil? ? nil : "<a href='/#/users/#{self.author.id}'>#{self.author.title_prefix(self.author.level_for_category self.category)}#{self.author.first_name} #{self.author.last_name}</a>", :contest => self.contest ? "<a href='/#/contest/#{contest.id}'>#{contest.title}</a>" : nil),
      :points => self.typekey == "points_vote" ? self.points_gave : self.points,
      :points_vote_message => self.typekey == "points_vote" ? I18n.t("log_typekeys.points_vote.you", default: ", you get <strong>+%{points}</strong>", points: self.points) : "",
      :image => image_url,
      :category => category.name
    }
  end  
end
