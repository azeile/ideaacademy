# == Schema Information
#
# Table name: tmp_statistic_category_users
#
#  id           :integer          not null, primary key
#  category_id  :integer
#  tmp_user_id  :integer
#  total_points :integer
#  live_points  :integer
#  level        :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class TmpStatisticCategoryUser < ActiveRecord::Base
  #Relations
  belongs_to :category
  belongs_to :tmp_user
  belongs_to :badge, :foreign_key => "level"

  #Validations
  # => Relations
  validates :category_id, :presence => true
  validates :tmp_user_id, :presence => true

  #Scopes 
  scope :list_by_tmp_user_id, lambda { |tmp_user_id| where(:tmp_user_id => tmp_user_id).order("category_id") }
  scope :list_by_category_id, lambda { |category_id| where(:category_id => category_id) }
  #Methods
 
  def add_points_vote
    current_total_points = total_points.nil? ? 0 : total_points
    current_live_points = live_points.nil? ? 0 : live_points

    self.total_points = current_total_points + POINTS_VOTE
    self.save

    # Points difference, for header
    self.live_points = current_live_points + (self.total_points - current_total_points)
    self.save
    
    update_user_level
  end
  
  def update_user_level    
    current_badges = Badge.current_level( total_points )
    unless badge == current_badges.first
      self.level = current_badges.first.id
      self.save
    end
  end

  def as_json(options={})
    {
      :id => self.category.id, 
      :name => I18n.t("categories.#{self.category.name}"), 
      :total_points => self.total_points.nil? ? 0 : self.total_points,
      :level => self.badge.nil? ? 0 : self.badge.image,
      :percents => self.badge.nil? ? 0 : self.badge.percents(self.total_points)
    }
  end
   
end
