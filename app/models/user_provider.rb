# encoding: UTF-8
# == Schema Information
#
# Table name: user_providers
#
#  id                   :integer          not null, primary key
#  user_id              :integer
#  social_id            :integer
#  name                 :string(255)
#  token                :string(255)
#  secret               :string(255)
#  created_at           :datetime
#  updated_at           :datetime
#  originated_from_flag :boolean
#

class UserProvider < ActiveRecord::Base

  #Relations
  belongs_to :user
  belongs_to :origin_user, :class_name => "User"

  #Validations
  # => Attributes
  #validates :social_id, :presence => { :if => proc {|up| AppUtils.facebook_env? } }
  validates :name, :presence => true
  #validates :token, :presence => { :if => proc {|up| AppUtils.facebook_env? } }
  
  scope :first_by_name, lambda {|name| where(:name => name).limit(1) }
  
  before_create :set_defaults

  def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
    
    data = access_token.extra.raw_info
    email = access_token.info.email.gsub("\u0000","@")

    if user = User.where(:email => email ).first
      user
    elsif user = User.where("fb_id IS NOT NULL").where(:fb_id => access_token.extra.raw_info.id).first
        user.update_attribute(:email, email)
        user.save
        user
    else # Create a user with a stub password. 
      pass = Devise.friendly_token[0,20]
      user = User.new(:fb_id => access_token.info.id, :first_name => access_token.info.first_name, :last_name => access_token.info.last_name, :email => email, :password => pass, :password_confirmation => pass, :image => access_token.info.image.gsub("=square","=large"), :user_type => 'user')
      #user.organization_users_attributes = Organization.find_by_name("IdeaAcademy").attributes
      user.save!
      user
    end
  end
  
  class << self
    def create_or_get_by_facebook fb_user, user = nil
      user_provider      = find_or_initialize_by_social_id_and_name( :social_id => fb_user[:uid].to_i, :name => "facebook", :token => fb_user[:credentials][:token] )
      user_provider.user = User.create_fb_user({
                            :user_provider => user_provider, 
                            :first_name    => fb_user[:info][:first_name], 
                            :last_name     => fb_user[:info][:last_name], 
                            :image         => fb_user[:info][:image].gsub("=square","=large"),
                            :gender        => fb_user[:extra][:raw_info][:gender],
                            :user          => user })

      user_provider.token = fb_user[:credentials][:token]
      user_provider.save!
      
      return user_provider.user
    end
    
    def create_with_devise params
      user = User.new params
      user.providers.build([{:name => "devise", :user => user}])
      if AppUtils.internal_app_env?
        user.build_admin_user(:email => user.email, :password => user.password)
        user.save
      else
        user.save
        user.set_default_organization Organization.find_by_name("IdeaAcademy")
        UserPopup.create!({ :user_id => user.id, :typekey => "welcome_user", :seen => false })
      end
      user
    end
  end
  
  private
  
  def set_defaults
    originated_from_flag = user.providers.empty? || user.providers.first.id.nil?
    true
  end
end
