# == Schema Information
#
# Table name: tmp_room_users
#
#  id          :integer          not null, primary key
#  contest_id  :integer
#  room_id     :integer
#  tmp_user_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class TmpRoomUser < ActiveRecord::Base
  #Relations
  belongs_to :contest
  belongs_to :tmp_user
  belongs_to :room

  #Validates
  # => Relations
  validates :contest_id, :presence => true
  validates :tmp_user_id, :presence => true
  validates :room_id, :presence => true
  
  scope :by_category, lambda { |category_id, tmp_user_id| joins(:contest).where('contests.category_id = ?',category_id).where("tmp_room_users.tmp_user_id = ?", tmp_user_id).order("room_users.created_at desc") }
  
end
