# == Schema Information
#
# Table name: sidebar_messages
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  message         :text
#  active          :boolean          default(TRUE)
#  created_at      :datetime
#  updated_at      :datetime
#  organization_id :integer
#

class SidebarMessage < ActiveRecord::Base

  # Relations
  has_many :sidebar_messages_views
  has_many :viewers, :through => :sidebar_messages_views, :source => :user
  belongs_to :organization

  class << self
    def for_user user, organization 
      where("active = ? AND id NOT IN (?) AND organization_id = ? ", true, user.seen_sidebars_ids, organization.id).first
    end

    def for_user_organization organization
      organization ? where("active = ? AND organization_id = ?", true, organization.id).first : nil
    end
  end
end
