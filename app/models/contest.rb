require 'open-uri'
class Contest < ActiveRecord::Base

  extend ActiveSupport::Memoizable
  include ActionView::Helpers::DateHelper
  include ManagesRooms
  attr_accessor :contest_image_id, :logo_url, :smaller_logo_url

# <Relations>
  belongs_to :category, :counter_cache => :contests_count
  has_one :organization, :through => :category
  has_one :contest_image, :as => :imageable, :dependent => :destroy
  has_many :rooms, :dependent => :destroy
  has_many :room_users, :dependent => :destroy
  has_many :ideas, :dependent => :destroy
  has_many :best_ideas, :class_name => "Idea", :order => "duels_won DESC, duels_in"
  has_many :statistic_contest_room_users, :dependent => :destroy
  has_many :statistic_contest_users, :dependent => :destroy
  has_one  :contest_request, :as => :parent

  has_attached_file :logo,
    :styles => { :thumbnail => "390x175#" }

  has_attached_file :smaller_logo,
    :styles => { :thumbnail => "75x75#" }

  def edu_access=(roles)
    self.edu_access_mask = (roles & EduUser::TYPES).map { |r| 2**EduUser::TYPES.index(r) }.sum
  end

  def edu_access
    EduUser::TYPES.reject { |r| ((edu_access_mask || 0) & 2**EduUser::TYPES.index(r)).zero? }
  end

# </Relations>
    
# <Validations>
  validates :category_id, :presence => true
  validates_attachment_size :logo, :less_than => 5.megabytes
  validates_attachment_content_type :logo, :content_type => ["image/jpeg", "image/jpg", "image/png"]
  
  validates :title, :presence => true, :length => { :minimum => 3, :maximum => 100 }
  validates :description, :presence => true
  validates :award, :presence => { :if  => Proc.new { |user| AppUtils.facebook_env? } }
  validates :typekey, :presence => true
  validates :ends_at, :presence => true
  validate  :correct_ends_at
  validates :language, :presence => { :if  => Proc.new { |user| AppUtils.facebook_env? } }, :inclusion => { :in => LanguageList::IDEA_ACADEMY }
# </Validations>
  
# <Scopes>
  scope :recent, lambda { |limit = 10| order("contests.created_at DESC").limit(limit) }
  scope :done, where("ends_at <= NOW()")
  scope :live, where("ends_at > NOW()")
  scope :crowdsourced, where("origin_id IS NOT NULL")
  #used to show ended contests
  scope :daynight_after_ends_at, where("ends_at > (NOW() - INTERVAL '24 HOURS')")
  scope :hour_after_ends_at, where("ends_at > (NOW() - INTERVAL '1 HOURS')")
  scope :without_drafts, where("contests.active_flag = ?", true)

  scope :by_category, lambda {|category|
    where("category_id = ?", category.id)
  }
  scope :with_category, { :joins => :category }
  scope :with_active_category, lambda { with_category.where("categories.active_flag = ?", true) }
  scope :belongs_to_active_organization_category, lambda { |organization| 
    with_active_category ? with_active_category.where("categories.organization_id = ? ", organization.id) : []
  }
  scope :with_room_users, { :joins => :room_users }
  scope :with_room_users_for_current_user, lambda { |user| 
    joins("LEFT JOIN room_users ON room_users.contest_id = contests.id AND room_users.user_id = #{user.id}") 
  }

  scope :without_statistics,
    where("NOT EXISTS (SELECT 1 FROM statistic_contest_users scu WHERE scu.contest_id = contests.id)")
    .where("EXISTS (SELECT 1 FROM statistic_contest_room_users scru WHERE scru.contest_id = contests.id)")

  scope :user_language_exists, lambda {|user|
    where("EXISTS (SELECT 1 FROM user_languages ul where user_id = ? AND ul.language = contests.language)", user.id)
  }

  scope :for_user_organization , lambda { |organization|
    belongs_to_active_organization_category(organization)
    .without_drafts
    .order("id desc")
  }

  scope :for_user_organization_with_languages , lambda { |organization, user|
    for_user_organization(organization)
    .user_language_exists(user)  
  }

  scope :with_edu_access, lambda { |user|
    
    (user && user.edu_user?) ? {:conditions => "edu_access_mask & #{2**EduUser::TYPES.index(user.user_type.to_s)} > 0 "} : {}
  }

  # => For user
  scope :for_user, lambda { |user, organization| 
    with_room_users_for_current_user(user)
    .with_edu_access(user)
    .belongs_to_active_organization_category(organization)
    .without_drafts
    .hour_after_ends_at
    .order("id desc")
  }

  scope :for_user_with_laguages, lambda { |user, organization| 
    for_user(user, organization)
    .user_language_exists(user)
  }
  
  scope :for_user_by_category, lambda { |user, category| 
    # with_room_users_for_current_user(user)
    by_category( category )
    .without_drafts
    .hour_after_ends_at
    .order("id desc")
  }

  scope :for_user_by_category_with_languages, lambda { |user, category| 
    for_user_by_category(user, category)
    .user_language_exists(user)
  }
  
  scope :first_contest , lambda { |user, organization|
    for_user(user, organization).reorder("id desc").limit(1)
  }
  
  scope :last_contest , lambda { |user, organization|
    for_user(user, organization).reorder("id asc").limit(1)
  }
  
  scope :next_contest , lambda { |user, organization, current_id|
    for_user(user, organization).where("contests.id > ?", current_id).reorder("id asc").limit(1)
  }
  
  scope :previous_contest , lambda { |user, organization, current_id|
    if user.nil?
      where("contests.id < ?", current_id).reorder("id desc").limit(1)
    else
      for_user(user, organization).where("contests.id < ?", current_id).reorder("id desc").limit(1)
    end
  }

  scope :full_list_with_active_contests, lambda { |user, organization|
    belongs_to_active_organization_category(organization)
    .with_edu_access(user)
    .without_drafts
    .live
    .user_language_exists(user)
    .order("category_id ASC, contests.id desc")
  }
  
# </Scopes>

# <Filters>
  after_create :update_contest_image
  after_save :update_counter_cache
  after_destroy :update_counter_cache
  after_initialize :set_defaults

# </Filters>

  PAYOUT_STRUCTURE = [10, 7, 5, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

# <Class methods>
  class << self

    def data_for_json contest, user
      room_user = RoomUser.find_by_contest_id_and_user_id( contest.id, user ) if user

      {:contest_id => contest.id,
       :status => contest.status,
       :created_at => contest.created_at.strftime("%d/%m/%Y"),
       :title => contest.title,
       :im_in => user ? (room_user.nil? ? false : true) : false
      }
    end

    def for_category_as_json category, user
      contests_json = []

      contests = Contest.by_category(category).with_edu_access(user).live.reorder('created_at DESC')
      contests_ended = Contest.by_category(category).with_edu_access(user).done.reorder('created_at DESC').limit(5)

      contests += contests_ended
      contests.each do |contest|
        contests_json << data_for_json(contest, user)
      end

      contests_json
    end

    # => Root contest for user
    def root organization, user
      if AppUtils.facebook_env?
        for_user_organization_with_languages(organization, user)
      else
        for_user_organization(organization).with_edu_access(user)
      end.live.recent(1).first
    end
    
    def root_in_category category, user = nil
      if AppUtils.facebook_env?
        for_user_by_category_with_languages(user, category)
      else
        for_user_by_category(user, category).with_edu_access(user)
      end.first
    end

    def root_contests_in_categories user = nil, organization = nil
      Category.by_organization(organization.id).map do |category|
        if AppUtils.facebook_env?
          for_user_by_category_with_languages(user, category)
        else
          for_user_by_category(user, category)
        end
      end.flatten
    end

    def update_done_statistcs
      live_contests = self.live
      live_contests.each do |contest|
        contest.update_status
      end 
    end

    def create_from_crowdsourced_contest params
      contest = Contest.create(params)

      [:smaller_logo, :logo ].each do |img|
        contest.load_image_from_url img, params.delete("#{img}_url".to_sym)
      end

      contest.save!
    end

    def readable_by_organization org
      joins(:category).
        where(categories: {
          :organization_id => org.id
      })
    end
  end
# </Class methods>

# <Filter methods>
  def update_counter_cache
    self.category.contests_count = category.contests.live.count
    category.save
  end

  def update_contest_image
    return if self.contest_image_id.nil? || self.contest_image_id.empty?
    contest_image = ContestImage.find(contest_image_id)
    contest_image.contest = self
    contest_image.save
  end
# </Filter methods>

# <Instance methods>
  # => Status check?
  def status
    case
      when ends_at > DateTime.now then (active? ? "live" : "draft")
      when ends_at <= DateTime.now then "done"
    end
  end

  def done?
    status == 'done'
  end
  
  def live?
    status == "live"
  end
  
  def draft?
    status == "draft"
  end

  def active?
    active_flag
  end

  def is_money?
    is_money
  end

  # => Response with @contest from @contest(self)
  #
  # => Given data: @user = nil
  #
  # Get previous contest for @contest(self) in scope "for_user".
  # If @contest(self) is first then it returns last from list.
  def previous user = nil, organization = nil
    @previous ||= if AppUtils.facebook_env?
        Contest.previous_contest(user, organization, id).user_language_exists(user).first || Contest.last_contest(user, organization).user_language_exists(user).first
      else
        Contest.previous_contest(user, organization, id).first || Contest.last_contest(user, organization).first
      end
  end

  # => Response with @contest from @contest(self)
  #
  # => Given data: @user = nil
  #
  # Get next contest for @contest(self) in scope "for_user".
  # If @contest(self) is last then it returns first from list.
  def next user = nil, organization = nil
    current_id = id
    if user.nil?
      @next = Contest.where("contests.id > ?", current_id).reorder("id asc").limit(1).first || Contest.reorder("id desc").limit(1).first
    # if AppUtils.facebook_env?
    #   @next = Contest.next_contest(user, organization, id).user_language_exists(user).first || Contest.first_contest(user, organization).user_language_exists(user).first
    # else
  else
      @next = Contest.next_contest(user, organization, id).first || Contest.first_contest(user, organization).first
    end
  end

  #=> Methods to end contest
  def should_end?
    return true if Time.now >= ends_at
    return true if status == "done"
    return false
  end


  def update_status
    if should_end?
      attributes = { :status => "done" }
      self.update_attributes(attributes)

      if self.contest_request && self.contest_request.email
        ContestDoneNotification.contest_done(self, self.contest_request.email).deliver
      end
      
      return self.generate_end_statistic
    end
  end

  def generate_end_statistic
    if done?
      #Update best top three ideas in contest
      best_ideas = Idea.best_top_ideas(id)
      best_ideas.update_all("in_topthree = true")

      #Update statistics contest users table
      statistic_contest_room_users.each do |statistic|
        statistic_user = statistic_contest_users.find_or_initialize_by_contest_id_and_user_id( statistic.contest_id, statistic.user_id )
        statistic_user.vote_points = statistic.vote_points ? statistic.vote_points : 0
        statistic_user.idea_points = statistic.idea_points ? statistic.idea_points : 0
        statistic_user.total_points = statistic.total_points ? statistic.total_points : 0
        statistic_user.save
      end

      #ADD top5 cups to users
      statistic_users = statistic_contest_room_users.order('total_points DESC').limit(5)
      statistic_users.each do |statistic|
        statistic.user.top5_cups = statistic.user.top5_cups + 1
        statistic.user.save
      end
     
      # Send notification e-mail to owner

      # If contest is money generate end money -> should return array containing number of how many people received money and how much
      # => if not return status "done"
      if self.is_money?
        return self.generate_end_money
      else
        # DraugiemApiRequest.add_top3_activity_for(best_ideas)
        return "done"
      end
    end
  end

  def calculate_end_money_for_place place, money
    case place
    when "first"
      return money * PERCENTS_OF_AWARD_FOR_FIRST_PLACE / 100
    when "second"
      return money * PERCENTS_OF_AWARD_FOR_SECOND_PLACE / 100
    when "third"
      return money * PERCENTS_OF_AWARD_FOR_THIRD_PLACE / 100
    end 
  end

  # Generate end money for top 50 in contest
  # => Checks if contest is_money
  # => Calculates money for each user (based on place - index + 1)
  # => Adds money to user -> earned_money if eligible to receive money
  # => Returns ideas_count
  def generate_end_money
    if self.is_money?
      self.best_ideas.limit(50).each_with_index do |idea, index|
        if idea.user
          money = PAYOUT_STRUCTURE[index] * self.award_value / 100
          idea.user.increment!(:earned_money, money)
          log_cash = LogCash.create :contest_id => id, :user_id => idea.user.id, :idea_id => idea.id, :earned_money => money
          FacebookActivity.user_earned_money( idea.user, title ) if AppUtils.facebook_env?
          
          if idea.user.earned_money >= ApplicationSetting["MINIMUM_TO_CHECK_OUT"]
            popup = UserPopup.update_all('seen = TRUE', "user_id = #{idea.user.id} AND typekey = 'earned_money'")
            UserPopup.create :user_id => idea.user.id, :typekey => 'earned_money', :seen => false, :earned_money => idea.user.earned_money
          end
        end
      end
    else
      return "not money contest"
    end
  end

  begin "rooms related methods"  
    def add_user_to_room user, room_id
      room_user = RoomUser.find_by_contest_id_and_user_id id, user.id
      if room_user.present?
        return room_user.room
      else
        current_room = Room.find( room_id )
        room_user_new = { :contest => self, :room => current_room, :user => user }
        room_user = RoomUser.new room_user_new
        room_user.save!
        statistic_contest_room_user_new = { :contest_id => self.id, :room_id => current_room.id, :user_id => user.id, :vote_points => 0, :idea_points => 0, :total_points => 0 }
        statistic_contest_room_user = StatisticContestRoomUser.new statistic_contest_room_user_new
        statistic_contest_room_user.save!
        current_room.add_participant_count_plus_one
        new_room if need_new_room?
        return current_room
      end  
    end
    
    def current_room user, first_idea_id = nil, second_idea_id = nil
      return new_room unless rooms.any?
      return rooms.last if user.nil?
      room_user = RoomUser.find_by_contest_id_and_user_id self, user
      if first_idea_id == nil && second_idea_id == nil
        first_idea = nil
        second_idea = nil      
      else
        first_idea = Idea.find first_idea_id
        second_idea = Idea.find second_idea_id
      end

      if room_user.present?
        return room_user.room if room_user.room.ideas_from_one_room_or_empty? first_idea, second_idea
      else
        if first_idea == nil && second_idea == nil
          return rooms.last
        else
          if first_idea.room_id == second_idea.room_id
            room = Room.find_by_id_and_contest_id(first_idea.room_id, id)
            return room if room != nil && !room.locked
          end
        end
      end

      return nil
    end
    
    #Tmp User methods
    
    def add_tmp_user_to_room tmp_user, room_id
      tmp_room_user = TmpRoomUser.find_by_contest_id_and_tmp_user_id id, tmp_user.id
      if tmp_room_user.present?
        return tmp_room_user.room
      else
        current_room = Room.find( room_id )
        room_user_tmp_new = { :contest => self, :room => current_room, :tmp_user => tmp_user }
        room_user_tmp = TmpRoomUser.new room_user_tmp_new
        room_user_tmp.save!
        return current_room
      end    
    end
    
    def current_room_tmp tmp_user, first_idea_id = nil, second_idea_id = nil
      return new_room unless rooms.any?

      room_user_tmp = RoomUser.find_by_contest_id_and_user_id( self, tmp_user )
      first_idea = ( first_idea_id.nil? && second_idea_id.nil? ) ? nil : Idea.find( first_idea_id )
      second_idea = ( first_idea_id.nil? && second_idea_id.nil? ) ? nil : Idea.find( second_idea_id )
 
      if room_user_tmp.present?
        return room_user_tmp.room if room_user_tmp.room.ideas_from_one_room_or_empty? first_idea, second_idea
      else
        if first_idea.nil? && second_idea.nil?
          return rooms.last
        else
          if first_idea.room_id == second_idea.room_id
            room = Room.find_by_id_and_contest_id(first_idea.room_id, id)
            return room if !room.nil? && !room.locked
          end
        end
      end
      nil
    end
    
    def new_room
      room_new = { :contest => self, :participants => 0 }
      room = Room.new room_new
      room.save!
      room
    end
  end

 def difference_for_ends_at type, ends_at
    difference = ends_at.to_i - Time.now.to_i
    seconds    =  difference % 60
    difference = (difference - seconds) / 60
    minutes    =  difference % 60
    difference = (difference - minutes) / 60
    hours      =  difference % 24
    difference = (difference - hours) / 24
    days       =  difference % 7
    difference = (difference - days) / 30
    months     =  difference % 12
    difference = (difference - months) / 12
    years      =  difference
    
    return years if type == 'years'
    return months if type == 'months'
    return days if type == 'days'
    return hours if type == 'hours'
    return minutes if type == 'minutes'
  end

  def time_to_end
    diff_seconds = ( ends_at - Time.now ).round

    "%02d : %02d" % [diff_seconds / 3600, ( diff_seconds % 3600) / 60 ] if diff_seconds > 60
  end
  
  def logo_image
    return contest_image.logo.url(:thumbnail) if contest_image
    logo? ? logo.url(:thumbnail) : ""
  end

  def organization_logo_url
    organization.logo.url(:thumbnail)
  end

  def to_json_with_next_and_previous user, organization
    current_user_room = self.current_room user
    
    next_contest     = self.next user, organization
    
    previous_contest = self.previous user, organization
    
    room_user = RoomUser.find_by_contest_id_and_user_id( self.id, user ) if user
    
    {
      :id                          => id,
      :category_id                 => category_id,
      :category                    => category.name,
      :current_room_ideas_count    => current_user_room.try(:ideas).try(:count),
      :rooms_count                 => rooms_count,
      :title                       => title,
      :im_in                       => user ? (room_user.nil? ? false : true) : false,
      :status                      => status,
      :typekey                     => typekey,
      :time_to_end                 => time_to_end,
      :is_money                    => is_money,
      :money                       => is_money ? award_value / 100 : false,
      :logo                        => logo_image,
      :smaller_logo                => smaller_logo? ? smaller_logo.url(:thumbnail) : "",
      :description                 => description,
      :next_id                     => next_contest.id,
      :next_title                  => next_contest.title,
      :next_time_to_end            => next_contest.time_to_end,
      :next_is_money               => next_contest.is_money,
      :previous_id                 => previous_contest.id,
      :previous_title              => previous_contest.title,
      :previous_time_to_end        => previous_contest.time_to_end,
      :previous_is_money           => previous_contest.is_money,
      #user is allowed to participate if it's not money contest or it's level is sufficient, unregistred users are not allowed to participate
      :user_allowed_to_participate => true, #!is_money || user && user.level_sufficient_for_contest?(self),
      #allowed to report spam
      :user_allowed_to_report_spam => user && user.level_sufficient_to_report_spam?(self),
      :organization_logo_url       => organization_logo_url
    }
  end

  def as_json(options={})
    {
      :id => id,
      :title => title,
      :category => category.name,
      :status => status
    }
  end

  def to_json_id_only
    { :id => id }
  end

  def to_s
    self.title
  end

  # def load_image_from_url(field, url)
  #   self.send "#{field}=", open(url) if url
  # end

  def update_from_crowdsourced_contest params
    [:smaller_logo, :logo ].each do |img|
      load_image_from_url img, params.delete("#{img}_url".to_sym)
    end

    update_attributes(params)
  end

  def load_image_from_url(field, url)
    return unless url
    begin
      io = open(URI.escape(url))
      if io
        def io.original_filename; base_uri.path.split('/').last; end
        io.original_filename.blank? ? nil : io
        self.send "#{field}=", io
      end
    rescue Exception => e
      logger.info "EXCEPTION# #{e.message}"
    end
  end

  def results
    ideas.as_crowdsourcing_results.map { |idea| idea.for_contest_results }
  end

# </Instance methods>

  def check_activity_for typekey, user
    if typekey == "vote"
      activity = LogActivity.where("contest_id = ? AND user_id = ? AND typekey = 'points_vote'", self.id, user.id)
      if activity.size > 0
        true
      else
        false
      end
    elsif typekey == "idea"
      # OLD LOGIC for idea activity
      # => search in api/statistics controller for new logic ~ line 159
      activity = LogActivity.where("contest_id = ? AND user_id = ? AND typekey = 'new_idea'", self.id, user.id)
      if activity.size > 0
        true
      else
        false
      end
    end
  end

  #memoize :next, :previous

  private

  def correct_ends_at
    errors.add(:ends_at, I18n.t("activerecord.errors.models.contest.attributes.ends_at.bad")) if ends_at < DateTime.now
  end

  def set_defaults
    self.active_flag = false          if active_flag.nil?
    self.language    = "English"      if AppUtils.internal_app_env?

    # setting full permissions by default
    self.edu_access  = EduUser::TYPES if self.new_record?
  end

end
