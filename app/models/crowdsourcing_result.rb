# == Schema Information
#
# Table name: crowdsourcing_results
#
#  id                      :integer          not null, primary key
#  text                    :string(255)
#  author                  :string(255)
#  wins                    :integer
#  total_duels             :integer
#  points                  :integer
#  author_image_url        :string(255)
#  crowdsourced_contest_id :integer
#  created_at              :datetime
#  updated_at              :datetime
#  ratio                   :decimal(6, 2)
#

class CrowdsourcingResult < ActiveRecord::Base
  belongs_to :crowdsourced_contest
  
  # scope :list_by_contest_id_and_room_id_with_users, lambda { |contest_id, room_id, offset| 
  #   where( :contest_id => contest_id, :room_id => room_id ).order('duels_won desc, id asc').limit(10).offset(offset || 0).includes(:user) 
  # }
  scope :for_organization_by_contest, lambda { |contest_id, organization, offset| 
    joins(:crowdsourced_contest).where("crowdsourced_contests.id = ? AND crowdsourced_contests.organization_id = ?", contest_id, organization.id)
    .limit(10).offset(offset || 0)
    .order("crowdsourcing_results.ratio DESC, wins DESC, total_duels DESC")
  }

  def as_json options={}
    json = [ :text, :author, :wins, :total_duels, :points, :author_image_url, :ratio ].inject({}) { |memo, val| 
      memo[val] = send val
      memo
    }
    
    json[:idea_type]  = crowdsourced_contest.typekey
    json 
  end
end
