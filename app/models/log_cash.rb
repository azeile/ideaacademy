# == Schema Information
#
# Table name: log_cashes
#
#  id           :integer          not null, primary key
#  contest_id   :integer
#  user_id      :integer
#  created_at   :datetime
#  updated_at   :datetime
#  earned_money :integer
#  idea_id      :integer
#

class LogCash < ActiveRecord::Base

  #Relations
  belongs_to :contest
  belongs_to :user

  #Validations
  # => Relations
  validates :contest_id, :presence => true
  validates :user_id, :presence => true

end
