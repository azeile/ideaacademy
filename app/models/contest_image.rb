# == Schema Information
#
# Table name: contest_images
#
#  id                :integer          not null, primary key
#  logo_file_name    :string(255)
#  logo_content_type :string(255)
#  logo_file_size    :integer
#  logo_updated_at   :datetime
#  created_at        :datetime
#  updated_at        :datetime
#  imageable_id      :integer
#  imageable_type    :string(255)
#

class ContestImage < ActiveRecord::Base
  
  belongs_to :imageable, :polymorphic => true
  
  has_attached_file :logo,
    :styles => { :thumbnail => "390x175#", :small => "300x150#"}    
  
  def as_json options={}
    {
      :id => id,
      :logo => logo? ? logo.url(:small) : "",
    }
  end
end
