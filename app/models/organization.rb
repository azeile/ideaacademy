#encoding: UTF-8
# == Schema Information
#
# Table name: organizations
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  heard_from        :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  phone_number      :string(255)
#  logo_file_name    :string(255)
#  logo_content_type :string(255)
#  logo_file_size    :integer
#  logo_updated_at   :datetime
#

class Organization < ActiveRecord::Base
  has_many :organization_users, :dependent => :destroy
  has_many :users, :through => :organization_users
  has_many :admin_users, :through => :users
  has_many :categories, :dependent => :destroy
  has_many :contests, :through => :categories
  has_many :ideas, :through => :contests
  has_many :statistic_category_users, :through => :categories
  has_many :user_popups, :through => :users, :source => :popups
  has_many :sidebar_messages

  has_attached_file :logo,
    styles: {thumbnail: "200x200"}

  attr_accessible :name, :heard_from, :phone_number, :logo, :org_type
  validates_attachment_size :logo, :less_than => 5.megabytes
  validates_attachment_content_type :logo, :content_type => ["image/jpeg", "image/jpg", "image/png"]
  validates_presence_of :name, :heard_from, :phone_number
  validates_uniqueness_of :name

  after_create :create_default_categories

  scope :default_org, lambda { includes(:organization_users).where([ "default_flag = ?", true ]) }
  scope :schools, lambda { where(org_type: :school ) }

  # Visur pašam jāieraksta, vienīgi pie How did you hear būs izvēlne ar opcijām- earch engine, advertisment, Facebook application, Article, referral, other
  HEARD_FROM_OPTIONS =
  [
    [ "organization.heard_from.advertisment",  :advertisment   ],
    [ "organization.heard_from.search_engine", :search_engine  ],
    [ "organization.heard_from.facebook",      :facebook       ],
    [ "organization.heard_from.article",       :article        ],
    [ "organization.heard_from.referral",      :referral       ],
    [ "organization.heard_from.other",         :other          ],
  ]

  TYPES = [:organization, :school]

  def user_registred
    @user_registred ||= users.where("organization_users.creator_flag = ?", true).first
  end

  def school?
    org_type == :school.to_s
  end

  def self.for_active_admin org
    where(id: org.id)
  end

  def title
    name
  end
  
  private

    def create_default_categories
      Category.create_default_for_organization self.id
    end

end
