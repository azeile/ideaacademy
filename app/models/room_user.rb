# == Schema Information
#
# Table name: room_users
#
#  id         :integer          not null, primary key
#  contest_id :integer
#  room_id    :integer
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class RoomUser < ActiveRecord::Base

  #Relations
  belongs_to :contest
  belongs_to :user
  belongs_to :room

  #Validates
  # => Relations
  validates :contest_id, :presence => true
  validates :user_id, :presence => true
  validates :room_id, :presence => true
  
  scope :by_category, lambda { |category_id, user_id| joins(:contest).where('contests.category_id = ?',category_id).where("room_users.user_id = ?", user_id).order("room_users.created_at desc") }

end
