class AdminAbility
  include CanCan::Ability

  ORGANIZATION_ADMIN_RESOURCES = [ :users, :contests, :ideas, :statistic_category_users, :user_popups, :categories ] #, :sidebar_messages

  def initialize(admin, current_organization)
    admin ||= AdminUser.new

    case
      when admin.superadmin?
        can :manage, :all
      when admin.active?(current_organization)
        can :manage, Category do |cat|
          cat.organization_id.nil? || cat.organization_id == current_organization.id
        end

        can :manage, OrganizationUser do |ou|
          ou.organization_id.nil? || ou.organization_id == current_organization.id
        end

        can :manage, Organization do |o|
          o.id.nil? || o.id == current_organization.id
        end

        #can :manage, Invitation do |i|
          #i.organization_id.nil? || i.organization_id == current_organization.id
        #end

        can :manage, UserMessage do |um|
          um.receiver_id == admin.id
        end

        # can :manage, User do |u|
        #   u.orgnizations.empty? || u.organizations.where("organizations.id = ?", current_organization.id).present?
        # end

        # can :create , User do |u|
        #   u.organizations_user.first.organization == current_organization.id
        # end

        can :manage, SidebarMessage do |sm|
          sm.organization_id.nil? || sm.organization_id == current_organization.id
        end

        can :manage, CrowdsourcedContest do |sm|
          sm.organization_id.nil? || sm.organization_id == current_organization.id
        end

        can :manage, Contest do |c|
          c.category_id.nil? || c.category.organization_id == current_organization.id
        end

        can :manage, Idea do |i|
          i.contest_id.nil? || i.contest.organization == current_organization
        end

        can :manage, StatisticCategoryUser do |sc|
          sc.category_id.nil? || sc.category.organization_id == current_organization.id
        end
      else
        cannot :manage, :all
    end
  end
end
