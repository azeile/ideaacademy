# == Schema Information
#
# Table name: application_settings
#
#  id              :integer          not null, primary key
#  setting_type_id :integer
#  code            :string(255)
#  value           :string(255)
#  name            :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class ApplicationSetting < ActiveRecord::Base
  belongs_to :setting_type
  validates_presence_of :code, :name, :value, :setting_type
  validates_uniqueness_of :code
  attr_accessible :code, :name, :value, :setting_type, :setting_type_id
  
  class << self
    def [] key
      find_by_code(key).try :value
    end
  end
  
  def value
    case setting_type.name
      when "decimal" then BigDecimal( read_attribute(:value).to_s )
      when "integer" then read_attribute(:value).to_i
      else read_attribute(:value)
    end
  end
end
