# == Schema Information
#
# Table name: payment_transactions
#
#  id                      :integer          not null, primary key
#  provider_transaction_id :string(255)
#  service_id              :integer
#  user_provider_id        :integer
#  status                  :string(255)
#  payment_sum             :decimal(, )
#  created_at              :datetime
#  updated_at              :datetime
#  retry_times             :integer
#

class PaymentTransaction < ActiveRecord::Base
  belongs_to :user_provider
  belongs_to :service
  has_one :user, :through => :user_provider
  validates_presence_of :user_provider
  
  class << self
    def start_transaction req, service
      create(
        :service => service,
        :provider_transaction_id => req.data["credits"]["order_id"], 
        :user_provider => UserProvider.find_by_social_id(req.data["credits"]["buyer"]),
        :payment_sum => req.data["credits"]["price"],
        :status => "STARTED"
        )
    end
  end
  
  def response_valid? params
    case user_provider.name
      when "draugiem" then DraugiemPayment.response_valid?(params, provider_transaction_id, service.draugiem_id)
    end
  end
  
  def as_json options={}
    {
      :id => id,
      :provider_transaction_id => provider_transaction_id,
      :status => status
    }
  end

  def finish_transaction
    transaction do
      begin
        update_attributes(:status => :finished)
        service.subscribe user
      rescue ActiveRecord::Rollback
        return false
      end
    end

    true
  end
  
  def finished?
    status.to_s == 'finished'
  end
  
  def retry_times_exceeded?
    (retry_times || 0) > 2
  end
  
  def update_retry_times
    increment!(:retry_times)
  end
  
end
