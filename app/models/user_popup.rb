# == Schema Information
#
# Table name: user_popups
#
#  id                      :integer          not null, primary key
#  user_id                 :integer
#  seen                    :boolean          default(FALSE)
#  typekey                 :string(255)
#  created_at              :datetime
#  updated_at              :datetime
#  like_idea_first_name    :string(255)
#  like_idea_last_name     :string(255)
#  like_idea_image_url     :string(255)
#  like_idea_my_image_url  :string(255)
#  category                :string(255)
#  new_achievement_level   :integer
#  earned_money            :integer
#  new_achievement_message :text
#

class UserPopup < ActiveRecord::Base
  belongs_to :user

  scope :user_present, lambda { where("user_id IS NOT NULL")}
  scope :achievement_present, lambda { where("new_achievement_level IS NOT NULL")}
  scope :badge, lambda { user_present().achievement_present() }

  class << self
    def same_organization user
      puts "--------------------"+ user.class.to_s
      o_id = user.organizations.first.id
      UserPopup.joins(:user).
        where(:users => {
          :id => User.joins(:organization_users).
            where(:organization_users => {
              :organization_id => o_id
          })
      })
    end
  end
end
