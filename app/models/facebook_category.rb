# == Schema Information
#
# Table name: facebook_categories
#
#  id         :integer          not null
#  name       :string(255)
#  fb_id      :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#

class FacebookCategory < ActiveRecord::Base
  set_primary_key :fb_id
  has_many :crowdsourced_contests, :primary_key => :fb_id, :foreign_key => :category_id, :inverse_of => :facebook_category
  attr_accessible :fb_id, :id, :name

  def name
    I18n.t "categories.#{read_attribute(:name).to_s.downcase}", :default => read_attribute(:name) if read_attribute(:name)
  end


  def as_json options={}
    {
      :id => fb_id,
      :name => name
    }
  end
end
