# == Schema Information
#
# Table name: statistic_contest_room_users
#
#  id           :integer          not null, primary key
#  contest_id   :integer
#  room_id      :integer
#  user_id      :integer
#  vote_points  :integer
#  idea_points  :integer
#  total_points :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class StatisticContestRoomUser < ActiveRecord::Base

  #Relations
  belongs_to :contest
  belongs_to :room
  belongs_to :user

  #Validations
  # => Relations
  validates :contest_id, :presence => true
  validates :room_id, :presence => true
  validates :user_id, :presence => true

  #Scopes
  scope :list_by_contest_id, lambda { |contest_id| where(:contest_id => contest_id).order("total_points DESC") }
  scope :list_by_contest_id_and_room_id, lambda { |contest_id, room_id| where(:contest_id => contest_id, :room_id => room_id).order("total_points DESC") }
  
  CLOSEST_ARRAY = [-3,-2,-1,1,2,3]

  def add_points_vote
    self.vote_points = (vote_points.nil? ? 0 : vote_points) + POINTS_VOTE
    self.total_points = (total_points.nil? ? 0 : total_points) + POINTS_VOTE
    self.save
  end
  
  def add_points_idea
    self.idea_points = (idea_points.nil? ? 0 : idea_points) + POINTS_IDEA
    self.total_points =  (total_points.nil? ? 0 : total_points) + POINTS_IDEA
    self.save    
  end
 
  class << self
    def get_statistics user, contest, room_id
      @current_user = user
      if room_id 
        statistics = self.list_by_contest_id_and_room_id( contest.id, room_id )
      else
        statistics = self.list_by_contest_id( contest.id )
      end
      if user
        prepare statistics, contest, user
      else
        prepare statistics, contest, user, false
      end
    end
    
    private
      def prepare players, contest, user, plan_show_friends=true
        @current_user = user
        @players = []
        show_more = false
        me = false
        show_friends = false
        players.each_with_index do |statistic, index|
          unless statistic.user.nil?
            statistic_category = StatisticCategoryUser.find_by_category_id_and_user_id contest.category_id, statistic.user.id
            if plan_show_friends
              friend = @current_user ? (@current_user.friends.where(:id => statistic.user.id).empty? ? false : true) : false

              # friend = @current_user.friends.where(:id => statistic.user.id).id
              #friend = statistic.user_id
              me = index if me?(statistic.user)
              show_friends = true if friend
            end

            @players << {
              :order => place(index),
              :prefix => statistic.user.title_prefix(statistic.user.level_for_category contest.category),
              :first_name => statistic.user.first_name,
              :last_name => statistic.user.last_name,
              :points => statistic.total_points.nil? ? 0 : statistic.total_points,
              :idea_points => statistic.idea_points.nil? ? 0 : statistic.idea_points,
              :vote_points => statistic.vote_points.nil? ? 0 : statistic.vote_points,
              :image => statistic.user.profile_image,
              :badge_image => statistic_category.nil? ? 0 : statistic_category.badge.image, 
              :friend => friend,
              :me => me?( statistic.user ),
              :close_friend => false, 
              :close_user => false,
              :show => me?(statistic.user) || [1,2,3].include?(place(index)) ? true : false,
              :user_id => statistic.user.id
            }
          end
        end

        set_closest_friends(me) if (plan_show_friends)
        set_closest_users(me)

        if @players.any?
          status = true
          message = 'statistics_loaded'
        else
          status = false
          message = I18n.t("statuses.statistics_not_found")
        end

        { :status => status, :message => message, :show_friends => show_friends, :show_more => @players.size > 3, :players => @players }
      end

      def me? user
        return false unless @current_user
        @current_user == user
      end

      def place index
        index+1
      end

      def set_closest_friends me
        return unless me
        friend_statistics = []      
        orders = []
        me_in_friends = false

        @players.each do |statistic|
          friend_statistics << statistic if statistic[:friend] || statistic[:me]
        end

        friend_statistics.each_with_index do |statistic, index|
          me_in_friends = index if statistic[:me]
        end

        places = closest me_in_friends
        friend_statistics.each_with_index do |statistic, index|
          orders << statistic[:order] if places.include?( index )
        end

        @players.each do |statistic|
          statistic[:close_friend] = true if orders.include?( statistic[:order] )
        end

      end

      def set_closest_users me
        return unless me
        places = closest me      
        @players.each_with_index do |statistic, index|
          statistic[:close_user] = true if places.include?( index )
        end
      end

      def closest me
        places = []
        CLOSEST_ARRAY.each do |closest|
          place = me + closest
          places << place if place >= 0 
        end
        places
      end
    
  end
  
end
