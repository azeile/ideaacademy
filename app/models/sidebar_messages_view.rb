# == Schema Information
#
# Table name: sidebar_messages_views
#
#  id                 :integer          not null, primary key
#  sidebar_message_id :integer
#  viewer_id          :integer
#  created_at         :datetime
#  updated_at         :datetime
#

class SidebarMessagesView < ActiveRecord::Base

  # Relations
  belongs_to :sidebar_message
  belongs_to :user, :class_name => "User", :foreign_key => :viewer_id

end
