class CrowdsourcedContestObserver < ActiveRecord::Observer
  
  def after_save(model)
    if model.active_flag_changed? && model.status == "live"
      #sync with FB
    end
  end
end