# == Schema Information
#
# Table name: admin_users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(128)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  user_id                :integer
#  superadmin_flag        :boolean
#  locked_at              :datetime
#

class AdminUser < ActiveRecord::Base
  include SharedOrganizationMethods
  
  belongs_to :user
  has_many :organization_users, :through => :user#, :conditions => ["admin_flag = ?", true]
  has_many :organizations , :through => :organization_users
  has_many :received_messages, class_name: "UserMessage", as: :receiver

  #default_scope joins(:organization_users).readonly(false).where("superadmin_flag = ? OR organization_users.admin_flag = ? ", true, true)

  #used to filter those, who are not organization admins anymore
  # default_scope joins(:organization_users).where("organization_users.admin_flag = ?", true)
  
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :lockable

  attr_accessible :email, :password, :password_confirmation, :remember_me, :user
  # after_update :sync_passwords
  before_create :get_user_password
  
  validates :password,  :presence   => { :unless  => :encrypted_password }
  # validates :user,      :presence   => true
  validates :email,     :presence   => true,
                        :uniqueness => true,
                        :email      => true

  # delegate :default_organization, :to => :user, :allow_nil => true
  
  class << self
    def create_with_user_password email, encrypted_pass, user_id = nil
      user_id ||= User.select(id).find_by_email(email)
      
      admin                    = new(:email => email)
      admin.user_id            = user_id
      admin.encrypted_password = encrypted_pass
      admin.save!
      admin
    end
  end

  def active?(organization)
    organization_users.where("organization_users.admin_flag = ? AND organization_users.organization_id = ? ", true, organization.id).present?
  end

  def superadmin?
    superadmin_flag
  end

  def unlock
    update_attribute(:locked_at, nil)
  end

  def lock
    update_attribute(:locked_at, DateTime.now)
  end
  
  private
    def get_user_password
      self.encrypted_password = user.encrypted_password if user
    end

    def set_user
    end
end