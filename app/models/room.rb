# == Schema Information
#
# Table name: rooms
#
#  id           :integer          not null, primary key
#  contest_id   :integer
#  participants :integer
#  created_at   :datetime
#  updated_at   :datetime
#  locked       :boolean          default(FALSE)
#

class Room < ActiveRecord::Base

  #Relations
  belongs_to :contest, :counter_cache => :rooms_count

  has_many :room_users
  has_many :users, :through => :room_users
  has_many :statistic_contest_room_users
  has_many :ideas
  has_many :log_votes

  #Validates
  # => Relations
  # validates :contest_id, :presence => true
  
  def add_idea user, params
    case contest.typekey
    when 'text'
      add_text_idea user, params["text"]
    when 'image'
      add_image_idea user, params
    when 'video'
      add_video_idea user, params["text"]
    end
  end

  def vote_idea user, first_idea_id, second_idea_id, selected_idea_id = nil
    if selected_idea_id == nil || selected_idea_id.to_i <= 0 || contest.done?
      do_skip_vote( user, first_idea_id, second_idea_id )
    else
      ignored_idea_id = get_ignored_idea_id( first_idea_id, second_idea_id, selected_idea_id )
      do_vote( user, selected_idea_id, ignored_idea_id )
    end
  end

  def add_participant_count_plus_one
    self.participants = participants + 1
    self.save
  end

  # Top ideas are 25% of all best ideas ordered by rarity (rarest are first) 
  def set_of_idea_pairs user, tmp = false
    rarest_ideas = Idea.ideas_for_vote(user, self).rarest.all
    top_ideas = Idea.where(:id => Idea.ideas_for_vote(user, self).top.limit((rarest_ideas.count * 0.25).ceil)).rarest.all
    used_pairs = (tmp ? tmp_log_votes : log_votes).by_user(user).map { |logvote| [logvote.selected_idea_id, logvote.ignored_idea_id].sort }
    pairs = []
    rarest_ideas.each do |idea1|
      break if top_ideas.size == 0 || pairs.size >= 9
      top_ideas.each do |idea2|
        next if idea1 == idea2 || pairs.include?(idea1) || pairs.include?(idea2) 
        unless used_pairs.include? [idea1.id, idea2.id].sort
          pairs << idea1 << idea2 
          top_ideas.delete(idea1) && top_ideas.delete(idea2)
          break
        end
      end
    end
    pairs
  end

  #deprecated. see set_of_idea_pairs
  def ideas_next_duel user 
    ideas = Idea.ideas_for_vote user, self
    vote_pairs = LogVote.by_vote_user_and_room user.id, id
    get_unique_pair ideas, vote_pairs
  end

  def ideas_from_one_room_or_empty? selected_idea, ignored_idea
    return true if selected_idea == nil && ignored_idea == nil
    return true if selected_idea.room_id == ignored_idea.room_id && selected_idea.room_id == id
    false
  end
    
  def vote_idea_tmp tmp_user, first_idea_id, second_idea_id, selected_idea_id = nil      
    if selected_idea_id == nil || selected_idea_id.to_i <= 0
      do_skip_vote_tmp( tmp_user, first_idea_id, second_idea_id )
    else 
      ignored_idea_id = get_ignored_idea_id( first_idea_id, second_idea_id, selected_idea_id )
      do_vote_tmp( tmp_user, selected_idea_id, ignored_idea_id )
    end
  end
  
  #deprecated. see set_of_idea_pairs
  def ideas_next_duel_tmp tmp_user 
    ideas = Idea.ideas_for_vote_tmp tmp_user, self
    vote_pairs = TmpLogVote.by_vote_user_and_room tmp_user.id, id
    get_unique_pair ideas, vote_pairs
  end

  protected
    def check_ideas_voted user, first_idea, second_idea
      vote_pairs = LogVote.by_vote_user_and_room user.id, id
      pair_given first_idea, second_idea, vote_pairs
    end
    
    def check_ideas_voted_tmp tmp_user, first_idea, second_idea
      vote_pairs = TmpLogVote.by_vote_user_and_room tmp_user.id, id
      pair_given first_idea, second_idea, vote_pairs
    end
    
    def get_unique_pair ideas, vote_pairs
      result = {:status => false, :first_idea => nil, :second_idea => nil, :message => "" }
      if ideas.length <= 1
        result[:message] = I18n.t("statuses.ideas_not_found")
        return result
      end
      ideas.each do |idea1|
        ideas.each do |idea2|
          unless pair_given( idea1, idea2, vote_pairs )
            return result = {:status => true, :first_idea => idea1, :second_idea => idea2, :message => "ideas_loaded" }
          end
        end
      end
      result[:message] = I18n.t("statuses.all_ideas_voted")
      result
    end
    
    def pair_given idea1, idea2, vote_pairs
      return true if idea1.id == idea2.id
  		tmp_idea_pair = [ idea1.id, idea2.id ]
      vote_pairs.each do |vote_pair|
  			tmp_vote_pair = [ vote_pair.selected_idea_id, vote_pair.ignored_idea_id ]
  			diff = tmp_idea_pair - tmp_vote_pair
  		  return true if diff.empty?
      end
  		false
    end
    
    def do_skip_vote user, first_idea_id, second_idea_id
      first_idea = Idea.find(first_idea_id)
      second_idea = Idea.find(second_idea_id)
      
      return response = { :status => false, :message => "ideas_voted" } if check_ideas_voted( user, first_idea, second_idea )
      
      first_idea_rate = UserIdeaRate.find_or_initialize_by_user_id_and_idea_id( user.id, first_idea.id )
      second_idea_rate = UserIdeaRate.find_or_initialize_by_user_id_and_idea_id( user.id, second_idea.id )
      return { :status => false, :message => "ideas_overrated" } if first_idea_rate.overrated || second_idea_rate.overrated
      
      first_idea_rate.add_rate
      second_idea_rate.add_rate
      
      first_idea.update_duels_in_plus_one
      second_idea.update_duels_in_plus_one
      
      contest.add_user_to_room( user, id)

      LogVote.add_record( user, first_idea, second_idea, "skip_vote" )
      {:status => true, :message => "VOTE_SUCCESFUL"}
    end
  
    def do_vote user, selected_idea_id, ignored_idea_id
      selected_idea = Idea.find(selected_idea_id)
      ignored_idea = Idea.find(ignored_idea_id)
      
      return response = { :status => false, :message => "ideas_yours" } if selected_idea.current_users( user ) || ignored_idea.current_users( user )
      return response = { :status => false, :message => "ideas_voted" } if check_ideas_voted( user, selected_idea, ignored_idea )
         
      selected_idea.update_duels_in_and_duels_won_plus_one
      ignored_idea.update_duels_in_plus_one
      
      contest.add_user_to_room( user, id)

      selected_idea_rate = UserIdeaRate.find_or_initialize_by_user_id_and_idea_id( user.id, selected_idea.id )
      ignored_idea_rate = UserIdeaRate.find_or_initialize_by_user_id_and_idea_id( user.id, ignored_idea.id )
      return { :status => false, :message => "ideas_overrated" } if selected_idea_rate.overrated || ignored_idea_rate.overrated
      
      selected_idea_rate.add_rate
      ignored_idea_rate.add_rate

      # UserIdeaRate.add_rate user, selected_idea
      # UserIdeaRate.add_rate user, ignored_idea

      # ADD Popups

      ## ADD first_points_popup for voting
      check_user_activity = LogActivity.where("user_id = ? AND typekey = ?", user.id, 'points_vote')
      
      if check_user_activity.count > 1
        popup = UserPopup.where("user_id = ? AND typekey = ?", user.id, "first_points_vote")
        
        unless popup
          new_popup = UserPopup.new :user_id => user.id, :typekey => "first_points_vote", :seen => false
          new_popup.save
        end
      end

      # ## ADD first_points_popup when received points from other people voting
      # check_idea_user_activity = LogActivity.where("user_id = ?", selected_idea.user.id).first
      # unless check_user_activity
      #   new_popup = UserPopup.create_or_initialize_by_user_id_and_typekey(selected_idea.user.id, "first_points_idea") # UserPopup.new :user_id => selected_idea.user.id, :typekey => "first_points_idea", :seen => false
        
      #   if new_popup.new_record?
      #     new_popup.seen = false
      #     new_popup.save
      #   end
      # end

      ## ADD like_idea popup to selected idea user if no points received for ideas
      check_selected_user_idea_activity = LogActivity.where("user_id = ? AND typekey = ?", selected_idea.user.id, "points_idea")
      if check_selected_user_idea_activity.count == 0
        new_popup = UserPopup.new :user_id => selected_idea.user.id, :typekey => "like_idea", :seen => false, :category => selected_idea.category.name, :like_idea_first_name => user.first_name, :like_idea_last_name => user.last_name, :like_idea_image_url => user.profile_image, :like_idea_my_image_url => selected_idea.user.profile_image
        new_popup.save
      end

      ## ADD popup `earn_more`
      ## when user voted for 5 ideas, and haven't added any idea
      user_welcome_popup = UserPopup.find(:first, :conditions => { :user_id => user.id, :typekey => "welcome_user" })

      if user_welcome_popup

        check_user_vote_activity = LogActivity.where("user_id = ? AND typekey = ? AND created_at > ?", user.id, "points_vote", "#{user_welcome_popup.updated_at}")
        if user.ideas.size == 0 && check_user_vote_activity.count > 10
          popup = UserPopup.where("user_id = ? AND typekey = ?", user.id, "earn_more").first
          unless popup
            new_popup = UserPopup.new :user_id => user.id, :typekey => "earn_more", :seen => false
            new_popup.save
          end
        end

      end

      ## ADD other_themes popup after 10 activities
      # check_user_total_activity = LogActivity.where("user_id = ?", user.id)
      # if check_user_total_activity.count == 9
      #   new_popup = UserPopup.new :user_id => user.id, :typekey => "other_themes", :seen => false
      #   new_popup.save
      # end

      #continue vote

      LogActivity.user_idea_voted user, selected_idea
      LogActivity.user_voted user, selected_idea
      LogVote.add_record( user, selected_idea, ignored_idea, "vote" )
      set_points_in_room_and_category( selected_idea.user, contest.category, "idea" ) if selected_idea.add_points
      set_points_in_room_and_category( user, contest.category, "vote" )
      {:status => true, :message => "VOTE_SUCCESFUL"}
    end

    def do_skip_vote_tmp tmp_user, first_idea_id, second_idea_id
      first_idea = Idea.find(first_idea_id)
      second_idea = Idea.find(second_idea_id)
      return response = { :status => false, :message => "ideas_voted" } if check_ideas_voted_tmp( tmp_user, first_idea, second_idea )
      
      contest.add_tmp_user_to_room( tmp_user, id) 
      first_idea_rate = TmpUserIdeaRate.find_or_initialize_by_tmp_user_id_and_idea_id( tmp_user.id, first_idea.id )
      second_idea_rate = TmpUserIdeaRate.find_or_initialize_by_tmp_user_id_and_idea_id( tmp_user.id, second_idea.id )
      return { :status => false, :message => "ideas_overrated" } if first_idea_rate.overrated || second_idea_rate.overrated
      
      first_idea_rate.add_rate
      second_idea_rate.add_rate
           
      TmpLogVote.add_record( tmp_user, first_idea, second_idea, "skip_vote" )
      {:status => true, :message => "VOTE_SUCCESFUL"}
    end
  
    def do_vote_tmp tmp_user, selected_idea_id, ignored_idea_id
      selected_idea = Idea.find(selected_idea_id)
      ignored_idea = Idea.find(ignored_idea_id)
      return response = { :status => false, :message => "ideas_voted" } if check_ideas_voted_tmp( tmp_user, selected_idea, ignored_idea )
      
      contest.add_tmp_user_to_room( tmp_user, id )
      selected_idea_rate = TmpUserIdeaRate.find_or_initialize_by_tmp_user_id_and_idea_id( tmp_user.id, selected_idea.id )
      ignored_idea_rate = TmpUserIdeaRate.find_or_initialize_by_tmp_user_id_and_idea_id( tmp_user.id, ignored_idea.id )
      return { :status => false, :message => "ideas_overrated" } if selected_idea_rate.overrated || ignored_idea_rate.overrated
      
      selected_idea_rate.add_rate
      ignored_idea_rate.add_rate

      check_tmp_user_vote = TmpLogVote.find(:all, :conditions => { :tmp_user_id => tmp_user.id } )
      if check_tmp_user_vote.size == 1
        new_popup = TmpUserPopup.new :tmp_user_id => tmp_user.id, :typekey => "first_points_vote", :seen => false
        new_popup.save
      end

      if check_tmp_user_vote.size == 4 || check_tmp_user_vote.size == 8 || check_tmp_user_vote.size == 12 || check_tmp_user_vote.size == 14 || check_tmp_user_vote.size == 18
        new_auth_popup = TmpUserPopup.new :tmp_user_id => tmp_user.id, :typekey => "authorize", :seen => false
        new_auth_popup.save
      end


      TmpLogVote.add_record( tmp_user, selected_idea, ignored_idea, "vote" )
      set_points_in_room_and_category_tmp( tmp_user, contest.category )
      {:status => true, :message => "VOTE_SUCCESFUL"}
    end
      
    def add_text_idea user, text
      idea = self.ideas.create(:text => text, :contest_id => self.contest.id, :user_id => user.id, :typekey => self.contest.typekey, :category_id => self.contest.category_id)
      idea
    end
  
    def add_image_idea user, params
      file = params[:image]
      file.content_type = MIME::Types.type_for(file.original_filename).to_s
      idea = self.ideas.create!(:image => file, :contest_id => self.contest.id, :user_id => user.id, :typekey => self.contest.typekey, :category_id => self.contest.category_id)
      idea  
    end
  
    def add_video_idea user, text
      idea = self.ideas.create(:text => text, :contest_id => self.contest.id, :user_id => user.id, :typekey => self.contest.typekey, :category_id => self.contest.category_id)
      idea
    end
    
  private
 
    def set_points_in_room_and_category_tmp tmp_user, category
      category_statistic = TmpStatisticCategoryUser.find_or_initialize_by_tmp_user_id_and_category_id( tmp_user.id, category.id )
      category_statistic.add_points_vote
    end
     
    def set_points_in_room_and_category user, category, type
      category_statistic = StatisticCategoryUser.find_or_initialize_by_user_id_and_category_id( user.id, category.id )
      statistic = StatisticContestRoomUser.find_or_initialize_by_room_id_and_user_id( id, user.id )
      statistic.contest_id = contest_id if statistic.new_record?
      case type
      when "vote"
        statistic.add_points_vote
        category_statistic.add_points_vote
      when "idea"
        statistic.add_points_idea
        category_statistic.add_points_idea
      end
    end
     
    def get_ignored_idea_id first_idea_id, second_idea_id, selected_idea_id
      return first_idea_id if selected_idea_id == second_idea_id
      second_idea_id
    end
    
end
