class InvitationObserver < ActiveRecord::Observer

  observe :invitation

  def after_create invitation
    User.delay.invite!({
      email: invitation.email,
      organization_ids: [invitation.organization_id]
    }, invitation.inviter)
  end
end
