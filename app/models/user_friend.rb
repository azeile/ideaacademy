# == Schema Information
#
# Table name: user_friends
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  friend_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class UserFriend < ActiveRecord::Base

  #Relations
  belongs_to :user
  belongs_to :friend, :class_name => "User", :foreign_key => "friend_id"

  #Validations
  # => Relations
  validates :user_id, :presence => true
  validates :friend_id, :presence => true

end
