# == Schema Information
#
# Table name: categories
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  contests_count  :integer          default(0)
#  organization_id :integer
#  active_flag     :boolean
#

class Category < ActiveRecord::Base

  #Relations
  has_many :contests
  has_many :statistic_category_users, :dependent => :destroy
  has_many :statistic_category_ordered_users, :dependent => :destroy
  has_many :user_category_notifications, :dependent => :destroy
  has_many :subscribers, :through => :user_category_notifications, :source => :user
  belongs_to :organization
  
  #Validations
  # => Attributes
  validates :name, :presence => true
  validate :not_more_than_six, :on => :create


  #Scopes
  #default_scope where( "active_flag = ?", true ).order('id ASC')

  scope :by_organization, lambda { |organization_id| where(:organization_id => organization_id)  }
  scope :by_user, lambda { |user| joins(:organization => :users).where("users.id = ?", user.id)  }
  scope :next, lambda {|organization, current_id| by_organization(organization.id).where("id > ? ", current_id ).limit(1) }
  scope :previous, lambda {|organization, current_id| by_organization(organization.id).where("id < ? ", current_id ).reorder("id desc").limit(1) }

  after_initialize :set_defaults
  after_create     :generate_user_statistics

  DEFAULT_CATEGORIES_FOR_FACEBOOK = %w(Default Texts Design Social)
  
  class << self
    def create_default_for_organization org_id
      if AppUtils.internal_app_env?
        create(:name => "Default", :organization_id => org_id)
      end
    end

    def by_id id
      Category.unscoped.find id
    end
  end

  def name
    I18n.t "categories.#{read_attribute(:name).to_s.downcase}", :default => read_attribute(:name) if read_attribute(:name)
  end

  def live_contests_count(user)
    contests.live.without_drafts.user_language_exists(user).count
  end

  def as_json(options={})
  	{ 
      :name => (options[:plain_name] ? read_attribute(:name) : name), 
      :id => id, 
      :contests_count => contests.count
    }
    #super(:only => [I18n.t(:name), :id])
  end

  def next_category organization
    category = Category.next(organization, id).first || Category.by_organization(organization.id).first

    return { :id => category.id, :title => category.name }
  end

  def previous_category organization
    category = Category.previous(organization, id).first || Category.by_organization(organization.id).last

    return { :id => category.id, :title => category.name }
  end

  private

    def set_defaults
      self.active_flag = true if new_record?
    end

    def not_more_than_six
      if Category.where("organization_id = ?", organization_id).count > 5
        errors.add(:base, I18n.t("activerecord.errors.models.category.not_more_than_six"))
      end
    end

    def generate_user_statistics
      transaction do
        unless read_attribute(:name) == 'Default'
          badge = Badge.first
          organization.organization_users.each {|ou| ou.create_category(id, ou.user_id, badge) }
        end
      end
    end
end
