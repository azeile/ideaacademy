# == Schema Information
#
# Table name: log_votes
#
#  id               :integer          not null, primary key
#  contest_id       :integer
#  room_id          :integer
#  selected_idea_id :integer
#  ignored_idea_id  :integer
#  user_id          :integer
#  created_at       :datetime
#  updated_at       :datetime
#  status           :string(255)
#

class LogVote < ActiveRecord::Base

  #Relations
  belongs_to :contest
  belongs_to :room
  belongs_to :selected_idea, :class_name => "Idea", :foreign_key => "selected_idea_id"
  belongs_to :ignored_idea, :class_name => "Idea", :foreign_key => "ignored_idea_id"
  belongs_to :user
  
  #Validations
  # => Relations
  validates :contest_id, :presence => true
  validates :room_id, :presence => true
  validates :selected_idea_id, :presence => true
  validates :ignored_idea_id, :presence => true
  
  scope :by_vote_user_and_room, lambda { |user_id, room_id| where(:room_id => room_id, :user_id => user_id, :status => ["vote", "skip_vote"] ) }
  scope :by_user, lambda { |user| 
    if user.nil?
      user = User.default_guest_user
    end
      where(:user_id => user.id) }
  
  
  class << self
    def add_record user, idea1, idea2, status
      record = self.new
      record.contest_id = idea1.contest_id 
      record.room_id = idea1.room_id
      record.user_id = user.id
      record.selected_idea_id = idea1.id
      record.ignored_idea_id = idea2.id
      record.status = status
      record.save
    end
  end

end
