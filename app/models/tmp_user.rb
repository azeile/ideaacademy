# == Schema Information
#
# Table name: tmp_users
#
#  id         :integer          not null, primary key
#  session_id :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class TmpUser < ActiveRecord::Base
  has_many :tmp_log_votes
  has_many :tmp_user_idea_rates
  has_many :tmp_statistic_category_users
  has_many :tmp_room_users
  
  has_many :organization_users, :dependent => :destroy
  has_many :organizations, :through => :organization_users
  
  after_save :create_categories
  before_destroy :remove_all_data
  
  def create_categories
    categories = Category.all
    categories.each do |category|
      statistic = TmpStatisticCategoryUser.find_or_initialize_by_category_id_and_tmp_user_id( category.id, id )
      statistic.save if statistic.new_record?
    end
  end
  
  def remove_all_data
    tmp_log_votes.destroy_all
    tmp_user_idea_rates.destroy_all
    tmp_statistic_category_users.destroy_all
    tmp_room_users.destroy_all
  end
  
  def convert_info_to_real_user user
    tmp_room_users.each do |room_user|
      room = room_user.contest.add_user_to_room( user, room_user.room.id )
      if room == room_user.room
        tmp_log_votes.where(:room_id => room.id ).each do |tmp_log_vote|
          selected_idea_id = tmp_log_vote.status == 'vote' ? tmp_log_vote.selected_idea_id : nil
          room.vote_idea user, tmp_log_vote.selected_idea_id, tmp_log_vote.ignored_idea_id, selected_idea_id
        end
      end

      # popups = TmpUserPopup.find(:all, :conditions => { :tmp_user_id => self.id, :typekey => "first_points_vote" })
      # popups.each do |popup|
      #   new_popup = UserPopup.new :user_id => user.id, :seen => true, :typekey => popup.typekey
      #   new_popup.save
      #   popup.delete
      # end
      # popups = UserPopup.where("user_id = ?", user.id)
      # popups.destroy_all

      UserPopup.update_all("seen = TRUE", "user_id = #{user.id} AND typekey IN ('first_points_vote')")
    end
    self.destroy
  end
  
  class << self
    def delete_old_users
      inactive_users = self.find(:all, :conditions => "updated_at <= NOW() - INTERVAL '24 HOURS'" )
      inactive_user_size = inactive_users.size
      inactive_users.each do |inactive_user|
        inactive_user.destroy
      end
      inactive_user_size
    end
  end
end
