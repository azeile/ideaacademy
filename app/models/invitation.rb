# == Schema Information
#
# Table name: invitations
#
#  id              :integer          not null, primary key
#  email           :string(255)      not null
#  organization_id :integer          not null
#  inviter_id      :integer          not null
#  created_at      :datetime
#  updated_at      :datetime
#

class Invitation < ActiveRecord::Base

  belongs_to :organization
  belongs_to :inviter, class_name: "User", polymorphic: true, foreign_key: "inviter_id"

  validates :email, presence: true, email: true
  validates :organization, presence: true
  validates :inviter_id, presence: true

  validate :validate_unique_email

  ### VALIDATIONS ###
  def validate_unique_email
    begin
      User.find_by_email! email
      AdminUser.find_by_email! email
    rescue
    else
      errors.add :email, "email already taken"
    end
  end

  def inviter_email
    begin
      user = AdminUser.find inviter_id
    rescue
      user = User.find(inviter_id)
    end
    user.email || ""
  end
end
