# == Schema Information
#
# Table name: service_provider_ids
#
#  id                        :integer          not null, primary key
#  service_id                :integer
#  service_provider_name     :string(255)
#  environment               :string(255)
#  service_social_network_id :string(255)
#  created_at                :datetime
#  updated_at                :datetime
#

class ServiceProviderId < ActiveRecord::Base
  default_scope where("environment = ?", Rails.env)
end
