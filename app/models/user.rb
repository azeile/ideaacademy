# == Schema Information
#
# Table name: users
#
#  id                         :integer          not null, primary key
#  first_name                 :string(255)
#  last_name                  :string(255)
#  email                      :string(255)
#  website                    :string(255)
#  phone                      :string(255)
#  image_url                  :string(255)
#  created_at                 :datetime
#  updated_at                 :datetime
#  about                      :text
#  work_tag                   :boolean          default(FALSE)
#  receive_notifications      :boolean          default(FALSE)
#  earned_money               :integer          default(0)
#  image_file_name            :string(255)
#  image_content_type         :string(255)
#  image_file_size            :integer
#  image_updated_at           :datetime
#  checked_out_amount         :integer          default(0)
#  top5_cups                  :integer          default(0)
#  block_reason               :string(255)
#  blocked_at                 :datetime
#  blocked_till               :datetime
#  unlimited_ideas_expires_at :datetime
#  encrypted_password         :string(255)      default("")
#  reset_password_token       :string(255)
#  reset_password_sent_at     :datetime
#  remember_created_at        :datetime
#  sign_in_count              :integer          default(0)
#  current_sign_in_at         :datetime
#  last_sign_in_at            :datetime
#  current_sign_in_ip         :string(255)
#  last_sign_in_ip            :string(255)
#  authentication_token       :string(255)
#  title                      :string(255)
#  gender                     :string(255)
#  invitation_token           :string(60)
#  invitation_sent_at         :datetime
#  invitation_accepted_at     :datetime
#  invitation_limit           :integer
#  invited_by_id              :integer
#  invited_by_type            :string(255)
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  include SharedOrganizationMethods
  include EduUser

  # Includes
  include ActionView::Helpers::SanitizeHelper  

  DEFAULT_GUEST_USER_ID = 2

  #Relations
  has_many :organization_users, :dependent => :destroy
  has_many :organizations, :through => :organization_users
  has_one :admin_user, :dependent => :destroy

  has_many :providers, :class_name => "UserProvider", :dependent => :destroy
  has_many :user_friends, :class_name => "UserFriend"
  has_many :friends, :through => :user_friends
  # has_many :user_badges
  has_many :badges, :through => :user_badges
  has_many :room_users
  has_many :rooms, :through => :room_users
  has_many :ideas
  has_many :statistic_category_users, :dependent => :destroy
  has_many :statistic_category_ordered_users, :dependent => :destroy
  has_many :statistic_contest_room_users
  has_many :statistic_contest_users
  has_many :payments
  has_many :withdrawals
  has_many :spam_reports

  has_many :sidebar_messages_views, :foreign_key => :viewer_id
  has_many :seen_sidebar_messages, :through => :sidebar_messages_views, :source => :sidebar_message
  has_many :subscriptions, :as => :subscriber, :dependent => :destroy
  has_many :log_activities
  has_many :popups, :class_name => "UserPopup", :dependent => :destroy
  has_many :languages, :class_name => "UserLanguage"

  has_many :messages, class_name: "UserMessage", as: :sender
  has_many :received_messages, class_name: "UserMessage", as: :receiver

  has_attached_file :image,
    :styles => { :smaller => "38x38#", :medium => "125x125#" }
      
  #Validations
  # => Relations
  # validates :origin_provider_id, :presence => true

  #devise
  devise *DEVISE_MODULES

  # => Attributes
  attr_accessible :first_name, :last_name, :email, :phone, :website, :work_tag, :about, :blocked_till, :image_url
  attr_accessible :block_reason, :password, :password_confirmation, :remember_me, :title, :organization_users_attributes
  attr_accessible :organization_ids, :user_type

  # accepts_nested_attributes_for :organizations
  accepts_nested_attributes_for :organization_users
  validates             :email,       :presence   => { :if      => Proc.new { |user| AppUtils.internal_app_env? } },
                                      :uniqueness => { :unless  => :email_empty? },
                                      :email      => { unless: :email_empty? }

  validates_associated  :organization_users, :on => :create
  validates_associated  :organizations,      :on => :create

  validates_presence_of     :first_name, :last_name
  validates_presence_of     :password, :password_confirmation, :if => Proc.new { |u| u.password_validation_required? }
  validates_confirmation_of :password,                         :if => Proc.new { |u| u.password_validation_required? }

  validation_group :invite_accepted, :if => :invite_accepted? do
    validates                 :first_name,  :presence => true
    validates                 :last_name,   :presence => true # If not twitter? Oh, no! Bye, bye~ twitter, not this time!
  end

  after_initialize  :set_defaults
  before_save       :strip_html, :blocking
  after_create      :add_vip_if_internal, :add_user_language, :post_to_wall_if_facebook
  after_update      :sync_accounts

  scope :recent, lambda { |limit = 10| order("users.created_at DESC").limit(limit) }
  scope :without_languages,     where("NOT EXISTS (SELECT 1 FROM user_languages where user_id = users.id )")
  scope :not_subscribed_to_vip, 
    where("NOT EXISTS (SELECT 1 FROM subscriptions ss, services s WHERE ss.subscriber_id = users.id AND s.id = ss.service_id AND s.code = 'VIP_ACCOUNT_LIFELONG')")
  
  class << self
    
    def default_guest_user
      User.find 2
    end

    def find_for_facebook_oauth(access_token, signed_in_resource=nil)
      data = access_token.extra.raw_info
      if user = self.find_by_email(data.email)
        user
      else # Create a user with a stub password. 
        self.create!(:email => data.email, :password => Devise.friendly_token[0,20])
      end
    end
    
    def new_with_session(params, session)
       super.tap do |user|
         if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
           user.email = data["email"]
         end
       end
    end
    
    def create_fb_user opts
      user         = opts[:user]
      user_provider = opts[:user_provider]
      
      case 
        when user then user_provider.user = user
        when user = user_provider.user
          user.image_url = opts[:image] if opts[:image]
          user.save!
        else
          user = new( :first_name         => opts[:first_name],
                      :last_name          => opts[:last_name],
                      :image_url          => opts[:image] ? opts[:image] : nil,
                      :gender             => opts[:gender].present? ? opts[:gender].to_s.downcase : "male"
                      )

          user.set_default_organization Organization.find_by_name("IdeaAcademy")
          user.providers << user_provider
          user.save!
          UserPopup.create!({ :user_id => user.id, :typekey => "welcome_user", :seen => false })
      end
      
      user
    end
    
    def new_with_admin params
      user = User.new params
      user.providers.build([{:name => "devise", :user => user}])
      if AppUtils.internal_app_env?
        user.build_admin_user(:email => user.email, :password => user.password)
        user.save
      else
        user.save
        user.set_default_organization Organization.find_by_name("IdeaAcademy")
        UserPopup.create!({ :user_id => user.id, :typekey => "welcome_user", :seen => false })
      end
      user
    end
  end
  
  def password_validation_required?
    new_record? || (not encrypted_password.present?)
  end

  def strip_html
    for column in User.content_columns
      if column.type == :string || column.type == :text
        if self[column.name] 
         self[column.name] = strip_tags(self[column.name])
        end
      end
    end
  end
  
  def origin_provider
    providers.where("originated_from_flag = ?", true).first
  end 

  def email_empty?
    email.nil? || email.empty? ? true : false 
  end

  def profile_image size = "medium"
    image_file_name.blank? ? image_url || ApplicationSetting["DEFAULT_FB_#{gender.upcase}_PICTURE"] : image.url(size)
  end

  begin "ideas related methods"  
    def ideas_added category_id
      ideas.where("category_id = #{category_id}").size
    end
    
    def ideas_in_top_three category_id
      ideas.where("category_id = #{category_id} AND in_topthree = true ").size
    end
  end

  def check_out_money account
    if self.earned_money.to_i >= ApplicationSetting["MINIMUM_TO_CHECK_OUT"] && account
      amount = self.earned_money.to_f
      if Withdrawal.create(:status => false, :user => self, :account => account, :amount => amount, :currency => SYSTEM_CURRENCY, :name => self.to_s )
        self.checked_out_amount = self.checked_out_amount + self.earned_money
        self.earned_money = 0
        self.save
      end
    end
  end

  def to_s 
    "#{self.first_name} #{self.last_name}"
  end 
  alias_attribute :display_name, :to_s # for activeadmin gem, display_name_methods are available in /lib/application.rb in gem source

  def as_json var
   {
      :id => id,
      :first_name => first_name,
      :last_name => last_name,
      :prefix => title_prefix,
      :about => about,
      :phone => phone,
      :website => website,
      :email => email,
      :image_url => profile_image,
      :work_tag => work_tag,
      :earned_money => "%.2f" % (earned_money / 100.0),
      :can_check_out => self.earned_money.to_i >= ApplicationSetting["MINIMUM_TO_CHECK_OUT"],
      :top5_cups => top5_cups,
	    :blocked_till => blocked_till ? I18n.l(blocked_till, :format => :short) : nil,
      :block_reason => block_reason,
      :blocked => blocked?,
      :hide_auth_buttons => Rails.env.draugiem? || Rails.env.facebook?, # || Rails.env.development?
      :unlimited_ideas_expires_at => unlimited_ideas_expires_at,
      :vip => vip?,
      :user_type => user_type
    }
  end

  def language_array
    languages.map(&:language)
  end
  
  def highest_level
    statistic_category_users.maximum(:level)
  end

  def title_prefix level = nil
    level ||= highest_level
    I18n.t("badge_prefixes.level_#{level}") + " " if vip? && level && level >= 3
  end

  def level_for_category category
    statistic_category_users.where(:category_id => category.id).first.try(:level).to_i
  end

  def level_sufficient_for_contest? contest
    level_for_category(contest.category) >= ApplicationSetting["SUFFICIENT_USER_LEVEL"]
  end
  
  def level_sufficient_to_report_spam? contest
    level_sufficient_for_contest? contest
  end
  
  def blocked?
    blocked_till && ( blocked_till > DateTime.now )
  end

	def allowed_to_add_ideas?
    subscribed_to_any?(*Service::UNLIMITED_IDEAS_SERVICES) || 
    ( Idea.today_added_by_user(id).count < ApplicationSetting["IDEA_LIMIT_PER_DAY"] )
  end
  
  def unlimited_ideas_expires_at
    subscriptions.active_or_future.by_service_codes(Service::UNLIMITED_IDEAS_SERVICES).maximum(:date_to) unless AppUtils.internal_app_env?
  end

  def draugiem_provider
    self.providers.where(:name => "draugiem").order("updated_at DESC").first
  end
  
  def subscribe_to_service service_code
    Service.subscribe service_code, self
  end
  
  def subscribed_to_any? *args
    return false if args.empty?
    subscriptions.active.by_service_codes(args).present?
  end
  
  def subscribed_to? service_code
    subscriptions.active.by_service_code(service_code).present?
  end
  
  def vip?
    subscribed_to_any? "VIP_ACCOUNT_ANNUAL", "VIP_ACCOUNT_MONTHLY", "VIP_ACCOUNT_LIFELONG"
  end
  
  def invite_accepted?
    AppUtils.internal_app_env? && !new_record?
  end

  def seen_sidebars_ids
    seen_sidebar_messages.select "sidebar_messages.id"
  end

  def add_language language
    UserLanguage.create(:user => self, :language => language)
  end

  def array_of_user_languages list
    languages.in_list(list).select(:language).map(&:language)
  end

  def update_profile user_params, languages_params
    transaction do
      begin
        update_attributes(user_params)
        languages.not_in_list(languages_params).destroy_all
        languages_params = languages_params - array_of_user_languages(languages_params)
        languages.build(languages_params.map {|val| { :language => val, :user => self } })
        save
      rescue ActiveRecord::Rollback
        errors.add_to_base I18n.t("activerecord.errors.models.service.general")
        return false
      end
    end 
  end

  def facebook_provider
    providers.first_by_name("facebook").first
  end

  def sent_messages
    UserMessage.where({sender_id: self.id})
  end

  def received_messages
    UserMessage.where({receiver_id: self.id})
  end

  ######   PRIVATE PRIVATE   ######
  private
  
  def blocking
    self.blocked_at = DateTime.now if blocked_till && blocked_at.nil?
  end
  
  def set_defaults
    self.gender = "male" if new_record? && gender.nil?
  end
  
  def add_vip_if_internal
    subscribe_to_service "VIP_ACCOUNT_LIFELONG" if AppUtils.internal_app_env?
  end

  def add_user_language
    transaction do
      add_language "English"
      add_language "Latvian"
    end
  end

  def post_to_wall_if_facebook
    FacebookActivity.user_joined_academy(self) if AppUtils.facebook_env? and self.facebook_provider
  end

end
