# == Schema Information
#
# Table name: user_badges
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  badge_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class UserBadge < ActiveRecord::Base
  belongs_to :user
  belongs_to :badge

  validates :user_id, :presence => true
  validates :badge_id, :presence => true

  class << self
    #FIXME It is recommended that user/badge award logging functionality would be moved to this model
    #as it does not exist yet, this method uses UserPopup model.
    def recent_badges(user, count = 3)
      puts user
      popups = UserPopup.badge().same_organization(user)
      popups = popups ? popups.order("created_at DESC").limit(count) : ""
      popups.map do |popup|
        { :image_url => popup.user && popup.user.profile_image, 
          :level => popup.new_achievement_level - 1, 
          :level_text => I18n.t("badges.level_#{popup.new_achievement_level}"), 
          :category => popup.category, 
          :name => popup.user.to_s, 
          :user_id => popup.user.id, 
          :prefix => popup.user.title_prefix(popup.new_achievement_level) 
        }
      end
    end
  end
end
