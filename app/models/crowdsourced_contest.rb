# == Schema Information
#
# Table name: crowdsourced_contests
#
#  id                        :integer          not null, primary key
#  title                     :string(255)
#  description               :text
#  category_id               :integer
#  money_flag                :boolean
#  award                     :string(255)
#  typekey                   :string(255)
#  users_per_room            :integer          default(200)
#  rooms_count               :integer
#  active_flag               :boolean
#  ends_at                   :datetime
#  language                  :string(255)
#  smaller_logo_file_name    :string(255)
#  smaller_logo_content_type :string(255)
#  smaller_logo_file_size    :integer
#  smaller_logo_updated_at   :datetime
#  logo_file_name            :string(255)
#  logo_content_type         :string(255)
#  logo_file_size            :integer
#  logo_updated_at           :datetime
#  created_at                :datetime
#  updated_at                :datetime
#  organization_id           :integer
#  sent_flag                 :boolean
#

require 'net/http'

class CrowdsourcedContest < ActiveRecord::Base
  has_one :contest_request, :as => :parent
  has_one :contest_image, :as => :imageable
  has_many :crowdsourcing_results
  belongs_to :organization
  belongs_to :facebook_category, :inverse_of => :crowdsourced_contests, :foreign_key => :category_id

  has_attached_file :logo,
    :styles => { :thumbnail => "390x175#" }

  has_attached_file :smaller_logo,
    :styles => { :thumbnail => "75x75#" }

  scope :active, where("active_flag = ? ", true)
  scope :done, where(" ends_at <= NOW() ")
  scope :without_results, where( "NOT EXISTS (SELECT 1 FROM crowdsourcing_results cr where cr.crowdsourced_contest_id = crowdsourced_contests.id )" )
  scope :by_organization, lambda {|organization|
    active.where("organization_id = ?", organization.id)
  }

  scope :first_crowdsourced_contest , lambda { |organization|
    by_organization(organization).order("id asc")
  }
  
  scope :last_crowdsourced_contest , lambda { |organization|
    by_organization(organization).reorder("id desc")
  }
  
  scope :next_crowdsourced_contest , lambda { |organization, current_id|
    by_organization(organization).where("crowdsourced_contests.id > ?", current_id)
  }
  
  scope :previous_crowdsourced_contest , lambda { |organization, current_id|
    by_organization(organization).where("crowdsourced_contests.id < ?", current_id).reorder("id desc")
  }

  validates :category_id, :presence => true
  validates :organization_id, :presence => true
  validates_attachment_size :logo, :less_than => 5.megabytes
  validates_attachment_content_type :logo, :content_type => ["image/jpeg", "image/jpg", "image/png"]
  
  validates :title, :presence => true, :length => { :minimum => 3, :maximum => 100 }
  validates :description, :presence => true
  # validates :award, :presence
  validates :typekey, :presence => true
  validates :ends_at, :presence => true
  validate  :correct_ends_at
  validates :language,  :presence   => { :if  => Proc.new { |user| AppUtils.facebook_env? } }, 
                        :inclusion  => { :in => ["Latvian", "English"]}

  after_initialize :set_defaults
  after_create :update_contest_image
  attr_accessor :contest_image_id
  after_save :send_to_fb_if_active

  UPDATEABLE_FB_FIELDS = [ :title, :description, :active_flag, :ends_at ]
  UPDATEABLE_FB_FIELDS_ON_CREATE = [ :rooms_count, :users_per_room, :language, :award, :typekey, :category_id ] + UPDATEABLE_FB_FIELDS
  SPECIAL_UPDATEABLE_FIELDS = [ :logo_url, :smaller_logo_url ]
  SPECIAL_UPDATEABLE_FIELDS_ON_CREATE = [ :organization_name, :origin_id ] + SPECIAL_UPDATEABLE_FIELDS

  def active?
    active_flag
  end

  def attribute_value attrib
    val = send(attrib)
    return val.name if val.respond_to?( :name )
    val
  end

  def logo_image
    logo.url if logo
    contest_image.logo.url(:thumbnail) unless contest_image.nil?
  end

  def as_json options={}
    json = { :contest => {} }
    
    special_fields = {
      :organization_name => organization.name,
      :logo_url          => logo_image,
      :smaller_logo_url  => ( smaller_logo.url if smaller_logo && !smaller_logo.url.match(/\/missing\.png/) ),
      :origin_id         => id
    }

    json[:contest] = UPDATEABLE_FB_FIELDS_ON_CREATE.inject({}) do |memo, val|
      memo[val] = self.send val
      memo
    end

    json[:contest].merge! special_fields
    json
  end

  def send_to_facebook
    params = self.as_json

    params[:contest] = if sent?
      UPDATEABLE_FB_FIELDS + SPECIAL_UPDATEABLE_FIELDS
    else
      UPDATEABLE_FB_FIELDS_ON_CREATE + SPECIAL_UPDATEABLE_FIELDS_ON_CREATE
    end.inject({}) { |memo, val|
      memo[val] = params[:contest][val]
      memo
    }

    main_path = sent? ? "/api/crowdsourced_contests/#{id}" : "/api/crowdsourced_contests"
    path      = "#{main_path}?api_id=#{CROWDSOURCING_CONF[:api_id]}&api_key=#{CROWDSOURCING_CONF[:api_key]}"

    req       = (sent? ? Net::HTTP::Put : Net::HTTP::Post).new(path, initheader = {'Content-Type' =>'application/json'})
    req.body  = params.to_json
    response  = Net::HTTP.new(CROWDSOURCING_CONF[:facebook_host], CROWDSOURCING_CONF[:facebook_port]).start {|http| http.request(req) }
    Rails.logger.debug "Response #{response.code} #{response.message}:
    #{response.body}"

    return false unless response.code == "200"
    
    json = JSON.parse(response.body)
    return json["status"] == 'OK'
  end

  def get_results_from_facebook
    main_path = "/api/contests/#{id}/crowdsourcing_results/get_from_facebook"
    path      = "#{main_path}?api_id=#{CROWDSOURCING_CONF[:api_id]}&api_key=#{CROWDSOURCING_CONF[:api_key]}"

    req       = Net::HTTP::Get.new(path, initheader = {'Content-Type' =>'application/json'})
    response  = Net::HTTP.new(CROWDSOURCING_CONF[:facebook_host], CROWDSOURCING_CONF[:facebook_port]).start {|http| http.request(req) }

    results = JSON.parse response.body
    
    transaction do
      results.each { |res| CrowdsourcingResult.create! res }
    end

    Rails.logger.info "Response #{response.code} #{response.message}:
    #{response.body}"
  end

  def sent?
    sent_flag
  end

  def previous organization = nil
    @previous ||= CrowdsourcedContest.previous_crowdsourced_contest(organization, id).first || CrowdsourcedContest.last_crowdsourced_contest(organization).first
  end

  def next organization = nil
    @next ||= CrowdsourcedContest.next_crowdsourced_contest(organization, id).first || CrowdsourcedContest.first_crowdsourced_contest(organization).first
  end

  def to_json_with_next_and_previous organization
    next_contest     = self.next organization
    previous_contest = self.previous organization
    
    {
      :id                          => id,
      :category_id                 => category_id,
      :category                    => facebook_category.name,
      :ideas_count                 => crowdsourcing_results.count,
      :title                       => title,
      :status                      => status,
      :typekey                     => typekey,
      :time_to_end                 => time_to_end,
      :logo                        => logo_image,
      :smaller_logo                => smaller_logo? ? smaller_logo.url(:thumbnail) : "",
      :description                 => description,
      :next_id                     => next_contest.id,
      :next_title                  => next_contest.title,
      :next_time_to_end            => next_contest.time_to_end,
      :previous_id                 => previous_contest.id,
      :previous_title              => previous_contest.title,
      :previous_time_to_end        => previous_contest.time_to_end
    }
  end
  
  def status
    case
      when ends_at > DateTime.now then (active? ? "live" : "draft")
      when ends_at <= DateTime.now then "done"
    end
  end
  
  def time_to_end
    diff_seconds = ( ends_at - Time.now ).round

    "%02d : %02d" % [diff_seconds / 3600, ( diff_seconds % 3600) / 60 ] if diff_seconds > 60
  end

  private

  def correct_ends_at
    errors.add(:ends_at, I18n.t("activerecord.errors.models.contest.attributes.ends_at.bad")) if ends_at < DateTime.now
  end

  def set_defaults
    self.sent_flag    = false if sent_flag.nil? 
    self.active_flag  = false if active_flag.nil?
    self.rooms_count  = 0     if rooms_count.nil?
  end

  def update_contest_image
    return if self.contest_image_id.nil? || self.contest_image_id.empty?
    contest_image = ContestImage.find(contest_image_id)
    contest_image.contest = self
    contest_image.save
  end

  def send_to_fb_if_active
    if active? || active_flag_changed?
      result = send_to_facebook 
      sent? ? result : result && update_attributes(:sent_flag => true)
    else
      true
    end
  end
end
