#encoding: UTF-8
class Payment
  
    # reuqest
    # action: transactions/create
    # app:  aplikācijas API atslēga (32 simboli)
    # apikey: lietotāja, kurš veic maksājumu, API atslēga (32 simboli)
    # service:  pakalpojuma ID, kas iegūts, izveidojot maksas pakalpojumu
    # price:  pakalpojuma cena santīmos/draugiem.lv kredītos 
    # (maksimālā vērtība 2000 - karšu maksājumiem, 1000 - kredītu maksājumiem, SMS maksājumiem šis parametrs nav jānorāda, jo cena ir fiksēta)
  
    # response after payment :
    # id: transakcijas ID
    # service:  maksas pakalpojuma ID
    # uid:  lietotāja ID
    # price:  maksājuma cena santīmos
    # status: vērtība ok
  
  class << self
    def request_params service_id, current_user
      service = Service.find service_id
      { :action => "transactions/create", 
        :app => DRAUGIEM_APP_KEY, 
        :apikey => current_user.providers.find_by_name("draugiem").token,
        :service => service.draugiem_id, 
        :price => service.price_in_santims
      }
    end
    
    def response_valid? params, transaction_id, requested_service_id
      Rails.logger.debug "params[:service] = #{params[:service]}, requested_service_id = #{requested_service_id}"
      Rails.logger.debug "params[:id] = #{params[:id]}, transaction_id = #{transaction_id}"
      params[:service] == requested_service_id && params[:id].to_i == transaction_id && params[:status].match(/^ok$/i)
    end
    
    def service_attribute service_id, attrib
      SERVICES.detect{ |s| s[:service_id] == service_id }[attrib]
    end
    
    def services
      case 
        when AppUtils.facebook_env? then Service.not_internal
        else Service.internal
      end
    end
    
    def vip_services
      Service.not_internal.vips
    end
  end
end
