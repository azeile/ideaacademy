# == Schema Information
#
# Table name: subscriptions
#
#  id              :integer          not null, primary key
#  service_id      :integer
#  subscriber_id   :integer
#  date_from       :datetime
#  date_to         :datetime
#  created_at      :datetime
#  updated_at      :datetime
#  subscriber_type :string(255)
#

class Subscription < ActiveRecord::Base
  belongs_to :service
  belongs_to :subscriber, :polymorphic => true
  
  scope :active, lambda { where("? BETWEEN date_from AND date_to", Time.now.to_datetime) }
  scope :active_or_future, lambda { where(" ? <= date_to", Time.now.to_datetime) }
  scope :by_service_code, lambda {|service_code| joins(:service).where("services.code = ?", service_code) }
  scope :by_service_codes, lambda {|service_codes| joins(:service).where("services.code in ( ? )", service_codes) }
  scope :by_user, lambda {|user_id| where("user_id", user_id) }
end
