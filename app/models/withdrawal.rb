# == Schema Information
#
# Table name: withdrawals
#
#  id         :integer          not null, primary key
#  status     :boolean
#  amount     :float
#  currency   :string(255)
#  user_id    :integer
#  account    :string(255)
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Withdrawal < ActiveRecord::Base
  belongs_to :user
end
