# == Schema Information
#
# Table name: spam_reports
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  idea_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class SpamReport < ActiveRecord::Base
  belongs_to :user
  belongs_to :idea
  
  validates_uniqueness_of :idea_id, :scope => :user_id
  
  def text
    self.idea.text
  end
  
  def idea_type
    self.idea.typekey
  end
    
  def user_full_name
    "#{self.user.first_name} #{self.user.last_name}" 
  end
  
  def user_email
    "#{self.user.email}" 
  end
end
