# == Schema Information
#
# Table name: tmp_log_votes
#
#  id               :integer          not null, primary key
#  contest_id       :integer
#  room_id          :integer
#  selected_idea_id :integer
#  ignored_idea_id  :integer
#  tmp_user_id      :integer
#  status           :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

class TmpLogVote < ActiveRecord::Base
  #Relations
  belongs_to :contest
  belongs_to :room
  belongs_to :selected_idea, :class_name => "Idea", :foreign_key => "selected_idea_id"
  belongs_to :ignored_idea, :class_name => "Idea", :foreign_key => "ignored_idea_id"
  belongs_to :tmp_user
  
  #Validations
  # => Relations
  validates :contest_id, :presence => true
  validates :room_id, :presence => true
  validates :selected_idea_id, :presence => true
  validates :ignored_idea_id, :presence => true
  
  scope :by_vote_user_and_room, lambda { |tmp_user_id, room_id| where(:room_id => room_id, :tmp_user_id => tmp_user_id ) }
  
  class << self
    def add_record tmp_user, idea1, idea2, status
      record = self.new
      record.contest_id = idea1.contest_id 
      record.room_id = idea1.room_id
      record.tmp_user_id = tmp_user.id
      record.selected_idea_id = idea1.id
      record.ignored_idea_id = idea2.id
      record.status = status
      record.save
    end
  end
end
