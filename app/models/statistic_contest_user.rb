# == Schema Information
#
# Table name: statistic_contest_users
#
#  id           :integer          not null, primary key
#  contest_id   :integer
#  user_id      :integer
#  vote_points  :integer
#  idea_points  :integer
#  total_points :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class StatisticContestUser < ActiveRecord::Base

  #Relations
  belongs_to :contest
  belongs_to :user

  #Validations
  # => Relations
  validates :contest_id, :presence => true
  validates :user_id, :presence => true

  #Scopes
  scope :list_by_contest_id, lambda { |contest_id| where(:contest_id => contest_id).order("total_points DESC") }

end
