# == Schema Information
#
# Table name: statistic_category_users
#
#  id           :integer          not null, primary key
#  category_id  :integer
#  user_id      :integer
#  total_points :integer
#  level        :integer
#  created_at   :datetime
#  updated_at   :datetime
#  vote_points  :integer
#  idea_points  :integer
#  live_points  :integer
#

class StatisticCategoryUser < ActiveRecord::Base

  #Relations
  belongs_to :category
  belongs_to :user
  belongs_to :badge, :foreign_key => "level"

  #Validations
  # => Relations
  validates :category_id, :presence => true
  validates :user_id, :presence => true

  #Scopes 
  scope :active_categories, joins(:category).where("categories.active_flag = ? ", true)
  scope :list_by_user_id, lambda { |user_id| 
    active_categories.where("statistic_category_users.user_id = ? ", user_id).order("category_id").readonly(false)
  }
  scope :list_by_category_id, lambda { |category_id| 
    # active_categories.where("categories.id = ? ", category_id).readonly(false)
    where("category_id = ?", category_id)
  }
  
  #Admin scopes
  scope :by_fb_id, lambda { |fb_id|
    joins(:user => :providers).where(:user_providers => { :name => 'facebook', :social_id => fb_id })
  }
  
  scope :by_draugiem_id, lambda { |draugiem_id|
    joins(:user => :providers).where(:user_providers => { :name => 'draugiem', :social_id => draugiem_id })
  }
  
  search_methods :by_fb_id
  search_methods :by_draugiem_id
  
  after_save :manual_level_update
  attr_accessor :points_manually_changed

  CLOSEST_ARRAY = [-3,-2,-1,1,2,3]
  #Methods
 
  def add_points_vote
    current_vote_points = vote_points.nil? ? 0 : vote_points
    current_total_points = total_points.nil? ? 0 : total_points
    current_live_points = live_points.nil? ? 0 : live_points

    self.vote_points = current_vote_points + POINTS_VOTE
    self.total_points = current_total_points + POINTS_VOTE
    self.save

    # Points difference, for header
    self.live_points = current_live_points + (self.total_points - current_total_points)
    self.save
    
    update_user_level
  end
  
  def add_points_idea
    current_idea_points = idea_points.nil? ? 0 : idea_points
    current_total_points = total_points.nil? ? 0 : total_points
    current_live_points = live_points.nil? ? 0 : live_points

    self.idea_points =  current_idea_points + POINTS_IDEA
    self.total_points =  current_total_points + POINTS_IDEA
    self.save    

    # Points difference, for header
    self.live_points = current_live_points + (self.total_points - current_total_points)
    self.save

    update_user_level
  end
  
  def update_user_level    
    current_badge = Badge.current_level(total_points).first
    unless badge == current_badge
      self.level = current_badge.id
      self.save

      if self.level >= 3
        if self.level == LEVEL_TO_RECEIVE_MONEY
          typekey = "new_money_achievement"
        else
          typekey = "new_achievement"
        end

        popup = UserPopup.new :user_id => self.user.id, :typekey => "#{typekey}", :category => self.category.name, 
                              :new_achievement_level => self.level, :new_achievement_message => I18n.t("popup_achievement_messages.level_#{self.level}", :category_name => category.name), 
                              :category => category.name
        popup.save

        # FIXME: Needs to be ported to FB
        #DraugiemApiRequest.add_earned_badge_activity self.user, self.level, self.category.name
        # FacebookActivity.user_reached_new_level( user, level, category.name ) if AppUtils.facebook_env?
      end
    end
  end

  def check_activity typekey
    @current = 0
    @total = self.category.contests.without_drafts.count
    
    self.category.contests.each do |contest|
      if contest.check_activity_for(typekey, self.user) == true
        @current = @current + 1
      end
    end

    return (@current.to_f/@total.to_f * 100).round if @current && @total && @total > 0
    return 0
  end

  def get_idea_effect
    ideas = Idea.where("user_id = ? AND category_id = ?", self.user.id, self.category.id)
    
    duels_won = ideas.sum(:duels_won).nil? ? 0 : ideas.sum(:duels_won)
    duels_in = ideas.sum(:duels_in).nil? ? 0 : ideas.sum(:duels_in)
    
    return 0 if duels_won == 0 && duels_in == 0 || duels_won == 0 || duels_in == 0


    percents = (duels_won.to_f/duels_in.to_f * 100).round
    
    return percents
  end

  def as_json(options={})
    {
      :id => self.category.id, 
      :name => category.name,
      :idea_points => self.idea_points.nil? ? 0 : self.idea_points,
      :vote_points => self.vote_points.nil? ? 0 : self.vote_points,
      :total_points => self.total_points.nil? ? 0 : self.total_points,
      :level => self.badge.nil? ? 1 : self.badge.image,
      :percents => self.badge.nil? ? 0 : self.badge.percents(self.total_points),
      :vote_activity_percents => self.check_activity("vote"),#self.vote_percents,
      :idea_activity_percents => self.get_idea_effect,#self.idea_percents,
      :contests => self.category.contests.size
    }
  end
  
  #columns for active_admin
  def category_name
   category.name
  end
  
  def user_name
    "#{user.first_name} #{user.last_name}" 
  end
  
  def user_provider_original_id provider_name
    user.providers.where(:name => provider_name).first.try(:social_id)
  end
  
  def user_fb_id
    user_provider_original_id('facebook')
  end
  
  def user_draugiem_id
    user_provider_original_id('draugiem')
  end
   
  class << self
    def get_points user_id
      statistics = self.list_by_user_id( user_id )
      sum_vote_points = 0
      sum_idea_points = 0
      statistics.each do |statistic|
        sum_vote_points = sum_vote_points + ( statistic.vote_points.nil? ? 0 : statistic.vote_points )
        sum_idea_points = sum_idea_points + ( statistic.idea_points.nil? ? 0 : statistic.idea_points )
      end
      statistics.each do |statistic|
        statistic[:vote_percents] = sum_vote_points > 0 ? ( ( statistic.vote_points.to_f / sum_vote_points ) * 100 ).round() : 0
        statistic[:idea_percents] = sum_idea_points > 0 ? ( ( statistic.idea_points.to_f / sum_idea_points ) * 100 ).round() : 0
      end
      statistics
    end

    def get_statistics category, user, page
      page        = 1 unless page
      limit       = 20
      pre_offset  = limit.to_i * page.to_i
      offset      = page == 0 ? 0 : pre_offset.to_i - limit.to_i
      @current_user = user

      statistics    = where("total_points > 0 AND category_id = #{category.id}").order("total_points DESC").limit(limit).offset(offset)
      total_players = where("total_points > 0 AND category_id = #{category.id}").count

      prepare_json statistics, total_players, offset
    end

    private

    def prepare_json statistics, total_players, offset
      @players = []
      
      offset = 0 unless offset

      show_friends = false
      show_more = false
      me = false

      statistics.each_with_index do |statistic, index|
        unless statistic.user.nil?
          friend = @current_user ? ( @current_user.user_friends.where(:friend_id => statistic.user_id).empty? ? false : true) : false
          me = index if me?(statistic.user)
          show_friends = true if show_friends == false && friend

          order = offset + index + 1

          @players << {
            :order => order,
            :first_name => statistic.user.first_name,
            :last_name => statistic.user.last_name,
            :points => statistic.total_points,
            :vote_points => statistic.vote_points.nil? ? 0 : statistic.vote_points,
            :idea_points => statistic.idea_points.nil? ? 0 : statistic.idea_points,
            :image => statistic.user.profile_image,
            :badge_image => statistic.badge.image, 
            :friend => friend,
            :me => me?( statistic.user ),
            :close_friend => false, 
            :close_user => false,
            :show => me?(statistic.user) || [1,2,3].include?( order ) ? true : false,
            :user_id => statistic.user.id
          }
        end
      end
      set_closest_friends(me) if show_friends
      set_closest_users(me)

      if @players.any?
        status = true
        message = 'statistics_loaded'
      else
        status = false
        message = I18n.t("statuses.statistics_not_found")
      end

      { :status => status, :message => message, :show_friends => show_friends, :show_more => @players.size > 3, :total_players => total_players, :players => @players }
    end

    def me? user
        return false unless @current_user
        @current_user == user
      end

    def set_closest_friends me
      return unless me
      friend_statistics = []      
      orders = []
      me_in_friends = false

      @players.each do |statistic|
        friend_statistics << statistic if statistic[:friend] || statistic[:me]
      end

      friend_statistics.each_with_index do |statistic, index|
        me_in_friends = index if statistic[:me]
      end

      places = closest me_in_friends
      friend_statistics.each_with_index do |statistic, index|
        orders << statistic[:order] if places.include?( index )
      end

      @players.each do |statistic|
        statistic[:close_friend] = true if orders.include?( statistic[:order] )
      end

    end

    def set_closest_users me
      return unless me
      places = closest me      
      @players.each_with_index do |statistic, index|
        statistic[:close_user] = true if places.include?( index )
      end
    end

    def closest me
      places = []
      CLOSEST_ARRAY.each do |closest|
        place = me + closest
        places << place if place >= 0 
      end
      places
    end
  end
  
  private
  
  def manual_level_update
    update_user_level if points_manually_changed
  end

end
