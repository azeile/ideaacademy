# == Schema Information
#
# Table name: organization_users
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  organization_id :integer
#  default_flag    :boolean
#  created_at      :datetime
#  updated_at      :datetime
#  creator_flag    :boolean
#  admin_flag      :boolean
#

class OrganizationUser < ActiveRecord::Base
  belongs_to :organization
  belongs_to :user
  
  scope :default_org, where( [ "default_flag = ?", true ] )
  
  # validates_presence_of :employee, :organization
  
  before_create :set_defaults
  after_update :renew_admin_user
  after_create :create_categories
  after_destroy :lock_user_if_last
  
  attr_accessible :organization, :organization_attributes, :admin_flag, :user_attributes, :user
  accepts_nested_attributes_for :organization, :user
  delegate :email, :to => :user

  def user_id
    user.id
  end

  def user_created_at
    user.id
  end

  def admin_flag_humanize
    admin_flag ? "Yes" : "No"
  end

  def school?
    organization.school?
  end

  def self.create_with_user params, current_organization
    org = OrganizationUser.new params
    org.organization = current_organization

    org.user.password = org.user.password_confirmation = Devise.friendly_token.first(10)
    org.user.providers.build([:name => "devise", :user => org.user])
    org.user.build_admin_user(:email => org.user.email, :password => org.user.password) if org.admin_flag

    org.user.send_reset_password_instructions if org.save
    org
  end

  def create_category category_id, user_id, badge
    statistic = StatisticCategoryUser.find_or_initialize_by_category_id_and_user_id( category_id, user_id , {:badge => badge, :total_points => 0})
    statistic.save if statistic.new_record?
  end
  
  private
    def set_defaults      
      if default_flag
        user.organization_users.update_all({ :default_flag => false }) if user.has_default_organization?
      elsif default_flag.nil?
        self.default_flag = !user.has_default_organization?
      end
      
      [:creator_flag, :admin_flag].each { |flag| send("#{flag}=", organization.users.empty?) if send(flag).nil? } 
    end

    def renew_admin_user
      return true unless admin_flag_changed?
      if admin_flag && user.admin_user.nil? 
        user.build_admin_user(:email => user.email, :password => user.password)
        user.save!
      elsif admin_flag
        user.admin_user.unlock
      else !admin_flag
        user.admin.lock
      end
    end

    def create_categories
      categories = organization.categories
      transaction do
        badge = Badge.first
        categories.each do |category|
          create_category( category.id, user.id, badge)
        end
      end
    end

    def lock_user_if_last
      user.destroy unless user.organization_users.where("organization_users.id != ?", id).present?
    end
end
