# == Schema Information
#
# Table name: setting_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class SettingType < ActiveRecord::Base
  has_many :application_settings
end
