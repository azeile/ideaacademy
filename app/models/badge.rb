# == Schema Information
#
# Table name: badges
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  rule       :integer
#  image      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Badge < ActiveRecord::Base

  #Relations
  has_many :user_badges
  has_many :badges, :through => :user_badges
  has_many :statistic_category_ordered_users

  #Validations
  # => Attributes
  validates :title, :presence => true
  
  scope :current_level, lambda { |rule| where( "rule >= #{rule}" ).order("rule").limit(1) }
  
  def percents total_points
    next_badge = get_next_badge
    previous_badge = get_previous_badge
    return 100 unless next_badge

    points_in_level = total_points - previous_badge.rule
    points_for_next_level = rule - previous_badge.rule
    points_to_get_in_level = points_for_next_level - points_in_level
    return 0 if points_for_next_level <= 0
    (points_in_level.to_f/points_for_next_level.to_f * 100).round
  end
  
  def get_next_badge
    Badge.where("rule > #{rule}").order("rule").limit(1).first
  end

  def get_previous_badge
    badge = Badge.where("rule < #{rule}").order("rule DESC").first
    return badge if badge
    return Badge.first unless badge
  end
end
