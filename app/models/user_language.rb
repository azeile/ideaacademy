# == Schema Information
#
# Table name: user_languages
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  language   :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class UserLanguage < ActiveRecord::Base
  belongs_to :user
  attr_accessible :user, :language
  validates_presence_of :user, :language

  scope :not_in_list, lambda {|list| list = list.empty? ? ["Empty"] : list; where("language NOT IN ( ? )", list)}
  scope :in_list, lambda {|list| where("language IN ( ? )", list)}
end
