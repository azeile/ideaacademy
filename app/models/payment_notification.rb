class PaymentNotification < ActiveRecord::Base

  # Payment service provider sends notifications to Academy software.
  # For example PayPal sends payment status information immediately after payment

  belongs_to :user
  belongs_to :service
  serialize :params
  after_update :mark_subscription_as_purchased
  
  private
  
  def mark_subscription_as_purchased
    # 1. find payment transaction
    # 2. check summs and payment status
    # 3. if ok - grant service

    if status == "Completed" # && service.price.to_s == params.price
      service.subscribe user
    end
  end

  # === === === === === === === === === === === ===
  # PAYPAL PARAMS
  # mc_gross: 9.90
  # invoice: 1383556521
  # protection_eligibility: Eligible
  # address_status: confirmed
  # payer_id: NK9LSKN9P7GKN
  # tax: 0.00
  # address_street: 1 Main Terrace
  # payment_date: 01:15:41 Nov 04, 2013 PST
  # payment_status: Completed
  # charset: windows-1252
  # address_zip: W12 4LQ
  # first_name: Arvis
  # mc_fee: 0.69
  # address_country_code: GB
  # address_name: Arvis Zeile
  # notify_version: 3.7
  # custom: 
  # payer_status: verified
  # business: arvis.zeile-facilitator@gmail.com
  # address_country: United Kingdom
  # address_city: Wolverhampton
  # quantity: 1
  # verify_sign: ALXBuZDGBc4SaxsENFSYL5niweZiALCQoXwtMIMb988Hx-gbN5zztu5J
  # payer_email: arvis.zeile+paypal@makit.lv
  # txn_id: 8AT15360N2153014B
  # payment_type: instant
  # last_name: Zeile
  # address_state: West Midlands
  # receiver_email: arvis.zeile-facilitator@gmail.com
  # payment_fee: 0.69
  # receiver_id: 92T5YTSJZ9ZCU
  # txn_type: web_accept
  # item_name: translation missing: en.VIP konta mēneša abonements
  # mc_currency: USD
  # item_number: 4
  # residence_country: GB
  # test_ipn: 1
  # handling_amount: 0.00
  # transaction_subject: 
  # payment_gross: 9.90
  # shipping: 0.00
  # ipn_track_id: 44de500ef0419
  # controller: pages
  # action: paypal
end