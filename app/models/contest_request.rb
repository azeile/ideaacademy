# == Schema Information
#
# Table name: contest_requests
#
#  id                 :integer          not null, primary key
#  parent_id          :integer
#  phone              :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  accepted           :boolean          default(FALSE)
#  crowdsourcing_flag :boolean
#  parent_type        :string(255)
#  user_id            :integer
#  organization_id    :integer
#

class ContestRequest < ActiveRecord::Base
  belongs_to :parent, :polymorphic => true
  belongs_to :user
  belongs_to :organization
  accepts_nested_attributes_for :parent

  after_save :set_contest_status
  after_initialize :set_defaults
  after_create :set_contest_ends_at

  default_scope :order => "created_at desc"
  scope :pending, lambda { where("accepted = ?", false) }
  scope :accepted, lambda { where("accepted = ?", true) }
  scope :recent_pending, lambda { |limit = 5| where("accepted = ?", false).order("created_at desc").limit(limit) }
  scope :by_organization, lambda {|org| where("organization_id = ?", org.id)}

  def self.create_with_parent params, user, organization

    if params[:crowdsourcing_flag]
      params[:parent_type]        = "CrowdsourcedContest"
      params[:parent_attributes]  = params[:parent_attributes].merge({:organization_id => organization.id})
    end

    parent                      = ( params[:parent_type] || "Contest" ).classify.constantize.new(params.delete(:parent_attributes))
    params                      = params.merge({:user => user, :organization => organization, :parent => parent })

    ContestRequest.create(params)
  end

  def to_s
    "#{self.name} contest: '#{self.contest.nil? ? "n/a" : self.contest.title}'"
  end

  private
    def set_contest_status
      if self.accepted
        self.parent.update_attributes(:active_flag => true)
      end
    end

    def set_contest_ends_at
      self.parent.update_attributes(:ends_at => Time.now + 2.days)
    end

    def set_defaults
      self.parent_type = "Contest" if new_record?
    end
end
