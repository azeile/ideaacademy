# == Schema Information
#
# Table name: tmp_user_idea_rates
#
#  id          :integer          not null, primary key
#  tmp_user_id :integer
#  idea_id     :integer
#  rate_count  :integer          default(0)
#  created_at  :datetime
#  updated_at  :datetime
#

class TmpUserIdeaRate < ActiveRecord::Base
  belongs_to :user
  belongs_to :idea
  
  def add_rate
    self.rate_count = self.rate_count + 1
    self.save   
  end
  
  def overrated
    rate_count >= ApplicationSetting["IDEA_SHOW_COUNT"] ? true : false
  end
end
