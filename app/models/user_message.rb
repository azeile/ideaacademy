# == Schema Information
#
# Table name: user_messages
#
#  id            :integer          not null, primary key
#  body          :text             not null
#  sender_id     :integer          not null
#  receiver_id   :integer          not null
#  created_at    :datetime
#  updated_at    :datetime
#  sender_type   :string(255)
#  receiver_type :string(255)
#

class UserMessage < ActiveRecord::Base

  belongs_to :sender, foreign_key: "sender_id", polymorphic: true
  belongs_to :receiver, foreign_key: "receiver_id", polymorphic: true

  validates :body, presence: true
  validates :sender, presence: true
  validates :receiver, presence: true

  scope :for_user, lambda { |user| where({receiver_id: user.id})}

  def self.send_admin_messages body, user
    organization = user.organizations.first
    admins = organization.admin_users
    for admin in admins
      UserMessage.create({body: body, sender: user, receiver: admin})
    end
  end

  def sender_email
    return sender.email if sender.present?
    return ""
  end
end
