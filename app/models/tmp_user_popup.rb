# == Schema Information
#
# Table name: tmp_user_popups
#
#  id          :integer          not null, primary key
#  tmp_user_id :integer
#  seen        :boolean          default(FALSE)
#  typekey     :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class TmpUserPopup < ActiveRecord::Base
end
