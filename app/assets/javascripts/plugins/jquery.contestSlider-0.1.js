/*
 * contestSlider - jQuery Plugin
 * Yet simple, single purpose contest sliding plugin for "Ideju Akadēmija" project.
 *
 * Project website: http://idejuakademija.lv/
 *
 * Copyright (c) 2011 "Creative Mobile" / Austris Landmanis - austris@creo.mobi
 *
 * Version: 0.1 (01/11/2011)
 * Requires: jQuery v1.6+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

(function( $ ){
  $.fn.contestslider = function( options ) {

    var settings = {

    };

    return this.each(function() {
      if ( options ) { 
        $.extend( settings, options );
      }

      // Prepare content for tooltip
      var content = $('.tooltip', this).html();

      // Element
      var element = $(this);
      var element_width = element.width();
      var element_height = element.height();

      // Tooltip
      var tooltip_selector = 'test';
      var tooltip_markup = '<div class="' + tooltip_selector + '"></div>';
      element.html(tooltip_markup);

      var tooltip = $('.' + tooltip_selector, this);
      tooltip.prepend(content);
      var tooltip_width = tooltip.width();
      var tooltip_height = tooltip.height();

      // Arrow
      var arrow_markup = '<div class="arrow"></div>';
      tooltip.append(arrow_markup);

      var arrow = $('.arrow', this);
      var arrow_height = arrow.height();
      var arrow_width = arrow.width();

      // Positioning
      var tooltip_vertical = '-' + (tooltip_height - element_height) / 2 + 'px';
      tooltip.css('top', tooltip_vertical);

      var arrow_vertical = (tooltip_height - arrow_height) / 2 + 'px';
      arrow.css('top', arrow_vertical);

      if (element.hasClass('previous')) {
        tooltip.css('left', element_width + 20 + 'px');
        arrow.addClass('left');
        arrow.css('left', '-' + (arrow_width + 1) + 'px');
      }
      else if (element.hasClass('next')) {
        tooltip.css('right', tooltip_width + 20 + 'px');
        arrow.addClass('right');
        arrow.css('right', '-' + (arrow_width + 1) + 'px');
      }
    });

  };
})( jQuery );