/*
 * toggleElement - jQuery Plugin
 * Toggler plugin for "Ideju Akadēmija" project.
 *
 * Project website: http://idejuakademija.lv/
 *
 * Copyright (c) 2011 "Creative Mobile" / Austris Landmanis - austris@creo.mobi
 *
 * Version: 0.1 (23/11/2011)
 * Requires: jQuery v1.6+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

(function( $ ){
  $.fn.toggleElement = function( options ) {

    var settings = {
      callback: function(){}
    };

    if ( options ) { 
      $.extend( settings, options );
    }

    this.each(function() {
      var element = $(this).attr('href');
      $(element).css('display', 'none');
    });

    this.click(function() {
      var element = $(this).attr('href');
      
      $(element).toggle();

      if ($(this).data('also')) {
        element_also = $(this).data('also');
        $(element_also).toggle();
      }

      var toggle_text = $(this).data('close');
      var current_text = $(this).html();

      $(this).toggleClass('fold');
      $(this).html(toggle_text);
      $(this).data('close', current_text);

      settings.callback()

      return false;
    });

  };
})( jQuery );