/*
 * DropBox - jQuery Plugin
 * Footer menu dropbox plugin for "Ideju Akadēmija" project.
 *
 * Project website: http://idejuakademija.lv/
 *
 * Copyright (c) 2011 "Creative Mobile" / Austris Landmanis - austris@creo.mobi
 *
 * Version: 0.1 (08/11/2011)
 * Requires: jQuery v1.6+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

function toggle_dropbox(selector, side) {
  if (side) {
    $(selector).css(side, '0px');
  }
  else {
    $(selector).css('left', '0px');
  }

  $(selector).slideToggle(300);
}

(function( $ ){
  $.fn.dropbox = function( options ) {

    var settings = {

    };

    if ( options ) { 
      $.extend( settings, options );
    }

    this.each(function() {
      var selector = $(this).attr('href');
      var side = $(this).attr('rel');
      var button_close = '<div class="close"></div>';
      $(selector).prepend(button_close);
      $('.close', selector).click(function() {
        toggle_dropbox(selector, side);
      });
    });

    this.click(function() {
      var selector = $(this).attr('href');
      var side = $(this).attr('rel');
      toggle_dropbox(selector, side);
      return false;
    });

  };
})( jQuery );