/*
 * posChoice - jQuery Plugin
 * Position choice ribbons and arrows plugin for "Ideju Akadēmija" project.
 *
 * Project website: http://idejuakademija.lv/
 *
 * Copyright (c) 2011 "Creative Mobile" / Austris Landmanis - austris@creo.mobi
 *
 * Version: 0.1 (09/11/2011)
 * Requires: jQuery v1.6+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

(function( $ ){
  $.fn.poschoice = function( options ) {

    var settings = {

    };

    if ( options ) { 
      $.extend( settings, options );
    }

    this.each(function() {
      var element = $(this);
      element.append('<div class="arrow_1"></div>');
      var arrow = $('.arrow_1', element);
      var ribbon = $('.ribbon', this);

      var e_height = element.height();
      var a_height = arrow.height();
      var r_height = ribbon.height();

      var a_top = (e_height / 2)
      var r_top = (e_height / 2)

      if (a_top <= 8) {
        a_top = 5
      }

      if (r_top <= 0) {
        r_top = 0
      }

      arrow.css('top', a_top + 'px');
      ribbon.css('top', r_top + 'px');

      // Button
      var button = $(this).parent().next().find('input');
      button.css('top', a_top - 3 + 'px');
    });

  };
})( jQuery );