$.fn.equalizeHeights = function(){
 return this.height( Math.max.apply(this, $(this).map(function(i,e){ return $(e).height() }).get() ))
}
