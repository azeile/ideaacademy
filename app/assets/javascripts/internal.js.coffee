#=require jquery
#=require plugins/html5shiv
#=require_self
$ ->
  $("#latest-activities .close").on "click", (e) ->
    e.preventDefault()
    $("#latest-activities ul").slideUp()
    $(this).fadeOut()
    $("#latest-activities h1").css
      cursor: "pointer"
  $("#latest-activities h1").on "click", (e) ->
    e.preventDefault()
    $("#latest-activities ul").slideDown()
    $("#latest-activities .close").fadeIn()
    $(this).css
      cursor: "default"
