class IA.Routers.StatisticsRouter extends Backbone.Router
  initialize: (options) ->
    @statistics = new IA.Collections.StatisticsCollection()
    # @contests.reset options.contests

  routes:
    "/:id"      : "show"

  show: (id) ->
    statistic = @statistics.get(id)
    
    @view = new IA.Views.Statistics.ShowView(model: statistic)
    $("#contests").html(@view.render().el)
  