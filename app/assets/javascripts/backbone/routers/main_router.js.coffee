class IA.Routers.MainRouter extends Backbone.Router
  before:
    '^.+$' : ->
      #FIXME: HVZ , kā to labāk dabūt gatavu
      location.href = location.href.replace(/#.+/, '') if @user.isBlocked()
  
  initialize: (options) ->
    @eventBus = options.eventBus
    @appView = options.appView
    @user = options.user

  routes:
    "/contests/:id"   : "showContest"
    "/crowdsourced_contests/:id"   : "showCrowdsourcedContest"
    "/crowdsourced_contests"   : "showCrowdsourcedContest"
    "/contests/no_contests"   : "noContests"
    "/new_contest"    : "newContest"
    "/new_contest_splash" : "newContestSplash"
    "/statistics/:id" : "showStatistic"
    "/users/:id"      : "showUser"
    "/users/:id/edit" : "editUser"
    "/test"           : "test"
    "/thanks"           : "thanks"
    "/contests/:id/payments" : "indexPayments"
    "/contests/payments/:id/start_transaction" : "startPaymentTransaction"
    "/splash"               : "indexSplash"
    "*path"                : "index"

  indexSplash: ->
    @contestsIndexView = null
    @statisticsIndexView = null
    @usersShowView = null
    @usersEditView = null
    
    @initSplashScreenView()
    #if @user.get('session_id') # if user is GUEST
    #  @initSplashScreenView()
    #else
    #  @index()

  newContestSplash: ->
    @contestsIndexView = null
    @statisticsIndexView = null
    @usersShowView = null
    @usersEditView = null
    
    @initNewContestSplashView()

  index: ->
    @statisticsIndexView = null
    @usersShowView = null
    @usersEditView = null
    @initContestIndexView()

  showContest: (id) ->
    @statisticsIndexView = null
    @usersShowView = null
    @usersEditView = null
    @initContestIndexView(id)

  showCrowdsourcedContest: (id) ->
    @statisticsIndexView = null
    @usersShowView = null
    @usersEditView = null
    @initCrowdsourcedContestView(id)

  newContest: ->
    @contestsIndexView = null
    @statisticsIndexView = null
    @usersShowView = null
    @usersEditView = null
    @initContestNewView()

  showStatistic: (id) ->
    @contestsIndexView = null
    @usersShowView = null
    @usersEditView = null
    @initStatisticIndexView(id)

  showUser: (id) ->
    @contestsIndexView = null
    @statisticsIndexView = null
    @usersEditView = null
    @initUserShowView(id)

  editUser: (id) ->
    @contestsIndexView = null
    @statisticsIndexView = null
    @usersShowView = null
    @initUserEditView(id)

  indexPayments: (contest_id) ->
    @contestsIndexView = null
    @initPaymentsIndexView(contest_id)

  initSplashScreenView: ->
    @splashShowView = if @user.isBlocked() then new IA.Views.SplashBlockedView({ eventBus: @eventBus, user: @user }) else new IA.Views.SplashView({ eventBus: @eventBus, user: @user })
    @eventBus.trigger("changeAppContent", @splashShowView)
    @eventBus.trigger('updateScrollbar')

  initNewContestSplashView: ->
    @splashShowView = new IA.Views.NewContestSplashView({ eventBus: @eventBus, user: @user })
    @eventBus.trigger("changeAppContent", @splashShowView)
    @eventBus.trigger('updateScrollbar')

  initContestIndexView: (id) ->
    if @contestsIndexView && !@statisticsIndexView && id
      @eventBus.trigger("changeContest", id)
    else
      @contestsIndexView = new IA.Views.Contests.IndexView({ id: id, eventBus: @eventBus, user: @user})
      @eventBus.trigger("changeAppContent", @contestsIndexView)

  initCrowdsourcedContestView: (id) ->
    @crowdsourcedContestsIndexView = new IA.Views.CrowdsourcedContests.IndexView({ id: id, eventBus: @eventBus, user: @user})
    @eventBus.trigger("changeAppContent", @crowdsourcedContestsIndexView)

  initContestNewView: ->
    @contestsCollection = new IA.Collections.ContestRequestsCollection()
    @contestsNewView = new IA.Views.Contests.NewView({ eventBus: @eventBus, collection: @contestsCollection })
    @eventBus.trigger("changeAppContent", @contestsNewView)

  initStatisticIndexView: (id) ->
    if @statisticsIndexView && !@contestsIndexView
      @eventBus.trigger("changeStatistic", id)
    else
      @statisticsIndexView = new IA.Views.Statistics.IndexView({ id: id, eventBus: @eventBus, user: @user })
      @eventBus.trigger("changeAppContent", @statisticsIndexView)

  initUserShowView: (id) ->
    @usersShowView = new IA.Views.Users.ShowView({ eventBus: @eventBus, user: @user, id: id })
    @eventBus.trigger("changeAppContent", @usersShowView)

  initUserEditView: (id) ->
    @usersEditView = new IA.Views.Users.EditView({ eventBus: @eventBus, user: @user, id: id })
    @eventBus.trigger("changeAppContent", @usersEditView)

  test: ->
    testView = new IA.Views.TestView({ eventBus: @eventBus, contests : @contests})
    @eventBus.trigger("changeAppContent", testView)

  thanks: ->
    thanksView = new IA.Views.ThanksView({ eventBus: @eventBus, user: @user })
    @eventBus.trigger("changeAppContent", thanksView)

  initPaymentsIndexView: (contest_id) ->
    @paymentsCollection = new IA.Collections.PaymentsCollection
    @paymentsView = new IA.Views.Payments.IndexView({ eventBus: @eventBus, collection: @paymentsCollection, contest: contest_id })
    @eventBus.trigger("changeAppContent", @paymentsView)
  
  startPaymentTransaction: (service_id) ->
    @transactionView = new IA.Views.Payments.StartTransactionView({ eventBus: @eventBus, service_id: service_id })
    @eventBus.trigger("changeAppContent", @transactionView)
