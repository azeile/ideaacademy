class IA.Routers.ContestsRouter extends Backbone.Router
  initialize: (options) ->
    @contests = new IA.Collections.ContestsCollection()
    # @contests.reset options.contests

  routes:
    "/new"          : "newContest"
    "/index"        : "index"
    "/:id/edit"     : "edit"
    "/:id"          : "show"
    "/no_contests"  : "noContests"
    ".*"            : "index"

  newContest: ->
    @view = new IA.Views.Contests.NewView(collection: @contests)
    $("#contests").html(@view.render().el)

  index: ->
    @view = new IA.Views.Contests.IndexView(contests: @contests)
    $("#contests").html(@view.render().el)
    
  noContests: ->
    @view = new IA.Views.Contests.NoContestsView
    $("#contests").html(@view.render().el)

  show: (id) ->
    contest = @contests.get(id)
    
    @view = new IA.Views.Contests.ShowView(model: contest)
    $("#contests").html(@view.render().el)
    
  edit: (id) ->
    contest = @contests.get(id)

    @view = new IA.Views.Contests.EditView(model: contest)
    $("#contests").html(@view.render().el)
  