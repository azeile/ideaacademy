class IA.Views.ThanksView extends Backbone.View
  template: JST["backbone/templates/thanks"]
  status_template: JST["backbone/templates/user_payment_status"]

  initialize: () ->
    _.bindAll(this, 'render', 'render_payment_notifications_info')
    @user = @options.user
    @payment_notification_collection = new IA.Collections.PaymentNotificationCollection
    @payment_notification_collection.fetchUserPaymentNotifications(@user.get('id'))
    @payment_notifications = @payment_notification_collection.bind('reset', @render_payment_notifications_info, this)

  render: -> 
    $(@el).html(@template())
    @render_payment_notifications_info()
    return this

  render_payment_notifications_info: ->
    @payment_notifications.each (payment_notification, index) =>
      console.info @
      @$("#user_payment_status").html( @status_template( { user_payment_status: payment_notification.get('status') } ) )
