IA.Views.Sidebars ||= {}
IA.Views.Sidebars.News ||= {}

class IA.Views.Sidebars.News.NewsItemView extends Backbone.View
  className: "news-item"
  template: JST["backbone/templates/sidebars/news/news_item"]

  initialize: ->
    _.bindAll(this, 'render')

  render: ->
    $(@el).html(@template(@model.toJSON()))
    $(@el).data("timestamp", @model.get("timestamp"))
    return this
