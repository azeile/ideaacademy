IA.Views.Sidebars ||= {}

class IA.Views.Sidebars.MessageBubbleView extends Backbone.View
  
  className: "sidebar_bubble"
  template: JST["backbone/templates/sidebars/bubbles/message_bubble"]

  events:
    "click .close": "closeBubble"

  initialize: () ->
    _.bindAll(this, 'render', 'closeBubble')
    @eventBus = @options.eventBus
    @user = @options.user

  render: -> 
    $(@el).html(@template(@model.toJSON()))
    return this

  closeBubble: ->
    $(@el).remove()
    @eventBus.trigger('markBubbleAsSeen', @model.get('id'))
