IA.Views.Sidebars ||= {}

class IA.Views.Sidebars.RecentUserBadgeView extends Backbone.View
  
  template: JST["backbone/templates/sidebars/recent_badges/recent_user_badge"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus
    @model = @options.model

  render: -> 
    $(@el).html(@template(@model.toJSON()))
    return this
