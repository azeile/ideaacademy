IA.Views.Sidebars ||= {}

class IA.Views.Sidebars.NewsView extends Backbone.View
  template: JST["backbone/templates/sidebars/news"]

  events:
    "click .more a" : "loadMoreIdeas"
    "click .less a" : "loadLessIdeas"
    "click #news h1" : "toggleNewsContainer"

  initialize: () ->
    _.bindAll(this, 'render', 'renderNewsItem', 'timestamp', 'doGetLogActivities', 'onGetLogActivities')
    @eventBus = @options.eventBus
    @eventBus.bind('doGetLogActivities', @doGetLogActivities)
    @user = @options.user
    @log_activities = new IA.Collections.LogActivityCollection
    @log_activities.fetchForUser(@user.get('id'), @onGetLogActivities)
    setInterval @doGetLogActivities, 20000

  doGetLogActivities: ->
    if @timestamp()
      @log_activities.fetchForUserUpdate(@user.get('id'), @timestamp(), @onGetLogActivities)
    else
      @log_activities.fetchForUser(@user.get('id'), @onGetLogActivities)

  doGetOlderLogActivities: ->
    @log_activities.fetchForUserOlderActivities(@user.get('id'), @olderTimestamp(), @onGetLogActivities)

  onGetLogActivities: (log_activities) ->
    #@$('#updates_wrap div.loading_small').remove()

    #updates_count = log_activities.length
    #current_updates_count = parseInt(@$('#updates_count_new').html())
    #updates_count = updates_count + current_updates_count
    #@$('#updates_count_new').html(updates_count)

    # Hide "more" when all (older) news loaded
    if @timestamp() && log_activities.models.length > 0 && @timestamp() > log_activities.models[0].get("timestamp") && log_activities.models.length < 20
      @$('.more').hide()

    #Show less only when there are more than initial number of items
    if @$(".widget-container .items .news-item").length > 0
      @$(".less").show()
    else
      @$(".less").hide()

    _.each log_activities.models, (model) =>
      @renderNewsItem(model)
    @refreshScrollPane()

  timestamp: ->
    @$('.widget-container .items .news-item:first').data('timestamp')

  olderTimestamp: ->
    @$('.widget-container .items .news-item:last').data('timestamp')

  toggleNewsContainer: ->
    $("#news .widget-container, #news .close").fadeToggle()

  loadMoreIdeas: (e) ->
    e.preventDefault()
    e.stopPropagation()
    @doGetOlderLogActivities()

  loadLessIdeas: (e) ->
    e.preventDefault()
    e.stopPropagation()
    @$(".widget-container .items .news-item:gt(4)").remove()
    @$(".less").hide()
    @$(".more").show()
    @refreshScrollPane()

  refreshScrollPane: ->
    @$(".items").jScrollPane
      reinitialise: true
      scrollbarWidth: 5

  render: ->
    $(@el).html(@template())
    return this

  renderNewsItem: (model) ->
    view = new IA.Views.Sidebars.News.NewsItemView({ model: model})
    if model.get("timestamp") > @timestamp()
      @$(".widget-container .items .news-item:first").before(view.render().el)
    else
      if @$(".widget-container .items .news-item").length > 0
        @$(".widget-container .items .news-item:last").after(view.render().el)
      else
        @$(".widget-container .items").append(view.render().el)
