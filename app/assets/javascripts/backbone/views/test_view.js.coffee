class IA.Views.TestView extends Backbone.View
  template: JST["backbone/templates/test"]

  initialize: () ->
    _.bindAll(this, 'render', 'renderContest', 'onGetContest')
    @contests = @options.contests

  render: -> 
    $(@el).html(@template)
    contest = new IA.Models.Contest
    contest.fetch {url : "/contests/root", success : @onGetContest}
    return this

  onGetContest: (contest) ->
    @renderContest(contest)
    contest.next(@renderContest)

  renderContest: (contest) ->    
    view = new IA.Views.Contests.ContestView {model : contest}
    @$("#testContestWrap").html(view.render().el)