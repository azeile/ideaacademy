IA.Views.Header.Guest ||= {}

class IA.Views.Header.Guest.CategoriesView extends Backbone.View
  template: JST["backbone/templates/header/guest/categories"]

  initialize: () ->
    _.bindAll(this, 'render', 'onGetCategories', 'doUpdateCategoryTmp', 'onUpdateCategories', 'appendCategory')
    @eventBus = @options.eventBus
    @eventBus.bind("doUpdateCategoryTmp", @doUpdateCategoryTmp)
    @categories = new IA.Collections.CategoriesCollection()
    @categories.fetchForTmpUser(@model.get('id'), @onGetCategories)
    @user = @model
    
  render: -> 
    $(@el).html(@template())
    return this

  doUpdateCategoryTmp: (user_id, category_id) ->
    @categories.fetchForTmpUserSingleUpdate(user_id, category_id, @onUpdateCategories)

  onUpdateCategories: (categories) ->
    _.each categories.models, (model) ->
      @updateCategories model
    , this

  updateCategories: (category) ->
    el_category = @$('#categories_view #theme_' + category.get('id'))
    el_total_points = @$('#categories_view #theme_' + category.get('id') + ' .points .total')
    el_live_points = @$('#categories_view #theme_' + category.get('id') + ' .points .plus')

    if category.get('live_points') > 0
      el_live_points.html(category.get('live_points')).show()
      
      if el_category.hasClass('highlight')
        @doUpdateCategoryTmp(@model.get('id'), category.get('id'))

    else
      el_total_points.html(category.get('total_points'))
      el_live_points.html('').hide()

  onGetCategories: (categories) ->
    _.each categories.models, (model) ->
      @appendCategory model
    , this
    @eventBus.trigger("categoriesLoaded")

  appendCategory: (category) ->
    view = new IA.Views.Header.Guest.CategoryView({eventBus : @eventBus, model : category, user: @user})
    @$("#categories_view").append(view.render().el)
