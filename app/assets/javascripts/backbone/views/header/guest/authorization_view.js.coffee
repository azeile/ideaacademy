IA.Views.Header ||= {}

class IA.Views.Header.AuthorizationView extends Backbone.View
  template: JST["backbone/templates/header/guest/authorization"]

  initialize: () ->
    _.bindAll(this, 'render')

  render: -> 
    $(@el).html(@template())

    $('a.authorization').live(
      click: ->
        url = $(this).attr('href')
        window.open(url, 'authorization_popup', 'toolbar=0,menubar=0,width=640,height=500,scrollbars=0')
        return false
    )

    return this