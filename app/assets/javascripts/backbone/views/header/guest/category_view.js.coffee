IA.Views.Header.Guest ||= {}

class IA.Views.Header.Guest.CategoryView extends Backbone.View
  template: JST["backbone/templates/header/guest/category"]

  events:
    "click .category" : "onCategoryClicked"

  initialize: () ->
    _.bindAll(this, 'render', 'onCategoryClicked', 'onChangeCategory')
    @eventBus = @options.eventBus
    @eventBus.bind("onChangeCategory", @onChangeCategory)
    @user = @options.user
    @category_active = @model.get("contests_count") > 0 ? true : false
    
  render: -> 
    if @category_active
      @model.set({active_class: "active"}) 
    else
      @model.set({active_class: ""}) 
    
    $(@el).html(@template(@model.toJSON()))
    return this

  onCategoryClicked: ->
    if @category_active?
  	  @eventBus.trigger("loadRootContestByCategory", @model.id) 
    return false

  onChangeCategory: (id) ->
    if id == @model.id
      $('.theme').removeClass('highlight')
      @$('.theme').addClass('highlight')
     
      @eventBus.trigger("doUpdateCategoryTmp", @user.get('id'), @model.get('id'))
      @eventBus.trigger('doGetPopupTmp')