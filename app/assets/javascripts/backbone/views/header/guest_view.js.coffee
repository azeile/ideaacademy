IA.Views.Header ||= {}

class IA.Views.Header.GuestView extends Backbone.View
  template: JST["backbone/templates/header/guest"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus

  render: -> 
    $(@el).html(@template())

    @renderCategories()
    @renderAuthorization()

    return this

  renderCategories: ->
    view = new IA.Views.Header.Guest.CategoriesView({eventBus: @eventBus, model: @model})
    @$("#categories_wrap").html(view.render().el)

  renderAuthorization: ->
    view = new IA.Views.Header.AuthorizationView()
    @$("#user_wrap").html(view.render().el)