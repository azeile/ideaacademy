IA.Views.Header ||= {}

class IA.Views.Header.UserView extends Backbone.View
  template: JST["backbone/templates/header/user"]

  initialize: () ->
    _.bindAll(this, 'render', 'renderCategories', 'renderProfile')
    @eventBus = @options.eventBus

  render: -> 
    $(@el).html(@template())

    @renderCategories()
    @renderProfile()

    return this

  renderCategories: ->
    view = new IA.Views.Header.User.CategoriesView({ eventBus : @eventBus, model: @model })
    @$("#categories_wrap").html(view.render().el)

  renderProfile: ->
    view = new IA.Views.Header.ProfileView({ model: @model })
    $("#content_wrap").prepend(view.render().el)