IA.Views.Header ||= {}

class IA.Views.Header.ProfileView extends Backbone.View
  template: JST["backbone/templates/header/user/profile"]

  initialize: () ->
    _.bindAll(this, 'render')
    @bindUserData()

  render: -> 
    $(@el).html(@template(@model.toJSON()))
    return this

  bindUserData: () ->
    @model.bind "change", () =>
      @$('img.photo').attr("src", @model.get("image_url"))
      if (!@model.get("can_check_out"))
        @$('.earned_money').html(@model.get("earned_money"))
        @$('.check_out_buttom_placeholder').html("")