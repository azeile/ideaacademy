class IA.Views.Payments.StartTransactionView extends Backbone.View
  template: JST["backbone/templates/payments/start_transaction"]
  
  initialize: (options) ->
    super(options)
    _.bindAll(this, 'render', 'openPaymentPopup', 'onClosePaymentPopup')
    # @collection.fetchPaymentOptions(@onGetPaymentOptions)
    @service_id = options.service_id
  
  render: ->
    @openPaymentPopup()
    return this

  openPaymentPopup: ->
    obj =
      method: 'pay',
      action: 'buy_item',
      order_info: {'service_id': @service_id},
      dev_purchase_params: {'oscif': true}

    FB.ui(obj, @onClosePaymentPopup)

  onClosePaymentPopup: ->
    contest = @contest
    window.location.hash = "/index" unless arguments[0].order_id?
    $.ajax
      url: "/api/payment_transactions/#{arguments[0].order_id}"
      beforeSend: (jqXHR, settings) =>
        $(@el).append("<div class='scroll loading'></div>")
      success: (data, status, jqxhr) ->
        if data.state == "in_progress"
          ajx = this
          $.doTimeout 1500, ->
            $.ajax ajx
        else
          window.location.hash = "index"
      error: (jqxhr, status, error) =>
          @$(".loading.scroll").remove()
          window.location.hash = "/index"
