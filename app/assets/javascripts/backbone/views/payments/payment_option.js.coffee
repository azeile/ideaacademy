IA.Views.Payments ||= {}

class IA.Views.Payments.PaymentOptionView extends Backbone.View

  template: JST["backbone/templates/payments/payment_option"]

  initialize: (options) ->
    super(options)
    _.bindAll(this, 'render')

  render: ->
    @model.req_url()
    $(@el).html(@template(@model.toJSON()))
    return this
