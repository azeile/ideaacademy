IA.Views.Footer ||= {}

class IA.Views.Footer.UserView extends Backbone.View
  template: JST["backbone/templates/footer/user"]

  # => Popup templates
  template_test_popup: JST["backbone/templates/popups/test_popup"]
  popup_welcome_user: JST["backbone/templates/popups/welcome_user"]
  popup_first_points_vote: JST["backbone/templates/popups/first_points_vote"]
  popup_first_points_idea: JST["backbone/templates/popups/first_points_idea"]
  popup_earn_more: JST["backbone/templates/popups/earn_more"]
  popup_like_idea: JST["backbone/templates/popups/like_idea"]
  popup_first_achievement: JST["backbone/templates/popups/first_achievement"]
  popup_new_achievement: JST["backbone/templates/popups/new_achievement"]
  popup_new_money_achievement: JST["backbone/templates/popups/new_money_achievement"]
  popup_achievements: JST["backbone/templates/popups/achievements"]
  popup_other_themes: JST["backbone/templates/popups/other_themes"]
  popup_earned_money: JST["backbone/templates/popups/earned_money"]
  popup_welcome_user_draugiem: JST["backbone/templates/popups/welcome_user_draugiem"]
  popup_rules: JST["backbone/templates/popups/rules"]

  splash_new_contest: JST["backbone/templates/splash/new_contest"]

  events:
    "click .statistic_link" : "statisticLink"
    "click .new_contest_link": "newContestLink"
    "click .rules_link": "rulesLink"
    "click .contact_link": "contactLink"
    "click .contact_admin_link": "contactAdminLink"

  initialize: () ->
    _.bindAll(this, 'render', 'renderLogActivities', 'doGetPopup', 'renderPopup', 'definePopup', 'statisticLink')
    @eventBus = @options.eventBus
    @eventBus.bind('doGetPopup', @doGetPopup)
    @user = @model

  statisticLink: ->
    window.location.hash = "/statistics/1"
    return false

  rulesLink: ->
    $.fancybox
      'content': @popup_rules()

    return false

  contactAdminLink: (e) ->
    e.stopPropagation()
    e.preventDefault()
    view = new IA.Views.Footer.User.ContactAdminView(user: @user)
    $("body").append(view.render().el)
  contactLink: (e) ->
    e.stopPropagation()
    e.preventDefault()
    view = new IA.Views.Footer.User.ContactView
    $("body").append(view.render().el)

  newContestLink: ->
    #$("#content").html(@splash_new_contest())
    window.location.hash = "/new_contest"
    #@eventBus.trigger('updateScrollbar')
    #@eventBus.trigger
    return false

  definePopup: (popup) ->
    switch popup.typekey
      when "test_popup" then @template_test_popup(popup)
      when "welcome_user" then @popup_welcome_user(popup)
      when "welcome_user_draugiem" then @popup_welcome_user_draugiem(popup)
      when "first_points_vote" then @popup_first_points_vote(popup)
      when "first_points_idea" then @popup_first_points_idea(popup)
      when "earn_more" then @popup_earn_more(popup)
      when "like_idea" then @popup_like_idea(popup)
      when "first_achievement" then @popup_first_achievement(popup)
      when "new_achievement" then @popup_new_achievement(popup)
      when "new_money_achievement" then @popup_new_money_achievement(popup)
      when "achievements" then @popup_achievements(popup)
      when "other_themes" then @popup_other_themes(popup)
      when "earned_money" then @popup_earned_money(popup)

  doGetPopup: ->
    @popup = new IA.Models.Popup
    @popup.getPopup(@user.get('id'), @renderPopup)

  renderPopup: (response) ->
    @popup = response.get('popup')

    if response.get('status') == true
      $.fancybox
        'content': @definePopup(@popup),
        onComplete: =>
          @onCompletePopup()
        onClosed: =>
          @onClosedPopup(response)

  onCompletePopup: ->
    $('.button.save').click -> $.fancybox.close()
    $('.button_1.green').click -> $.fancybox.close()

  onClosedPopup: (response) ->
    popup = response.get('popup')
    @popup = new IA.Models.Popup
    @popup.markAsSeen(popup.user_id, popup.id, @triggerMorePopup)

  triggerMorePopup: ->
    @eventBus.trigger('doGetPopup')

  renderLogActivities: ->
    view = new IA.Views.Footer.User.LogActivitiesView({ eventBus : @eventBus, user: @user })
    @$("#log_activities_wrap").html(view.render().el)

  renderContests: () ->
    view = new IA.Views.Footer.User.ContestsView({ eventBus : @eventBus, user: @user })
    @$("#footer_contests_wrap").html(view.render().el)

  renderCategoryStatistics: () ->
    view = new IA.Views.Footer.User.StatisticsView({ eventBus: @eventBus, user: @user })
    @$("#footer_statistics_wrap").html(view.render().el)

  renderLanguages: ->
    languages = ["en", "lv", "de"]
    _.each languages, (lang) =>
      view = new IA.Views.Footer.User.LanguagesView({ eventBus: @eventBus, user: @user, language: lang })
      @$("#lang_selector_wrap").append(view.render().el)

  render: ->
    $(@el).html(@template())

    #@renderLogActivities() if AppUtils.isInternalApp
    @renderContests()
    @renderCategoryStatistics()
    @renderLanguages()

    $('.slide').contestslider()

    return this
