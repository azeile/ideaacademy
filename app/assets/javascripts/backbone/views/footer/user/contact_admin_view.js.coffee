IA.Views.Footer ||= {}
IA.Views.Footer.User ||= {}

class IA.Views.Footer.User.ContactAdminView extends Backbone.View
  id: "contact-form"
  template: JST["backbone/templates/footer/user/contact_admin"]

  sendBtn:  "#contact-admin-btn"
  msgInput: "#contact-admin-message"

  events:
    "click .close" : "closeForm"
    "submit form" : "submitForm"

  initialize: ->
    _.bindAll(this, "render", "closeForm", "submitForm")
    @user = @options.user
    @model = new IA.Models.AdminMessage(user_id: @user.get("id"))

  render: ->
    $(@el).html(@template())
    return this

  closeForm: (e) ->
    e.stopPropagation()
    e.preventDefault()
    $(@el).remove()

  submitForm: (e) =>
    e.stopPropagation()
    e.preventDefault()
    message = $(@msgInput).val()
    if message.length > 0
      @disableSendButton()
      @model.set "body", message
      @model.save {},
        success: @saveSuccessHandler
        error:   @saveErrorHandler

  saveSuccessHandler: () =>
    @enableSendButton()
    $(@el).remove()

  saveErrorHandler: (e) =>
    @enableSendButton()
    alert "error: message could not be sent"
    $(@el).remove()

  disableSendButton: =>
    $(@sendBtn).attr "disabled", "disabled"

  enableSendButton: =>
    $(@sendBtn).removeAttr "disabled"