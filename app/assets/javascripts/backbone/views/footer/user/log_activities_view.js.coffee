IA.Views.Footer ||= {}
IA.Views.Footer.User ||= {}

class IA.Views.Footer.User.LogActivitiesView extends Backbone.View
  template: JST["backbone/templates/footer/user/log_activities"]

  initialize: () ->
    _.bindAll(this, 'render', 'doGetLogActivities', 'onGetLogActivities', 'appendLogActivity')
    @eventBus = @options.eventBus
    @eventBus.bind('doGetLogActivities', @doGetLogActivities)
    @user = @options.user
    @log_activities = new IA.Collections.LogActivityCollection()
    @log_activities.fetchForUser(@user.get('id'), @onGetLogActivities)

  doGetLogActivities: ->
    last_update = @$('#updates_wrap').data('timestamp')
    if last_update
      @log_activities.fetchForUserUpdate(@user.get('id'), last_update, @onGetLogActivities)
    else
      @log_activities.fetchForUser(@user.get('id'), @onGetLogActivities)
    
  onGetLogActivities: (log_activities) ->
    @$('#updates_wrap div.loading_small').remove()

    updates_count = log_activities.length
    current_updates_count = parseInt(@$('#updates_count_new').html())
    updates_count = updates_count + current_updates_count
    @$('#updates_count_new').html(updates_count)

    _.each log_activities.models, (model) ->
      @appendLogActivity model
    , this

  render: ->
    $(@el).html(@template())
    return this

  appendLogActivity: (log_activity) ->
    @log_activity_options = {
      eventBus: @eventBus,
      model: log_activity,
      user: @user
    }

    switch log_activity.get('typekey')
      when 'new_idea' then view = new IA.Views.Footer.User.LogActivities.NewIdeaView(@log_activity_options)
      when 'points_idea' then view = new IA.Views.Footer.User.LogActivities.PointsIdeaView(@log_activity_options)
      when 'points_vote' then view = new IA.Views.Footer.User.LogActivities.PointsVoteView(@log_activity_options)

    updates_elements_count = @$('#updates_wrap div').length

    if updates_elements_count >= 10
      elements_to_remove = (updates_elements_count - 10) + 1
      @$('#updates_wrap div:lt(' + elements_to_remove + ')').remove()

    @$('#updates_wrap').append(view.render().el)
    @$('#updates_wrap').data('timestamp', log_activity.get('timestamp'))
    $.sticky @$('#updates_wrap div p[data-timestamp=' + log_activity.get('timestamp') + ']').html()
