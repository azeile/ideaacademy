IA.Views.Footer ||= {}
IA.Views.Footer.User ||= {}
IA.Views.Footer.User.Contests ||= {}

class IA.Views.Footer.User.ContestsView extends Backbone.View
  template: JST["backbone/templates/footer/user/contests"]

  initialize: () ->
    _.bindAll(this, 'render', 'onContestsLoaded', 'appendContest')
    @eventBus = @options.eventBus
    @user = @options.user
    @collection = new IA.Collections.SmallContestsCollection()
    @collection.fetchRootContests(@onContestsLoaded)

  render: -> 
    $(@el).html(@template())
    return this

  onContestsLoaded: (contests) ->
    contests.each @appendContest

  appendContest: (contest) ->
    view = new IA.Views.Footer.User.Contests.ContestView({eventBus: @eventBus, user: @user, model: contest})
    @$('#footer_contests_list_wrap').append(view.render().el)
