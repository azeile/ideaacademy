IA.Views.Footer.User ||= {}
IA.Views.Footer.User.Contests ||= {}

class IA.Views.Footer.User.Contests.ContestView extends Backbone.View
  template: JST["backbone/templates/footer/user/contests/contest"]

  events:
    "click .footer_contest_log" : "onContestClicked"

  initialize: () ->
    _.bindAll(this, 'render')

  render: -> 
    $(@el).html(@template(@model.toJSON()))
    return this

  onContestClicked: () ->
    window.location.hash = "/contests/#{@model.id}"
    return false