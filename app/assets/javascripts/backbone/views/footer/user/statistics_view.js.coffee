IA.Views.Footer ||= {}
IA.Views.Footer.User ||= {}
IA.Views.Footer.User.Contests ||= {}

class IA.Views.Footer.User.StatisticsView extends Backbone.View
  template: JST["backbone/templates/footer/user/statistics"]

  initialize: () ->
    _.bindAll(this, 'render', 'onStatisticsLoaded', 'appendCategory')
    @eventBus = @options.eventBus
    @user = @options.user
    
    @collection = new IA.Collections.CategoriesCollection()
    @collection.fetchForNotifications(@onStatisticsLoaded)

    #@collection = new IA.Collections.SmallContestsCollection()
    #@collection.fetchRootContests(@onContestsLoaded)

  render: -> 
    $(@el).html(@template())
    return this

  onStatisticsLoaded: (categories) ->
    categories.each @appendCategory

  appendCategory: (category) ->
    view = new IA.Views.Footer.User.Statistics.StatisticView({eventBus: @eventBus, user: @user, model: category})
    @$('#statistics_categories').append(view.render().el)
