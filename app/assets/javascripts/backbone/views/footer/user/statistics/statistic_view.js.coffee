IA.Views.Footer.User ||= {}
IA.Views.Footer.User.Statistics ||= {}

class IA.Views.Footer.User.Statistics.StatisticView extends Backbone.View
  template: JST["backbone/templates/footer/user/statistics/statistic"]

  events:
    "click .footer_statistic_log" : "onCategoryClicked"

  initialize: () ->
    _.bindAll(this, 'render')

  render: -> 
    $(@el).html(@template(@model.toJSON()))
    return this

  onCategoryClicked: () ->
    window.location.hash = "/statistics/#{@model.id}"
    return false