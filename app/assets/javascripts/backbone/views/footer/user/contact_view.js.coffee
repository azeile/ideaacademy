IA.Views.Footer ||= {}
IA.Views.Footer.User ||= {}

class IA.Views.Footer.User.ContactView extends Backbone.View
  id: "contact-form"
  template: JST["backbone/templates/footer/user/contact"]

  events:
    "click .close" : "closeForm"
    "submit form" : "submitForm"

  initialize: ->
    _.bindAll(this, "render", "closeForm", "submitForm")

  render: ->
    $(@el).html(@template())
    return this

  closeForm: (e) ->
    e.stopPropagation()
    e.preventDefault()
    $(@el).remove()

  submitForm: (e) ->
    e.stopPropagation()
    e.preventDefault()
    data =
      message: $("#contact-message").val()
      email: $("#contact-email").val()
    $.post "/api/contact_form/create", data
    $(@el).remove()