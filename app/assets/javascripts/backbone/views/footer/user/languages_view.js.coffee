IA.Views.Footer ||= {}
IA.Views.Footer.User ||= {}

class IA.Views.Footer.User.LanguagesView extends Backbone.View
  template: JST["backbone/templates/footer/user/language"]

  tagName: "li"

  initialize: ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus
    @language = @options.language

  render: ->
    $(@el).html(@template({url: "/?lang=#{@language}", locale: @language}))
    return this
