IA.Views.Footer ||= {}
IA.Views.Footer.User ||= {}
IA.Views.Footer.User.LogActivities ||= {}

class IA.Views.Footer.User.LogActivities.NewIdeaView extends Backbone.View
  template: JST["backbone/templates/footer/user/log_activities/new_idea"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus
    @user = @options.user
    @log_activity = @model

  render: -> 
    $(@el).html(@template(@log_activity.toJSON()))
    return this