IA.Views.Footer ||= {}

class IA.Views.Footer.GuestView extends Backbone.View

  template: JST["backbone/templates/footer/guest"]

  # => Popup templates
  template_test_popup: JST["backbone/templates/popups/test_popup"]
  popup_welcome: JST["backbone/templates/popups/welcome"]
  popup_authorize: JST["backbone/templates/popups/authorize"]
  popup_first_points_vote: JST["backbone/templates/popups/first_points_vote"]
  popup_first_points_idea: JST["backbone/templates/popups/first_points_idea"]
  popup_earn_more: JST["backbone/templates/popups/earn_more"]
  popup_like_idea: JST["backbone/templates/popups/like_idea"]
  popup_first_achievement: JST["backbone/templates/popups/first_achievement"]
  popup_rules: JST["backbone/templates/popups/rules"]

  splash_new_contest: JST["backbone/templates/splash/new_contest"]

  events:
    "click .new_contest_link": "newContestLink"
    "click .rules_link": "rulesLink"

  initialize: () ->
    _.bindAll(this, 'render', 'renderContests', 'doGetPopupTmp', 'renderPopupTmp', 'definePopupTmp')
    @eventBus = @options.eventBus
    @eventBus.bind('doGetPopupTmp', @doGetPopupTmp)

  newContestLink: ->
    $("#content").html(@splash_new_contest())
    return false

  rulesLink: ->
    $.fancybox
      'content': @popup_rules()

  definePopupTmp: (popup) ->
    switch popup.typekey
      when "test_popup" then @template_test_popup(popup)
      when "welcome_popup" then @popup_welcome(popup)
      when "authorize" then @popup_authorize(popup)
      when "first_points_vote" then @popup_first_points_vote(popup)
      when "first_points_idea" then @popup_first_points_idea(popup)
      when "earn_more" then @popup_earn_more(popup)
      when "like_idea" then @popup_like_idea(popup)
      when "first_achievement" then @popup_first_achievement(popup)

  doGetPopupTmp: ->
    @popup = new IA.Models.Popup
    @popup.getPopupTmp(@model.get('id'), @renderPopupTmp)

  renderPopupTmp: (response) ->
    @popup = response.get('popup')

    if response.get('status') == true
      $.fancybox
        'content': @definePopupTmp(@popup),
        onComplete: ->
          $('.button.save').click -> $.fancybox.close()
          $('.button_1.green').click -> $.fancybox.close()
        onClosed: =>
          @onClosedPopup(response)

  onClosedPopup: (response) ->
    popup = response.get('popup')
    @popup = new IA.Models.Popup
    @popup.markTmpAsSeen(popup.tmp_user_id, popup.id, @triggerMorePopup)

  triggerMorePopup: ->
    @eventBus.trigger('doGetPopupTmp')

  render: -> 
    $(@el).html(@template())
    @renderContests()
    @eventBus.trigger('doGetPopupTmp')
    return this

  renderContests: () ->
    view = new IA.Views.Footer.User.ContestsView({ eventBus : @eventBus })
    @$("#footer_contests_wrap").html(view.render().el)