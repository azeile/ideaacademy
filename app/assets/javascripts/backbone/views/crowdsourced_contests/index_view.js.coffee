IA.Views.CrowdsourcedContests ||= {}

class IA.Views.CrowdsourcedContests.IndexView extends Backbone.View

  template: JST["backbone/templates/crowdsourced_contests/index"]

  events:
    "click .toRight" : "toRight"
    "click .toLeft" : "toLeft"
    "click #toggle_about" : "toggle_about"
    "mouseenter .slide": "hoverArrow"

  initialize: () ->
    _.bindAll(this, 'toLeft', 'toRight', 'render', 'loadRightSidebar', 'renderContest', 'beforeNextGet', 'beforePreviousGet', 'loadRootContest', 'loadContest', 'loadRootContestByCategory', 'onGetRootContest', 'changeCategory', 'hoverArrow')
    @eventBus = @options.eventBus
    @user = @options.user
    @contest = new IA.Models.CrowdsourcedContest
    @current_id = @options.id if @options.id > 0
    @eventBus.bind("changeContest", @loadContest)
    # @eventBus.bind("loadRootContestByCategory", @loadRootContestByCategory)
    # @eventBus.bind("updateAboutCategory", @updateAboutCategory)
    @fromRight = true
    @rootLoad = true
   
  render: ->
    $(@el).html(@template())

    @loadRightSidebar() if AppUtils.isInternalApp
    @loadSidebarWidgets()

    @loadRootContest()

    return this
  
  # updateAboutCategory: (name, about) ->   
  #   @$('#about_category h1').html(name)
  #   @$('#about_category p').html(about)

  toggle_about: ->
    @$('#show_about').fadeToggle()

  loadRightSidebar: ->
    view = new IA.Views.Sidebars.RightView({ eventBus: @eventBus, user: @user })
    @$('#right_sidebar').html(view.render().el)

  loadSidebarWidgets: ->
    view = new IA.Views.Sidebars.SidebarWidgetsView({ eventBus: @eventBus, user: @user })
    if AppUtils.isInternalApp
      @$('#right_sidebar').append(view.render().el)
    else if AppUtils.isFacebookApp
      @$('#left_sidebar').append(view.render().el)

  loadRootContest: ->
    if @current_id
      @loadContest(@current_id)
    else
      @contest.root(@onGetRootContest)

  loadRootContestByCategory: (categoryId) ->
    @contest.rootByCategory(categoryId, @onGetRootContest)

  onGetRootContest: (contest) ->
    if contest.id > 0
      window.location.hash = "/crowdsourced_contests/#{contest.id}"
    else
      @view = new IA.Views.Contests.Contest.NoContestsView
      $(this.el).html(@view.render().el)

  loadContest: (id) ->
    @contest.another id, @renderContest

  toLeft: () ->
    @$("#contest_slider_wrap").children("div div").animate({"left": "230px", "top": 0, "opacity": 0}, 500, 'easeInBack', @beforePreviousGet)
    return false
  
  toRight: () ->
    @$("#contest_slider_wrap").children("div div").animate({"left": "-230px", "top": 0, "opacity": 0}, 500, 'easeInBack', @beforeNextGet)
    return false

  beforeNextGet: ->
    id = @contest.get("next_id")
    if id is @contest.get("id")
      mainRouter.showCrowdsourcedContest(id)
    else
      window.location.hash = "/crowdsourced_contests/#{id}"
    @fromRight = true
    return false

  beforePreviousGet: ->
    id = @contest.get("previous_id")
    if id is @contest.get("id")
      mainRouter.showCrowdsourcedContest(id)
    else
      window.location.hash = "/crowdsourced_contests/#{id}"
    @fromRight = false
    return false

  renderContest: (contest) ->
    @contest = contest
    @animateContest(@contest)
    @renderTooltip(".toLeft div", @contest.previousSmallContest())
    @renderTooltip(".toRight div", @contest.nextSmallContest())
    @rootLoad = false
    
    if @contest.get('time_to_end')
      @$('.grid_6 .countdown').html(@contest.get('time_to_end')).css("display", "inline-block")

    if @contest.get('is_money') == true
      @$('.grid_6 .money').css("display", "inline-block")

    $('.slide').contestslider()
    $('.toggle').toggleElement()
    $('.countdown, .ideas_count, .rooms_count, .money').qtip ->
      content: 'This is an active list element',
      show: 'mouseover',
      hide: 'mouseout'

    @eventBus.trigger('doGetPopup')
    
    that = this
    setTimeout ->
      that.changeCategory(that.contest)
    , 100

    hide_description = -> 
      $.doTimeout "hide_about", 10000, ->
        $('#about_category.visible-on-ready #show_about').fadeOut()
        $('#about_category').removeClass("visible-on-ready")
    hide_description()
    $("#about_category.visible-on-ready")
      .on "mouseenter", ->
        $.doTimeout "hide_about"
      .on "mouseleave", ->
        hide_description()

  changeCategory: (contest) ->
    @eventBus.trigger("onChangeCategory", contest.get("category_id"))

  renderTooltip: (elName, model) ->
    view = new IA.Views.Contests.TooltipView({ model: model, user: @user })
    @$(elName).html(view.render().el)

  animateContest: (contest) ->
    view = new IA.Views.CrowdsourcedContests.CrowdsourcedContest.MainView({ eventBus: @eventBus, model: contest, user: @user })
    contestSlider = @$("#contest_slider_wrap")
    contestSlider.html(view.render().el)

    if @rootLoad
      contestSlider.children("div div").css({"position": "absolute", "left": "230px", "top": 0, "opacity": 0})
      contestSlider.children("div div").animate({"left": 0, "top": 0, "opacity": 1}, 1300, 'easeOutBack')

      @eventBus.trigger('updateScrollbar')
    else
      contestSlider.children("div div").css({"position": "absolute", "left": 0, "top": 0})
      if @fromRight
        contestSlider.children("div div").css({"left": "230px", "top": 0, "opacity": 0})
        contestSlider.children("div div").animate({"left": 0, "top": 0, "opacity": 1}, 800, 'easeOutBack')
      else
        contestSlider.children("div div").css({"left": "-230px", "top": 0, "opacity": 0})
        contestSlider.children("div div").animate({"left": 0, "top": 0, "opacity": 1}, 800, 'easeOutBack')

      @eventBus.trigger('updateScrollbar')

  hoverArrow: (e) ->
    if AppUtils.isFacebookApp
      @$(".test").hide()
      $(e.currentTarget).find(".test").show()
