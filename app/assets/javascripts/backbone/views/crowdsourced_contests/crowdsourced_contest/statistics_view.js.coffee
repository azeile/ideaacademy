IA.Views.CrowdsourcedContests ||= {}
IA.Views.CrowdsourcedContests.CrowdsourcedContest ||= {}

class IA.Views.CrowdsourcedContests.CrowdsourcedContest.StatisticsView extends Backbone.View

  template: JST["backbone/templates/crowdsourced_contests/crowdsourced_contest/statistics"]
  resultTemplate: JST["backbone/templates/crowdsourced_contests/crowdsourced_contest/result"]
  
  events:
    "click #load_more_ideas" : "doGetMoreResults"

  initialize: () ->
    _.bindAll(this, 'render', 'renderResults', 'renderError', 'renderMoreResults', 'doGetMoreResults')
    @eventBus = @options.eventBus
    @user     = @options.user
    @contest  = @options.contest
    @status   = @contest.get('status')
    
    @results  = new IA.Collections.CrowdsourcingResultsCollection
    @results.fetchResults(@contest.get('id'), @render, @render)
    

  renderResults: ->
    @results.each (result) =>
      @$('#mt tbody.all').append @renderResult(result)
  
  renderResult: (result) ->
    @resultTemplate(result.toJSON())
  
  renderError: ->
    @$("statistics_view").html("<div class=\"error\">#{I18n.t 'contests.contest.statistics.crowdsourcing.no_results', { defaultValue: "No results yet" }  }</div>")
    
  doGetMoreResults: ->
    $(@.el).append('<div class="loading_scroll_small"></div>')
    offset = @$('#load_more_ideas').attr('data-offset')
    @$("#load_more_ideas").hide()
    
    @results.fetchMoreResults(@contest.get('id'), @renderMoreResults, @renderError, offset)
    @eventBus.trigger('updateScrollbar')    

  renderMoreResults: (response) ->
    @$('.loading_scroll_small').remove()
    @renderResults()

    @$("#load_more_ideas").attr('data-offset', parseInt( @$("#load_more_ideas").data('offset') ) + 10)
    @$("#load_more_ideas").show() unless @results.length < 10

  render: ->
    $(this.el).html(@template())
    if @results.length > 0 
      @renderResults()
    else
      @renderError()
     
    @eventBus.trigger('updateScrollbar')
    return this

  renderMyIdeas: (response) ->
    if response.get('status') == true
      my_ideas = new Backbone.Collection(response.get('ideas'))
      view = new IA.Views.Contests.Contest.Statistics.MyIdeasView({ ideas: my_ideas })
      @$('#my_ideas_view').html(view.render().el)
    else
      @$('#my_ideas_view').html('<div class="error">' + response.get('message') + '</div>')
        
    @eventBus.trigger('updateScrollbar')
  