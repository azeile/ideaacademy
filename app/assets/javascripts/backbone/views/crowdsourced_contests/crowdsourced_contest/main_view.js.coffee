IA.Views.CrowdsourcedContests ||= {}
IA.Views.CrowdsourcedContests.CrowdsourcedContest ||= {}

class IA.Views.CrowdsourcedContests.CrowdsourcedContest.MainView  extends Backbone.View
  template: JST["backbone/templates/crowdsourced_contests/crowdsourced_contest/main"]

  initialize: () ->
    _.bindAll(this, 'render', 'renderStatisticsDone')
    @eventBus = @options.eventBus
    @user = @options.user
    @contest = @model

  render: ->
    $(this.el).html(@template(@model.toJSON()))
    @renderStatisticsDone()
    return this

  renderStatisticsDone: ->
    view = new IA.Views.CrowdsourcedContests.CrowdsourcedContest.StatisticsView({ eventBus: @eventBus, user: @user, contest: @contest })
    @$("#statistics_done_wrap").html(view.render().el)
    @eventBus.trigger('updateScrollbar')