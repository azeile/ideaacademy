IA.Views.Contests ||= {}

class IA.Views.Contests.TooltipView extends Backbone.View
  template: JST["backbone/templates/contests/tooltip"]
   
  render: ->
    $(this.el).html(@template(@model.toJSON() ))
    
    @addAttributes()

    return this

  addAttributes: ->
    if @model.get('time_to_end')
      @$('.tooltip_wrap .attributes').append('\n<li class="countdown" style="display: inline-block;" title="Laiks līdz konkursa beigām">' + @model.get('time_to_end') + '</li>')

    if @model.get('is_money') == true
      unless @options.user.get('session_id')
        @$('.tooltip_wrap .attributes').append('\n<li class="money" style="display: inline-block;"><img src="/assets/dollar_sign.png" width="9" height="9" /></li>')
