IA.Views.Contests ||= {}

class IA.Views.Contests.ShowView extends Backbone.View
  template: JST["backbone/templates/contests/show"]
   
  render: ->
    $(this.el).html(@template(@model.toJSON() ))
    return this