IA.Views.Contests ||= {}

class IA.Views.Contests.IndexView extends Backbone.View

  template: JST["backbone/templates/contests/index"]

  events:
    "click .toRight" : "toRight"
    "click .toLeft" : "toLeft"
    "mouseenter .slide": "hoverArrow"

  initialize: () ->
    _.bindAll(this, 'toLeft', 'toRight', 'render', 'loadRightSidebar', 'renderContest', 'beforeNextGet', 'beforePreviousGet', 'loadRootContest', 'loadContest', 'loadRootContestByCategory', 'onGetRootContest', 'changeCategory', 'hoverArrow')
    @eventBus = @options.eventBus
    @user = @options.user
    @contest = new IA.Models.Contest
    @eventBus.bind("changeContest", @loadContest)
    @eventBus.bind("loadRootContestByCategory", @loadRootContestByCategory)
    @fromRight = true
    @rootLoad = true

    if @options.id > 0
      @current_id = @options.id

  render: ->
    $(@el).html(@template())

    @loadRightSidebar()
    @loadSidebarWidgets()

    @loadRootContest()

    return this

  loadRightSidebar: ->
    view = new IA.Views.Sidebars.RightView({ eventBus: @eventBus, user: @user })
    if AppUtils.isInternalApp
      @$('#right_sidebar').html(view.render().el)
    else if AppUtils.isFacebookApp
      $("#app-view").append(view.render().el)

  loadSidebarWidgets: ->
    view = new IA.Views.Sidebars.SidebarWidgetsView({ eventBus: @eventBus, user: @user })
    if AppUtils.isInternalApp
      @$('#right_sidebar').append(view.render().el)
    else if AppUtils.isFacebookApp
      @$('#left_sidebar').append(view.render().el)

  loadRootContest: ->
    if @current_id
      @loadContest(@current_id)
    else
      @contest.root(@onGetRootContest)

  loadRootContestByCategory: (categoryId) ->
    @contest.rootByCategory(categoryId, @onGetRootContest)

  loadBubbles: ->

  onGetRootContest: (contest) ->
    if contest.id > 0
      window.location.hash = "/contests/#{contest.id}"
    else
      @view = new IA.Views.Contests.Contest.NoContestsView
      $(this.el).html(@view.render().el)

  loadContest: (id) ->
    @contest.another id, @renderContest

  toLeft: () ->
    @$("#contest_slider_wrap").children("div div").animate({"left": "230px", "top": 0, "opacity": 0}, 500, 'easeInBack', @beforePreviousGet)
    return false

  toRight: () ->
    @$("#contest_slider_wrap").children("div div").animate({"left": "-230px", "top": 0, "opacity": 0}, 500, 'easeInBack', @beforeNextGet)
    return false

  beforeNextGet: ->
    id = @contest.get("next_id")
    if id is @contest.get("id")
      mainRouter.showContest(id)
    else
      window.location.hash = "/contests/#{id}"
    @fromRight = true
    return false

  beforePreviousGet: ->
    id = @contest.get("previous_id")
    if id is @contest.get("id")
      mainRouter.showContest(id)
    else
      window.location.hash = "/contests/#{id}"
    @fromRight = false
    return false

  showOrganizationLogo: (contest) ->
    # $("#organization-logo").attr "src", contest.get "organization_logo_url"

  renderContest: (contest) ->
    @contest = contest
    @animateContest(@contest)

    @showOrganizationLogo(contest)

    if AppUtils.isInternalApp
      @renderTooltip(".toLeft div", @contest.previousSmallContest())
      @renderTooltip(".toRight div", @contest.nextSmallContest())
    else if AppUtils.isFacebookApp
      @renderTooltip("#left_sidebar .toLeft > div", @contest.previousSmallContest())
      @renderTooltip("#left_sidebar .toRight > div", @contest.nextSmallContest())

    @rootLoad = false

    if @contest.get('time_to_end')
      @$('.grid_6 .countdown').html(@contest.get('time_to_end')).css("display", "inline-block")

    if @contest.get('is_money') == true
      @$('.grid_6 .money').append("#{@contest.get("money")}")
      @$('.grid_6 .money').css("display", "inline-block")

    $('.slide').contestslider()
    $('.toggle').toggleElement()
    $('.countdown, .ideas_count, .rooms_count, .money').qtip ->
      content: 'This is an active list element',
      show: 'mouseover',
      hide: 'mouseout'

    @eventBus.trigger('doGetPopup')

    that = this
    setTimeout ->
      that.changeCategory(that.contest)
    , 100

    @renderContactAdminWindow()

    if AppUtils.isFacebookApp
      max_tooltip_height = 0
      @$(".test").each ->
        height = $(@).outerHeight()
        max_tooltip_height = Math.max(height, max_tooltip_height)
      @$(".navigation").height(max_tooltip_height + 43)
      @$("#left_sidebar .toRight .test").hide()


  changeCategory: (contest) ->
    @eventBus.trigger("onChangeCategory", contest.get("category_id"))

  renderTooltip: (elName, model) ->
    view = new IA.Views.Contests.TooltipView({ model: model, user: @user })
    @$(elName).html(view.render().el)

  renderContactAdminWindow: ->
    el = "#contact-admin"
    view = new IA.Views.Footer.User.ContactAdminView(el: el, user: @user, contest: @contest)
    $(el).html(view.render())

  animateContest: (contest) ->
    if contest.get('status') == 'done'
      view = new IA.Views.Contests.Contest.ContestDoneView({ eventBus: @eventBus, model: contest, user: @user })
    else
      if contest.get('user_allowed_to_participate')
        view = new IA.Views.Contests.Contest.ContestView({ eventBus: @eventBus, model: contest, user: @user })
      else
        view = new IA.Views.Contests.Contest.ContestNotAvailableView({ eventBus: @eventBus, model: contest, user: @user })

    contestSlider = @$("#contest_slider_wrap")
    contestSlider.html(view.render().el)

    if @rootLoad
      contestSlider.children("div div").css({"position": "absolute", "left": "230px", "top": 0, "opacity": 0})
      contestSlider.children("div div").animate({"left": 0, "top": 0, "opacity": 1}, 1300, 'easeOutBack')

      @eventBus.trigger('updateScrollbar')
    else
      contestSlider.children("div div").css({"position": "absolute", "left": 0, "top": 0})
      if @fromRight
        contestSlider.children("div div").css({"left": "230px", "top": 0, "opacity": 0})
        contestSlider.children("div div").animate({"left": 0, "top": 0, "opacity": 1}, 800, 'easeOutBack')
      else
        contestSlider.children("div div").css({"left": "-230px", "top": 0, "opacity": 0})
        contestSlider.children("div div").animate({"left": 0, "top": 0, "opacity": 1}, 800, 'easeOutBack')

      @eventBus.trigger('updateScrollbar')

  hoverArrow: (e) ->
    if AppUtils.isFacebookApp
      @$(".test").hide()
      $(e.currentTarget).find(".test").show()
