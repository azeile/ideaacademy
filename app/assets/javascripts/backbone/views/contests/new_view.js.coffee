IA.Views.Contests ||= {}

class IA.Views.Contests.NewView extends Backbone.View    

  template: JST["backbone/templates/contests/new"]
  template_rules: JST["backbone/templates/popups/rules"]

  ###################################################
  #Input elements, Events & Constructor
  constructor: (options) ->
    super(options)
    _.bindAll(this, 'render', 'onReady', 'events', 'initUploadify', 'getCategories', 'onGetCategories', 'appendCategory')
    @model = new @collection.model()
    @tmp_model = new @collection.model()
    @eventBus = options.eventBus

    #@model.bind("change:errors", () =>
    #  this.render()
    #)

  # => Events
  _events =
    "click #openRulesPopup": "openRulesPopup"
    "click #forward": "forward"
    "click #backward": "backward"
    "click #cancel": "cancel"
    "submit #new-contest": "save"
    "click #contest_request_crowdsourcing_flag" : "fetchAppropriateCategories"

  events: =>
    events = _.extend {}, _events
    #events["keyup #{@getInputElement('title')}"] = "setAttributeValue"
    return events

  setAttributeValue: (event) ->
    element = event.currentTarget
    attribute = @$(element).attr('name')
    value = @$(element).val()
    
    @attributeValue = {}
    @attributeValue = _.extend {}, @attributeValue

    @attributeValue["#{attribute}"] = value

    @validateAttribute(element)

    @tmp_model.set(@attributeValue)

    if @$(element).attr('name') == "contest_request[person_type]"
      if value == "juridiska"
        @$('#company_name_wrap').fadeIn(300)
      else
        @$('#company_name_wrap').fadeOut(300)

    if @$(element).attr('name') == "contest_request[accept_rules]"
      @$(element).attr('disabled', 'disabled')

  setElementEvent: (events, selector, handlers, callback) ->
    @$(selector).each ->
      attribute_id = $(@).attr('id')
      events["#{event} ##{attribute_id}"] = "#{callback}" for event in handlers if attribute_id
    
    return events

  updateEvents: ->
    events = _.extend {}, @events()
    events = @setElementEvent(events, 'textarea', ["keyup", "change"], "setAttributeValue")
    events = @setElementEvent(events, 'select, input[type=checkbox], input[type=hidden]', ["change"], "setAttributeValue")

    @delegateEvents(events)

  validateAttribute: (element) ->
    attribute = $(element).attr('name')
    value = $(element).val()

    errors = false

    switch attribute
      when "contest_request[parent_attributes][typekey]"
        errors = true if value == ''
      when "contest_request[parent_attributes][title]"
        errors = true if value.length < 3
        errors = true if value.length > 100
      when "contest_request[parent_attributes][description]"
        errors = true if value.length < 5
      when "contest_request[parent_attributes][category_id]"
        errors = true if value == ''
      when "contest_request[person_type]"
        errors = true if value == ''
      when "contest_request[name]"
        errors = true if value.length < 1
      when "contest_request[email]"
        atpos = value.indexOf("@")
        dotpos = value.lastIndexOf(".")
        errors = true if value.length < 1
        errors = true if atpos < 1 || dotpos < atpos+2 || dotpos+2 >= value.length
      when "contest_request[phone]"
        errors = true if value.length < 1
        errors = true if value.length > 8
        errors = true if isNaN(value)
      when "contest_request[address]"
        errors = true if value.length < 1
      when "contest_request[accept_rules]"
        if $(element).is(':checked') == false
          errors = true

    if errors
      $(element).parents().addClass('invalid')
    else
      $(element).parents().removeClass('invalid').addClass('valid')

    return errors

  validateFormStepFirst: ->
    error_typekey = @validateAttribute(@$("#contest_request_parent_attributes_typekey"))
    error_title = @validateAttribute(@$("#contest_request_parent_attributes_title"))
    error_description = @validateAttribute(@$("#contest_request_parent_attributes_description"))
    error_category = @validateAttribute(@$("#contest_request_parent_attributes_category_id"))

    return true if error_typekey == false && error_title == false && error_description == false && error_category == false
    return false

  validateForm: ->
    error_typekey = @validateAttribute(@$("#contest_request_parent_attributes_typekey"))
    error_title = @validateAttribute(@$("#contest_request_parent_attributes_title"))
    error_description = @validateAttribute(@$("#contest_request_parent_attributes_description"))
    error_category = @validateAttribute(@$("#contest_request_parent_attributes_category_id"))
    error_person_type = @validateAttribute(@$("#contest_request_person_type"))
    error_company_name = @validateAttribute(@$("#contest_request_company_name"))
    error_name = @validateAttribute(@$("#contest_request_name"))
    error_email = @validateAttribute(@$("#contest_request_email"))
    error_phone = @validateAttribute(@$("#contest_request_phone"))
    error_address = @validateAttribute(@$("#contest_request_address"))
    error_accept_rules = @validateAttribute(@$("#contest_request_accept_rules"))

    return true if error_typekey == false && error_title == false && error_description == false && error_category == false && error_person_type == false && error_company_name == false && error_name == false && error_email == false && error_phone == false && error_address == false && error_accept_rules == false
    return false

  ###################################################
  # Form steps forward/backward
  forward: ->
    @$('#contest_info_wrap').fadeOut(300)
    @$('#contacts_info_wrap').delay(300).fadeIn(300)
    @eventBus.trigger('updateScrollbar')

  backward: (event) ->
    event.preventDefault()
    @$('#contacts_info_wrap').fadeOut(300)
    @$('#contest_info_wrap').delay(300).fadeIn(300)
    @eventBus.trigger('updateScrollbar')

  ###################################################
  cancel: (event) ->
    event.preventDefault()
    window.location = "/"

  # Do submit
  save: (event) ->
    event.preventDefault()
    event.stopPropagation()

    if @validateForm() == true
      @model.unset("errors")
      @$('.form_error').each ->
        $(@).remove()

      ends_at = new Date()
      ends_at.setDate(ends_at.getDate() + 2)

      @model.set({
        contest_request: {
          crowdsourcing_flag : $("#contest_request_crowdsourcing_flag").is(':checked'),
          parent_attributes: {
            category_id  : @tmp_model.get('contest_request[parent_attributes][category_id]'),
            title        : @tmp_model.get('contest_request[parent_attributes][title]'),
            description  : @tmp_model.get('contest_request[parent_attributes][description]'),
            typekey      : @tmp_model.get('contest_request[parent_attributes][typekey]'),
            language     : @tmp_model.get('contest_request[parent_attributes][language]'),
            ends_at      : ends_at,
            contest_image_id: @$('#contest_image_id').val(),
            award        : "Nav norādīts"
          }
        }
      })

      @collection.create(@model.toJSON(),
        url: "/api/contest_requests",
        success: (contest) =>
          #@model = contest
          #window.location.hash = "/#{@model.id}"
          @$('#new-contest').html( I18n.t('messages.contest_request_received', { defaultValue: 'Thank You, contest request received!' }) )
          
        error: (contest, jqXHR) =>
          @model.set({errors: $.parseJSON(jqXHR.responseText)})
          @renderErrors(@model.get('errors'))
      )

    else
      @backward() if @validateFormStepFirst() == false

  ###################################################
  # Callback on ready render
  onReady: ->
    $('select').each -> $(this).chosen()

  ###################################################
  # Render errors (server side)
  renderErrors: (errors) ->
    #@model.get('errors').each(@addError)
    if errors["parent.title"] || errors["parent.description"]
      @$('#contacts_info_wrap').fadeOut(300)
      @$('#contest_info_wrap').delay(300).fadeIn(300)

    if errors["parent.title"]
      input = @$("#contest_request_parent_attributes_title")
      @appendError input, message for message in errors["parent.title"]
    
    if errors["parent.description"]
      input = @$("#contest_request_parent_attributes_description")
      @appendError input, message for message in errors["parent.description"]
    
    if errors["person_type"]
      input = @$("#contest_request_person_type")
      @appendError input, message for message in errors["person_type"]

    if errors["company_name"]
      input = @$("#contest_request_company_name")
      @appendError input, message for message in errors["company_name"]
    
    if errors["name"]
      input = @$("#contest_request_name")
      @appendError input, message for message in errors["name"]

    if errors["email"]
      input = @$("#contest_request_email")
      @appendError input, message for message in errors["email"]

    if errors["phone"]
      input = @$("#contest_request_phone")
      @appendError input, message for message in errors["phone"]

    if errors["address"]
      input = @$("#contest_request_address")
      @appendError input, message for message in errors["address"]

  appendError: (input, message) ->
    error_markup = '<div class="form_error"><strong>' + message + '</strong></div>'
    input.after(error_markup)

  # Render form
  render: ->
    $(this.el).html(@template(@model.toJSON()))

    # => Plugin to display tooltips
    @$('textarea, input').each(->
        tooltip = $(@).data('tooltip')

        if tooltip
            tooltip_markup = '<div class="form_tooltip" style="display:none;">' + tooltip + '</div>'

            $(@).focus(->
                $(@).after(tooltip_markup)
                $(@).nextAll('div.form_tooltip').fadeIn(300)
            )

            $(@).focusout(->
                $(@).nextAll('div.form_tooltip').fadeOut(300).delay(300).remove()
            )
    )

    #@$('textarea').autoResize ->
    #    onResize: ->
    #        $(this).css({opacity:0.8})
    #    
    #    animateDuration: 300

    #@$('textarea').elastic()
    @$('textarea').autogrow()

    @updateEvents()

    #this.$("form").backboneLink(@model)
    @initUploadify()

    @getCategories()

    @eventBus.trigger('updateScrollbar')

    return this

#==============================================================
# Get categories for dropdown
#==============================================================

  fetchAppropriateCategories: (event) ->
    $('#language-select-label, #language-select-container').toggle()
    @getCategories $(event.target).is(':checked')
    $("#contest_request_parent_attributes_language").trigger("change");

  getCategories: (facebook_flag) ->
    @categories = new IA.Collections.CategoriesCollection
    if facebook_flag
      @categories = @categories.fetchForCrowdsourcing(@onGetCategories)
    else
      @categories = @categories.fetchForNotifications(@onGetCategories)
      
  onGetCategories: (categories) ->
    @$('#contest_request_parent_attributes_category_id').html("")
    categories.each @appendCategory
    $("#contest_request_parent_attributes_category_id").trigger("liszt:updated");
    $("#contest_request_parent_attributes_category_id").trigger("change");

  appendCategory: (category) ->
    @$('#contest_request_parent_attributes_category_id').append('<option value="' + category.get('id') + '">' + category.get('name') + '</option>')
    
    #Hack to assign `chosen` plugin on view load
    setTimeout(@onReady, 1)

    #@$('.category_list').append('<li><input type="checkbox" data-id="' + category.get('id') + '" name="category_' + category.get('id') + '" id="category_' + category.get('id') + '"><label for="category_' + category.get('id') + '">' + category.get('name') + '</label></li>')

#==============================================================

  openRulesPopup: ->
    $.fancybox
      'content': @template_rules()
    
    return false
 
  initUploadify: () ->
    uploadifyConfig = @setUploadifyConfig()
    func = =>
      @$("#image").uploadify(uploadifyConfig)
    setTimeout(func, 1)
    true

  setUploadifyConfig: ->
    @uploadify_script_data = {}
    @setCsrfForUploadify()
    @setSessionForUploadify()
    uploadifyConfig = {
      queueSizeLimit: 1
      auto:true
      uploader: '/assets/plugins/uploadify.swf'
      expressInstall: '/assets/plugins/expressInstall.swf'
      script: "/api/contests/upload_image.json"
      cancelImg: '/assets/close.png'
      fileDataName: @$("#image").attr('name')
      fileExt: '*.jepg,*.jpg;*.gif;*.png'
      fileDesc: 'Image Files'
      sizeLimit: 5242880
      scriptData: @uploadify_script_data
      onComplete: (event, queueID, fileObj, response, data) =>
         tmp_image = $.parseJSON(response)
         @$("#contest_image").html('<img src="' + tmp_image.logo + '" alt=""  />');
         @$("#contest_image_id").val(tmp_image.id)
    }  
    uploadifyConfig

  setCsrfForUploadify: ->
    csrf_token = $('meta[name=csrf-token]').attr('content')
    if csrf_token != undefined
      csrf_param = jQuery('meta[name=csrf-param]').attr('content')
      @uploadify_script_data[csrf_param] = encodeURI(csrf_token)

  setSessionForUploadify: ->
    session_token = $('meta[name=session-token]').attr('content')
    if session_token != undefined
      session_param = jQuery('meta[name=session-param]').attr('content')
      @uploadify_script_data[session_param] = encodeURI(session_token)
