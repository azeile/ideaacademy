IA.Views.Contests.Contest.Ideas ||= {}

class IA.Views.Contests.Contest.Ideas.NewView extends Backbone.View
  template: JST["backbone/templates/contests/contest/ideas/new"]

  minLenght: 4
  maxLenght: 140

  events:
    "submit #new-idea": "save"
    "focus #text": "clearDefault"
    "blur #text": "addDefault"
    "mouseover form": "formMouseOver"
    "click #new-idea": "signup"

  initialize: () ->
    _.bindAll(this, 'render', 'reset', 'renderError')
    @eventBus = @options.eventBus
    @user = @options.user
    @model = new @collection.model()
    @model.set({text: @defaultTextareaValue})
    @contest = @options.contest
    @defaultTextareaValue = I18n.t("contests.contest.ideas.new.my_idea")
    @model.bind("change:errors", () =>
      this.render()
    )

  render: ->
    $(this.el).html(@template(@model.toJSON()))

    @$('div.photo').html('<img class="photo" src="' + @user.get('image_url') + '" width="38" height="38" alt="" />')

    #@$('textarea').autoResize ->
    #  onResize: ->
    #    $(this).css({opacity:0.8})
    #  animateCallback: ->
    #    $(this).css({opacity:1})
    #  animateDuration: 300

    #@$('textarea').autoResize()
    @$('textarea').autogrow()

    @$('textarea').data('maxlength', @maxLenght)
    @$('#form_counter').html(@maxLenght)

    @$('textarea').live('keyup', (el) ->
      current_count = $('#text').val().length
      count_left = parseInt($(el.currentTarget).data('maxlength')) - current_count

      fc = $('#form_counter')
      
      if count_left < 0
        fc.css('color', '#CC0000')
      else
        fc.css('color', '#666666')

      fc.html(count_left)
    )

    @addDefault()
    
    if @user.get("user_type") == 'guest'
      
      login_popup = new IA.Views.NewHeader.LoginPopup
      @$("#new-idea").fancybox
        content: login_popup.render().el
        width: 600
        onClosed: ->
          login_popup.delegateEvents()
    

    return this
  
  signup: (e) ->
    if @user.get("user_type") == 'guest'
      e.preventDefault()
      e.stopPropagation()
      login_popup = new IA.Views.NewHeader.LoginPopup
      @$("#new-idea").fancybox
        content: login_popup.render().el
        width: 600
        onClosed: ->
          login_popup.delegateEvents()
      
  save: (e) ->
    e.preventDefault()
    e.stopPropagation()    
    text = @getText()

    if text != @defaultTextareaValue && text.length < @minLenght
      @$("#form_error").html("| <strong>#{I18n.t('contests.contest.ideas.errors.idea_too_short', { defaultValue: 'Idea too short.' })}</strong>")

    if text != @defaultTextareaValue && text.length > @maxLenght
      @$("#form_error").html("| <strong>#{I18n.t('contests.contest.ideas.errors.idea_too_long', { defaultValue: 'Idea too long.' })}</strong>")

    if text != @defaultTextareaValue && text.length <= @maxLenght && text.length >= @minLenght
      @model.unset("errors")
      @model.set({contest_id: 1, text: text})
      
      @collection.create(@model.toJSON(),
        url: "/api/contests/#{@contest.id}/ideas"
        success: (post, jqXHR) =>
          @model = post
          unless jqXHR.status == false
            @reset()
          else
            window.location.hash = "/contests/#{@contest.id}/payments"

        error: (post, jqXHR) =>
          @model.set({errors: $.parseJSON(jqXHR.responseText)})
          @renderError()
      )
    
  reset: ->
    @model = new @collection.model()
    @model.set({text: @defaultTextareaValue})
    @eventBus.trigger("doUpdateCategory", @user.get('id'), @contest.get('category_id'))
    @eventBus.trigger('updateStatistics')
    @eventBus.trigger('doGetLogActivities')
    @render()

  renderError: ->
    @$("#form_error").html('| <strong>' + @model.get("errors").text[0] + '</strong>')

  clearDefault: (e) ->
    if @getText() == @defaultTextareaValue
      @setText("")

  addDefault: (e) ->
    if @getText() == ""
      @setText(@defaultTextareaValue)
  
  formMouseOver: (e) ->
    #@$(".grid_10").animate({ backgroundColor: "#303030" }, 400)

  getText: ->
    @$("#text").val()

  setText: (text) ->
    @$("#text").val(text)
     
