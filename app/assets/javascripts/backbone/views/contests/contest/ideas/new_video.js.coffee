IA.Views.Contests.Contest.Ideas ||= {}

class IA.Views.Contests.Contest.Ideas.NewVideoView extends Backbone.View
  template: JST["backbone/templates/contests/contest/ideas/new_video"]

  defaultValue: "Youtube video adrese..."
  minLenght: 4
  maxLenght: 140

  events:
    "submit #new-idea": "save"
    "focus #text": "clearDefault"
    "blur #text": "addDefault"
    "mouseover form": "formMouseOver"

  initialize: () ->
    _.bindAll(this, 'render', 'reset', 'renderError')
    @eventBus = @options.eventBus
    @user = @options.user
    @model = new @collection.model()
    @model.set({text: @defaultValue})
    @contest = @options.contest
    @model.bind("change:errors", () =>
      this.render()
    )

  render: ->
    $(this.el).html(@template(@model.toJSON()))

    @$('div.photo').html('<img class="photo" src="' + @user.get('image_url') + '" width="38" height="38" alt="" />')

    #@$('textarea').autoResize ->
    #  onResize: ->
    #    $(this).css({opacity:0.8})
    #  animateCallback: ->
    #    $(this).css({opacity:1})
    #  animateDuration: 300

    #@$('textarea').autoResize();
    @$('textarea').autogrow()

    @$('textarea').data('maxlength', @maxLenght)
    @$('#form_counter').html(@maxLenght)

    @$('textarea').live('keyup', (el) ->
      current_count = $('#text').val().length
      count_left = parseInt($(el.currentTarget).data('maxlength')) - current_count

      fc = $('#form_counter')
      
      if count_left < 0
        fc.css('color', '#CC0000')
      else
        fc.css('color', '#666666')

      fc.html(count_left)
    )

    return this
    
  save: (e) ->
    e.preventDefault()
    e.stopPropagation()
    text = @getText()

    if text != @defaultValue && text.length < @minLenght
      @$("#form_error").html('| <strong>Ideja par īsu.</strong>')

    if text != @defaultValue && text.length > @maxLenght
      @$("#form_error").html('| <strong>Ideja par garu.</strong>')

    if text != @defaultValue && text.length <= @maxLenght && text.length >= @minLenght
      @model.unset("errors")
      @model.set({contest_id: 1, text: text})
      
      @collection.create(@model.toJSON(), 
        url: "/api/contests/#{@contest.id}/ideas" 
        success: (post) =>
          @model = post
          @reset()
          @eventBus.trigger('doGetLogActivities')

        error: (post, jqXHR) =>
          @model.set({errors: $.parseJSON(jqXHR.responseText)})
          @renderError()
      )
    
  reset: -> 
    @model = new @collection.model()
    @model.set({text: @defaultValue})
    @eventBus.trigger("doUpdateCategory", @user.get('id'), @contest.get('category_id'))
    @eventBus.trigger('updateStatistics')
    @eventBus.trigger('doGetLogActivities')
    @render()

  renderError: ->
    @$("#form_error").html('| <strong>' + @model.get("errors").text[0] + '</strong>')

  clearDefault: (e) ->
    if @getText() == @defaultValue
      @setText("")

  addDefault: (e) ->
    if @getText() == ""
      @setText(@defaultValue)
  
  formMouseOver: (e) ->
    @$(".grid_10").animate({ backgroundColor: "#303030" }, 400)

  getText: ->
    @$("#text").val()

  setText: (text) ->
    @$("#text").val(text)
     
