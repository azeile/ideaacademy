IA.Views.Contests.Contest.Ideas ||= {}

class IA.Views.Contests.Contest.Ideas.IdeaView extends Backbone.View
  template: JST["backbone/templates/contests/contest/ideas/idea"]

  initialize: () ->
    # if @options.contest.get('user_allowed_to_report_spam') && AppUtils.isFacebookApp then @model.show_spam_button = true else @model.show_spam_button = false
    @model.show_spam_button = false
    _.bindAll(this, 'render')

  render: ->
    $(this.el).html(@template(@model))
    return this
