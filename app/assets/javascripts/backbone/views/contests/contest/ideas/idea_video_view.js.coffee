IA.Views.Contests.Contest.Ideas ||= {}

class IA.Views.Contests.Contest.Ideas.IdeaVideoView extends Backbone.View
  template: JST["backbone/templates/contests/contest/ideas/idea_video"]

  initialize: () ->
    _.bindAll(this, 'render')
    @idea = @model

  render: ->
    $(this.el).html(@template(@model))
    @$('img').fancybox({href:"http://www.youtube.com/embed/#{@idea.video_info}?autoplay=1", type:"iframe", padding:10})
    return this