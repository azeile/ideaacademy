IA.Views.Contests.Contest.Ideas ||= {}

class IA.Views.Contests.Contest.Ideas.IdeaImageView extends Backbone.View
  template: JST["backbone/templates/contests/contest/ideas/idea_image"]

  initialize: () ->
    _.bindAll(this, 'render')
    @idea = @model

  render: ->
    $(this.el).html(@template(@model))
    @$('img').fancybox({
      href:@idea.image
      autoDimensions: true
      autoScale: true
      type:"image"
      onComplete: -> 
        $("#fancybox-img").css("border-radius", "17px")
    })
    return this