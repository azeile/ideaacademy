IA.Views.Contests.Contest.Ideas ||= {}

class IA.Views.Contests.Contest.Ideas.NewImageView extends Backbone.View
  template: JST["backbone/templates/contests/contest/ideas/new_image"]

  events:
    "submit #new-idea": "save"

  initialize: () ->
    _.bindAll(this, 'render', 'initUploadify', 'renderError')
    @eventBus = @options.eventBus
    @user = @options.user
    @model = new @collection.model()
    @contest = @options.contest
    
  render: ->
    $(this.el).html(@template(@model.toJSON()))
    @$('div.photo').html('<img class="photo" src="' + @user.get('image_url') + '" width="38" height="38" alt="" />')
    @initUploadify()
    this
    
  save: (e) ->
    e.preventDefault()
    e.stopPropagation()
    file = @$("#image")
    if file.uploadifySettings('queueSize') > 0
      file.uploadifyUpload();

  renderError: ->
    @$("#form_error").html('| <strong>' + @model.get("errors").text[0] + '</strong>')

  initUploadify: () ->
    uploadifyConfig = @setUploadifyConfig()
    func = =>
      @$("#image").uploadify(uploadifyConfig)
    setTimeout(func, 1)
    true
          
  setUploadifyConfig: ->
    @uploadify_script_data = {}
    @setCsrfForUploadify()
    @setSessionForUploadify()
    uploadifyConfig = {
      queueSizeLimit: 1
      auto:true
      uploader: '/assets/plugins/uploadify.swf'
      expressInstall: '/assets/plugins/expressInstall.swf'
      script: "/api/contests/#{@contest.id}/ideas.json"
      cancelImg: '/assets/close.png'
      fileDataName: @$("#image").attr('name')
      fileExt: '*.jepg,*.jpg;*.gif;*.png'
      fileDesc: 'Image Files'
      sizeLimit: 5242880
      scriptData: @uploadify_script_data
      onComplete: (event, queueID, fileObj, response, data) =>
        @model = response
        @eventBus.trigger('doGetLogActivities')
    }  
    uploadifyConfig
    
  setCsrfForUploadify: ->
    csrf_token = $('meta[name=csrf-token]').attr('content')
    if csrf_token != undefined
      csrf_param = jQuery('meta[name=csrf-param]').attr('content')
      @uploadify_script_data[csrf_param] = encodeURI(csrf_token)

  setSessionForUploadify: ->
    session_token = $('meta[name=session-token]').attr('content')
    if session_token != undefined
      session_param = jQuery('meta[name=session-param]').attr('content')
      @uploadify_script_data[session_param] = encodeURI(session_token)
