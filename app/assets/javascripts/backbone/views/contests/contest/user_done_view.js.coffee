IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}

class IA.Views.Contests.Contest.UserDoneView extends Backbone.View
  template: JST["backbone/templates/contests/contest/user_done"]

  initialize: () ->
    _.bindAll(this, 'doGetResults', 'render', 'renderResults', 'renderStatisticsDone')
    @eventBus = @options.eventBus
    @user = @options.user
    @contest = @model
    @results = new Backbone.Model

  render: ->
    $(this.el).html(@template(@model.toJSON()))

    @doGetResults()
    @renderStatisticsDone()

    return this

  doGetResults: ->
    @results.fetch { url: "/api/statistics/contest_end_results/" + @contest.get('id'), success: @renderResults }
    @eventBus.trigger('updateScrollbar')

  renderResults: (results) ->
    view = new IA.Views.Contests.Contest.ResultsView({ eventBus: @eventBus, model: results, user: @user })
    @$("#results_wrap").html(view.render().el)
    @eventBus.trigger('updateScrollbar')

  renderStatisticsDone: ->
    view = new IA.Views.Contests.Contest.StatisticsDoneView({ eventBus: @eventBus, user: @user, contest: @contest })
    @$("#statistics_done_wrap").html(view.render().el)
    @eventBus.trigger('updateScrollbar')