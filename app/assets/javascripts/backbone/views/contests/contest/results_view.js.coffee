IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}

class IA.Views.Contests.Contest.ResultsView extends Backbone.View

  template: JST["backbone/templates/contests/contest/results"]
  template_money: JST["backbone/templates/contests/contest/results_money"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus
    @user = @options.user

  render: ->
    if @model.get('is_money')
      $(this.el).html(@template_money(@model.toJSON()))
    else
      $(this.el).html(@template(@model.toJSON()))

    return this