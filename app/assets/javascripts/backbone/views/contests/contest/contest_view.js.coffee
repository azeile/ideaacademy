IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}

class IA.Views.Contests.Contest.ContestView extends Backbone.View
  template: JST["backbone/templates/contests/contest/contest"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus
    @user = @options.user
    
  render: ->
    $(this.el).html(@template(@model.toJSON()))
    if @user.get("session_id")
      view = new IA.Views.Contests.Contest.GuestView({ eventBus: @eventBus, model: @model, user: @user })
    else
      view = new IA.Views.Contests.Contest.UserView({ eventBus: @eventBus, model: @model, user: @user })
    @$("#contest_container").html(view.render().el)
    return this