IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}

class IA.Views.Contests.Contest.GuestView extends Backbone.View
  template: JST["backbone/templates/contests/contest/guest"]

  initialize: () ->
    _.bindAll(this, 'render', 'renderDuel')
    @user = @options.user
    @eventBus = @options.eventBus
    @ideasCollection = new IA.Collections.IdeasCollection()
    
  render: ->
    $(this.el).html(@template(@model.toJSON()))
    
    @renderDuel()
     
    return this
    
  renderDuel: ->
    view = new IA.Views.Contests.Contest.DuelView({ eventBus: @eventBus, model: @model, user: @user })
    @$("#duel_wrap").html(view.render().el)