IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}

class IA.Views.Contests.Contest.GuestNotAvailableView extends Backbone.View
  template: JST["backbone/templates/contests/contest/guest_not_available"]

  initialize: () ->
    _.bindAll(this, 'render')

  render: ->
    $(this.el).html(@template(@model.toJSON()))
    return this