IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}

class IA.Views.Contests.Contest.StatisticsView extends Backbone.View

  template: JST["backbone/templates/contests/contest/statistics"]

  events:
    "click #tab_players" : "doGetPlayers"
    "click #tab_my_ideas" : "doGetMyIdeas"
    "click #tab_top_ideas" : "doGetTopIdeas"
    "click #tab_all_ideas" : "doGetAllIdeas"

  initialize: () ->
    _.bindAll(this, 'render', 'doGetPlayers', 'doGetMyIdeas', 'renderPlayers', 'doGetTopIdeas', 'renderTopIdeas', 'renderAllIdeas', 'doGetAllIdeas', 'updateStatistics')
    @eventBus = @options.eventBus
    @eventBus.bind("updateStatistics", @updateStatistics)
    @user = @options.user
    @contest = @options.contest
    @status = @contest.get('status')
    
    if @options.total == true
      @total = true
    else
      @total = false

  doGetPlayers: ->
    @$('#statistics_tabs.tabs li').removeClass('active')
    @$('#tab_players').addClass('active')
    @hide_other_tabs('#statistics_view')
    @$('#statistics_view').show().html('<div class="loading_scroll_small"></div>')

    if @total == true
      @contest.fetch { url: "/api/statistics/contest_users/" + @contest.get('id'), success: @renderPlayers }
    else
      @contest.fetch { url: "/api/statistics/contest_users/" + @contest.get('id') + "/" + @user.get('id'), success: @renderPlayers }

    @eventBus.trigger('updateScrollbar')

  doGetMyIdeas: ->
    @$('#statistics_tabs.tabs li').removeClass('active')
    @$('#tab_my_ideas').addClass('active')
    @hide_other_tabs('#my_ideas_view')
    @$('#my_ideas_view').show().html('<div class="loading_scroll_small"></div>')

    if @total == true
      @contest.fetch { url: "/api/statistics/contest_ideas/" + @contest.get('id') + '?ideas_scope=my_ideas', success: @renderMyIdeas }
    else
      @contest.fetch { url: "/api/statistics/contest_ideas/" + @contest.get('id') + "/" + @user.get('id') + '?ideas_scope=my_ideas', success: @renderMyIdeas }

    @eventBus.trigger('updateScrollbar')
    
  doGetTopIdeas: ->
    @$('#statistics_tabs.tabs li').removeClass('active')
    @$('#tab_top_ideas').addClass('active')
    @hide_other_tabs('#top_ideas_view')
    @$('#top_ideas_view').show().html('<div class="loading_scroll_small"></div>')

    @contest.fetch { url: "/api/statistics/contest_ideas/" + @contest.get('id') + '?ideas_scope=top_ideas', success: @renderTopIdeas }

    @eventBus.trigger('updateScrollbar')

  doGetAllIdeas: ->
    @$('#statistics_tabs.tabs li').removeClass('active')
    @$('#tab_all_ideas').addClass('active')
    @hide_other_tabs('#all_ideas_view')
    @$('#all_ideas_view').show().html('<div class="loading_scroll_small"></div>')

    @contest.fetch { url: "/api/statistics/contest_ideas/" + @contest.get('id') + '?ideas_scope=all_ideas', success: @renderAllIdeas }

    @eventBus.trigger('updateScrollbar')  

  updateStatistics: ->
    @$('#statistics_tabs.tabs li').removeClass('active')
    @$('#tab_players').addClass('active')
    @$('#my_ideas_view').hide()
    @$('#top_ideas_view').hide()
    @$('#statistics_view').show()
    @contest.fetch { url: "/api/statistics/contest_room/" + @contest.get('id'), success: @renderPlayers }

    @eventBus.trigger('updateScrollbar')

  render: ->
    $(this.el).html(@template())
    @doGetPlayers()  
    @eventBus.trigger('updateScrollbar')
    return this

  renderPlayers: (response) ->
    if response.get('status') == true
      statistics = new Backbone.Collection(response.get('players'))
      view = new IA.Views.Contests.Contest.Statistics.PlayersView({ eventBus: @eventBus, players: statistics, show_more: response.get('show_more') })
      @$('#statistics_view').html(view.render().el)
    
    else
      @$('#statistics_view').html('<div class="error">' + response.get('message') + '</div>')

    @eventBus.trigger('updateScrollbar')

  renderMyIdeas: (response) ->
    if response.get('status') == true
      my_ideas = new Backbone.Collection(response.get('ideas'))
      view = new IA.Views.Contests.Contest.Statistics.MyIdeasView({ ideas: my_ideas })
      @$('#my_ideas_view').html(view.render().el)

    else
      @$('#my_ideas_view').html('<div class="error">' + response.get('message') + '</div>')
        
    @eventBus.trigger('updateScrollbar')
  
  renderTopIdeas: (response) ->
    if response.get('status') == true
      ideas = new Backbone.Collection(response.get('ideas'))
      view = new IA.Views.Contests.Contest.Statistics.TopIdeasView({ ideas: ideas, eventBus: @eventBus, contest: @contest })
      @$('#top_ideas_view').html(view.render().el)
    else
      @$('#top_ideas_view').html('<div class="error">' + response.get('message') + '</div>')  

    @eventBus.trigger('updateScrollbar')

  renderAllIdeas: (response) ->
    if response.get('status') == true
      ideas = new Backbone.Collection(response.get('ideas'))
      view = new IA.Views.Contests.Contest.Statistics.AllIdeasView({ ideas: ideas, eventBus: @eventBus, contest: @contest })
      @$('#all_ideas_view').html(view.render().el)
    else
      @$('#all_ideas_view').html('<div class="error">' + response.get('message') + '</div>')  

    @eventBus.trigger('updateScrollbar')

  hide_other_tabs: (current_tab_view) ->
    @$(".tab-view:not(#{current_tab_view})").hide()