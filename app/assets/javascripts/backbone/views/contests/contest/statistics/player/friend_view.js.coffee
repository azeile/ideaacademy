IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}
IA.Views.Contests.Contest.Statistics ||= {}
IA.Views.Contests.Contest.Statistics.Player ||= {}

class IA.Views.Contests.Contest.Statistics.Player.FriendView extends Backbone.View
  template: JST["backbone/templates/contests/contest/statistics/player/friend"]

  initialize: () ->
    _.bindAll(this, 'render')
         
  render: ->
    @$(this.el).unbind()
    this.el = this.template(this.model.toJSON())
    this.delegateEvents();
    return this