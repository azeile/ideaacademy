IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}
IA.Views.Contests.Contest.Statistics ||= {}
IA.Views.Contests.Contest.Statistics.Idea ||= {}

class IA.Views.Contests.Contest.Statistics.Idea.DefaultView extends Backbone.View
  template: JST["backbone/templates/contests/contest/statistics/idea/default"]
  only_idea_template: JST["backbone/templates/contests/contest/statistics/idea/default_idea_only"]

  initialize: () ->
    _.bindAll(this, 'render')
         
  render: (options={})->
    @$(this.el).unbind()
    @el = if options.idea_only? then @only_idea_template(this.model.toJSON()) else @template(this.model.toJSON())
    @delegateEvents();
    return this