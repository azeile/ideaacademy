IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}
IA.Views.Contests.Contest.Statistics ||= {}
IA.Views.Contests.Contest.Statistics.Idea ||= {}

class IA.Views.Contests.Contest.Statistics.Idea.ImageView extends Backbone.View
  template: JST["backbone/templates/contests/contest/statistics/idea/image"]
  only_idea_template: JST["backbone/templates/contests/contest/statistics/idea/image_idea_only"]

  events: 
    "click .idea" : "showFancybox"

  initialize: () ->
    _.bindAll(this, 'render')
         
  render: (options={})->
    #@$(this.el).unbind()
    #this.el = this.template(this.model.toJSON())
    #this.delegateEvents();
    $(this.el).html( if options.idea_only? then @only_idea_template(@model.toJSON()) else @template(@model.toJSON()) )
    return this
    
  showFancybox: () ->
    $.fancybox({
      href:@model.get("image")
      autoDimensions: true
      autoScale: true
      type:"image"
      onComplete: -> 
        $("#fancybox-img").css("border-radius", "17px")
    })