IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}
IA.Views.Contests.Contest.Statistics ||= {}
IA.Views.Contests.Contest.Statistics.Idea ||= {}

class IA.Views.Contests.Contest.Statistics.Idea.VideoView extends Backbone.View
  template: JST["backbone/templates/contests/contest/statistics/idea/video"]
  template: JST["backbone/templates/contests/contest/statistics/idea/video_idea_only"]
  
  events: 
    "click .idea" : "showFancybox"
  
  initialize: () ->
    _.bindAll(this, 'render')
         
  render: (options={})->
    #@$(this.el).unbind()
    #this.el = this.template(this.model.toJSON())
    #this.delegateEvents();      
    $(@el).html( if options.idea_only? then @only_idea_template(@model.toJSON()) else @template(@model.toJSON()) )
    return this
    
  showFancybox: () ->
    $.fancybox({href:"http://www.youtube.com/embed/#{@model.get("video_info")}?autoplay=1", type:"iframe", padding:10})