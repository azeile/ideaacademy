IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}
IA.Views.Contests.Contest.Statistics ||= {}

class IA.Views.Contests.Contest.Statistics.MyIdeasView extends Backbone.View
  template: JST["backbone/templates/contests/contest/statistics/my_ideas"]

  initialize: () ->
    _.bindAll(this, 'addOne', 'addAll', 'render')
    @ideas = @options.ideas.bind('reset', @addAll)
   
  addAll: () ->
    @ideas.each(@addOne)
  
  addOne: (idea) ->
    if idea.get("typekey") == "text"
      view = new IA.Views.Contests.Contest.Statistics.Idea.DefaultView({ model: idea })
    
    else if idea.get("typekey") == "image"
      view = new IA.Views.Contests.Contest.Statistics.Idea.ImageView({ model: idea })
    
    else if idea.get("typekey") == "video"
      view = new IA.Views.Contests.Contest.Statistics.Idea.VideoView({ model: idea }) 
           
    @$('.leaderboard tbody').append(view.render().el)
              
  render: ->
    $(@el).html(@template( show_more_button: false ))
    @addAll()
    
    return this