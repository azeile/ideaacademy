IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}
IA.Views.Contests.Contest.Statistics ||= {}

class IA.Views.Contests.Contest.Statistics.PlayersView extends Backbone.View

  template: JST["backbone/templates/contests/contest/statistics/players"]
  template_category: JST["backbone/templates/statistics/players"]
  template_more_button: JST["backbone/templates/statistics/more_button"]

  initialize: () ->
    _.bindAll(this, 'addOne', 'addAll', 'afterAddAll', 'render', 'updateScrollbar')
    @eventBus = @options.eventBus
    @show_more = @options.show_more
    @category_statistics = @options.category_statistics if @options.category_statistics
    @total_players = @options.total_players
    @page = @options.page if @options.page
    @players = @options.players.bind('reset', @addAll)

  updateScrollbar: () ->
    @eventBus.trigger('updateScrollbar')

  initView: (view, options) ->
    if @category_statistics
      switch view
        when "me" then view = new IA.Views.Statistics.Player.MeView(options)
        when "topfriend" then view = new IA.Views.Statistics.Player.TopFriendView(options)
        when "top" then view = new IA.Views.Statistics.Player.TopView(options)
        when "friend" then view = new IA.Views.Statistics.Player.FriendView(options)
        when "default" then view = new IA.Views.Statistics.Player.DefaultView(options)
    else
      switch view
        when "me" then view = new IA.Views.Contests.Contest.Statistics.Player.MeView(options)
        when "topfriend" then view = new IA.Views.Contests.Contest.Statistics.Player.TopFriendView(options)
        when "top" then view = new IA.Views.Contests.Contest.Statistics.Player.TopView(options)
        when "friend" then view = new IA.Views.Contests.Contest.Statistics.Player.FriendView(options)
        when "default" then view = new IA.Views.Contests.Contest.Statistics.Player.DefaultView(options)
    return view

  addAll: () ->
    @players.each(@addOne)
    @afterAddAll("all")
    @afterAddAll("friends")
    @$('.toggle_players').toggleElement({callback: @updateScrollbar})
    @$('.toggle_friends').toggleElement({callback: @updateScrollbar})
  
  addOne: (player) ->
    if player.get('me') == true
      view = @initView('me', { model: player })
      
      if player.get('order') <= 3
        @$('#leaderboard_all tbody.top').append(view.render().el)
        @$('#leaderboard_friends tbody.top').append(view.render().el)
      
      else
        @$('#leaderboard_all tbody.all').append(view.render().el)
        if @$('#leaderboard_friends tbody.top tr').length < 3
          @$('#leaderboard_friends tbody.top').append(view.render().el)
        else
          @$('#leaderboard_friends tbody.all').append(view.render().el)

    else if player.get('order') <= 3
      if player.get('friend') == true
        view = @initView('topfriend', { model: player })
        @$('#leaderboard_friends tbody.top').append(view.render().el)
      else
        view = @initView('top', { model: player })

      @$('#leaderboard_all tbody.top').append(view.render().el)
    
    else if player.get('friend') == true
      view = @initView('friend', { model: player })
      @$('#leaderboard_all tbody.all').append(view.render().el)

      if @$('#leaderboard_friends tbody.top tr').length < 3
        view = @initView('topfriend', { model: player })
        @$('#leaderboard_friends tbody.top').append(view.render().el)

      else
        view = @initView('friend', { model: player })
        @$('#leaderboard_friends tbody.all').append(view.render().el)        
    else
      view = @initView('default', { model: player })
      @$('#leaderboard_all tbody.all').append(view.render().el)

  afterAddAll: (who = "all") ->
    if @category_statistics
      @continue_logic = true if @total_players > 3
    else
      @continue_logic = true if @show_more

    if @continue_logic
      top_count = @$('#leaderboard_' + who + ' tbody.top tr').length
      more_count = @$('#leaderboard_' + who + ' tbody.all tr').length
      
      if @category_statistics
        show_more_count = @total_players - 3
      else
        show_more_count = more_count
      
      all_tbody = @$('#leaderboard_' + who + ' tbody.all')

      unless parseInt(@page) > 1
        all_tbody.after(@template_more_button({who: who, show_more_count: show_more_count}))


      preview_more = @$('#leaderboard_' + who + ' tbody.more')
      me = @$('#leaderboard_' + who + ' tbody.' + who + ' tr[data-me=true]')
      prev = me.prev().clone()
      next = me.next().clone()
      first = @$('#leaderboard_' + who + ' tbody.all tr').first().clone()
      last = @$('#leaderboard_' + who + ' tbody.all tr').last().clone()


      if more_count == 3
        preview_more.prepend(first.css('opacity', .75))
        preview_more.append(last.css('opacity', .5))
      else
        if parseInt(me.data('order')) > 3
          me = me.clone()
          preview_more.html(me)
          me.before(prev.css('opacity', .75))
          me.after(next.css('opacity', .75))

          if parseInt(first.data('order')) < parseInt(preview_more.children().first().data('order'))
            preview_more.prepend(first.css('opacity', .5))

          if parseInt(last.data('order')) > parseInt(preview_more.children().last().data('order'))
            preview_more.append(last.css('opacity', .5))

        else
          if more_count > 2
            preview_more.prepend(first.css('opacity', .75))
            preview_more.append(last.css('opacity', .5))

    unless @page
      @$('#leaderboard_' + who + ' tbody.all').hide()

    if @page
      @$('#pagination').removeAttr('style')

      if parseInt(@page) > 1
        preview_more.hide()
        @$('#leaderboard_' + who + ' tbody.top').hide()
        
               
  render: ->
    if @category_statistics
      $(@el).html(@template_category(players: @players.toJSON() ))
    else
      $(@el).html(@template(players: @players.toJSON() ))

    @addAll()

    if AppUtils.isInternalApp
      @$('td').each(->
          tooltip = $(@).data('tooltip')

          if tooltip
              tooltip_markup = '<div class="form_tooltip">' + tooltip + '</div>'

              $(@).mouseover(->
                  $(@).find('div').append(tooltip_markup)
              )

              $(@).mouseout(->
                  $(@).find('div').html('')
              )
      )

    @$(".toggle_players").click(->
      $("#pagination").toggle()
    )

    if @page
      data_html = @$(".toggle_players").html()
      data_close = @$(".toggle_players").data('close')
      @$(".toggle_players").data('close', data_html)
      @$(".toggle_players").removeClass('fold').html(data_close)

    recordsPerPage = 20
    #totalRecords = @$('#leaderboard_all tbody.all tr').length
    totalRecords = @total_players

    #@$('#leaderboard_all tbody.all tr').hide()

    #@$('#leaderboard_all tbody.all tr').slice(0, recordsPerPage).each(-> $(this).show())

    initpage = @page
    initpage = 0 unless @page

    @$('#leaderboard_all #smart-paginator').smartpaginator({
      totalrecords: totalRecords,
      recordsperpage: recordsPerPage,
      initval: initpage,
      onchange: (page) =>
        last_index = page * recordsPerPage
        first_index = last_index - recordsPerPage

        #$("#leaderboard_all tbody.all tr").hide()

        #$("#leaderboard_all tbody.all tr").slice(first_index, last_index).each(->
        #  $(this).show()
        #)
        window.stop()
        @eventBus.trigger('changeCategoryPlayersPage', page)
    })
      

    return this
