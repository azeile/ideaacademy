IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}
IA.Views.Contests.Contest.Statistics ||= {}

class IA.Views.Contests.Contest.Statistics.TopIdeasView extends Backbone.View
  template: JST["backbone/templates/contests/contest/statistics/my_ideas"]

  initialize: () ->
    _.bindAll(this, 'addOne', 'addAll', 'render', 'renderMoreIdeas', 'doGetMoreIdeas')
    @ideas = @options.ideas.bind('reset', @addAll)
    @eventBus = @options.eventBus
    @eventBus.bind("updateStatistics", @updateStatistics)
    @contest = @options.contest
    
  
  events:
    "click #load_more_ideas" : "doGetMoreIdeas"
   
  addAll: (ideas) ->
    ideas ?= @ideas
    ideas.each(@addOne)
  
  addOne: (idea) ->
    if idea.get("typekey") == "text"
      view = new IA.Views.Contests.Contest.Statistics.Idea.DefaultView({ model: idea })
    
    else if idea.get("typekey") == "image"
      view = new IA.Views.Contests.Contest.Statistics.Idea.ImageView({ model: idea })
    
    else if idea.get("typekey") == "video"
      view = new IA.Views.Contests.Contest.Statistics.Idea.VideoView({ model: idea }) 
           
    @$('.leaderboard tbody').append(view.render().el)
  
  doGetMoreIdeas: ->
    $(@.el).append('<div class="loading_scroll_small"></div>')
    offset = @$('#load_more_ideas').attr('data-offset')
    @$("#load_more_ideas").hide()
    
    @contest.fetch { url: "/api/statistics/contest_ideas/#{@contest.get('id')}?ideas_scope=top_ideas&offset=#{offset}", success: @renderMoreIdeas }
    @eventBus.trigger('updateScrollbar')    

  renderMoreIdeas: (response) ->
    if response.get('status') == true
      @$('.loading_scroll_small').remove()
      @ideas = new Backbone.Collection(response.get('ideas'))
      @addAll()

      @$("#load_more_ideas").attr('data-offset', parseInt( @$("#load_more_ideas").attr('data-offset') ) + 10)
      @$("#load_more_ideas").show() unless @ideas.length < 10
    else
      $(@.el).html('<div class="error">' + response.get('message') + '</div>')
              
  render: ->
    $(@el).html(@template( show_more_button: true ))
    @addAll()
    
    return this