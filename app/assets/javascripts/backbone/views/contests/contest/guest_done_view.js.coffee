IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}

class IA.Views.Contests.Contest.GuestDoneView extends Backbone.View
  template: JST["backbone/templates/contests/contest/guest_done"]

  initialize: () ->
    _.bindAll(this, 'render')

  render: ->
    $(this.el).html(@template(@model.toJSON()))
    return this