IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}

class IA.Views.Contests.Contest.StatisticsDoneView extends Backbone.View

  template: JST["backbone/templates/contests/contest/statistics_done"]
  template_no_room: JST["backbone/templates/contests/contest/statistics_done_no_room"]

  events:
    "click #tab_room" : "renderRoom"
    "click #tab_standings" : "renderStandings"

  initialize: () ->
    _.bindAll(this, 'render', 'renderRoom', 'renderStandings')
    @eventBus = @options.eventBus
    @user = @options.user
    @contest = @options.contest
    @im_in = @contest.get('im_in')

  render: ->
    if @im_in == false
      $(this.el).html(@template_no_room())
      @renderStandings()
    else
      $(this.el).html(@template())
      @renderRoom()

    @eventBus.trigger('updateScrollbar')
    return this

  renderRoom: ->
    @$('#statistics_done_tabs.tabs li').removeClass('active')
    @$('#tab_room').addClass('active')
    #@$('#statistics_done_view').show().html('<div class="loading_scroll_small"></div>')
    view = new IA.Views.Contests.Contest.StatisticsView({ eventBus: @eventBus, user: @user, contest: @contest })
    @$("#statistics_done_view").html(view.render().el)

    @eventBus.trigger('updateScrollbar')

  renderStandings: ->
    @$('#statistics_done_tabs.tabs li').removeClass('active')
    @$('#tab_standings').addClass('active')
    #@$('#statistics_done_view').show().html('<div class="loading_scroll_small"></div>')
    view = new IA.Views.Contests.Contest.StatisticsView({ eventBus: @eventBus, user: @user, contest: @contest, total: true })
    @$("#statistics_done_view").html(view.render().el)

    @eventBus.trigger('updateScrollbar')
