IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}

class IA.Views.Contests.Contest.ContestDoneView extends Backbone.View
  template: JST["backbone/templates/contests/contest/contest_done"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus
    @user = @options.user
    
  render: ->
    $(this.el).html(@template(@model.toJSON()))
    if @user.get("session_id")
      view = new IA.Views.Contests.Contest.GuestDoneView({ eventBus: @eventBus, model: @model })
    else
      view = new IA.Views.Contests.Contest.UserDoneView({ eventBus: @eventBus, model: @model, user: @user })
    
    @$("#contest_container").html(view.render().el)
    
    return this