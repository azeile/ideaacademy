IA.Views.Contests ||= {}
IA.Views.Contests.Contest ||= {}

class IA.Views.Contests.Contest.UserView extends Backbone.View
  template: JST["backbone/templates/contests/contest/user"]

  initialize: () ->
    _.bindAll(this, 'render', 'renderDuel', 'renderNewForm', 'renderNewFormImage', 'renderNewFormVideo', 'renderStatistics')
    @user = @options.user
    @eventBus = @options.eventBus
    @ideasCollection = new IA.Collections.IdeasCollection()

  events: ->
    "click #invite-friends": "inviteFriendsPopup"

  render: ->
    $(this.el).html(@template(@model.toJSON()))

    @renderDuel()
    
    if @model.get("typekey") == "text" 
      @renderNewForm()
      
    else if @model.get("typekey") == "image" 
      @renderNewFormImage()
      
    else if @model.get("typekey") == "video" 
      @renderNewFormVideo()
      
    @renderStatistics()

    return this

  renderDuel: ->
    view = new IA.Views.Contests.Contest.DuelView({ eventBus: @eventBus, model: @model, user: @user })
    @$("#duel_wrap").html(view.render().el)
    @eventBus.trigger('updateScrollbar')

  renderNewForm: ->
    view = new IA.Views.Contests.Contest.Ideas.NewView({ eventBus: @eventBus, collection : @ideasCollection, contest : @model, user: @user })
    @$("#idea_wrap").html(view.render().el)
    @eventBus.trigger('updateScrollbar')

  renderNewFormImage: ->
    view = new IA.Views.Contests.Contest.Ideas.NewImageView({ eventBus: @eventBus, collection : @ideasCollection, contest : @model, user: @user })
    @$("#idea_wrap").html(view.render().el)
    @eventBus.trigger('updateScrollbar')
    
  renderNewFormVideo: ->
    view = new IA.Views.Contests.Contest.Ideas.NewVideoView({ eventBus: @eventBus, collection : @ideasCollection, contest : @model, user: @user })
    @$("#idea_wrap").html(view.render().el)
    @eventBus.trigger('updateScrollbar')

  renderStatistics: ->
    view = new IA.Views.Contests.Contest.StatisticsView({ eventBus: @eventBus, user: @user, contest: @model })
    @$("#statistics_wrap").html(view.render().el)
    @eventBus.trigger('updateScrollbar')

  inviteFriendsPopup: (event) ->
    event.preventDefault()
    draugiemSendInvite "Iesniedz savu ideju konkursam!", {contest_id: @model.id}
