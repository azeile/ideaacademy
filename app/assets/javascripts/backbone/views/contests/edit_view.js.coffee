IA.Views.Contests ||= {}

class IA.Views.Contests.EditView extends Backbone.View
  template : JST["backbone/templates/contests/edit"]
  
  events :
    "submit #edit-contest" : "update"
    
  update : (e) ->
    e.preventDefault()
    e.stopPropagation()
    
    @model.save(null,
      success : (contest) =>
        @model = contest
        window.location.hash = "/#{@model.id}"
    )
    
  render : ->
    $(this.el).html(this.template(@model.toJSON() ))
    
    this.$("form").backboneLink(@model)
    
    return this