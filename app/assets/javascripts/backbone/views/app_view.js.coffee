class IA.Views.AppView extends Backbone.View
  template: JST["backbone/templates/app"]

  initialize: () ->
    _.bindAll(this, 'render', 'renderNewHeader', 'renderHeader', 'renderFooter', 'changeAppContent', 'onCategoriesLoaded', 'updateScrollbar')
    @eventBus = @options.eventBus
    @eventBus.bind("changeAppContent", @changeAppContent)
    @user = @options.user
    @scrollbar = @options.scrollbar
    @eventBus.bind("categoriesLoaded", @onCategoriesLoaded)
    @eventBus.bind("updateScrollbar", @updateScrollbar)
    @categoriesLoaded = false
    @currentView = null

  render: ->
    $(@el).html(@template)

    @renderNewHeader()
    #@renderHeader()
    @renderFooter()

    setInterval @updateScrollbar, 1000

    return this

  updateScrollbar: ->
    $(".scroll, .sidebar").css({height: "auto"})
    $(".scroll, .sidebar").equalizeHeights()
    scroll_height = $('.scroll').height()
    $('#AppView').height(scroll_height + 238 + 50) #50px for space in bottom
    #@scrollbar.tinyscrollbar_update('relative')

  renderNewHeader: ->
    if @user.get("session_id")
      view = new IA.Views.NewHeader.GuestView({ eventBus: @eventBus, model: @user })

    else
      view = new IA.Views.NewHeader.UserView({ eventBus: @eventBus, model: @user })

    @$("#new_header_wrap").append(view.render().el)

  renderHeader: ->
    if @user.get("session_id")
      view = new IA.Views.Header.GuestView({ eventBus: @eventBus, model: @user })
    
    else
      view = new IA.Views.Header.UserView({ eventBus: @eventBus, model: @user })

    @$("#header_wrap").append(view.render().el)

  renderFooter: ->
    console.info @user.get("user_type")
    console.info @user.get("session_id")

    if @user.get("session_id")
      view = new IA.Views.Footer.GuestView({ eventBus: @eventBus, model: @user })
    else
      view = new IA.Views.Footer.UserView({ eventBus: @eventBus, model: @user })

    console.info view

    @$("#footer_wrap").append(view.render().el)

  changeAppContent: (view) ->
    @$("#content_wrap").html(view.render().el)
    # @currentView = view
    # if @categoriesLoaded?
    #  @$("#content_wrap").html(view.render().el)

  onCategoriesLoaded: ->
    # @categoriesLoaded = true
    # if @currentView?
    #  @changeAppContent(@currentView)
