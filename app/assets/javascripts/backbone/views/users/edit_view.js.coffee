IA.Views.Users ||= {}

class IA.Views.Users.EditView extends Backbone.View

  template: JST["backbone/templates/users/edit"]

  initialize: () ->
    _.bindAll(this, 'render', 'renderForm')
    @eventBus = @options.eventBus
    @user = @options.user
    @id = @options.id

    @profile = new IA.Models.User
    @profile.getUser @id, @renderForm

  render: ->
    if parseInt(@id) == parseInt(@user.get('id')) && @user.get('user_type') != 'guest'
      $(@el).html(@template())
      
    else
      window.location.hash = "/users/#{@id}"
      
    return this

  renderForm: (response) ->
    view = new IA.Views.Users.Edit.FormView({ eventBus: @eventBus, user: @user, model: response })
    @$('#edit_form_wrap').html(view.render().el)

    @eventBus.trigger('updateScrollbar')