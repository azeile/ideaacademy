IA.Views.Users ||= {}
IA.Views.Users.Profile ||= {}

class IA.Views.Users.Profile.StatisticsView extends Backbone.View

  template: JST["backbone/templates/users/profile/statistics"]
  template_default: JST["backbone/templates/users/profile/statistics/default"]
  template_activity_vote: JST["backbone/templates/users/profile/statistics/activity_vote"]
  template_activity_idea: JST["backbone/templates/users/profile/statistics/activity_idea"]

  initialize: () ->
    _.bindAll(this, 'render', 'addStatistics', 'addStatistic')
    @eventBus = @options.eventBus
    @user = @options.user
    @statistics_collection = new Backbone.Collection(@options.statistics)
    @statistics = @statistics_collection.bind('reset', @addStatistics)

  addStatistics: ->
    @statistics.each(@addStatistic)

  addStatistic: (statistic) ->
    @$('#totals tbody').append(@template_default(statistic.toJSON()))
    @$('#idea_activity tbody').append(@template_activity_idea(statistic.toJSON()))
    @$('#vote_activity tbody').append(@template_activity_vote(statistic.toJSON()))

  render: ->
    $(@el).html(@template())
    @addStatistics()
    return this