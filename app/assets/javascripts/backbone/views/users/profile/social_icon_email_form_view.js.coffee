IA.Views.Users ||= {}
IA.Views.Users.Profile ||= {}

class IA.Views.Users.Profile.SocialIconEmailFormView extends Backbone.View

  template: JST["backbone/templates/users/profile/social_icon_email_form"]

  events:
    "submit #save-user-category-notifications": "save"

  initialize: () ->
    _.bindAll(this, 'getEmailCategories', 'onGetEmailCategories', 'appendCategory', 'render')
    @eventBus = @options.eventBus
    @collection = new IA.Collections.UserCategoryNotificationsCollection
    @model = new @collection.model()
    
  render: ->
    $(@el).html(@template())

    @getEmailCategories()

    return this

  getEmailCategories: ->
    @categories = new IA.Collections.CategoriesCollection
    @categories = @categories.fetchForNotifications(@onGetEmailCategories)

  onGetEmailCategories: (categories) ->
    @$('.loading_scroll_small').remove()
    categories.each @appendCategory

  appendCategory: (category) ->
    @$('.category_list').append('<li><input type="checkbox" data-id="' + category.get('id') + '" name="category_' + category.get('id') + '" id="category_' + category.get('id') + '"><label for="category_' + category.get('id') + '">' + category.get('name') + '</label></li>')

  save: (event) ->
    event.preventDefault()
    event.stopPropagation()

    category_list_id = new Array()

    @$("input[type=checkbox]").each ->
      if $(@).is(":checked")
        category_list_id.push(parseInt($(@).data('id')))

    @model.set({ category_list_id: category_list_id })

    @collection.create(@model.toJSON(),
      url: "/api/users/update_category_notifications",
      success: =>
        @setSuccessToForm()
        
      error: (contest, jqXHR) =>
        alert "error"
#        @model.set({errors: $.parseJSON(jqXHR.responseText)})
#        @renderErrors(@model.get('errors'))
    )

    return false

  setSuccessToForm: ->
    $.fancybox.close()