IA.Views.Users ||= {}
IA.Views.Users.Profile ||= {}

class IA.Views.Users.Profile.IdeasView extends Backbone.View

  template: JST["backbone/templates/users/profile/ideas"]
  template_default: JST["backbone/templates/users/profile/ideas/default"]
  template_video: JST["backbone/templates/users/profile/ideas/video"]
  template_image: JST["backbone/templates/users/profile/ideas/image"]

  events:
    "click div.choice a" : "contestLink"

  initialize: () ->
    _.bindAll(this, 'render', 'renderIdeas', 'addIdea')
    @eventBus = @options.eventBus
    @user = @options.user
    @profile = @options.profile
    @ideas_collection = new IA.Collections.IdeasCollection
    if @profile.get("vip") && @user.get("id") == @profile.get("id")
      @ideas_collection.fetchPortfolioIdeas(@profile.id)
      @isPortfolio = true
    else if @profile.get("vip")
      @ideas_collection.fetchPortfolioIdeas(@profile.id)
    else
      @ideas_collection.fetchTopIdeas(@profile.id, 10)
    @ideas = @ideas_collection.bind('reset', @renderIdeas)

  renderIdeas: ->
    @$('#top_ideas_count').text(@ideas.length)
    @ideas.each(@addIdea)

  addIdea: (idea, index) ->
    data = idea.toJSON()
    _.extend(data, {order: index + 1, portfolio: false})
    if idea.get("typekey") == "text"
      @$('#ideas_wrap').append(@template_default(data))
    
    else if idea.get("typekey") == "image"
      @$('#ideas_wrap').append(@template_image(data))
      @$('[data-idea=' + idea.id + ']').fancybox({
        href: idea.get("image")
        autoDimensions: true
        autoScale: true
        type:"image"
        onComplete: ->
          $("#fancybox-img").css("border-radius", "17px")
      })
      
    else if idea.get("typekey") == "video"
      @$('#ideas_wrap').append(@template_video(data))
      @$('[data-idea=' + idea.id + ']').fancybox({href:"http://www.youtube.com/embed/#{idea.get("video_info")}?autoplay=1", type:"iframe", padding:10})
            
  contestLink: (e) ->
    contest_url = $(e.currentTarget).attr('href')
    window.location.hash = contest_url
    return false

  render: ->
    $(@el).html(@template({ portfolio: @isPortfolio }))
    @renderIdeas()
    return this
