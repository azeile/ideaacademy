IA.Views.Users ||= {}
IA.Views.Users.Profile ||= {}

class IA.Views.Users.Profile.EditPortfolioPopupView extends Backbone.View

  template: JST["backbone/templates/users/profile/edit_portfolio_popup"]
  template_default: JST["backbone/templates/users/profile/ideas/default"]
  template_video: JST["backbone/templates/users/profile/ideas/video"]
  template_image: JST["backbone/templates/users/profile/ideas/image"]

  initialize: ->
    _.bindAll(this, "render", "renderIdeas", "addIdea", "valueChanged")
    @eventBus = @options.eventBus
    @user = @options.user
    @ideas_collection = new IA.Collections.IdeasCollection
    @ideas_collection.fetchTopIdeas @user.id, 100
    @ideas = @ideas_collection.bind('reset', @renderIdeas)

  events: ->
    "change input[type='checkbox']" : "valueChanged"
    "click div.choice a" : "contestLink"

  render: ->
    $(@el).html(@template({ count: @ideas.size() }))
    @renderIdeas()
    return this

  renderIdeas: ->
    @$("span.count").html @ideas.size()
    @ideas.each @addIdea

  addIdea: (idea, index) ->
    data = idea.toJSON()
    _.extend(data, {order: index + 1, portfolio: true})
    if idea.get("typekey") == "text"
      @$('.ideas .overview').append(@template_default(data))
    
    else if idea.get("typekey") == "image"
      @$('.ideas .overview').append(@template_image(data))
      
    else if idea.get("typekey") == "video"
      @$('.ideas .overview').append(@template_video(data))

  valueChanged: (e) ->
    id = parseInt e.currentTarget.value
    if e.currentTarget.checked
      @ideas.get(id).save { is_portfolio_idea: true },
        url: "/api/contests/#{@ideas.get(id).get("contest_id")}/ideas/#{@ideas.get(id).id}"
        success: (data) ->
          $(e.currentTarget).attr("checked", data.get("is_portfolio_idea"))
        error: (data) ->
          $(e.currentTarget).attr("checked", false)
    else
      @ideas.get(id).save { is_portfolio_idea: false },
        url: "/api/contests/#{@ideas.get(id).get("contest_id")}/ideas/#{@ideas.get(id).id}"
        success: (data) ->
          $(e.currentTarget).attr("checked", data.get("is_portfolio_idea"))

  contestLink: (e) ->
    contest_url = $(e.currentTarget).attr('href')
    $.fancybox.close()
    window.location.hash = contest_url
    return false
