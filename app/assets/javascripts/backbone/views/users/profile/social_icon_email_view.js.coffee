IA.Views.Users ||= {}
IA.Views.Users.Profile ||= {}

class IA.Views.Users.Profile.SocialIconEmailView extends Backbone.View

  tagName: 'li'
  className: 'email'

  template: JST["backbone/templates/users/profile/social_icon_email"]
  template_notif: JST["backbone/templates/users/profile/social_icon_email_notif"]

  events:
    "click a.receive": "getPopup"
    "click a.need_email": "getNotifPopup"

  # => Popup templates
  social_icon_email_form_template: JST["backbone/templates/popups/social_icon_email"]
  social_icon_email_notif_template: JST["backbone/templates/popups/social_icon_email_notif"]

  initialize: () ->
    _.bindAll(this, 'render')

  render: ->
    if @model.get('email') && @model.get('email') != ''
      $(@el).html(@template())

    else
      $(@el).html(@template_notif())

    return this

  getPopup: () ->
    @renderSocialIconEmailPopup()
    return false

  getNotifPopup: () ->
    $.fancybox
      'content': @social_icon_email_notif_template(@model.toJSON()),
      onComplete: ->
        $('.button.save').click -> $.fancybox.close()
        $('.button_1.green').click -> $.fancybox.close()
        $('a.your_profile').click -> $.fancybox.close()
  
    return false

  renderSocialIconEmailPopup: () ->
    $.fancybox
      'content': @social_icon_email_form_template(),
      onComplete: =>
        @bindSocialIconEmailPopup()
      onClosed: =>
        @unbindSocialIconEmailPopup()

  bindSocialIconEmailPopup: () ->
    $('.button.save').click -> $.fancybox.close()
    $('.button_1.green').click -> $.fancybox.close()
    
    view = new IA.Views.Users.Profile.SocialIconEmailFormView()
    $("#receive_notifications_form_wrap").html(view.render().el)

  unbindSocialIconEmailPopup: () ->