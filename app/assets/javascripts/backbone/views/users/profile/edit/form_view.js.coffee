IA.Views.Users ||= {}
IA.Views.Users.Edit ||= {}

class IA.Views.Users.Edit.FormView extends Backbone.View

  template: JST["backbone/templates/users/profile/edit/form"]
  template_picture_popup: JST["backbone/templates/users/profile/edit/picture_popup"]

  events:
    "submit #edit-user": "update"
    "click .popup": "openPicturePopup"

  maxLengthAbout: 400

  initialize: () ->
    _.bindAll(this, 'render', 'addSocialIcons', 'addSocialIcon', 'openPicturePopup', 'initUploadify')
    @eventBus = @options.eventBus
    @user = @options.user
    @bindUserData()
    @available_providers_collection = new Backbone.Collection(@model.get('available_providers'))
    @available_providers = @available_providers_collection.bind('reset', @addSocialIcons)

  bindUserData: () ->
    @user.bind "change", () =>
      @$('img.photo').attr("src", @user.get("image_url"))
          
  openPicturePopup: ->
    $.fancybox
      'content': @template_picture_popup(),
      onComplete: -> @test_z()
    return false
  
  test_z: ->
    alert "buu"

  update: (e) ->
    e.preventDefault()
    e.stopPropagation()

    @model.set(
      first_name: @getFirstName(), 
      last_name: @getLastName(), 
      phone: @getPhone(), 
      email: @getEmail(), 
      website: @getWebsite(), 
      about: @getAbout(), 
      work_tag: @getWorkTag,
      languages: @getCheckedLanguages())
    
    @model.save(@model.toJSON(),
      url: "/api/users/#{@model.get('id')}"
      success: (post) =>
        @model = post
        window.location.hash = "/users/#{@model.get('id')}"
    )

  render: ->
    $(@el).html(@template(@model.toJSON()))

    if @model.get('work_tag') == true
      @$('#user_work_tag').attr('checked', 'checked')
    else
      @$('#user_work_tag').removeAttr('checked')

    @$(".language_cb").each (i, val) =>
      $(val).attr("checked", "checked") if _.indexOf( @model.get('languages'), @extractLanguage($(val)) ) > -1 

    @addSocialIcons() if AppUtils.isFacebookApp

    $('a.authorization').live(
      click: ->
        url = $(this).attr('href')
        window.open(url, 'authorization_popup', 'toolbar=0,menubar=0,width=640,height=500,scrollbars=0')
        return false
    )
    
    @initUploadify()
    return this

  addSocialIcons: ->
    if @available_providers.length > 0
      @$('ul.social').before('<h6 class="invite">savienot profilu ar</h6>')

    @available_providers.each(@addSocialIcon)

  addSocialIcon: (provider) ->
    #@$('ul.social').append('\r\n<li class="' + provider.get('name') + '"><a class="authorization" href="/login/synchronize/create/' + provider.get('name') + '">#</a></li>')

  getFirstName: ->
    @$("#user_first_name").val()

  getLastName: ->
    @$("#user_last_name").val()

  getPhone: ->
    @$("#user_phone").val()

  getEmail: ->
    @$("#user_email").val()

  getWebsite: ->
    @$("#user_website").val()

  getAbout: ->
    @$("#user_about").val()

  getWorkTag: ->
    if @$('#user_work_tag').is(":checked")
      true
    else
      false

  extractLanguage: (element) ->
    language = element.attr('id').match(/user_([\w]+)_cb/)[1]
    language.charAt(0).toUpperCase() + language.slice(1);

  getCheckedLanguages: ->
    _.map @$(".language_cb:checked"), (val) =>
      @extractLanguage( $(val) )
    
  initUploadify: () ->
    placeUploadify = =>
      uploadifyConfig = @setUploadifyConfig()
      @$("#image").uploadify(uploadifyConfig)
    setTimeout(placeUploadify, 1)
    true

  setUploadifyConfig: ->
    @uploadify_script_data = {}
    @setCsrfForUploadify()
    @setSessionForUploadify()
    uploadifyConfig = {
      auto:true
      queueSizeLimit: 1
      uploader: '/assets/plugins/uploadify.swf'
      expressInstall: '/assets/plugins/expressInstall.swf'
      script: "/api/users/#{@user.id}/update_image.json"
      cancelImg: '/assets/close.png'
      fileDataName: @$("#image").attr('name')
      fileExt: '*.jepg,*.jpg;*.gif;*.png'
      fileDesc: 'Image Files'
      method: "PUT"
      sizeLimit: 5242880
      scriptData: @uploadify_script_data
      onComplete: (event, queueID, fileObj, response, data) =>
        @user.set($.parseJSON(response))
    }

  setCsrfForUploadify: ->
    csrf_token = $('meta[name=csrf-token]').attr('content')
    if csrf_token != undefined
      csrf_param = jQuery('meta[name=csrf-param]').attr('content')
      @uploadify_script_data[csrf_param] = encodeURI(csrf_token)

  setSessionForUploadify: ->
    session_token = $('meta[name=session-token]').attr('content')
    if session_token != undefined
      session_param = jQuery('meta[name=session-param]').attr('content')
      @uploadify_script_data[session_param] = encodeURI(session_token)
