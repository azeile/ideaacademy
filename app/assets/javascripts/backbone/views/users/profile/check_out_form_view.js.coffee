IA.Views.Users ||= {}
IA.Views.Users.Profile ||= {}

class IA.Views.Users.Profile.CheckOutFormView extends Backbone.View

  template: JST["backbone/templates/users/profile/check_out_form"]
  template_success: JST["backbone/templates/users/profile/check_out_form/success"]

  events:
    "submit #new_account": "save"
    "focus #account": "clearDefault"
    "blur #account": "addDefault"

  initialize: () ->
    _.bindAll(this, 'save', 'onCheckedOut', 'setStatusSuccessToForm', 'render')
    @defaultValue = I18n.t("users.profile.check_out_form.default_text.title", { defaultValue: "Your bank account number" })
    @eventBus = @options.eventBus
   
  render: ->
    $(@el).html(@template(@model.toJSON()))
    @setAccount(@defaultValue)
    return this

  save: (e) ->
    e.preventDefault()
    e.stopPropagation()
    
    account = @getAccount()
    if account != @defaultValue && (account.length < 10 || account.length > 25)
      @$("#form_error").html("#{I18n.t 'users.profile.check_out_form.form_error.invalid_account_number.title', {defaultValue: '<strong>Invalid account number!</strong>'}}")

    if account == @defaultValue || account.length < 1
      @$("#form_error").html("#{I18n.t 'users.profile.check_out_form.form_error.no_account_number.title', {defaultValue: '<strong>Empty account number</strong>'}}")

    if account != @defaultValue && account.length <= 25 && account.length >= 10
      @model.checkOut account, @onCheckedOut

  onCheckedOut: (user) =>
    @model.set(user.attributes)
    if @model.get("can_check_out")
      @$("#form_error").html("#{I18n.t 'users.profile.check_out_form.form_error.invalid_account_number.title', {defaultValue: '<strong>Invalid account number!</strong>'}}")
    else 
      @setStatusSuccessToForm()
     

  setStatusSuccessToForm: () ->
    # @eventBus.trigger "hideCheckOutButton"
    $(@el).html(@template_success)
    $('.button_1.green').click -> $.fancybox.close()
 
  clearDefault: (e) ->
    if @getAccount() == @defaultValue
      @setAccount("")

  addDefault: (e) ->
    if @getAccount() == ""
      @setAccount(@defaultValue)

  getAccount: ->
    @$("#account").val()

  setAccount: (account) ->
    @$("#account").val(account)




  