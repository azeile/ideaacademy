IA.Views.Users ||= {}
IA.Views.Users.Profile ||= {}

class IA.Views.Users.Profile.CheckOutButtonView extends Backbone.View

  tagName: 'span'

  template: JST["backbone/templates/users/profile/check_out_button"]

  # => Popup templates
  check_out_form_template: JST["backbone/templates/popups/check_out_form"]

  events:
    "click a" : "checkOut"

  initialize: () ->
    _.bindAll(this, 'checkOut', 'render')
    @eventBus = @options.eventBus

  render: ->
    $(@el).html(@template(@model.toJSON()))
    return this

  checkOut: () ->
    @renderCheckOutFormPopup()
    return false

  renderCheckOutFormPopup: () ->
    $.fancybox
      'content': @check_out_form_template(),
      onComplete: =>
        @bindCheckOutPopup()
        

  bindCheckOutPopup: () ->
    $('.button.save').click -> $.fancybox.close()
    $('.button_1.green').click -> $.fancybox.close()
    
    view = new IA.Views.Users.Profile.CheckOutFormView({ eventBus : @eventBus, model : @model })
    $("#checkout_form_placeholder").html(view.render().el)
