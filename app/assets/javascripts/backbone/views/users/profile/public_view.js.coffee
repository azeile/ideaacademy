IA.Views.Users ||= {}
IA.Views.Users.Profile ||= {}

class IA.Views.Users.Profile.PublicView extends Backbone.View

  template: JST["backbone/templates/users/profile/public"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus
    @user = @options.user

  render: ->
    $(@el).html(@template(@model.toJSON()))

    if @model.get('work_tag') == true
      @$('#tag_wrap').append('<div class="tag">Darba meklējumos</div>')

    return this
