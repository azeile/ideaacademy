IA.Views.Users ||= {}

class IA.Views.Users.ShowView extends Backbone.View

  template: JST["backbone/templates/users/show"]

  initialize: () ->
    _.bindAll(this, 'render', 'renderProfile', 'renderIdeas', 'renderStatisticsAndIdeas', 'renderProfileStatistics', 'renderProfileAwards', 'renderProfileTopIdeas')
    @eventBus = @options.eventBus
    @user = @options.user
    @id = @options.id

    @profile = new IA.Models.User

  render: ->
    $(@el).html(@template())

    @doGetProfile()
    @doGetStatistics()

    @eventBus.trigger('updateScrollbar')

    return this

  doGetProfile: ->
    @profile.getUser @id, @renderProfile

  doGetStatistics: ->
    @profile.getUserStatistic @id, @renderStatisticsAndIdeas

  renderProfile: (response) ->
    if parseInt(@id) == parseInt(@user.get('id')) && @user.get('user_type') != 'guest'
      view = new IA.Views.Users.Profile.PrivateView({ eventBus: @eventBus, user: @user, model: response })
    else
      view = new IA.Views.Users.Profile.PublicView({ eventBus: @eventBus, user: @user, model: response })
    
    @$('#user_profile_wrap').html(view.render().el)
    @$('#top5_cups').text(response.get('top5_cups'))

    @eventBus.trigger('updateScrollbar')

  renderStatisticsAndIdeas: (response) ->
    statistics_view = new IA.Views.Users.Profile.StatisticsView({ eventBus: @eventBus, user: @user, statistics: response.get('statistics') })
    @$('#user_profile_statistics_wrap').html(statistics_view.render().el)

    @renderIdeas()

    @eventBus.trigger('updateScrollbar')

  renderIdeas: ->
    ideas_view = new IA.Views.Users.Profile.IdeasView({ eventbus: @eventBus, user: @user, profile: @profile })
    @$('#user_profile_ideas_wrap').html(ideas_view.render().el)

    if @user.get("vip") && @user.get("id") == @profile.get("id")
      edit_portfolio_view = new IA.Views.Users.Profile.EditPortfolioPopupView({ eventbus: @eventBus, user: @user, profile: @profile })
      that = this
      @$("#edit-portfolio").fancybox
        content: edit_portfolio_view.render().el
        padding: 20
        onComplete: ->
          $("#fancybox-content .popup-scrollbar").tinyscrollbar()
        onClosed: ->
          that.renderIdeas()

  renderProfileStatistics: ->
    alert "render profile stats"

  renderProfileAwards: ->
    alert "render profile awards"
  
  renderProfileTopIdeas: ->
    alert "render profile top ideas"
