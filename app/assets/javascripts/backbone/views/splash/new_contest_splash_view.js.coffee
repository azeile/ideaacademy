class IA.Views.NewContestSplashView extends Backbone.View
  
  template: JST["backbone/templates/splash/new_contest"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus
    @user     = @options.user

  render: -> 
    $(@el).html(@template)
    return this