class IA.Views.SplashView extends Backbone.View
  
  template: JST["backbone/templates/splash/welcome"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus
    @user = @options.user

  render: -> 
    $(@el).html(@template)
    return this