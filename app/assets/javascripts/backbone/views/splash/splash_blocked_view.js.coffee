class IA.Views.SplashBlockedView extends Backbone.View
  
  template: JST["backbone/templates/splash/blocked"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus
    @user = @options.user

  render: -> 
    $(@el).html(this.template(@user.toJSON() ))
    return this