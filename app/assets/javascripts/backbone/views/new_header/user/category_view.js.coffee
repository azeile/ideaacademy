IA.Views.NewHeader ||= {}
IA.Views.NewHeader.User ||= {}

class IA.Views.NewHeader.User.CategoryView extends Backbone.View
  template: JST["backbone/templates/new_header/user/category"]

  events:
    "click .category_name" : "onCategoryClicked"

  initialize: () ->
    _.bindAll(this, 'render', 'onCategoryClicked', 'onChangeCategory')
    @eventBus = @options.eventBus
    @eventBus.bind("onChangeCategory", @onChangeCategory)
    @user = @options.user
    @category_active = @model.get("contests_count") > 0 ? true : false


  render: -> 
    if @category_active
      @model.set({active_class: "active"}) 
    else
      @model.set({active_class: ""}) 

    $(@el).html(@template(@model.toJSON()))
    return this

  onCategoryClicked: ->
    if @category_active?
      @eventBus.trigger("loadRootContestByCategory", @model.id)

    return false

  onChangeCategory: (id) ->
    if id == @model.id
      $('.category_name').removeClass('highlight')
      @$('.category_name').addClass('highlight')
      
      @eventBus.trigger("updateAboutCategory", @model.get('name'), @model.get('about_category'))

      @eventBus.trigger("doUpdateCategory", @user.get('id'), @model.get('id'))