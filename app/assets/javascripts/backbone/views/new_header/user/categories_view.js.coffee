IA.Views.NewHeader ||= {}
IA.Views.NewHeader.User ||= {}

class IA.Views.NewHeader.User.CategoriesView extends Backbone.View
  template: JST["backbone/templates/new_header/user/categories"]

  initialize: () ->
    _.bindAll(this, 'render', 'onGetCategories', 'doUpdateCategories', 'doUpdateCategory', 'onUpdateCategories', 'updateCategory', 'appendCategory')
    @eventBus = @options.eventBus
    @eventBus.bind("doUpdateCategory", @doUpdateCategory)
    @categories = new IA.Collections.CategoriesCollection()
    @categories.fetchForUser(@model.get('id'), @onGetCategories)

    setInterval @doUpdateCategories, 10000

  render: -> 
    $(@el).html(@template())
    return this

  doUpdateCategories: ->
    @categories.fetchForUserUpdate(@model.get('id'), @onUpdateCategories)

  doUpdateCategory: (user_id, category_id) ->
    @categories.fetchForUserSingleUpdate(user_id, category_id, @onUpdateCategories)

  onUpdateCategories: (categories) ->
    _.each categories.models, (category) ->
      @updateCategory category
    , this
  
  onGetCategories: (categories) ->
    _.each categories.models, (category) ->
      @appendCategory category
    , this
    @eventBus.trigger("categoriesLoaded")

  appendCategory: (category) ->
    view = new IA.Views.NewHeader.User.CategoryView({eventBus : @eventBus, model : category, user : @model})
    @$("#categories_view").append(view.render().el)

    el_live_points = @$('#categories_view #theme_' + category.get('id') + ' .points .plus')
    
    if category.get('live_points') > 0
      el_live_points.html(category.get('live_points')).show()

  updateCategory: (category) ->
    el_category = @$('#categories_view #theme_' + category.get('id'))
    el_badge = @$('#categories_view #theme_' + category.get('id') + ' .badge')
    el_total_points = @$('#categories_view #theme_' + category.get('id') + ' .points .total')
    el_live_points = @$('#categories_view #theme_' + category.get('id') + ' .points .plus')
    el_progress_complete = @$('#categories_view #theme_' + category.get('id') + ' .progress .complete')
    el_progress = @$('#categories_view #theme_' + category.get('id') + ' .progress p')

    el_badge.each -> $(@).removeClass().addClass('badge badge_' + category.get('level'))

    if category.get('percents') != parseInt(el_progress.html())
      el_progress_complete.animate({width: Math.floor(category.get('percents') / 100 * 114) }, 1000)
      el_progress.html(category.get('percents') + "%")

    if category.get('live_points') > 0
      el_live_points.html(category.get('live_points')).show()
      
      if el_category.hasClass('highlight')
        @doUpdateCategory(@model.get('id'), category.get('id'))

    else
      el_total_points.html(category.get('total_points'))
      el_live_points.html('').hide()
      
