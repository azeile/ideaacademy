IA.Views.NewHeader ||= {}

class IA.Views.NewHeader.GuestView extends Backbone.View
  template: JST["backbone/templates/new_header/guest"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus

  events: ->
    "click .logo .clickable" : "goToIndex"

  render: -> 
    $(@el).html(@template())

    @renderCategories()
    #@renderAuthorization()

    return this

  renderCategories: ->
    view = new IA.Views.NewHeader.Guest.CategoriesView({eventBus: @eventBus, model: @model})
    @$("#categories_wrap").html(view.render().el)

  renderAuthorization: ->
    view = new IA.Views.NewHeader.AuthorizationView()
    @$("#user_wrap").html(view.render().el)

  goToIndex: (e) ->
    e.preventDefault()
    window.location = "/"
