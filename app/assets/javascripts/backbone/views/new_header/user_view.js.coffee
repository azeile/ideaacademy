IA.Views.NewHeader ||= {}

class IA.Views.NewHeader.UserView extends Backbone.View
  template: JST["backbone/templates/new_header/user"]

  initialize: () ->
    _.bindAll(this, 'render', 'renderCategories', 'renderProfile')
    @eventBus = @options.eventBus

  events: ->
    "click .logo .clickable" : "goToIndex"

  render: -> 
    $(@el).html(@template())

    @renderCategories()
    @renderProfile()

    @renderFacebookLogoText() if AppUtils.isFacebookApp

    return this

  renderCategories: ->
    view = new IA.Views.NewHeader.User.CategoriesView({ eventBus : @eventBus, model: @model })
    @$("#categories_wrap").html(view.render().el)

  renderProfile: ->
    view = new IA.Views.NewHeader.ProfileView({ eventBus: @eventBus, model: @model })
    @$("#user_wrap").html(view.render().el)

  renderFacebookLogoText: ->
    @$("#new_header").append("<div class='logo-slogan'>")

  goToIndex: (e) ->
    e.preventDefault()
    window.location = "/"
