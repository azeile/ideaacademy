IA.Views.Statistics ||= {}
IA.Views.Statistics.Statistic ||= {}

class IA.Views.Statistics.Statistic.UserView extends Backbone.View
  template: JST["backbone/templates/statistics/statistic/user"]

  initialize: () ->
    _.bindAll(this, 'render', 'renderCategoryContests', 'renderCategoryPlayers', 'changeCategoryPlayersPage', 'renderCategpryPlayersPage')
    @eventBus = @options.eventBus
    @eventBus.bind("changeCategoryPlayersPage", @changeCategoryPlayersPage)
    @user = @options.user
    @statistics = @options.statistics
    
  render: ->
    $(@el).html(@template(@statistics.toJSON()))
    
    @renderCategoryContests()
    @renderCategoryPlayers()

    return this


  renderCategoryContests: ->
    contests = new Backbone.Collection(@statistics.get('contests'))
    view = new IA.Views.Statistics.ContestsView({ eventBus: @eventBus, contests: contests })
    @$('#category_contests_wrap').html(view.render().el)
    @eventBus.trigger('updateScrollbar')


  renderCategoryPlayers: ->
    statistics = new Backbone.Collection(@statistics.get('statistics').players)
    
    view = new IA.Views.Contests.Contest.Statistics.PlayersView({ eventBus: @eventBus, players: statistics, show_more: @statistics.get('statistics').show_more, total_players: @statistics.get('statistics').total_players, category_statistics: true })

    @$('#category_players_wrap').html(view.render().el)
    @eventBus.trigger('updateScrollbar')

  changeCategoryPlayersPage: (page) ->
    @statistics.nextPage @statistics.get('category').id, page, @renderCategpryPlayersPage

  renderCategpryPlayersPage: (response) ->
    @statistics = response
    statistics = new Backbone.Collection(@statistics.get('statistics').players)
    
    view = new IA.Views.Contests.Contest.Statistics.PlayersView({ eventBus: @eventBus, players: statistics, show_more: @statistics.get('statistics').show_more, total_players: @statistics.get('statistics').total_players, category_statistics: true, page: @statistics.get('page') })

    @$('#category_players_wrap').html(view.render().el)
    @eventBus.trigger('updateScrollbar')

