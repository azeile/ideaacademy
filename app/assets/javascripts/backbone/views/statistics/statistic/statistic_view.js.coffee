IA.Views.Statistics ||= {}
IA.Views.Statistics.Statistic ||= {}

class IA.Views.Statistics.Statistic.StatisticView extends Backbone.View
  template: JST["backbone/templates/statistics/statistic/statistic"]

  initialize: () ->
    _.bindAll(this, 'render')
    @eventBus = @options.eventBus
    @user = @options.user
    @statistics = @options.statistics

  render: ->
    $(this.el).html(@template())
    
    if @user.get('session_id')

    else
      view = new IA.Views.Statistics.Statistic.UserView({ eventBus: @eventBus, user: @user, statistics: @statistics })
      @$("#statistic_container").html(view.render().el)

    @eventBus.trigger('updateScrollbar')
    
    return this