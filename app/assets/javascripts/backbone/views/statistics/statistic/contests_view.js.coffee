IA.Views.Statistics ||= {}

class IA.Views.Statistics.ContestsView extends Backbone.View

  template: JST["backbone/templates/statistics/contests"]

  initialize: () ->
    _.bindAll(this, 'addAll', 'addOne', 'render')
    @eventBus = @options.eventBus
    @contests = @options.contests.bind('reset', @addAll)

  addAll: () ->
    @contests.each(@addOne)

  addOne: (contest) ->
    unless contest.get('status') == 'draft'

      contest.set({im_in_class: 'participate active'}) if contest.get('im_in') == true
      contest.set({im_in_class: 'participate'}) if contest.get('im_in') == false
      contest.set({im_in_class: 'participate'})
      if contest.get('status') == 'live'
        view = new IA.Views.Statistics.Contest.LiveView({ model: contest })
        @$('tbody.live').append(view.render().el)

      if contest.get('status') == 'done'
        view = new IA.Views.Statistics.Contest.DoneView({ model: contest })
        @$('tbody.done').append(view.render().el)

  render: ->
    $(@el).html(@template())

    @addAll()

    return this