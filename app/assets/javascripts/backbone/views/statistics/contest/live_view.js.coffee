IA.Views.Statistics ||= {}
IA.Views.Statistics.Contest ||= {}

class IA.Views.Statistics.Contest.LiveView extends Backbone.View

  tagName: 'tr'

  template: JST["backbone/templates/statistics/contest/live"]

  events:
    "click .contest_link" : "contestLink"

  initialize: () ->
    _.bindAll(this, 'render', 'contestLink')

  contestLink: ->
    id = @model.get('contest_id')
    window.location.hash = "/contests/#{id}"
    return false

  render: ->
    $(@el).html(@template(@model.toJSON() ))

    return this