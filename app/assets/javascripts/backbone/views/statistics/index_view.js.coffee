IA.Views.Statistics ||= {}

class IA.Views.Statistics.IndexView extends Backbone.View

  template: JST["backbone/templates/statistics/index"]

  events:
    "click .toRightStatistic" : "toRightStatistic"
    "click .toLeftStatistic" : "toLeftStatistic"

  initialize: () ->
    _.bindAll(this, 'render', 'toLeftStatistic', 'toRightStatistic', 'renderStatistic', 'loadStatistic', 'beforeNextGet', 'beforePreviousGet', 'animateStatistic')
    @eventBus = @options.eventBus
    @statistic = new IA.Models.Statistic
    @eventBus.bind("changeStatistic", @loadStatistic)
    @user = @options.user
    @fromRight = true
    @rootLoad = true

    if @options.id > 0
      @current_id = @options.id
    else
      @current_id = 1
   
  render: ->
    $(@el).html(@template())
    @loadStatistic(@current_id)
    @eventBus.trigger('updateScrollbar')
    return this

  loadStatistic: (id) ->
    @statistic.another id, @renderStatistic
    @eventBus.trigger('updateScrollbar')

  toLeftStatistic: () ->
    @$("#contest_slider_wrap").children("div div").animate({"left": "230px", "top": 0, "opacity": 0}, 500, 'easeInBack', @beforePreviousGet)
    return false
  
  toRightStatistic: () ->
    @$("#contest_slider_wrap").children("div div").animate({"left": "-230px", "top": 0, "opacity": 0}, 500, 'easeInBack', @beforeNextGet)
    return false

  beforeNextGet: ->
    id = @response.get('next_category').id
    if id is @current_id
      mainRouter.showStatistic(id)
    else
      window.location.hash = "/statistics/#{id}"
    @fromRight = true
    return false

  beforePreviousGet: ->
    id = @response.get('previous_category').id
    if id is @current_id
      mainRouter.showStatistic(id)
    else
      window.location.hash = "/statistics/#{id}"
    @fromRight = false
    return false

  renderStatistic: (response) ->
    @eventBus.trigger("onChangeCategory", response.get('category').id)

    @response = response

    view = new IA.Views.Statistics.Statistic.StatisticView({ eventBus: @eventBus, user: @user, statistics: response })
    @animateStatistic(view)

    @rootLoad = false

    @renderTooltip(".toLeftStatistic div", @response.get('previous_category'))
    @renderTooltip(".toRightStatistic div", @response.get('next_category'))

    $('.slide').contestslider()

  renderTooltip: (elName, test) ->
    view = new IA.Views.Statistics.TooltipView({ model : test })
    @$(elName).html(view.render().el)

  animateStatistic: (view) ->
    contestSlider = @$("#contest_slider_wrap")
    contestSlider.html(view.render().el)
    
    if @rootLoad
      contestSlider.children("div div").css({"position": "absolute", "left": "230px", "top": 0, "opacity": 0})
      contestSlider.children("div div").animate({"left": 0, "top": 0, "opacity": 1}, 1300, 'easeOutBack')

      @eventBus.trigger('updateScrollbar')
    else
      contestSlider.children("div div").css({"position": "absolute", "left": 0, "top": 0})
      if @fromRight
        contestSlider.children("div div").css({"left": "230px", "top": 0, "opacity": 0})
        contestSlider.children("div div").animate({"left": 0, "top": 0, "opacity": 1}, 800, 'easeOutBack')
      else
        contestSlider.children("div div").css({"left": "-230px", "top": 0, "opacity": 0})
        contestSlider.children("div div").animate({"left": 0, "top": 0, "opacity": 1}, 800, 'easeOutBack')

      @eventBus.trigger('updateScrollbar')