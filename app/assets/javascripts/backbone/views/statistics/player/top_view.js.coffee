IA.Views.Statistics ||= {}
IA.Views.Statistics.Player ||= {}

class IA.Views.Statistics.Player.TopView extends Backbone.View
  template: JST["backbone/templates/statistics/player/top"]

  initialize: () ->
    _.bindAll(this, 'render')
         
  render: ->
    @$(this.el).unbind()
    this.el = this.template(this.model.toJSON())
    this.delegateEvents();
    return this