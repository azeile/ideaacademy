IA.Views.Statistics ||= {}

class IA.Views.Statistics.TooltipView extends Backbone.View
  template: JST["backbone/templates/statistics/tooltip"]
   
  render: ->
    $(this.el).html(@template(@model))
    return this