class IA.Models.Category extends IA.Models.AbstractModel
  paramRoot: 'category'

  # Defaults
  defaults:
    name: null
    total_points: null
    live_points: null
    level: null
    percents: null
    contests_count: 0
  
class IA.Collections.CategoriesCollection extends IA.Models.AbstractCollection
  model: IA.Models.Category
  url: '/api/categories'
  fbUrl: "/api/facebook_categories"
  fetchForEmailNotifications: 'test'
  forUserUrl: '/statistics/user/'
  forUserUpdateUrl: '/statistics/user_update/'
  forUserSingleUpdateUrl: '/statistics/user_single_update/'
  forTmpUserUrl: '/statistics/tmp_user/'
  forTmpUserSingleUpdateUrl: '/statistics/tmp_user_single_update/'

  fetchForNotifications: (callback) ->
    @fetch { url: @url, success: callback }
  
  fetchForCrowdsourcing: (callback) ->
    @fetch { url: @fbUrl, success: callback }

  fetchForUser: (id, callback) ->
    @fetch { url: @buildUrl(@forUserUrl + id), success: callback }

  fetchForUserUpdate: (id, callback) ->
    @fetch { url: @buildUrl(@forUserUpdateUrl + id), success: callback }

  fetchForUserSingleUpdate: (id, category_id, callback) ->
    @fetch { url: @buildUrl(@forUserSingleUpdateUrl + id + '/' + category_id), success: callback }
  
  fetchForTmpUser: (id, callback) ->
    @fetch { url: @buildUrl(@forTmpUserUrl + id), success: callback }

  fetchForTmpUserSingleUpdate: (id, category_id, callback) ->
    @fetch { url: @buildUrl(@forTmpUserSingleUpdateUrl + id + '/' + category_id), success: callback }
