class IA.Models.ContestRequest extends IA.Models.AbstractModel

  contestUrl: '/contest_requests/'


class IA.Collections.ContestRequestsCollection extends Backbone.Collection
  model: IA.Models.ContestRequest
  url: '/contest_requests'