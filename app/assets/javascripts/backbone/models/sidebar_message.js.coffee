class IA.Models.SidebarMessage extends IA.Models.AbstractModel

  markAsSeenUrl: '/api/mark_bubble_as_seen/'

  # Defaults
  defaults:
    title: null
    message: null

  markAsSeen: (id, user_id) ->
    @fetch { url: @markAsSeenUrl + id + '/' + user_id }

class IA.Collections.SidebarMessagesCollection extends IA.Models.AbstractCollection
  model: IA.Models.SidebarMessage
  url: '/api/sidebar_messages'

  fetchForCurrentUser: (callback) ->
    @fetch { url: @url, success: callback }