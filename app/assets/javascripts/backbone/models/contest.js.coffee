class IA.Models.Contest extends IA.Models.AbstractModel

  paramRoot: 'contest'
  contestUrl: '/contests/'
  rootUrl: 'root'

  # Defaults
  defaults:
    category_id: null
    category: null
    title: null
    ends_at: null
    time_to_end: null
    is_money: false
    description: null
    award: null
    contest_image_id: null
    logo: null
    typekey: null
    next_id: null
    next_title: null
    next_time_to_end: null
    next_is_money: false
    previous_id: null
    previous_title: null
    previous_time_to_end: null
    previous_is_money: false
    allowed_to_report_spam: false

  # Methods
  previousSmallContest: () ->
    params =
      id:           @get("previous_id")
      title:        @get("previous_title")
      time_to_end:  @get('previous_time_to_end')
      is_money:     @get('previous_is_money')

    new IA.Models.SmallContest params

  nextSmallContest: () ->
    params =
      id:           @get("next_id")
      title:        @get("next_title")
      time_to_end:  @get('next_time_to_end')
      is_money:     @get('next_is_money')

    new IA.Models.SmallContest params

  next: (callback) ->
    @another(@get("next_id"), callback)

  previous: (callback) ->
    @another(@get("previous_id"), callback)
  
  another: (id, callback) ->
    @fetch {url : @buildUrl(@contestUrl + id), success : callback}

  root: (callback) ->
    @fetch {url : @buildUrl(@contestUrl + @rootUrl), success : callback}

  rootByCategory: (categoryId, callback) ->
    @fetch {url : @buildUrl(@contestUrl + @rootUrl + "?category_id=#{categoryId}"), success : callback} 

  isText: ->
    @get("typekey") == "text"
  isImage: ->
    @get("typekey") == "image"
  isVideo: ->
    @get("typekey") == "video"

class IA.Collections.ContestsCollection extends Backbone.Collection
  model: IA.Models.Contest
  url: '/contests'
