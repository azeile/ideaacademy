class IA.Models.Statistic extends IA.Models.AbstractModel

  paramRoot: 'statistic'
  statisticUrl: '/statistics/category_users/'

  # Defaults
  defaults:
    category: null
    contests: null
    statistics: null
    user_info: null
    contests_in_category: null

  another: (id, callback) ->
    @fetch {url : @buildUrl(@statisticUrl + id), success : callback}

  nextPage: (id, page, callback) ->
    @fetch {url: @buildUrl(@statisticUrl + id + "/" + page), success: callback}
