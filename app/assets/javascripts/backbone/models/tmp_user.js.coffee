class IA.Models.TmpUser extends IA.Models.AbstractModel

  paramRoot: 'tmp_user'
  url: '/tmp_users'
  currentUrl: '/current'

  defaults:
    session_id: null

  current: (callback) ->
    @fetch {url : @buildUrl(@url + @currentUrl), success : callback}
  
  isBlocked: ->
    false