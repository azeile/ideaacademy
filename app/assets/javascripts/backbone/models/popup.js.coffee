class IA.Models.Popup extends IA.Models.AbstractModel
  paramRoot: 'popup'
  popupUrl: '/popups/'
  tmpPopupUrl: '/tmp_popups/'

  # Defaults
  defaults:
    user_id: null
    typekey: null
    like_idea_first_name: null
    like_idea_last_name: null
    like_idea_image_url: null
    like_idea_my_image_url: null
    like_idea_in_category: null

  getPopup: (id, callback) ->
    @fetch {url : @buildUrl(@popupUrl + id), success : callback}

  markAsSeen: (user_id, popup_id, callback) ->
    @fetch { url: @buildUrl(@popupUrl + user_id + '/' + popup_id), success: callback }

  getPopupTmp: (id, callback) ->
    @fetch {url : @buildUrl(@tmpPopupUrl + id), success : callback}

  markTmpAsSeen: (user_id, popup_id, callback) ->
    @fetch { url: @buildUrl(@tmpPopupUrl + user_id + '/' + popup_id), success: callback }