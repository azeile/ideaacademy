class IA.Models.AdminMessage extends IA.Models.AbstractModel

  # body
  # user_id
  # contest_id

  paramroot: "admin_messages"

  initialize: ->
    @url = @buildUrl("/admin_messages")

  defaults:
    body: null
