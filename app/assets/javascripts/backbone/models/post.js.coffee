class IA.Models.Post extends Backbone.Model
  paramRoot: 'post'

  defaults:
    title: null
    body: null
  
class IA.Collections.PostsCollection extends Backbone.Collection
  model: IA.Models.Post
  url: '/posts'