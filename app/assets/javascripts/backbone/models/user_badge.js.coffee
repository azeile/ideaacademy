class IA.Models.UserBadge extends IA.Models.AbstractModel

class IA.Collections.UserBadgesCollection extends IA.Models.AbstractCollection
  model: IA.Models.UserBadge
  url: "/api/users/recent_badges"

  fetchRecentBadges: (callback) ->
    @fetch { url: @url, success: callback }
