class IA.Models.Idea extends IA.Models.AbstractModel
  paramRoot: 'idea'

  # Defaults
  defaults:
    text: null
    contest_id: null
    video_image: null
    video_info: null
    image: null
    typekey: null
    show_spam_button: false
    show_idea: false
  
class IA.Collections.IdeasCollection extends IA.Models.AbstractCollection
  model: IA.Models.Idea
  url: '/'
  fetchIdeaPairs: (contest_id, callback, error_callback) ->
    @fetch { url: @buildUrl("/contests/" + contest_id + "/ideas"), success: callback, error: error_callback }
  fetchTmpIdeaPairs: (contest_id, callback, error_callback) ->
    @fetch { url: @buildUrl("/contests/" + contest_id + "/ideas?tmp=true"), success: callback, error: error_callback }

  fetchTopIdeas: (user_id, count) ->
    @fetch { url: @buildUrl("/users/#{user_id}/top_ideas/#{count}") }

  fetchPortfolioIdeas: (user_id) ->
    @fetch { url: @buildUrl("/users/#{user_id}/portfolio_ideas") }

