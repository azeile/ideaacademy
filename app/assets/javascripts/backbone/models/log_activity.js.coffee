class IA.Models.LogActivity extends IA.Models.AbstractModel
  paramRoot: 'log_activity'

  # Defaults
  defaults:
    author_id: null
    category_id: null
    contest_id: null
    idea_id: null
    points: null
    points_gave: null
    room_id: null
    status: null
    typekey: null
    user_id: null
  
class IA.Collections.LogActivityCollection extends IA.Models.AbstractCollection
  model: IA.Models.LogActivity
  url: '/api/logs'
  forUserUrl: '/logs/user/'

  fetchForUser: (id, callback) ->
    @fetch { url: @buildUrl(@forUserUrl + id), success: callback }

  fetchForUserUpdate: (id, timestamp, callback) ->
    @fetch { url: @buildUrl(@forUserUrl + id + '/' + timestamp), success: callback }

  fetchForUserOlderActivities: (id, timestamp, callback) ->
    @fetch { url: @buildUrl(@forUserUrl + id + '/' + timestamp + "?older=true"), success: callback }
