class IA.Models.PaymentNotification extends Backbone.Model
  paramRoot: 'payment_notification'
  
class IA.Collections.PaymentNotificationCollection extends Backbone.Collection
  model: IA.Models.PaymentNotification
  url: '/user_payment_notifications'

  fetchUserPaymentNotifications: (current_user_id) ->
    @fetch  { url: @url, data: { user_id: current_user_id } }
