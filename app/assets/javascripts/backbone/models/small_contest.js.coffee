class IA.Models.SmallContest extends IA.Models.AbstractModel
  paramRoot: 'smallContest'

  # Defaults
  defaults:
    title: null
    time_to_end: null
    category: null

class IA.Collections.SmallContestsCollection extends IA.Models.AbstractCollection
  model: IA.Models.Contest
  url: '/contests'
  root_in_categories: '/root_in_categories'

  fetchRootContests: (callback) ->
    @fetch { url: @buildUrl(@url + @root_in_categories), success: callback }
