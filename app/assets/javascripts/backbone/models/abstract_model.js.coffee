class IA.Models.AbstractModel extends Backbone.Model
  urlPrefix : '/api'

  buildUrl: (url) ->
    @urlPrefix + url

class IA.Models.AbstractCollection extends Backbone.Collection
  urlPrefix : '/api'

  buildUrl: (url) ->
    @urlPrefix + url