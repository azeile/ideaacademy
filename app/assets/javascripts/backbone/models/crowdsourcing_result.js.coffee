class IA.Models.CrowdsourcingResult extends IA.Models.AbstractModel
 
 
class IA.Collections.CrowdsourcingResultsCollection extends Backbone.Collection
  model: IA.Models.CrowdsourcingResult
  url: '/crowdsourcing_results'
 
  fetchResults: (contest_id, callback, errorCallback) ->
    @fetch { url: "api/crowdsourced_contests/" + contest_id + @url, success: callback, error: errorCallback }
  
  fetchMoreResults: (contest_id, callback, errorCallback, offset) ->
    @fetch { url: "api/crowdsourced_contests/" + contest_id + @url + "?offset=#{offset}", success: callback, error: errorCallback }
  