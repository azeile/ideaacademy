class IA.Models.Payment extends IA.Models.AbstractModel
  transaction_req_url: null

  is_vip_for_month: ->
    @get("code").match /VIP.+MONTH/

  req_url: ->
    main_url = "https://www.facebook.com/dialog/pay"
    redirect_uri = "localhost:3000"
    # ?app_id=YOUR_APP_ID&
    #                                 redirect_uri=YOUR_REDIRECT_URI&
    #                                 action=buy_item&
    #                                 order_info={"item_id":"1a"}&
    #                                 dev_purchase_params={"oscif":true} 
    @set({'transaction_req_url': "#{main_url}?app_id=#{AppUtils.facebookAppId}&redirect_uri=#{redirect_uri}&action=buy_item" +
      "&order_info={\"service_id\":\"#{@get('service_id')}\"}&dev_purchase_params={\"oscif\":true}"})
  

class IA.Collections.PaymentsCollection extends IA.Models.AbstractCollection
  model: IA.Models.Payment
  url: "/api/payments"

  fetchPaymentOptions: (callback) ->
    @fetch { url: @url, success: callback }
  
  fetchVipPaymentOptions: ->
    @fetch  { url: @url, data: { vip_only: true } }


