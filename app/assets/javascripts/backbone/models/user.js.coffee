class IA.Models.User extends IA.Models.AbstractModel

  paramRoot: 'user'
  url: '/users/'
  currentUrl: 'current'
  profileStatisticUrl: '/statistics/user_profile/'
  checkoutUrl: 'check_out_money'

  defaults:
    first_name: null
    last_name: null
    email: null
    phone: null
    website: null
    about: null
    work_tag: null
    image: null
    can_check_out: false
    account: null
    blocked: false
    languages: null

  current: (callback) ->
    @fetch {url : @buildUrl(@url + @currentUrl), success : callback}

  getUser: (id, callback) ->
    @fetch { url: @buildUrl(@url + id), success: callback }

  getUserStatistic: (id, callback) ->
    @fetch {url: @buildUrl(@profileStatisticUrl + id), success: callback}

  checkOut: (account, callback) ->
    @fetch {url: @buildUrl(@url + @checkoutUrl + "?account=" + account), success: callback}
  
  isBlocked: ->
    @id && @get("blocked")

class IA.Collections.UsersCollection extends Backbone.Collection
  model: IA.Models.User
  url: '/users'
