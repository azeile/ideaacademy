ActiveAdmin.register Organization do

  config.clear_action_items!

  controller do
    def scoped_collection
      Organization.for_active_admin current_organization
    end
  end

  form html: {enctype: "multipart/form-data"} do |f|
    f.inputs "Organization" do
      f.input :logo, as: :file, hint: f.template.image_tag(f.object.logo)
    end
    f.buttons
  end

  index do
    column :id
    column :name
    column :logo do |org|
      image_tag org.logo.url(:thumbnail), width: 30, height: 30
    end

    #default_actions
  end
end
