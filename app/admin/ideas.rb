#encoding:UTF-8
ActiveAdmin.register Idea do
  actions :all, :except => [:show, :new]
  
  if AppUtils.facebook_env?
    scope :by_reported_spam do |ideas|
      ideas.where( "( SELECT count(1) FROM spam_reports sr WHERE sr.idea_id = ideas.id ) >= ?", ApplicationSetting["SPAM_FILTER_TRASHOLD"] )
    end
    
    scope :by_reported_spam_not_approved do |ideas|
      ideas.where("( ideas.admin_approved IS NULL ) " + 
            "AND ( SELECT count(1) FROM spam_reports sr WHERE sr.idea_id = ideas.id ) >= ?", ApplicationSetting["SPAM_FILTER_TRASHOLD"])
    end
  end
  
  scope :deleted do |ideas|
    ideas.where(:is_deleted => true)
  end

  # filter :user_last_name, :as => :string, :label => "Uzvārds"
  # filter :by_fb_id, :as => :number, :label => "FB identifikators"
  # filter :by_draugiem_id, :as => :number, :label => "Draugiem identifikators"
  filter :category, :as => :select, :collection => proc { current_organization.try(:categories) }
  filter :typekey, :as => :select, :collection => Idea::TYPES
  filter :text
  filter :contest_title, :as => :string 
  filter :user_email, :as => :string 
  filter :user_first_name_or_user_last_name, :as => :string
  #filter :user_last_name, :as => :string

  controller do
    #load_and_authorize_resource :except => :index
    def scoped_collection
      if current_admin_user.superadmin?
        end_of_association_chain
      else
        end_of_association_chain.joins(:organization).where("organizations.id = ?", current_organization.id)
      end
    end
  end

  form do |f|
    f.inputs "Idea" do
      f.input :text
      f.input :is_deleted
    end

    if AppUtils.facebook_env?
      f.input :admin_approved, :as => :select, :include_blank => false
    end

    if f.object.image?
      f.inputs "Image" do
        f.input :image, :as => :file, :hint => f.template.image_tag(f.object.logo_image)
      end
    end

    f.buttons

  end

  index do
    column :id
    column :typekey
    column :content do |idea|
      if idea.typekey == 'image'
        image_tag(idea.logo_image)
      else
        idea.text
      end
    end

    column :contest do |idea|
      idea.contest.title
    end

    column :user do |idea|
      idea.user
    end

    column :duels_won
    column :duels_won
    column :duels_in
    column :created_at

    #buttons
    default_actions
  end
  
end
