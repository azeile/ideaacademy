ActiveAdmin.register ContestRequest do

  controller do
    def scoped_collection
      if current_admin_user.superadmin?
        end_of_association_chain
      else
        ContestRequest.by_organization current_organization
      end
    end
  end

  scope :all
  scope :pending, :default => true
  scope :accepted

  index do
    column :id
    column :parent
    column :created_at

    default_actions
  end
end
