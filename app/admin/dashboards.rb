ActiveAdmin::Dashboards.build do

  # section "Recent users checked out" do
  #   ul do
  #     Payment.recent_pending(5).collect do |payment|
  #       li link_to(payment, admin_payment_path(payment))
  #     end
  #   end
  # end
  # 
  # 
  # section "Recent not accepted contests" do
  #   ul do
  #     ContestRequest.recent_pending(5).collect do |request|
  #       li link_to(request, admin_contest_request_path(request))
  #     end
  #   end
  # end

  section "Recent contests" do
    ul do
      Contest.recent(5).collect do |contest|
        li link_to(contest.title, admin_contest_path(contest))
      end
    end
  end
end
