ActiveAdmin.register SidebarMessage do
  controller do
    load_and_authorize_resource :except => :index
    def scoped_collection
      if current_admin_user.superadmin?
        end_of_association_chain
      else
        end_of_association_chain.joins(:organization).where("organizations.id = ?", current_organization.id)
      end
    end
  end

  filter :title
  filter :created_at

  form do |f|

    f.inputs I18n.t("active_admin.forms.sidebar_messages.title") do
      f.input :title
      f.input :message
      f.input :active, :as => :select
    end

    f.input :organization_id, :as => :hidden, :input_html => {:value => current_organization.try(:id) }

    f.buttons

  end

  index do
    column :id
    column :title
    column :message
    column :active do |sm|
      status_tag(sm.active ? "Yes" : "No")
    end
    column :created_at
    column "actions" do |sidebar_message|
       action_links = []
       action_links << link_to("delete", admin_sidebar_message_path(:id => sidebar_message.id), :class => "member_link delete_link", :rel => "nofollow", 'data-method' => "delete", 'data-confirm' => "Are you sure you want to delete this?")
        action_links.join('').html_safe
    end
  end
end
