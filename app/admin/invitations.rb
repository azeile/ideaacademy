ActiveAdmin.register InvitationForm, as: "Invtitation" do

  config.comments = false
  before_filter do @skip_sidebar = true end
  config.clear_action_items!

  controller do
    def index
      params[:action] = "Invitation"
      render "invitations/index", layout: "active_admin"
    end
  end
end
