#encoding: UTF-8
ActiveAdmin.register StatisticCategoryUser, :as => "Scores" do
  actions :all, :except => [:show, :new]
  
  filter :user_email, :as => :string 
  filter :user_first_name_or_user_last_name, :as => :string
  filter :category, :as => :select, :collection => proc { Category.unscoped.where("organization_id = ?", current_organization.try(:id)) }

  controller do
    def update
      params[:scores].merge({:points_manually_changed => true})
      super
    end

    def scoped_collection
      if current_admin_user.superadmin?
        end_of_association_chain
      else
        end_of_association_chain.joins(:category).where("categories.organization_id = ?", current_organization.id)
      end
    end
  end
  
  index do
    column :id
    column :category_name
    column :user_name
    column :total_points
    column :level
    
    default_actions
  end
  
  form do |f|
    f.inputs do
      f.input :category, :as => :select, :input_html => { :disabled => true }
      f.input :user, :as => :select, :input_html => { :disabled => true }
      f.input :badge, :as => :select, :input_html => { :disabled => true }
      f.input :total_points, :as => :number
      f.input :points_manually_changed, :as => :hidden, :value => true
    end

    f.buttons
  end
end
