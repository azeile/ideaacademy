ActiveAdmin.register OrganizationUser do

  #menu :label => I18n.t("active_admin.menu.organization_users")

  filter :user_email, :as => :string
  filter :user_first_name, :as => :string
  filter :user_last_name, :as => :string
  filter :user_created_at

  controller do
    #load_and_authorize_resource :except => :index

    def new
      @organization_user = OrganizationUser.new({organization: current_organization})
      @organization_user.build_user
    end

    def create
      @organization_user = OrganizationUser.create_with_user params[:organization_user], current_organization

      create! do |format|
        format.html { redirect_to admin_organization_users_path, :flash => flash }
      end
    end


    def scoped_collection
      if current_admin_user.superadmin?
        end_of_association_chain
      else
        end_of_association_chain.where(:organization_id => current_organization.id)
      end
    end
  end

  index do
    column :user_id
    column :user, :label => I18n.t("active_admin.organization_users.columns.user_email")
    column :email, :label => I18n.t("active_admin.organization_users.columns.email")
    column :admin_flag_humanize, :label => I18n.t("active_admin.organization_users.columns.admin_flag")
    column :created_at, :label => I18n.t("active_admin.organization_users.columns.created_at")

    #buttons
    default_actions
  end

  # unless controller.current_admin_user.superadmin?
  #   scope_to :current_organization
  # end

  form partial: "form"

end
