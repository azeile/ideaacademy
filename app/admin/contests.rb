ActiveAdmin.register Contest do

  controller do
    def scoped_collection
      if current_admin_user.superadmin?
        end_of_association_chain
      else
        Contest.readable_by_organization current_organization
      end
    end

    def new
      #@contest = Contest.new({organization: current_admin_user.organizations.first})
      @contest = Contest.new
    end
  end

  index do
    column :id
    column :title
    column :description do |contest|
      contest.description[0..50] + ('...' if contest.description.length > 50).to_s
    end
    column :logo do |contest|
      image_tag contest.smaller_logo.url(:thumbnail)
    end

    column :created_at
    column :ends_at

    #buttons
    default_actions
  end

  form :partial => "form"
end
