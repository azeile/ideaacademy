ActiveAdmin.register Category do

  menu :label => I18n.t("active_admin.menu.categories")

  filter :name

  controller do
    def scoped_collection
      if current_admin_user.superadmin?
        end_of_association_chain.unscoped
      else
        end_of_association_chain.unscoped.where("organization_id = ?", current_organization.id).order(:id)
      end
    end

    def resource
      Category.unscoped { super }
    end
  end

  index do
    column :id
    column :name, :label => I18n.t("active_admin.categories.columns.name") do |cat|
      cat.read_attribute(:name)
    end
    column :created_at, :label => I18n.t("active_admin.categories.columns.created_at")

    #buttons
    default_actions
  end

  form :partial => "form"

end
