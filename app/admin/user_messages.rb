ActiveAdmin.register UserMessage do

  controller do
    def scoped_collection
      UserMessage.for_user current_admin_user
    end
  end

  index do
    column :id
    column :sender do |i|
      i.sender_email
    end
    column :body

    default_actions
  end

  form do |f|
    f.inputs do
      f.input :body
    end
  end

end
