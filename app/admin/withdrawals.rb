if AppUtils.facebook_env?
  ActiveAdmin.register Withdrawal do
    index do
      column :id
      column :status
      column :amount do |withdrawal|
        number_to_currency(withdrawal.amount / 100, :unit => "")
      end
      column :currency
      column :account
      column :name
      column :created_at
      column :updated_at
    end
  end
end
