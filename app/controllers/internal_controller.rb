class InternalController < ApplicationController
  
  #MENU_ACTIONS = %w( home product faq testimonials pricing )
  MENU_ACTIONS = %w( home product faq pricing )

  def home
  end

  def product
  end

  def faq
  end

  def testimonials
  end

  def pricing
    @services = Service.organization
  end

  def thanks_for_payment
    
  end
end
