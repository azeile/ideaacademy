class RegistrationsController < Devise::RegistrationsController

  layout "internal"

  def new
    @resource = build_nested_resources User.new
  end
  
  def create
    @resource = UserProvider.create_with_devise params[:user]
    if @resource.valid?
      flash[:notice] = I18n.t "devise.registrations.signed_up"
      sign_in_and_redirect(:user, @resource)      
    else
      clean_up_passwords @resource
      render :new
    end

  end
  
  private 
    def build_nested_resources resource
      ou       = resource.organization_users.build
      ou.build_organization
      resource
    end
end
