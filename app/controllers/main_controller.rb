class MainController < AuthenticatedController
  before_filter :set_session_domain, :except => :time
  before_filter :check_user_block, :except => :blocked
  before_filter :update_fb_token, :only => :index
  before_filter :authenticate, :except => :index

  def index
    redirect_to home_internal_path if AppUtils.internal_app_env?
  end
  
  def blocked
  end

  def time
    render :text => Time.now
  end

  private

  def update_fb_token
    if params[:signed_request]
      auth = FbGraph::Auth.new(FACEBOOK_CONF[:client], FACEBOOK_CONF[:client_secret])
      auth.from_signed_request(params[:signed_request])
      fb_provider = current_user.facebook_provider
      fb_provider.update_attributes!(:token => auth.user.access_token.to_s) unless fb_provider.token == auth.user.access_token.to_s
    end
  end

  def set_visitor
    visitor = VisitorAttribute.find_by_ip_address( request.remote_ip.to_s )
    unless visitor
      new_visitor = VisitorAttribute.new({ :ip_address => request.remote_ip.to_s })
      new_visitor.save!

      tmp_user_popup = TmpUserPopup.find_by_tmp_user_id_and_typekey( current_tmp_user.id, "welcome_popup" )

      unless tmp_user_popup
        new_tmp_user_popup = TmpUserPopup.new({ :tmp_user_id => current_tmp_user.id, :typekey => "welcome_popup" })
        new_tmp_user_popup.save!
      end
    end
  end
  
  def set_session_domain
    if params[:domain]
      session[:domain] = params[:domain]
    end
  end

end
