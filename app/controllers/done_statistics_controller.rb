class DoneStatisticsController < AuthenticatedController
  respond_to :html, :xml, :json

  def show
  	@contest = Contest.done.find(params[:id])
    @ideas = @contest.ideas.top
    
    respond_with @ideas, :all_attributes => true
  end

end
