class StaticController < ApplicationController

  protect_from_forgery :except => [:paypal_notifications]

  require "net/http"
  require "uri"
  require 'rack/oauth2'

  def login

  end

  def logout
    store_location
    unless AppUtils.internal_app_env?
      user = User.find_by_id(session[:user_id])
      current_user = nil
      sign_out(user)
    end
    session[:user_id] = nil
    redirect_back_or_default AppUtils.internal_app_env? ? login_path : root_path
  end

  def test
    contest = Contest.find params[:contest_id]
    id = contest.current_room current_user
    render :text => id.id
  end

  def update_null_points
    stats = StatisticContestRoomUser.all

    stats.each do |stat|
      stat.vote_points = 0 if stat.vote_points.nil?
      stat.idea_points = 0 if stat.idea_points.nil?
      stat.total_points = 0 if stat.total_points.nil?
      stat.save
    end

    render :text => "done!"
  end

  def update_status
    # users = User.all

    # users.each do |user|
    #   popup = UserPopup.where("user_id = ? AND typekey = ?", user.id, 'welcome_user')

    #   unless popup
    #     new_popup = UserPopup.new :user_id => user.id, :typekey => "welcome_user", :seen => true
    #   end
    # end

    # render :text => "done! #{users.count}"
    
    # contest = Contest.find params[:contest_id]
    # response = contest.update_status
    # render :json => response

    StatisticCategoryOrderedUser.update_statistic
    render :text => "done!"
  end

  def update_statistic
    StatisticCategoryOrderedUser.update_statistic
    render :text => "done!"
  end

  def index
  end

  def welcome
  end

  def contest
  end

  def contest_guest
  end

  def contest_new
  end

  def contest_results
  end

  def contest_statistics
  end

  def profile
  end

  def profile_edit
  end

  def popup
  end

  def thanks_for_payment
    render :layout => 'main'
  end

  def paypal_notifications
    payment_notification = PaymentNotification.find_by_subscription_timestamp(params[:invoice])

    payment_notification.update_attributes(:params => params,
                                           :status => params[:payment_status],
                                           :transaction_id => params[:txn_id])
    render :nothing => true
  end

  # Check if user has payment notifications. If not, then there are no payments
  # if yes, check the status
  def user_payment_notifications
    payment_notifications = PaymentNotification.find_all_by_user_id(params[:user_id])
    response = ""

    if payment_notifications.any?
      if payment_notifications.last.status == "Completed"
        response = { status: "OK" }.to_json
      else
        response = { status: "NOK" }.to_json  
      end
    else
      response = { status: "EMPTY" }.to_json
    end

    render json: response
  end

  def popup_profile_edit_image
    render :layout => 'popup'
  end

  def popup_tos
    render :layout => 'popup'
  end

  def popup_default
    render :layout => 'popup'
  end

  def popup_welcome
    render :layout => 'popup'
  end

  def popup_money
    render :layout => 'popup'
  end

  def popup_first_points
    render :layout => 'popup'
  end

  def popup_ten_points
    render :layout => 'popup'
  end

  def popup_other_themes
    render :layout => 'popup'
  end

  def popup_social
    render :layout => 'popup'
  end

  def popup_earn_more
    render :layout => 'popup'
  end

  def popup_first_achievement
    render :layout => 'popup'
  end

  def popup_achievement
    render :layout => 'popup'
  end

  def popup_like_idea
    render :layout => 'popup'
  end

end
