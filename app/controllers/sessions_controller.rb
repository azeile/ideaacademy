class SessionsController < Devise::SessionsController
  # layout "internal", :only => [:new]
  
  def new
    resource = build_resource
    clean_up_passwords(resource)
    render "internal/product"
  end

  def create
    # logger.debug "aaaabbbb"
    respond_to do |format|  
          format.html { 
            logger.debug "aaaaddddd"
            super }  
          format.json { 
            logger.debug "aaaacccc"
            resource = warden.authenticate!(:scope => resource_name, :recall => :failure)
            sign_in(resource_name, resource)
            render :json => {:success => true}
          }  
    end
  end
  
  def failure
    respond_to do |format|  
      format.json{
          render :json => {:success => false, :errors => ["Login failed."]}
      }
   end
 end

  def destroy
    super
  end

  private

  def after_sign_out_path_for(resource_or_scope)
    root_path
  end
end
