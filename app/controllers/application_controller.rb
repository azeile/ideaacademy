class ApplicationController < ActionController::Base
  include RequestCurl
  layout :layout_by_resource
  helper_method :current_ability, :current_organization, :current_tmp_user
  before_filter :set_locale

  #Security
  #protect_from_forgery

  def set_locale
    if params[:lang]
      session[:locale] = params[:lang]
      redirect_to request.path
    end
    I18n.locale = session[:locale] || :en
  end

  def send_json json, all_attributes = false
    respond_to do |format|
      format.json { render :json => JSON.pretty_generate( json.as_json(:all_attributes => all_attributes) ) }
    end
  end

  def render_error msg
    error = {:status => false, :message => msg}
    respond_with error, :location => nil
  end

  rescue_from CanCan::AccessDenied do |exception|
    #flash[:error] = I18n.t("active_admin.authorizations.error")
    redirect_to admin_dashboard_path, :alert => exception.message
  end
  
  private

    def store_location
      session[:return_to] = request.env['HTTP_REFERER'] # request.fullpath # request.request_uri
    end

    def redirect_back_or_default(default)
      redirect_to(session[:return_to] || default)
      session[:return_to] = nil
    end
    
    def layout_by_resource
      if devise_controller?
        "internal"
      else
        "application"
      end
    end

    def current_organization
      @current_organization ||= if AppUtils.facebook_env?
        Organization.first
      else
        session[:current_organization_id] ? Organization.find(session[:current_organization_id]) : current_admin_user.default_organization
      end
    end

    def current_ability
      @current_ability ||= AdminAbility.new(current_admin_user, current_organization)
    end
end
