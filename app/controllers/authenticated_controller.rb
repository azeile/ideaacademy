class AuthenticatedController < ApplicationController
  helper_method :current_organization
  
  
  def current_organization
    @current_organization ||= ( session[:current_organization_id] ? Organization.find(session[:current_organization_id]) : current_user ? current_user.default_organization : current_tmp_user.default_organization )
  end

  
  private
  
  def authenticate
    if AppUtils.facebook_env?
      authenticate_user!
    else
      authenticate_user! 
    end
  end
  
  def check_user_block
    redirect_to blocked_path if current_user && current_user.blocked?
  end  
end
