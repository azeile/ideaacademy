# encoding: UTF-8

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  #require 'browser'
  
  def facebook
    # You need to implement the method below in your model
        @user = UserProvider.find_for_facebook_oauth(request.env["omniauth.auth"], current_user)
    # @user = UserProvider.create_or_get_by_facebook(request.env["omniauth.auth"], current_user)

    if @user.persisted?
      Rails.logger.info "\n\n\n"
      Rails.logger.info "user persisted"
      Rails.logger.info @user
      Rails.logger.info session
      Rails.logger.info "\n\n\n"

      flash[:notice]      = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      if browser.ie6?
        request.env['warden'].set_user @user
        redirect_to root_path
      else
        sign_in_and_redirect @user, :event => :authentication
      end
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
  
  def passthru
    # render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false
    # # Or alternatively,
    raise ActionController::RoutingError.new('Not Found')
  end

  def failure
    render :layout => false
  end
end
