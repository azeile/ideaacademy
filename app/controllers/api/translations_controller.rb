class Api::TranslationsController < ApplicationController
  def index
    data = Net::HTTP.get_response("growing-ocean-3171.herokuapp.com", "/api/v2/projects/7fcd526727c5a027dba1cda8863c4afe/draft_blurbs?format=hierarchy")
    json = JSON.parse(data.body)
    lang = params[:lang] ? json[params[:lang]] : json
    render :json => lang
  end
end
