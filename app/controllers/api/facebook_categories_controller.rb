class Api::FacebookCategoriesController < ApiController
  
  def index
    respond_with FacebookCategory.all
  end

end
