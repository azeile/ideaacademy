class Api::PopupsController < ApiController

  def user
    if params[:popup_id]
      popup = UserPopup.update_all("seen = TRUE", "id = #{params[:popup_id]} AND user_id = #{params[:user_id]}")
      response = { :status => true, :message => "done" }
      respond_with response.as_json
    else
      popup = UserPopup.limit(1).where("seen IS FALSE").find_by_user_id(params[:user_id])

      popup.category = I18n.t("categories.#{popup.category}") if popup && popup.typekey == 'like_idea' || popup && popup.typekey == 'new_achievement'
  
      response = { :status => true, :message => "popup_loaded", :popup => popup } if popup
      response = { :status => false, :message => "popup_not_found" } unless popup

      respond_with response.as_json
    end
  end

  def tmp_user
    if params[:popup_id]
      popup = TmpUserPopup.update_all("seen = TRUE", "id = #{params[:popup_id]} AND tmp_user_id = #{params[:user_id]}")
      response = { :status => true, :message => "done" }
      respond_with response.as_json
    else
      popup = TmpUserPopup.limit(1).where("seen IS FALSE").find_by_tmp_user_id(params[:user_id])
 
      response = { :status => true, :message => "popup_loaded", :popup => popup } if popup
      response = { :status => false, :message => "popup_not_found" } unless popup

      respond_with response.as_json
    end
  end

end
