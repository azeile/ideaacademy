class Api::AdminMessagesController < ApiController

  def create
    body = params[:body]
    user = User.find params[:user_id]

    UserMessage.send_admin_messages body, user

    render json: {user_id: user.id, body: body}
  end
end