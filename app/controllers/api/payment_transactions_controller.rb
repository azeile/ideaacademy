class Api::PaymentTransactionsController < ApiController
  respond_to :js
  
  def show
    if @payment = PaymentTransaction.find_by_provider_transaction_id(params[:id].to_s)
      if @payment.finished?
        render :json => { :msg => t('.payment_successful') }
      elsif @payment.retry_times_exceeded?
        render :json => { :msg => t("statuses.payment_failed") }, :status => 403
      else
        @payment.update_retry_times
        render :json => { :msg => t('payment_in_progress'), :state => "in_progress" }
      end
    end
  end
end
