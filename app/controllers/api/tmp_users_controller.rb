class Api::TmpUsersController < ApiController
  
  # Methods
  def current
    respond_with current_tmp_user if current_tmp_user
  end

end
