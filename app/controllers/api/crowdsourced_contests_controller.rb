class Api::CrowdsourcedContestsController < ApiController
  skip_before_filter :authenticate, :only => [ :create, :update ]
  before_filter :authorize_request, :only => [ :create, :update ]
  
  def index
    respond_with CrowdsourcedContest.last.to_json_with_next_and_previous current_organization
  end

  #executed in internal app
  def show
    respond_with CrowdsourcedContest.find(params[:id]).to_json_with_next_and_previous current_organization
  end

  #executed on Facebook
  def create
    # Contest.delay.create_from_crowdsourced_contest(params[:contest])
    if Contest.create_from_crowdsourced_contest(params[:contest])
      render :json => { :status => "OK" }
    else
      render :json => { :status => "NOK" }
    end
  end

  #executed on Facebook
  def update
    if Contest.find_by_origin_id(params[:id]).update_from_crowdsourced_contest(params[:contest])
      render :json => { :status => "OK" }
    else
      render :json => { :status => "NOK" }
    end
  end

end