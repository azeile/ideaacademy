class Api::StatisticsController < ApiController
  
  def contest_users
    @user = User.find( params[:user_id] ) if params[:user_id]
    @contest = Contest.find( params[:contest_id] ) if params[:contest_id]
    return respond_with( {:status => false, :message => "contest_is_draft"} ) if @contest.draft?

    if current_user
      @room = @contest.current_room( current_user )
      @room = @contest.current_room( @user ) if @user && @contest.done?
      return respond_with( {:status => false, :message => "room_not_found"} ) unless @room

      @users = StatisticContestRoomUser.get_statistics( current_user, @contest, @room.id ) if @contest.live? || ( @user && @contest.done? )
      @users = StatisticContestRoomUser.get_statistics( current_user, @contest, false ) if @user.nil? && @contest.done?
    else
      @room = @contest.current_room( current_user )
      return respond_with( {:status => false, :message => "room_not_found"} ) unless @room
      @users = StatisticContestRoomUser.get_statistics( current_user, @contest, @room.id ) if @contest.live?
      @users = StatisticContestRoomUser.get_statistics( current_user, @contest, false ) if @contest.done?
    end
    return respond_with( {:status => false, :message => I18n.t("statuses.statistics_not_found")} ) unless @users

    respond_with @users
  end

  def contest_ideas
    @user = User.find(params[:user_id]) if params[:user_id]
    @contest = Contest.find(params[:contest_id]) if params[:contest_id]
    return respond_with( {:status => false, :message => "contest_is_draft"} ) if @contest.draft?

    if current_user
      @room = @contest.current_room( current_user )
      @room = @contest.current_room( @user ) if @user && @contest.done?
      return respond_with( {:status => false, :message => "room_not_found"} ) unless @room
      
      @ideas = Idea.list_my_by_contest_id( current_user.id, @contest.id ).order_by_duels_won if params[:ideas_scope] == 'my_ideas'

      @ideas = Idea.authors_without_ideas( @contest.id, @room.id, params[:offset] ) if params[:ideas_scope] == 'top_ideas'
      @ideas = Idea.ideas_without_authors( @contest.id, @room.id, params[:offset] ) if params[:ideas_scope] == 'all_ideas'
      @ideas = Idea.list_by_contest_id( @contest.id ).order_by_duels_won if @user.nil? && @contest.done? # => All ideas in contest, when done
    end
    
    response = {:status => true, :message => "ideas_loaded", :ideas => @ideas}
    response = {:status => false, :message => I18n.t("statuses.ideas_not_found")} if @ideas.nil?

    send_json response, all_attributes = true
  end

  #for current_user
  def contest_room
    @contest = Contest.find params[:contest_id]
    return if @contest.draft?     
      @user = current_user
      @room = @contest.current_room @user
      logger.debug "room is " + @room.to_s
      if @room.nil?
        send_json( { :status => false, :message => 'room_not_found' } )
      else 
        send_json StatisticContestRoomUser.get_statistics( current_user, @contest, @room.id )
      end
  end

  # returns my_ideas tab info when contest is open, for current_user
  def contest_user_ideas
    if current_user
      @contest = Contest.find params[:contest_id]
      return if @contest.draft?

      @user = current_user
      @room = @contest.current_room @user

      @ideas = Idea.my_best_ideas @user, @contest, @room

      response = { :status => true, :message => "ideas_loaded", :ideas => @ideas }
      response = { :status => false, :message => I18n.t("statuses.ideas_not_found") } if @ideas.nil?

      send_json response
    else
      response = { :status => false, :message => "user_not_authorized" }
      send_json response
    end
  end

  #returns all submited ideas without submiter and score, ordered by creation date
  def contest_all_ideas

  end

  def user
    statistics = StatisticCategoryUser.list_by_user_id( params[:user_id] )
    respond_with( get_user_category_statistics_json statistics )
  end
  
  def tmp_user
    statistics = TmpStatisticCategoryUser.list_by_tmp_user_id( params[:tmp_user_id] )
    respond_with( get_user_category_statistics_json statistics )
  end

  def user_update
    statistics = StatisticCategoryUser.list_by_user_id( params[:user_id] ).order("category_id")

    @statistics = statistics.map do |statistic|
      {
        :id => statistic.category.id,
        :name => statistic.category.name,
        :total_points => statistic.total_points.nil? ? 0 : statistic.total_points,
        :live_points => statistic.live_points.nil? ? 0 : statistic.live_points,
        :level => statistic.badge.nil? ? 1 : statistic.badge.image,
        :percents => statistic.badge.nil? ? 0 : statistic.badge.percents(statistic.total_points),
        :contests_count => statistic.category.contests.size.to_s
      }
    end
    
    respond_with @statistics
  end

  def tmp_user_single_update
    statistics = TmpStatisticCategoryUser.list_by_tmp_user_id( params[:user_id] ).list_by_category_id( params[:category_id] ).order("category_id")
    
    @statistics = []
    statistics.each do |statistic|
      statistic.live_points = nil
      statistic.save

      @statistics << {
        :id => statistic.category.id,
        :name => category.name,
        :total_points => statistic.total_points.nil? ? 0 : statistic.total_points,
        :live_points => statistic.live_points.nil? ? 0 : statistic.live_points,
        :level => statistic.badge.nil? ? 1 : statistic.badge.image,
        :percents => statistic.badge.nil? ? 0 : statistic.badge.percents(statistic.total_points),
        :contests_count => statistic.category.contests.size.to_s
      }
    end

    respond_with @statistics
  end

  def user_single_update
    statistics = StatisticCategoryUser.unscoped.list_by_user_id( params[:user_id] ).list_by_category_id( params[:category_id] ).order("category_id")

    @statistics = []
    statistics.each do |statistic|
      statistic.live_points = nil
      statistic.save

      @statistics << {
        :id => statistic.category.id,
        :name => statistic.category.name,
        :total_points => statistic.total_points.nil? ? 0 : statistic.total_points,
        :live_points => statistic.live_points.nil? ? 0 : statistic.live_points,
        :level => statistic.badge.nil? ? 1 : statistic.badge.image,
        :percents => statistic.badge.nil? ? 0 : statistic.badge.percents(statistic.total_points),
        :contests_count => statistic.category.contests.size.to_s
      }
    end

    respond_with @statistics
  end

  def user_profile
    @statistics = StatisticCategoryUser.get_points( params[:user_id] )
    @ideas = Idea.users_best_ideas( params[:user_id] )

    status = true
    message = "user_profile_loaded"

    @ideas_output = []
    @ideas.each_with_index do |idea, index|
      unless idea.contest.nil?
        @ideas_output << {
          :order => index + 1,
          :id => idea.id,
          :typekey => idea.typekey,
          :text => idea.image? ? idea.image.url(:medium) : idea.text,
          :contest_id => idea.contest.id,
          :contest_title => idea.contest.title,
          :video_info => idea.video_info,
          :video_image => idea.video_image,
          :image => idea.image? ? idea.image.url(:normal) : ""
        }
      end

    end
    response = { :status => status, :message => message, :statistics => @statistics, :ideas => @ideas_output }

    send_json( response )
  end

  def category_users
    page = params[:users_page].nil? ? 0 : params[:users_page]
    category = Category.find(params[:category_id])
    contests_json = Contest.for_category_as_json category, current_user
    if current_user
      ideas_added = current_user.ideas_added category.id
      ideas_in_top_three = current_user.ideas_in_top_three category.id
      friends_in_category = LogActivity.friends_in_category current_user, category.id
      user_info = {:ideas_added => ideas_added, :ideas_in_top_three => ideas_in_top_three, :friends_in_category => friends_in_category}
    else
      user_info = false
    end
    contests_in_category = Contest.where(:category_id => category.id).size
    statistics = StatisticCategoryUser.get_statistics category, current_user, page
    next_category = category.next_category current_organization
    previous_category = category.previous_category current_organization
    send_json( {:previous_category => previous_category, :next_category => next_category, :category => category, :contests => contests_json,  :statistics => statistics, :page => page, :user_info => user_info, :contests_in_category => contests_in_category } )
  end
  
  def contest_end_results
    user = current_user

    user = User.default_guest_user unless user
      @contest = Contest.find(params[:contest_id])
      @winner = @contest.ideas.best_top_ideas(@contest.id, 1)
      @log_cash = LogCash.where("contest_id = ? AND user_id = ?", @contest.id, @winner.first.user.id).first

      response = {
        :is_money => @contest.is_money,
        :total_participants => @contest.room_users.count,
        :total_rooms => @contest.rooms.count,
        :my_ideas => @contest.ideas.where({ :contest_id => @contest.id, :user_id => user.id }).size,
        :my_ideas_topthree => @contest.ideas.where({ :contest_id => @contest.id, :user_id => user.id, :in_topthree => true }).size,
        :winner_id => @winner.first.user.id,
        :winner => "#{@winner.first.user.first_name} #{@winner.first.user.last_name}",
        #:winner_receives => @contest.is_money? ? (@log_cash.earned_money.nil? && @log_cash.earned_money < 1 ? 0 : @log_cash.earned_money) : 0
        :winner_receives => @contest.is_money? ? (@log_cash.nil? ? 0 : (@log_cash.earned_money.nil? && @log_cash.earned_money < 1 ? 0 : @log_cash.earned_money.to_f/100)) : 0
      }

    response = { :status => false, :message => 'user_not_authorized' } if user.nil?
    response = { :status => false, :message => 'contest_is_live' } if @contest.status == 'live'

    send_json response
  end
  
  private 
    def get_user_category_statistics_json statistics
      @statistics = []    
      statistics.each do |statistic|
        @statistics << {
          :id => statistic.category.id,
          :name => statistic.category.name,
          :about_category => I18n.t("about_categories.#{statistic.category.read_attribute(:name)}"),
          :total_points => statistic.total_points.nil? ? 0 : statistic.total_points,
          :live_points => statistic.live_points.nil? ? 0 : statistic.live_points,
          :level => statistic.badge.nil? ? 1 : statistic.badge.image,
          :percents => statistic.badge.nil? ? 0 : statistic.badge.percents(statistic.total_points),
          :contests_count => statistic.category.live_contests_count(current_user) #contests.size.to_s
        }
      end
      
      @statistics
    end
  
end
