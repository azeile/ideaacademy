class Api::PaymentsController < ApiController
  skip_before_filter :authenticate, :only => :create
  before_filter :process_signed_request, :only => :create
  respond_to :js
  
  def index
    respond_with ( params[:vip_only] ? Payment.vip_services : Payment.services )
  end

  def create
    order = @req.data[:credits]

    res = case params[:method]
      when 'payments_get_items'
        PaymentTransaction.start_transaction @req, @service
        {
          :content => [@service.as_json_for_fb]
        }
      when 'payments_status_update'
        @transaction = PaymentTransaction.includes([:user, :service]).find_by_provider_transaction_id(order[:order_id].to_s)
        status = if order[:status] == "placed" && @transaction.finish_transaction 
          :settled
        elsif order[:status] == "placed"
          :canceled
        end

        {
          :content => {
            :order_id => order[:order_id],
            :status => status
          }
        }   
      end

    res[:method] = params[:method]
    logger.info res.to_json
    render :json => res
  end

  
  private

  def process_signed_request(redirect_uri = nil)
    @req      = FbGraph::Auth.new(FACEBOOK_CONF[:client], FACEBOOK_CONF[:client_secret]).from_signed_request(params[:signed_request])
    render :text => "Acces denied!", :status => :unauthorized unless @req.client.secret == FACEBOOK_CONF[:client_secret]
    @service  = Service.find JSON.parse(@req.data["credits"]["order_info"])["service_id"] if params[:method] == 'payments_get_items'
  end
end
