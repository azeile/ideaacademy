class Api::CrowdsourcingResultsController < ApiController
  skip_before_filter :authenticate, :only => :get_from_facebook
  before_filter :authorize_request, :only => :get_from_facebook

  #used in internal version to view crowdsourced results
  def index
    respond_with CrowdsourcingResult.for_organization_by_contest params[:crowdsourced_contest_id], current_organization, params[:offset]
  end

  #executed in facebook version to retrieve results for internal version
  def get_from_facebook
    respond_with Contest.find_by_origin_id(params[:contest_id]).results
  end

end