class Api::UsersController < ApiController
  # before_filter :require_user, :only => [:check_out_money]
  before_filter :my_profile, :only => [:update_image, :update]

  # Methods
  def current
    respond_with current_user || current_tmp_user
  end
  
  def update_image
    @user = User.find params[:id].to_i    
    params[:user][:image].content_type = MIME::Types.type_for(params[:user][:image].original_filename).to_s
    @user.image = params[:user][:image]
    if @user.save!
      respond_with @user, :location => nil
    else
      p @user.errors
    end
  end

  def update
    @user = User.find params[:id]
    @user.update_profile params[:user], params[:languages]
    respond_with @user, :location => nil   
  end

  def update_category_notifications
    category_ids = params[:category_list_id]

    previous_selection = UserCategoryNotification.where("user_id = ?", current_user.id)
    previous_selection.delete_all

    category_ids.each_with_index do |id|
      user_cat_notices = UserCategoryNotification.new :user_id => current_user.id, :category_id => id
      user_cat_notices.save
    end

    respond_with category_ids, :location => nil
  end

  def show
    user = User.find( params[:id] )

    providers_list = ["draugiem", "facebook"]


    if user
      providers = []
      if Rails.env.draugiem? || Rails.env.facebook?
        providers_list = []
      else      
        user.providers.each do |provider|
          case provider.name
          when "draugiem"
            profile_url = "http://www.draugiem.lv/user/#{provider.social_id}"
          when "facebook"
            profile_url = "http://www.facebook.com/#{provider.social_id}"
          end

          providers << {
            :name => provider.name,
            :social_id => provider.social_id,
            :profile_url => profile_url
          }

          providers_list.delete_if{ |x| x == provider.name }
        end
      end
    end

    available_providers = []

    providers_list.each_with_index do |provider, index|
      available_providers << {
        :name => provider
      }
    end

    url_regex = Regexp.new("((https?|ftp|file):((//)|(\\\\))+[\w\d:\#@%/;$()~_?\+-=\\\\.&]*)")
    response = {
      :id => user.id,
      :first_name => user.first_name,
      :last_name => user.last_name,
      :about => user.about,
      :phone => user.phone,
      :website => user.website =~ url_regex ? user.website : "http://#{user.website}",
      :email => user.email,
      :image_url => user.profile_image,
      :work_tag => user.work_tag,
      :earned_money => user.earned_money.to_f/100,
      :top5_cups => user.top5_cups,
      :providers => providers,
      :available_providers => available_providers,
      :vip => user.vip?,
      :prefix => user.title_prefix,
      :languages => user.language_array
    }

    return respond_with response.to_json if user
    return respond_with nil unless user
  end

  def check_out_money
    current_user.check_out_money params[:account]
    respond_with current_user
  end
  
  def my_profile
    unless current_user.id.to_i == params[:id].to_i
      render :text => "you can update only your profile"
      return false
    end
  end

  def recent_badges
    respond_with UserBadge.recent_badges(current_user || current_tmp_user, 3)
  end

  def top_ideas
    user = User.find(params[:id])
    respond_with user.ideas.top.limit(params[:count] || 100).as_json({:profile_ideas => true})
  end

  def portfolio_ideas
    user = User.find(params[:id])
    respond_with user.ideas.portfolio.top.as_json({:profile_ideas => true})
  end

end
