class Api::SidebarMessagesController < ApiController

  def for_current_user
    respond_with ( if (current_user ? current_user.seen_sidebar_messages.any? : current_tmp_user.seen_sidebar_messages.any?)
      SidebarMessage.for_user (current_user ? current_user : current_tmp_user), current_organization
    else
      SidebarMessage.for_user_organization current_organization
    end )
  end

  def mark_as_seen
    message = SidebarMessagesView.new :sidebar_message_id => params[:id], :viewer_id => params[:user_id]
    message.save

    render :json => message
  end

end
