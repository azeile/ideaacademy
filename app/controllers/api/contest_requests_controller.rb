class Api::ContestRequestsController < ApiController
  
  def create
  	contest_request = ContestRequest.create_with_parent(params[:contest_request], current_user, current_organization)
  	respond_with contest_request, :location => nil
  end

end
