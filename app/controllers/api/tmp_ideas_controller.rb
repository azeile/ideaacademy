class Api::TmpIdeasController < ApiController
 before_filter :validate_ideas_and_require_room
  
  def show_duel
    @response = @room.ideas_next_duel_tmp current_tmp_user
    send_json @response
  end

  def vote
    @response = @room.vote_idea_tmp current_tmp_user, params[:first_idea_id], params[:second_idea_id], params[:selected_idea_id]  
    send_json @response
  end
  
  private 
    def validate_ideas_and_require_room
      contest = Contest.find params[:contest_id]
      if params[:first_idea_id] && params[:second_idea_id]
        first_idea = Idea.find params[:first_idea_id]
        second_idea = Idea.find params[:second_idea_id]
      else
        params[:first_idea_id] = nil
        params[:second_idea_id] = nil
      end
      unless selected_idea_in_ideas_or_empty params[:first_idea_id], params[:second_idea_id], params[:selected_idea_id]
        render_error 'selected_idea_error'
        return false
      end
      @room = contest.current_room_tmp current_tmp_user, params[:first_idea_id], params[:second_idea_id]
      unless @room
        render_error 'dont_have_room'
        return false
      end
    end
    
    def selected_idea_in_ideas_or_empty first_idea_id, second_idea_id, selected_idea_id
      return true unless selected_idea_id
      [first_idea_id, second_idea_id].include?(selected_idea_id)
    end
end
