class Api::ContactFormController < ApplicationController
  def create
    @email = params[:email]
    @message = params[:message]
    ContactMailer.contact_form_message(@email, @message).deliver
    render :json => {:success => true}
  end
end
