class Api::CategoriesController < ApiController
  skip_before_filter :authenticate, :only => :index
  respond_to :json

  def index
    categories =  if current_user
  	  Category.by_organization(current_organization.id)
  	else
      Category.all
  	end
  	
  	render :json => categories.to_json({:plain_names => params[:plain_names]})
  end

  def show
  end

end
