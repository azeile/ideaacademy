class Api::IdeasController < ApiController
  # before_filter :require_user, :validate_ideas_and_require_room
  before_filter :validate_ideas_and_require_room
  before_filter :validate_if_allowed_to_add, :only => [:create]
  before_filter :validate_if_allowed_to_report_spam, :only => :report_spam
  respond_to :js

  def index
    @pairs = @room.set_of_idea_pairs current_user, params[:tmp]
    if @pairs.blank?
      render :json => { :message => I18n.translate("statuses.ideas_not_found") }, :status => 404
    else
      send_json @pairs
    end
  end

  def show_duel
    @response = @room.ideas_next_duel current_user
    send_json @response
  end
  
  def vote
    @response = @room.vote_idea (current_user || User.default_guest_user), params[:first_idea_id], params[:second_idea_id], params[:selected_idea_id]  
    send_json @response
  end
  
  def report_spam
    if SpamReport.create( :idea_id => params[:id], :user_id => current_user.id )
      render :json => { :msg => t('statuses.spam_reported'), :status => true }
    else
      render :json => { :msg => t('statuses.spam_report_failed'), :status => false }
    end
  end
  
  def create
    @idea = @room.add_idea(current_user, params)
    respond_with @idea, :location => nil
  end

  def update
    @idea = Idea.find params[:id]
    return render :status => 403 unless @idea && @idea.user == current_user
    if @idea.update_attributes({:is_portfolio_idea => params[:idea][:is_portfolio_idea]})
      respond_with @idea.as_json({:profile_ideas => true})
    else
      render :json => @idea.errors, :status => 403
    end
  end
  
  private
  
    def validate_if_allowed_to_report_spam
      contest = Contest.find params[:contest_id]
      render_error t('statuses.cant_report_spam') unless current_user.level_sufficient_to_report_spam? contest
    end
    
    def validate_if_allowed_to_add
      render_error t('statuses.cant_add_ideas') unless current_user && current_user.allowed_to_add_ideas?
    end
    
    def validate_ideas_and_require_room
      contest = Contest.find params[:contest_id]

      if params[:first_idea_id] && params[:second_idea_id]
        first_idea = Idea.find params[:first_idea_id]
        second_idea = Idea.find params[:second_idea_id]
      end
      
      if current_users_idea?(first_idea) || current_users_idea?(second_idea)
        render_error 'your_idea_error'
        return false
      end

      unless selected_idea_in_ideas_or_empty params[:first_idea_id], params[:second_idea_id], params[:selected_idea_id]
        render_error 'selected_idea_error'
        return false
      end

      if params[:first_idea_id] && params[:second_idea_id]      
        @room = contest.current_room current_user, params[:first_idea_id], params[:second_idea_id]
      else
        @room = contest.current_room current_user
      end
      
      unless @room
        render_error 'dont_have_room'
        return false
      end
    end
    
    def current_users_idea? idea
      return false if idea.nil?
      current_user == idea.user 
    end
    
    def selected_idea_in_ideas_or_empty first_idea_id, second_idea_id, selected_idea_id
      return true unless selected_idea_id
      [first_idea_id, second_idea_id].include?(selected_idea_id)
    end
    
end
