class Api::LogsController < ApiController

  def user
    if params[:timestamp] && params[:older]
      logs = LogActivity.list_older_by_date(params[:user_id], Time.at(params[:timestamp].to_i)).limit(20).order("created_at DESC")
    elsif params[:timestamp]
      logs = LogActivity.list_by_user_id_for_update(params[:user_id], Time.at(params[:timestamp].to_i + 1)).order("id DESC")
    else
      logs = LogActivity.list_by_user_id(params[:user_id]).limit(5)
    end

    respond_with logs.as_json
  end

end
