class Api::ContestsController < ApiController

  before_filter :validate_if_allowed_to_read, only: [:show]

  def root
    if params[:category_id]
      category = Category.find(params[:category_id].to_i)
      contest = Contest.root_in_category category, ( current_user ? current_user : current_tmp_user ) if category
    elsif session[:contest_id]
      contest = contest.find(session[:contest_id])
      session[:contest_id] = nil
    else
      contest = Contest.root current_organization, (current_user ? current_user : current_tmp_user)
    end
    if contest
      respond_with contest.to_json_id_only
    else
      respond_with 0
    end
  end

  def root_in_categories
    contests = Contest.full_list_with_active_contests ( current_user ? current_user : current_tmp_user ), current_organization
    respond_with contests
  end
  
  def upload_image
    file = params[:logo_file_name]
    file.content_type = MIME::Types.type_for(file.original_filename).to_s
    contest_image = ContestImage.create!(:logo => file)
    respond_with contest_image, :location => nil
  end

  def show #should_be_validated
    @contest = Contest.find params[:id].to_i
    render json: @contest.to_json_with_next_and_previous(current_user, current_organization)
  end

  private
    def validate_if_allowed_to_read
      c = Contest.find params[:id]
      if current_user.nil?
        os = User.default_guest_user.organizations
      else
        os = current_user.organizations
      end
      render(json: :error, status: 403) unless os.include? c.organization
    end
end
