class ApiController < AuthenticatedController
  # Responds
  respond_to :json
  
  def current_user
    super || current_tmp_user
  end
  
  private

  def current_tmp_user
    @current_tmp_user ||= User.find_by_user_type('guest')
  end

  def authorize_request
    FACEBOOK_CONF[:client] == params[:api_id] && FACEBOOK_CONF[:client_secret] == params[:api_key]
  end
end
