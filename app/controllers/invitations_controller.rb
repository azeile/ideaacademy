class InvitationsController < ApplicationController

  def create
    inv_form = params[:invitation_form]
    user_id = inv_form[:user_id]
    org_id = inv_form[:organization_id]

    split_emails = inv_form[:email_list].split(/[\s\,]/).reject {|a| a.empty?}
    emails = split_emails.map! {|item| item.strip}

    emails.each do |m|
      Invitation.create({email: m, inviter_id: user_id, organization_id: org_id})
    end

    redirect_to "/admin/organization_users" #for example
  end
end
