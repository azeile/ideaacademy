class PaymentsGatewayController < ApplicationController

  def pay_with_paypal
    service = Service.find(params[:id])
    subscription_timestamp = "#{current_user.id}#{Time.now.to_i}" # AKA invoice number
    return_url = (Rails.env.development? ? "http://localhost:3000/#/thanks" : "https://public.academyideas.com/#/thanks")

    PaymentNotification.create!(:user_id => current_user.id,
                                :service_id => service.id, 
                                :subscription_timestamp => subscription_timestamp)

    redirect_to service.paypal_url(subscription_timestamp, return_url)
  end

end
