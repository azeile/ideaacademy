class FacebookActivity
  TRANSLATABLE_FEED_FIELDS = [ :message ]

  class << self
    def user_joined_academy user
      options = {
        :message      => { :default => "has joined Idea Academy!" },
        :name         => { :default => "Joining message" }
      }

      new.feed(:user_joined_academy, user, options, I18n.locale)
    end

    def user_reached_new_level user, level, category_name
      options = {
        :message      => {
                            :category_name  => category_name, 
                            :default        => "has reached new level at Idea Academy in %{category_name} category!",
                            :key            => "facebook_activities.user_reached_new_level.message_level#{level}"
                          },
        :name         => {  :default => "New level message" }
      }

      new.feed(:user_reached_new_level, user, options, I18n.locale)
    end

    def user_earned_money user, contest_name
      options = {
        :message      => {
                            :default => "has earned money at Idea Academy in \"%{contest_name}\" contest!",
                            :contest_name => contest_name
                          },
        :name         => { :default => "Earned money message" }
      }

      new.feed(:user_earned_money, user, options, I18n.locale)
    end
  end

  def feed method_name, user, options, locale
    old_locale  = I18n.locale
    I18n.locale = locale

    me      = FbGraph::User.me user.facebook_provider.token rescue nil
    message = { 
                :picture     => "#{FACEBOOK_CONF[:omni_auth_full_host_path]}/#{UrlHelper.image_path('facebook/logo_text')}", 
                :link        => FACEBOOK_CONF[:omni_auth_full_host_path],
                :description => String.new( I18n.t("facebook_activities.description", :default => "Share and acknowledge the best ideas") ),
                :name        => String.new( I18n.t("facebook_activities.app_name", :default => "Show your talents and earn money by creating ideas to real business and social challenges") )
              }
    message.merge!(TRANSLATABLE_FEED_FIELDS.inject({}) {|memo, val|
      tstring   = options[val][:key] ? options[val].delete(:key) : "facebook_activities.#{method_name}.#{val}"
      memo[val] = String.new I18n.t(tstring, options[val])
      memo
    })

    me.feed!(message) if me

    I18n.locale = old_locale
  end

  handle_asynchronously :feed
end