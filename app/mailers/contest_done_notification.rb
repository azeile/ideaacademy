class ContestDoneNotification < ActionMailer::Base
  default :from => "admin@#{AppUtils::HOST}"
  
  def contest_done contest, email
    @contest = contest
    
    mail :to => email, :subject => I18n.t("emails.contest_done")
  end
end
