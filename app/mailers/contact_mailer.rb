#coding:utf-8
class ContactMailer < ActionMailer::Base
  default :from => "notify@idejuakademija.lv"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contact_mailer.contact_form_message.subject
  #
  def contact_form_message email, message
    @email = email
    @message = message
    mail to: "andrim@apollo.lv", subject: "Vēstule no Ideju Akadēmijas lietotāja"
  end
end
