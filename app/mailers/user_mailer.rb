class UserMailer < Devise::Mailer

  def headers_for(action)
    headers = super
  end

  def invitation_instructions(record)
    headers = super
    headers[:subject] = invitation_subject record
  end

  def reset_password_instructions(record)
    headers = super
    headers[:subject] = invitation_subject record
  end

  def invitation_subject record
    "Join #{record.organizations.last.name} brainstorming platform!"
  end
end
