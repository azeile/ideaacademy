class NotificationMailer < ActionMailer::Base

  default :from => "admin@#{AppUtils::HOST}"

  def contest_new_notification contest
    @contest = contest

    #@users = User.where("receive_notifications = TRUE")
    @users = @contest.category.subscribers
    @emails = @users.map(&:email)

    mail :to => "Austris Landmanis <aus3ys@gmail.com>", 
      :bcc => @emails,
      :subject => "New challenge in #{@contest.organization.name} Academy of Ideas"
  end
end
