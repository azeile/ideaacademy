module SendContestUpdateEmail
  @queue = :send_contest_update_email

  def self.perform(contest_id)
  	contest = Contest.find(contest_id)
    NotificationMailer.contest_new_notification(contest).deliver
  end
end