module UpdateStatistics
  @queue = :update_statistics
  
  def self.perform
    StatisticCategoryOrderedUser.update_statistic
    Contest.update_done_statistcs
  end
end