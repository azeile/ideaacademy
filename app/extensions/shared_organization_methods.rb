module SharedOrganizationMethods
  FIELDS_TO_SYNC = [:email, :encrypted_password]
  
  def has_default_organization?
    organization_users.default_org.present?
  end
  
  def default_organization
    organizations.default_org.first
  end
    
  def default_organization_link
    organization_users.default_org.first
  end
  
  def set_default_organization organization
    default_organization_link.update_attribute(:default_flag, false) if has_default_organization?
    
    self.organization_users.build( { :organization => organization, :user => self, :default_flag => true } )
  end
  
  def self.included(base)
    private
    def sync_accounts
      
      if AppUtils.internal_app_env?
        assoc = self.class.to_s =~ /^User$/ ? "admin_user" : "user"
        
        transaction do
          FIELDS_TO_SYNC.select { |field| send "#{field}_changed?" }.each do |field|
            (send assoc).update_attribute( field, send(field) ) if send(assoc) && send(assoc).send(field) != send(field)
          end
        end
      end
    end
  end
end
