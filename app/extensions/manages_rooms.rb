module ManagesRooms
  def need_new_room?
    rooms.last.participants >= users_per_room ? true : false
  end
end
