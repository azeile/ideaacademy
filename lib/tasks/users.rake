namespace :users do
  desc "Create superuser"
  task "create_superuser" => :environment do
    admin = AdminUser.create({:email => "admin@idejuakademija.lv", :password => "1dejuakad5MijA"})
    admin.update_attribute(:superadmin_flag, true)
  end

  task "create_default_language" => :environment do
    User.without_languages.each do |u|
      u.languages.build(:language => "English")
      u.save!
    end
  end

  desc "Creates VIP subscriptions for internal app users if needed"
  task :subscribe_to_vip => :environment do
    if AppUtils.internal_app_env?
      User.not_subscribed_to_vip.each { |u| u.subscribe_to_service "VIP_ACCOUNT_LIFELONG" }
    end
  end

  desc "Reset top 5 statistics data"
  task :reset_top5_cup_statistics => :environment do
    User.update_all :top5_cups => 0
    Contest.done.each do |contest|
      statistic_users = contest.statistic_contest_room_users.order('total_points DESC').limit(5)
      statistic_users.each do |statistic|
        statistic.user.increment!(:top5_cups)
      end
    end
  end
end
