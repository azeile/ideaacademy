namespace :contests do
  task :add_language => :environment do
    Contest.live.where(:language => nil)
    .joins(:category).where("categories.active_flag = ?", true).readonly(false).each do |c|
      c.language = "English"
      c.save!
    end
  end
  
  desc "runs in all environments"
  task :create_jobs_to_generate_statistics => :environment do
    Contest.done.without_statistics.each { |c| c.delay.generate_end_statistic }
  end
  
  desc "runs in Internal environments, fetches crowdsoruced contest results from FB"
  task :get_crowdsourcing_results => :environment do
    CrowdsourcedContest.done.without_results.each { |c| c.delay.get_results_from_facebook }
  end
end
