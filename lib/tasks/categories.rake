require 'open-uri'

namespace :db do
  task "update_categories_activity" => :environment do
    Category.where("active_flag IS NULL").update_all(:active_flag => true)
  end
end

namespace :categories do
  task :get_categories_from_facebook => :environment do
    content = JSON.parse( open("#{CROWDSOURCING_CONF[:facebook_app_url]}/api/categories?plain_names=true").read )
    content.each do |cat|
      FacebookCategory.find_or_create_by_fb_id(cat["id"], { :name => cat["name"] } )
    end
  end

  task :populate_with_default_categories => :environment do
    if AppUtils.facebook_env?
      Category::DEFAULT_CATEGORIES_FOR_FACEBOOK.each do |cat|
        Category.find_or_create_by_name( cat, { :active_flag => true, :organization => Organization.first })
      end
    end
  end
end