namespace :i18n do
  desc "Push new translation keys in Backbone templates/views to Copycopter"
  task "parse_js" => :environment do
    translations = {}
    Dir.glob("#{Rails.root}/app/assets/javascripts/backbone/{templates,views}/**/*").each do |file|
      next unless File.file?(file)
      File.open(file) do |f|
        f.each_line do |line|
          line.scan(/I18n.t\W*(['"])(\S*)\1(,\W*defaultValue:\W*(['"])(.*?)\4)?/).each do |match|
            unless CopycopterClient.cache.keys.include?("en.#{match[1]}")
              translations.merge!("en.#{match[1]}" => match[4].to_s)
            end
          end
        end
      end
    end
    p translations
    #10.times do |i|
    #  `curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"en.popup.rules_#{i}":"test"}' --connect-timeout 30  http://copycopter.ejekabsons.com/api/v2/projects/7fcd526727c5a027dba1cda8863c4afe/draft_blurbs`
    #end
    translations.each do |key, value|
      CopycopterClient.client.upload(key => value)
    end
  end
end
