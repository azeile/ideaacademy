class InternalAppFailure < Devise::FailureApp
  def redirect_url
    return super if ![:user].include?(scope) and AppUtils.internal_app_env?
    AppUtils.internal_app_env? ? home_internal_path : root_path
  end

  # You need to override respond to eliminate recall
  def respond
    if http_auth?
      http_auth
    else
      redirect
    end
  end
end
