class CustomMailInterceptor
  def self.delivering_email(message)

    # Deliver to original recipients if everyone in "to" field is whitelisted
    return if (message.to.to_a - AppUtils.allowed_emails).empty?

    message.subject = "To: #{message.to.to_a.join(', ')} | #{message.subject}"
    message.to = AppUtils.allowed_emails
    message.from = "\"IdeaAcademy [#{Rails.env.upcase[0..2]}]\" <no-reply@ideacademy.com>"
  end
end