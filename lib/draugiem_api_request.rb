#coding:utf-8
class DraugiemApiRequest
  extend RequestCurl

  class << self

    def add_top3_activity_for ideas
      ideas.each do |idea|
        add_activity idea.user, :prefix => "bija starp 3 labākajiem Ideju Akadēmijas uzdevumā", :text => %Q{"#{idea.contest.title}"}
      end
    end

    def add_earned_money_activity log_cash
      if log_cash.earned_money > 0
        add_activity log_cash.user, :prefix => "nopelnīja #{"%.2f" % (log_cash.earned_money / 100.0)} LVL Ideju Akadēmijas uzdevumā", :text => %Q{"#{log_cash.contest.title}"}
      end
    end

    def add_earned_badge_activity user, level, category
      add_activity(user, :prefix => "pabeidza Ideju Akadēmijas līmeni", :text => %Q{"#{I18n.t("badges.level_#{level}")}" novirzienā "#{I18n.t("categories.#{category}")}"})
    end

    def add_activity(user, params)
      request_curl(DRAUGIEM_API_PATH, { :app => DRAUGIEM_APP_KEY, :apikey => user.draugiem_provider.try(:token), :action => "add_activity" }.merge(params))
    end

  end
end
