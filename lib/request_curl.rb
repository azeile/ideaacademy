require "net/http"
require "uri"

module RequestCurl
  def request_curl url, params = { }
    uri = URI.parse("#{url}?#{params.to_query}")
    http = Net::HTTP.new(uri.host, uri.port)
    res = http.get(uri.request_uri)
    res.body
  end
end
