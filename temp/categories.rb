ActiveAdmin.register Category do

  form do |f|

    f.inputs "Category" do
      f.input :name
    end

    f.input :organization_id, :as => :hidden, :input_html => {:id => current_organization.id }

    f.buttons

  end

end
