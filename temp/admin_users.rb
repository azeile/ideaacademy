ActiveAdmin.register AdminUser do
  menu false
  index do
    column :email
    column :created_at
    column :sign_in_count
    column :last_sign_in_at

    default_actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.buttons
  end
end
