ActiveAdmin.register Payment do
  
  scope :all
  scope :pending, :default => true
  scope :complete

  index do
    column :id    
    column :account
    column :name
    column :amount
    column :currency
    column :status
    column :created_at

    default_actions
  end
end
