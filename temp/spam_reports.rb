#encoding: UTF-8
ActiveAdmin.register SpamReport do
  filter :user_email, :as => :string
  filter :created_at
  
  index do
    column :idea_type, :label => "Idejas tips"
    column "Ideja" do |sr|
      if sr.idea_type == 'image'
        link_to image_tag(idea.logo_image), edit_admin_idea_path(:id => sr.idea_id) 
      else
        link_to sr.text, edit_admin_idea_path(:id => sr.idea_id)
      end
    end
    column "Idejas ID" do |sr|
      link_to sr.idea_id, edit_admin_idea_path(:id => sr.idea_id)
    end
    column :user_email, :label => "Ziņotāja lietotājvārds"
    column :user_full_name, :label => "Ziņotāja vārds"
    column :created_at, :label => "Izveidota"
    
    #buttons
    default_actions
  end
end