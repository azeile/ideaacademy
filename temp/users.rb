ActiveAdmin.register User do
  controller do
    load_and_authorize_resource :except => :index

    def create
      password = Devise.friendly_token.first(10)
      params[:user].merge({:password => password, :password_confirmation => password})
      UserProvider.create_by_devise
    end


    def scoped_collection
      current_organization.users
    end
  end

	# unless controller.current_admin_user.superadmin?
	# 	scope_to :current_organization
	# end

  form do |f|

    f.inputs "User" do
      f.input :first_name
      f.input :last_name
      f.input :email_name
    end

    f.buttons

  end
end
