ActiveAdmin.register ApplicationSetting do

  form do |f|

    f.inputs "Attributes" do
      f.input :name
      f.input :code
      f.input :value
      f.input :setting_type, :as => :select
    end
    
    f.buttons

  end

end
