require "spec_helper"

describe NotificationMailer do

  describe "contest_new_notification" do
    let(:organization) { mock_model(Organization, {title: "test organization"}) }
    let(:contest) { mock_model(Contest, {title: "test", organization: organization}) }
    let(:user) { mock_model(User, {email: "test@test.com"}) }
    let(:mail) { NotificationMailer.contest_new_notification(contest) }

    before do
      contest.stub_chain(:category, :subscribers).and_return([user])
      contest.stub_chain(:category, :name).and_return("Category_name")
    end

    it "renders the subject" do
      mail.subject.should == "New challenge in #{organization.title} Academy of Ideas"
    end

    it "contains organization name in its body" do
      mail.body.encoded.should match(organization.title)
    end

    it "contains challenge title" do
      mail.body.encoded.should match(contest.title)
    end

    it "contains contest categories title" do
      mail.body.encoded.should match(contest.category.name)
    end

  end
end
