require "spec_helper"

describe UserMailer do

  describe "reset password instructions" do
    let(:organization) { mock_model(Organization, {name: "Lido Grupa"}) }
    let(:organizations) { [organization] }
    let(:user) { mock_model(User, { sign_in_count: 0, to_s: "Email Test", email: "test@test.com", organizations: organizations}) }
    let(:mail) { UserMailer.reset_password_instructions(user) }

    it "renders a subject" do
      mail.subject.should == "Join #{organization.name} brainstorming platform!"
    end

    it "sends an email to correct address" do
      mail.to.should == [user.email]
    end

    it "includes users name" do
      mail.body.encoded.should match(user.to_s)
    end

    it "includes organization name" do
      mail.body.encoded.should match(organization.name)
    end

  end
end
