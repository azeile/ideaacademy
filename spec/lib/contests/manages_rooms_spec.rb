require "fast_spec_helper"
require "manages_rooms"

class DummyContest
  include ManagesRooms
end

describe "Managing rooms" do
  context "knowing if a new room is needed" do
    before do
      @contest = DummyContest.new
      @contest.stub_chain(:rooms, :last, :participants).and_return 14
      @contest.stub(:users_per_room).and_return 8
    end

    it "needs a new room if there are too many participants in the last room" do
      @contest.need_new_room?.should be_true
    end

    it "doesnt need one if there are less participiants than max number of users in room" do
      @contest.stub_chain(:rooms, :last, :participants).and_return 4
      @contest.need_new_room?.should be_false
    end
  end
end
