require "spec_helper"

describe Contest do
  let(:last_room) {Room.new({participants: 14})}
  let(:rooms) { [last_room] }

  before { @contest = Contest.new({rooms: rooms, users_per_room: 8})}

  context "knowing if it needs a new room" do
    it "needs a new room if there are too many participants in the last room" do
      @contest.need_new_room?.should be_true
    end

    it "doesnt need one if there are less participiants than max number of users in room" do
      @contest.rooms.last.participants = 4
      @contest.need_new_room?.should be_false
    end
  end
end
