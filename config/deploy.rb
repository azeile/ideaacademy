require "#{File.dirname(__FILE__)}/initializers/string_ext" #for file interpolations

#for asset checkusm check
def remote_file_exists?(full_path)
  'true' ==  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip
end

set :user, 'root'
set :domain, 'test.idejua.com'
set :application, 'idea-academy'

set :stages, ["internal-test", "fb-test", "production", "fb-prod"]
set :default_stage, "internal-test"
require 'capistrano/ext/multistage'

set :whenever_command, "whenever"
set :whenever_environment, defer { stage }
set :whenever_identifier, defer { "#{application}_#{stage}" }
set :whenever_roles, [:worker]
require "whenever/capistrano"

# the rest should be good
set :repository,  "git@bitbucket.org:acheksters/idea-academy-last.git"
#set :deploy_to, "/home/#{user}/#{application}-#{stage || default_stage}"
set :deploy_via, :remote_cache
set :scm, 'git'
set :branch, `git branch | grep \\*`.gsub(/[^\d\w\-\_]/,'')
#set :git_shallow_clone, 1
set :scm_verbose, true
set :use_sudo, false


# require "rvm/capistrano"
# set :rvm_type, :system
# set :rvm_path, "/usr/local/rvm"
# set :rvm_ruby_string, "ruby-1.9.2-p320@idea-academy"

#require 'bundler/capistrano'

namespace :deploy do
  %w[start stop restart].each do |command|
    desc "#{command} unicorn server"
    task command, roles: :unicorn, except: {no_release: true} do
      run "/etc/init.d/unicorn-#{application}-#{rails_env} #{command}"
    end
  end

  desc "Prepare environment for deploy"
  task :setup_config, roles: :app do
    run "mkdir -p #{shared_path}/initial_conf"
    put File.read("config/unicorn_init.sh").interpolate({:deploy_to => deploy_to, :rails_env => rails_env}), "#{shared_path}/initial_conf/unicorn_init.sh"
    put File.read("config/nginx.conf").interpolate({:deploy_to => deploy_to, :rails_env => rails_env, :server_name => domain}), "#{shared_path}/initial_conf/nginx.conf"

    run "chmod +x #{shared_path}/initial_conf/unicorn_init.sh"

    sudo "ln -nfs #{shared_path}/initial_conf/nginx.conf /etc/nginx/sites-enabled/#{application}-#{rails_env}.conf"
    sudo "ln -nfs #{shared_path}/initial_conf/unicorn_init.sh /etc/init.d/unicorn-#{application}-#{rails_env}"
    
    run "mkdir -p #{shared_path}/config"
    put File.read("config/database.yml"), "#{shared_path}/config/database.yml"
    puts "Now edit the config files in #{shared_path}."
  end

  namespace :assets do
    task :precompile, :roles => :web, :except => { :no_release => true } do
      # from = remote_file_exists?(current_path) ? source.next_revision(current_revision) : nil
      # if from.nil? || ENV['FORCE_PRECOMPILE'] || capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ app/assets/ | wc -l").to_i > 0
        # run %Q{cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} #{asset_env} assets:precompile}
      # else
        # logger.info "Skipping asset pre-compilation because there were no asset changes"
      # end
    end
  end

  task :symlink_config, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end

  after "deploy:finalize_update", "deploy:symlink_config"
end

namespace :delayed_job do 
  desc "Restart the delayed_job process"
  task :restart, :roles => :worker do
      run "cd #{current_path}; RAILS_ENV=#{rails_env} script/delayed_job -n #{workers_count} restart"
  end
end

namespace :categories do 
  desc "Fetch categories from Facebook"
  task :get_categories_from_facebook, :roles => :worker do
    if ["internal-test", "production"].include?(rails_env)
      run "cd #{current_path}; RAILS_ENV=#{rails_env} bundle exec rake categories:get_categories_from_facebook"
    end
  end
end

after "deploy:restart", "delayed_job:restart"
after "deploy:restart", "categories:get_categories_from_facebook"

