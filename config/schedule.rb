# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, "log/cron_log.log"
#
every 10.minutes do
 rake "contests:create_jobs_to_generate_statistics"
end

puts @environment
if [ "internal-test", "production" ].include?(@environment.to_s)
 every 15.minutes do
   rake "contests:get_crowdsourcing_results"
 end
end

#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
