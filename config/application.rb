require File.expand_path('../boot', __FILE__)

require 'rails/all'

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  #Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  Bundler.require(:default, :assets, Rails.env)
end

module IdejuAkademija
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    # config.autoload_paths += %W(#{config.root}/extras)
    config.autoload_paths += %W(#{config.root}/app/middleware/ #{config.root}/lib #{config.root}/app/extensions #{config.root}/app/forms)
    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer
    config.active_record.observers = :contest_observer, :invitation_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    config.time_zone = 'Riga'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    config.i18n.available_locales = [:en, :lv]
    config.i18n.default_locale = :en

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]

    # Enable the asset pipeline
    config.assets.enabled = true
    config.assets.initialize_on_precompile = true
    config.assets.precompile += %w( devise.css devise_views.css i18n.js main.js internal.js internal.css active_admin.css active_admin.js facebook.css )

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'
    config.log_level = :debug
    
    # No escaping in HamlCoffee templates
    # config.hamlcoffee.escapeHtml = true
    
    # config.middleware.insert_after 'Rack::Lock', 'Dragonfly::Middleware', :images, '/media'
    #     config.middleware.insert_before 'Dragonfly::Middleware', 'Rack::Cache', {
    #       :verbose     => true,
    #       :metastore   => "file:#{Rails.root}/tmp/dragonfly/cache/meta",
    #       :entitystore => "file:#{Rails.root}/tmp/dragonfly/cache/body"
    #     }

    # # Enable email sending through 'sendmail'
    # config.action_mailer.delivery_method = :sendmail
    # # Defaults to:
    # config.action_mailer.sendmail_settings = {
    #   :location => '/usr/sbin/sendmail',
    #   :arguments => '-i'
    # }
  end
end
