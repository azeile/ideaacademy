set :domain, "test.academyideas.com"
server domain, :app, :web, :db, :primary => true
set :rails_env, "internal-test"
set :deploy_to, "/home/#{user}/#{application}-internal-test"
role :worker, "test.academyideas.com"
role :unicorn, domain
set :delayed_job_server_role, :worker
set :workers_count, 1
