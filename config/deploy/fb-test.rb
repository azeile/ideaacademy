set :domain, "fb-test.academyideas.com"
server domain, :app, :web, :db, :primary => true
set :rails_env, "fb-test"
set :deploy_to, "/home/#{user}/#{application}-fb-test"
role :worker, domain
role :unicorn, domain
set :delayed_job_server_role, :worker
set :workers_count, 1