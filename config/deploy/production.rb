set :domain, "academyideas.com"
server domain, :app, :web, :db, :primary => true
set :rails_env, "production"
set :deploy_to, "/home/#{user}/#{application}-production"
role :app, "academyideas.com", "108.166.84.62"
role :worker, "108.166.84.62"
role :unicorn, domain
set :delayed_job_server_role, :worker
set :workers_count, 2
