if not Rails.env.test?
  CopycopterClient.configure do |config|
    # config.api_key = '7fcd526727c5a027dba1cda8863c4afe'
    # config.host = 'copycopter.academyideas.com'
    config.api_key = 'e400e5c9c6a3738d053655d00d7c4021e1aa1ee599bb74d0'
    config.host = 'sleepy-everglades-9758.herokuapp.com'
    config.development_environments = AppUtils::DEV_ENVS
    config.polling_delay = 300
    config.http_open_timeout = 60
    config.http_read_timeout = 60
    config.secure = false
    config.middleware = nil
  end
end
