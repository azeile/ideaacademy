class String
  def interpolate(hash)
    str = self
    hash.each { |k,v| str.gsub! /%{#{k}}/, v}
    return str
  end
end