
if AppUtils.local_storage_env?
  Paperclip::Attachment.default_options[:storage] = :filesystem
else
  options = YAML.load_file("#{Rails.root}/config/s3.yml")[Rails.env].inject({}) do |memo, val|
    memo[val[0].to_sym] = ( val[1].is_a?(Hash) ? val[1].symbolize_keys : val[1] )
    memo
  end

  Paperclip::Attachment.default_options.merge!(options)
end
