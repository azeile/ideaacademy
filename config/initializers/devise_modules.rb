DEVISE_MODULES = [  :database_authenticatable, 
                    :recoverable, 
                    :rememberable, 
                    :trackable, 
                    :token_authenticatable, 
                    :timeoutable,
                    :registerable,
                    :invitable
                  ]

DEVISE_MODULES << :omniauthable if AppUtils.facebook_env?
