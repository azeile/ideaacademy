if not Rails.env.test?
  yml_dump = YAML.load_file("#{Rails.root}/config/crowdsourcing.yml")[Rails.env]
  CROWDSOURCING_CONF = yml_dump.inject({}) do |memo, val|
    memo[val[0]] = if val[1].is_a?(Array) && !val[1].first.is_a?(Hash) 
      ( val[1].first % val[1][1..-1].map {|key| yml_dump[key] } )
    else
      val[1]
    end

    memo
  end.symbolize_keys
end
