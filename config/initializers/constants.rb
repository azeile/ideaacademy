POINTS_VOTE     = 1 # points add for voteing
POINTS_IDEA     = 2 # points add for idea to be voted

HOST ||= "http://localhost:3000"

CONTEST_AWARDS_COUNT = 10 # users count to get contest badge
IDEA_LIMIT_FOR_POINTS = 5 # number of ideas for user to get points

# Currency
LEVEL_TO_RECEIVE_MONEY = 10 # id number in data base
PERCENTS_OF_AWARD_FOR_FIRST_PLACE = 25
PERCENTS_OF_AWARD_FOR_SECOND_PLACE = 25
PERCENTS_OF_AWARD_FOR_THIRD_PLACE = 25
SYSTEM_CURRENCY = "USD" # all amounts are in coins

# Starting with this level user is allowed to participate in money contests and report spam
SUFFICIENT_USER_LEVEL = 8

