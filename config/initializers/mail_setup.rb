default_options = {
  :address => "smtp.gmail.com",
  :port => 587,
  :user_name => "rektors@idejuakademija.lv",
  :password => "rektors123",
  :authentication => "plain",
  :enable_starttls_auto => true
}

default_options[:domain] = ActionMailer::Base.default_url_options[:host] = AppUtils::HOST

ActionMailer::Base.smtp_settings = default_options