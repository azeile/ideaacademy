class AppUtils
  FACEBOOK_ENVS     = %w(development fb-dev fb-test fb-prod)
  INTERNAL_APP_ENVS = %w(internal-dev internal-test production)
  DEV_ENVS          = %w(development fb-dev internal-dev fb-prod)
  TEST_ENVS         = %w(fb-test test internal-test)
  PRODUCTION_ENVS     = %w(fb-prod production)

  ALLOWED_DEV_MAILS   = ["janis.pauls@gmail.com", "zhu1230@gmail.com"]
  ALLOWED_TEST_MAILS  = ALLOWED_DEV_MAILS + [ "andrim@apollo.lv" ]
  INTERCAPTABLE_ENVS  = %w(fb-dev fb-test development internal-dev internal-test)
  DELAYED_JOB_ENVS    = %w(fb-test internal-test production)
  LOCAL_STORAGE_ENVS  = %w(development fb-dev test fb-prod)

  HOST = case Rails.env
    when "development"
      "localhost:3000"
    when "internal-test"
      "test.academyideas.com"
    when "production"
      "academyideas.com"
    when "fb-test"
      "fb-test.academyideas.com"
    when "fb-prod"
      "academyideas.com"
    else
      "academyideas.com"
  end
  
  class << self
    def facebook_env?
      FACEBOOK_ENVS.include? Rails.env
    end

    def public_page
      if Rails.env == 'development'
        'http://localhost:3000'
      elsif Rails.env == 'production'
        'http://www.academyideas.com/public'
      elsif Rails.env == 'internal-test'
        'http://test.academyideas.com/public'
      end
    end
  
    def internal_app_env?
      INTERNAL_APP_ENVS.include? Rails.env
    end

    def allowed_emails
      @allowed ||= if DEV_ENVS.include? Rails.env
        ALLOWED_DEV_MAILS
      elsif TEST_ENVS.include? Rails.env
        ALLOWED_TEST_MAILS
      else
        []
      end
    end

    def production_env?
      PRODUCTION_ENVS.include? Rails.env
    end

    def delayed_job_env?
      DELAYED_JOB_ENVS.include? Rails.env
    end

    def local_storage_env?
      LOCAL_STORAGE_ENVS.include? Rails.env
    end
  end
end
