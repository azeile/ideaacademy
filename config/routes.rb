IdejuAkademija::Application.routes.draw do

  post "invitations", controller: :invitations, action: :create
  post "/paypal-notifications" => "static#paypal_notifications"

  get "/pay-with-paypal/:id" => "payments_gateway#pay_with_paypal", :as => :pay_with_paypal
  get "/user_payment_notifications" => "static#user_payment_notifications", :as => :pay_with_paypal

  get "test/new_request"
  #User devise auth
  devise_for :users, :controllers => { :registrations => "registrations", :omniauth_callbacks => "users/omniauth_callbacks", :sessions => "sessions"} do
     # get '/users/auth/:provider' => 'users/omniauth_callbacks#passthru'
  end

  # Administration
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  # Authentifications => test page
  get "/login" => "static#login", :as => :login
  get "/logout" => "static#logout", :as => :logout

  #=> Login
  namespace :login do
    get "draugiem/auth", :as => :draugiem_auth
    get "draugiem/callback"
    get "draugiem/iframe"
    post "facebook/auth", :as => :facebook_auth
    get "facebook/callback"
    get "synchronize/create/:name" => "synchronize#create", :as => :synchronization
    get "synchronize/destroy/:id" => "synchronize#destroy", :as => :desynchronization
  end

  # API
  namespace :api do
    match "sidebar_messages" => "sidebar_messages#for_current_user", :as => :sidebar_messages_for_current_user
    match "mark_bubble_as_seen/:id/:user_id" => "sidebar_messages#mark_as_seen", :as => :sidebar_messages_seen

    resources :categories
    resources :facebook_categories, :only => :index
    resources :contest_requests
    resources :crowdsourced_contests, :only => [:create, :update, :show, :index] do
      resources :crowdsourcing_results, :only => [:index] do

      end
    end

    #=> Contest
    resources :contests do
      collection do
        post "upload_image"
        get "root"
        get "root_in_categories"
      end

      resources :crowdsourcing_results, :only => [:index] do
        collection do
          get 'get_from_facebook'
        end
      end

      #=> Ideas
      resources :ideas do
        collection do
          match "vote/:first_idea_id/:second_idea_id(/:selected_idea_id)" => "ideas#vote", :as => :vote
          match "show_duel" => "ideas#show_duel", :as => :show_duel
        end

        member do
          get "report_spam" => "ideas#report_spam", :as => :report_spam
        end
      end

      resources :tmp_ideas do
        collection do
          match "vote/:first_idea_id/:second_idea_id(/:selected_idea_id)" => "tmp_ideas#vote", :as => :vote
          match "show_duel" => "tmp_ideas#show_duel", :as => :show_duel
        end

        member do
          get "report_spam" => "ideas#report_spam", :as => :report_spam
        end
      end
    end

    resources :payment_transactions, :only => :show
    resources :payments , :only => [:index, :create] do
      collection do
        get 'update_transaction'
      end
    end

    resources :translations

     #=> Statistic
    resources :statistics do
      collection do
        match "contest_users/:contest_id(/:user_id)" => "statistics#contest_users", :as => :contest_users
        match "contest_ideas/:contest_id(/:user_id)" => "statistics#contest_ideas", :as => :contest_ideas
        match "contest_room/:contest_id(/:user_id)" => "statistics#contest_room", :as => :contest_room
        match "contest_user_ideas/:contest_id(/:user_id)" => "statistics#contest_user_ideas", :as => :contest_user_ideas
        match "user/:user_id(/:category_id)" => "statistics#user", :as => :user
        match "tmp_user/:tmp_user_id(/:category_id)" => "statistics#tmp_user", :as => :tmp_user
        match "tmp_user_single_update/:user_id/:category_id" => "statistics#tmp_user_single_update", :as => :tmp_user_signle_update
        match "user_update/:user_id" => "statistics#user_update", :as => :user_update
        match "user_single_update/:user_id/:category_id" => "statistics#user_single_update", :as => :user_single_update
        match "user_profile(/:user_id)" => "statistics#user_profile", :as => :user_profile
        match "category_users/:category_id(/:users_page)" => "statistics#category_users", :as => :category_users
        match "contest_end_results/:contest_id" => "statistics#contest_end_results", :as => :contest_end_results
      end
    end

    #=> Logs
    resources :logs do
      collection do
        match "user/:user_id(/:timestamp)" => "logs#user", :as => :user_logs
      end
    end

    # => Popups
    match "popups/:user_id(/:popup_id)" => "popups#user", :as => :user_popups
    match "tmp_popups/:user_id(/:popup_id)" => "popups#tmp_user", :as => :tmp_user_popups

    # => Contact Form
    post "contact_form/create"

    # => Users
    resources :users do
      collection do
        get "current"
        match ":id/update_image" => "users#update_image", :as => :update_image
        get "check_out_money"
        post "check_out_money"
        match "update_category_notifications" => "users#update_category_notifications", :as => :update_category_notifications
        get "recent_badges"
      end
      member do
        get "top_ideas/:count" => "users#top_ideas"
        get "portfolio_ideas"
      end
    end

    # => TmpUsers
    resources :tmp_users do
      collection do
        get "current"
      end
    end

    # Admin messages
    resources :admin_messages, only: [:create]
  end

  resources :done_statistics

  get "update/category_users_statistic"
  get "server_time" => "main#time"

  #Internal app pages
  get "internal" => "internal#home", :as => :home_internal
  get "internal/about" => "internal#product", :as => :product_internal
  get "internal/faq" => "internal#faq", :as => :faq_internal
  get "internal/testimonials" => "internal#testimonials", :as => :testimonials_internal
  get "internal/pricing" => "internal#pricing", :as => :pricing_internal
  get "internal/thanks_for_payment" => "internal#thanks_for_payment", :as => :thanks_for_payment

  #Static (psd 2 html)
  scope "static" do
    get "/welcome" => "static#welcome", :as => :welcome
    get "/thanks_for_payment" => "static#thanks_for_payment", :as => :public_thanks_for_payment

    get "/test/:contest_id" => "static#test"

    get "/update_status/:contest_id" => "static#update_status", :as => :update_status
    get "/update_statistic" => "static#update_statistic", :as => :update_statistic

    get "/update_null_points" => "static#update_null_points", :as => :update_null_points

    get "/contest/guest" => "static#contest_guest", :as => :contest_guest
    get "/contest" => "static#contest", :as => :contest
    get "/contest/new" => "static#contest_new", :as => :new_contest
    get "/results" => "static#contest_results", :as => :contest_results
    get "/statistics" => "static#contest_statistics", :as => :contest_statistics

    get "/profile" => "static#profile", :as => :profile
    get "/profile/edit" => "static#profile_edit", :as => :profile_edit

    get "/popup" => "static#popup", :as => :popup
    get "/popup/tos" => "static#popup_tos", :as => :popup_tos
    get "/popup/default" => "static#popup_default", :as => :popup_default

    get "/popup/profile_edit_image" => "static#popup_profile_edit_image", :as => :popup_profile_edit_image

    get "/popup/welcome" => "static#popup_welcome", :as => :popup_welcome
    get "/popup/money" => "static#popup_money", :as => :popup_money
    get "/popup/first_points" => "static#popup_first_points", :as => :popup_first_points
    get "/popup/ten_points" => "static#popup_ten_points", :as => :popup_ten_points
    get "/popup/other_themes" => "static#popup_other_themes", :as => :popup_other_themes
    get "/popup/social" => "static#popup_social", :as => :popup_social
    get "/popup/earn_more" => "static#popup_earn_more", :as => :popup_earn_more
    get "/popup/first_achievement" => "static#popup_first_achievement", :as => :popup_first_achievement
    get "/popup/achievement" => "static#popup_achievement", :as => :popup_achievement
    get "/popup/like_idea" => "static#popup_like_idea", :as => :popup_like_idea
  end

  #Root path
  match '/blocked' => 'main#blocked', :as => 'blocked'
  root :to => "main#index", :as => :root
end
