root = "/home/root/idea-academy-#{ENV['RACK_ENV']}/current"
working_directory root
pid "#{root}/tmp/pids/unicorn-ia-#{ENV['RACK_ENV']}.pid"
stderr_path "#{root}/log/unicorn.log"
stdout_path "#{root}/log/unicorn.log"

listen "#{root}/tmp/ia.#{ENV['RACK_ENV']}.sock"

worker_processes ["fb-prod", "production"].include?(ENV['RACK_ENV']) ? 4 : 2
timeout 120
